/**
 * @file	fatfs_extension.c
 * @author 	Kyb
 * 
 * @literature  http://elm-chan.org/fsw/ff/en/getfree.html
 */

#include <stdint.h>
#include "ff.h"
#include "Sledge/FatFS/fatfs_extension.h"

/** ==== DEBUG ====
 */
#define DEBUG_LEVEL  DEBUG_L1
#include "Sledge/debug.h"
#include "Sledge/assert.h"


/// Array of FatFs objects. Index matches drive number.
/// FATFS type contains array of max_sector_size bytes. Be careful it takes much memory.
/*volatile*/ FATFS FatFses[_VOLUMES] = {0};
//STATIC_ASSERT( sizeof(FATFS) == 4148 );


int get_ldnumber( const TCHAR** );  // from ff.c, must be changed to non-static

/**
 * 
 * @param 	drivename 	 [IN]	for example "0:" or "sd:"
 * @param 	total_sectors[OUT]	May be NULL
 * @param 	free_sectors [OUT]	May be NULL
 */
FRESULT f_getDriveSize(char *drivename, uint32_t *total_sectors, uint32_t *free_sectors) 
{
	FATFS *fs;
	DWORD/*uint32_t*/ fre_clusters;
	FRESULT ferr;

	// Get volume information and free clusters of drive
	ferr = f_getfree( drivename, &fre_clusters, &fs );
	if (ferr != FR_OK) {
		return ferr;
	}

	// Calculate total sectors and free sectors. NULL-pointers protected
	if(total_sectors) *total_sectors = (fs->n_fatent - 2) * fs->csize;
	if(free_sectors)  *free_sectors  = fre_clusters * fs->csize;
	
	/* Return OK */
	return FR_OK;
}


/**
 * Централизованное монтирование дисков
 * \return  <=0 if OK, >0 on error
 * 			FR_INVALID_DRIVE  если путь к носителю неверный
 * 			any of FR_xxx which can return f_mount()
 * 			 0  FR_OK
 * 			-1  Deferred
 * 			-2  Allready mounted
 */
int/*FRESULT*/ f_imount(
	FATFS** fs,			/* [OUT] Pointer to pointer the file system object. Returns the pointer to instance of FATFS object from FatFses array */
	const TCHAR* path,	/* [IN]  Logical drive to be mounted/unmounted. May be autoresolved from filename */
	const BYTE opt		/* [IN]  0:Do not mount (delayed mount), 1:Mount immediately */
){
	int ret = FR_OK;  //FRESULT ferr = FR_OK;
	int logicalDrvNum;
	char diskname[3] = "_:"; 	/// This buffer is to don't waste time for second time resolving logicalDrvNum by get_ldnumber() called from f_mount(). Useful if _STR_VOLUME_ID!=0
	
	debugf2("f_imount(): Mount drive... \t" );
	logicalDrvNum = get_ldnumber(&path);
	if( logicalDrvNum < 0 ){
		ret = FR_INVALID_DRIVE;
		goto error1;
	}
	diskname[0] = logicalDrvNum+'0'; //diskname[0] = logicalDrvNum >= 0 ? logicalDrvNum+'0' : '0';
	
	// Check if already mounted
	if( FatFses[logicalDrvNum].fs_type == 0 ){ 	/// Not mounted?
		ret = f_mount( &FatFses[logicalDrvNum], diskname/*path*/, opt);
		if( ret != FR_OK ){
			goto error1;
		}
		if( opt == 1 ){  // Mount immediately 
			ret = 0;
			debugf2("OK."NEWLINE );
		} else {
			ret = -1;
			debugf2("Deferred."NEWLINE );
		}
	} else {
		ret = -2;  // = FR_ALREADY_MOUNTED;
		debugf2("Already mounted. OK"NEWLINE );
	}

	if(fs) *fs = &FatFses[logicalDrvNum];	
	return ret;
	
error1:
	debugf2("FAILED!"NEWLINE );
	if(fs) *fs = NULL;
	return ret;
}


/**
 * \Returns true if file is closed.
 */
bool f_isClosed( FIL *fil ){
    return ((fil->fs == 0) || (fil->id == 0));
}
//#define isClosed(fil)  ( ((fil)->fs == 0) && ((fil)->id == 0) )

