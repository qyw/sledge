/**
 * Sledge - wide functionality library, suitable for embedded systems, 
 * mainly for ARM Cortex and more particularly for STM32 family
 * 
 * Copyright (c) 2015, Ivan Kuvaldin. All rights reserved.
 * 
 * Licensed under BSD or MIT. 
 * Please refer to Licence.BSD.txt or Licence.MIT.txt provided with these sources.
 * You may obtain a copy of the Licenses at
 *      http://opensource.org/licenses/bsd-license.php
 *      http://opensource.org/licenses/mit-license.php
 */

/**
 * @name	retarget_stdio.c
 * @brief	'Retarget' layer for target-dependent low level functions
 * 
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "../dassel/logger/comport.h"
#include "ff.h"  // FatFS by ChaN
#include "Sledge/FatFS/fatfs_extension.h"
#include "fs2.h"  // lwip raw pseudo fs
#include "./itm.h"
#include "usbd_cdc_vcp.h"
//#include <rt_misc.h>


/* For Keil и не только.
 */
#pragma import(__use_no_semihosting_swi)

/**
 * Is fprintf in the KEIL microlib?
 * Yes. Microlib stdio support is controlled by the #pragma import(__use_full_stdio) directive,
 * the documentation is not clear about what is excluded if this is not used. 
 * Try it without and use it if anything is missing. That said I would imagine that printf() 
 * is implemented as an fprintf() to the stdout stream, so if you have printf() you have fprintf().
 * @ref http://www.keil.com/support/man/docs/ARMCC/ARMCC_chr1359124987988.htm
 */
//#pragma import(__use_full_stdio)



typedef enum {
	Other_FILE = 0,
	Hw_FILE,
	RawFs_FILE,
	FatFS_FILE,
} FileType_e;

struct __FILE {
	FileType_e type;
	//int handle;
	void *handle;		/// For example, FatFS FIL object, or integer id
	char *path;
	//char mode[3];
	int (*putc)(int);	//, FILE*);
	int (*getc)();		//FILE*);
};

/*enum {
	  DEFAULT_FILE = 0
	, STDIN_FILE  
	, STDOUT_FILE 
	, STDERR_FILE 
	, ITM_FILE
	, DEBUG_COM_PORT_FILE
	, USB_VCP_FILE 
//	, LAST_DEFAULT_FILE_FILE			//нужно чтобы определить количество дефолтных файлов
//	, ALL_INPUTS_FILE 	= -1
	, ALL_OUTPUTS_FILE 	= -2		//похоже это малопроизводительно будет
//	, ALL_FILES_FILE 		= -3
	, FatFS_FILE = 128
};*/

__weak int vcp_putchar(int ch){ return -1; };


extern __INLINE int putc_serial(int ch);
int stdout_putc(int ch);

FILE  __stdin  		= { Hw_FILE, 0, "/dev/uart0", 0          , 0/*getchar_serial*/ }
	, __stdout 		= { Hw_FILE, 0, "/dev/uart0", stdout_putc, 0 }
	, __stderr 		= { Hw_FILE, 0, "/dev/uart0", stdout_putc, 0 } 
	, itm_file		= { Hw_FILE, 0, "/dev/itm"  , itm_putc   , itm_getc }
	, usb_vcp_file 	= { Hw_FILE, 0, "/dev/tty2" , vcp_putchar, 0/*usb_vcp_getchar*/ }
	, debugCOM_file = { Hw_FILE, 0, "/dev/uart0", putc_serial, 0/*getchar_serial*/ }
	;

static bool automountFs = false;

	
/**
 * writes the character specified by c (converted to an unsigned char) to
 * the output stream pointed to by stream, at the position indicated by the
 * asociated file position indicator (if defined), and advances the
 * indicator appropriately. If the file position indicator is not defined,
 * the character is appended to the output stream.
 * @Returns: the character written. If a write error occurs, the error
 *          indicator is set and fputc returns EOF.
 */	
#ifdef __GNUC__ /* && small_printf */
/* With GCC/RAISONANCE, small printf (option LD Linker->Libraries->Small printf
   set to 'Yes') calls __io_putchar() */
int __io_putchar(int ch)
#else
int fputc(int ch, FILE *f)
#endif /* __GNUC__ */
{
    int ret = EOF;
	
	/*if( f == stdout || f == stderr ){
		ret = usb_vcp_file.putc(ch);
		ret = debugCOM_file.putc(ch);
		ret = itm_file.putc(ch);	//ret = ITM_SendChar(ch);
		return ret;
	}*/
	
	/*if( f->putc )
		ret = f->putc(ch);
	else
	if( strncmp( f->path, "sd:/", 4 ) == 0 )
		ret = f_putc( ch, f->handle );*/
	
	switch( f->type ){
		case Other_FILE:
		case Hw_FILE: 
			if(f->putc)  ret = f->putc(ch); 
			break;
		case RawFs_FILE: 
			break;
		case FatFS_FILE: 
			f_putc(ch, f->handle); 
			ret=ch; 
			break;
	}
	
    return ret ;
}


int stdout_putc(int ch)
{
	int ret = EOF;
	ret = usb_vcp_file.putc(ch);
	ret = itm_file.putc(ch);	//ret = ITM_SendChar(ch);
	ret = debugCOM_file.putc(ch);
	return ret;
}

//int stdglue_fatfs_putc(int ch, FILE *f)  { return f_putc( ch, f->handle ); }


/**
 * Obtains the next character (if present) as an unsigned char converted to
 * an int, from the input stream pointed to by stream, and advances the
 * associated file position indicator (if defined).
 * @Returns: the next character from the input stream pointed to by stream.
 *          If the stream is at end-of-file, the end-of-file indicator is
 *          set and fgetc returns EOF. If a read error occurs, the error
 *          indicator is set and fgetc returns EOF.
 */
int fgetc(FILE *f) 
{
	int ret = EOF ;
	char buf; 
	
	/*if( f->getc ) 
		ret = f->getc();*/
	
	switch( f->type ){
		case Other_FILE:
		case Hw_FILE: 
			if(f->getc)  ret = f->getc(); 
			break;
		case RawFs_FILE: 
			ret = fs_getc(f->handle);
			break;
		case FatFS_FILE: 
			f_gets(&buf, 1, f->handle); 
			ret = buf;
	}
	
	return ret;
}

//static int stdglue_usart_getc(FILE *f)   { return getc_serial(); };
//static int stdglue_lwipfs_getc(FILE *f)  { return fs_getc(f->handle); };
//static int stdglue_fatfs_getc(FILE *f)   { char buf; f_gets(&buf, 1, f->handle); return buf; };


/**
 * Reads at most one less than the number of characters specified by n from
 * the stream pointed to by stream into the array pointed to by s. No
 * additional characters are read after a new-line character (which is
 * retained) or after end-of-file. A null character is written immediately
 * after the last character read into the array.
 * \Return  s if successful. If end-of-file is encountered and no characters
 *          have been read into the array, the contents of the array remain
 *          unchanged and a null pointer is returned. If a read error occurs
 *          during the operation, the array contents are indeterminate and a
 *          null pointer is returned.
 */
char* fgets(char * __restrict s, int n, FILE * __restrict f)
{
	switch( f->type ){
		case Other_FILE:
		case Hw_FILE: 
			//if(f->getc)  ret = f->gets(); 
			//break;
			return 0;
		case RawFs_FILE: 
			///\todo No additional characters are read after a new-line character (which is retained) or after end-of-file.
			if( fs_read( f->handle, s, n ) == -1)
				s = NULL;
			break;
		case FatFS_FILE: 
			return f_gets( s, n, f->handle); 
	}	
	return s;
}


/**
 * Reads into the array pointed to by ptr, up to nmemb members whose size is
 * specified by size, from the stream pointed to by stream. The file
 * position indicator (if defined) is advanced by the number of characters
 * successfully read. If an error occurs, the resulting value of the file
 * position indicator is indeterminate. If a partial member is read, its
 * value is indeterminate. The ferror or feof function shall be used to
 * distinguish between a read error and end-of-file.
 * \Returns the number of members successfully read, which may be less than
 *          nmemb if a read error or end-of-file is encountered. If size or
 *          nmemb is zero, fread returns zero and the contents of the array
 *          and the state of the stream remain unchanged.
 */
size_t fread(void * __restrict ptr, size_t size, size_t nmemb, FILE * __restrict f)
{
	size_t bytes_read = 0;
	switch( f->type ){
		case Other_FILE:
		case Hw_FILE: 
			//if(f->getc)  ret = f->gets(); 
			//break;
			return 0;
		case RawFs_FILE: 
			///\todo No additional characters are read after a new-line character (which is retained) or after end-of-file.
			return fs_read( f->handle, ptr, size*nmemb );
		case FatFS_FILE: 
			f_read( f->handle, ptr, size*nmemb, &bytes_read );
			return bytes_read;
	}
	return 0;
}


/**
 * Get size of the file.
 */
size_t fsize( FILE * __restrict f )
{
	size_t size, seek_back;
	switch( f->type ){
		case Other_FILE:
			// http://stackoverflow.com/questions/238603/how-can-i-get-a-files-size-in-c
			seek_back = ftell(f); // remember current position of file cursor
			fseek(f, 0, SEEK_END); // seek to end of file
			size = ftell(f);
			fseek(f, 0, seek_back); // seek back
			return size;
		case Hw_FILE: 
			//if(f->getc)  ret = f->gets(); 
			//break;
			return 0;
		case RawFs_FILE: 
			///\todo No additional characters are read after a new-line character (which is retained) or after end-of-file.
			return fs_size( f->handle );
		case FatFS_FILE: 
			return f_size( (FIL*)f->handle );
	}
	return 0;
}


/**
 * Opens the file whose name is the string pointed to by filename, and
 * associates a stream with it.
 * The argument mode points to a string beginning with one of the following
 * sequences:
 * "r"         open text file for reading
 * "w"         create text file for writing, or truncate to zero length
 * "a"         append; open text file or create for writing at eof
 * "rb"        open binary file for reading
 * "wb"        create binary file for writing, or truncate to zero length
 * "ab"        append; open binary file or create for writing at eof
 * "r+"        open text file for update (reading and writing)
 * "w+"        create text file for update, or truncate to zero length
 * "a+"        append; open text file or create for update, writing at eof
 * "r+b"/"rb+" open binary file for update (reading and writing)
 * "w+b"/"wb+" create binary file for update, or truncate to zero length
 * "a+b"/"ab+" append; open binary file or create for update, writing at eof
 *
 * Opening a file with read mode ('r' as the first character in the mode
 * argument) fails if the file does not exist or cannot be read.
 * Opening a file with append mode ('a' as the first character in the mode
 * argument) causes all subsequent writes to be forced to the current end of
 * file, regardless of intervening calls to the fseek function. In some
 * implementations, opening a binary file with append mode ('b' as the
 * second or third character in the mode argument) may initially position
 * the file position indicator beyond the last data written, because of the
 * NUL padding.
 * When a file is opened with update mode ('+' as the second or third
 * character in the mode argument), both input and output may be performed
 * on the associated stream. However, output may not be directly followed
 * by input without an intervening call to the fflush fuction or to a file
 * positioning function (fseek, fsetpos, or rewind), and input be not be
 * directly followed by output without an intervening call to the fflush
 * fuction or to a file positioning function, unless the input operation
 * encounters end-of-file. Opening a file with update mode may open or
 * create a binary stream in some implementations. When opened, a stream
 * is fully buffered if and only if it does not refer to an interactive
 * device. The error and end-of-file indicators for the stream are
 * cleared.
 * @Returns: a pointer to the object controlling the stream. If the open
 *          operation fails, fopen returns a null pointer.
 */
/** Sledge imlementation NOTICE
 * 
 */
FILE* fopen( const char * filename,
			 const char * mode )
{
	FILE *f;
	int imerr;
	FRESULT fr;
	FIL *fil;
	BYTE ffmode;
	struct fs_file *fshandle;
	
	if( strcmp( filename, stdout->path/*"/dev/uart0"*/ ) == 0 )
	{
		DebugComPort_Init();
		f = stdout;
	} 
	else if( strncmp( filename, "sd:/", 4 ) == 0 )
	{
		if( automountFs ){
			imerr = f_imount( 0, filename, 1 );
			if( imerr > FR_OK )
				return NULL;
		}
		
		switch( mode[0] ){
			case 'r': 	 // open text file for reading
				ffmode = FA_READ | FA_OPEN_EXISTING; 
				if( mode[1] == '+' || mode[2] == '+' )
					ffmode |= FA_WRITE;
				break;
			case 'w': 	 // create text file for writing, or truncate to zero length
				ffmode = FA_WRITE | FA_CREATE_ALWAYS; 
				if( mode[1] == '+' || mode[2] == '+' )
					ffmode |= FA_READ;
				break;
			case 'a':
				ffmode =  FA_WRITE | FA_OPEN_ALWAYS; 
				if( mode[1] == '+' || mode[2] == '+' )
					ffmode |= FA_READ;
				break;
		}
		
		/// Change filename for FatFS
		//ffname[0] = filename[1];
		//ffname[1] = filename[2];
		//ffname[2] = ':';
		
		fil = malloc( sizeof(FIL) );
		fr = f_open( fil, filename, ffmode );
		if( fr != FR_OK ){
			free(fil);
			return NULL;
		}
		
		if( ffmode & FA_OPEN_ALWAYS ){	/// if mode append
			fr = f_lseek( fil, f_size(fil) ); 	/// Seek to the end of file
			if( fr != FR_OK ){
				f_close(fil);
				free(fil);
				return NULL;
			}
		}
		
		f = malloc( sizeof(FILE) );
		if( !f ){
			f_close(fil);
			free(fil);
			return NULL;
		}
		f->type = FatFS_FILE;
		f->handle = fil;
		//strcpy( f->mode, mode );
		f->path = malloc( strlen(filename) * sizeof(char) );
		if( !f->path ){
			f_close(fil);
			free(fil);
			goto free_and_return1;
		}
		strcpy( f->path, filename );
		f->putc = NULL;
		f->getc = NULL;
	}
	else if( filename[0] == '/' )
	{
		fshandle = fs_open( filename );
		if( !fshandle )
			return NULL;
		f = malloc( sizeof(FILE) );
		if( !f ){
			fs_close(fshandle);
			return NULL;
		}
		f->type = RawFs_FILE;
		f->handle = fshandle;
		//strcpy( f->mode, mode );
		f->path = malloc( strlen(filename) * sizeof(char) );
		if( !f->path ){
			fs_close(fshandle);
			goto free_and_return1;
		}
		strcpy( f->path, filename );
		f->putc = NULL;
		f->getc = NULL;
	}
	//else if( strncmp( filename, "/ramfs:/", 7 ) == 0 ){
	
	return f;
	
	//free_and_return2:
	//free(path);
	free_and_return1:
	free(f);
	return NULL;
}


/**
 * Causes the stream pointed to by stream to be flushed and the associated
 * file to be closed. Any unwritten buffered data for the stream are
 * delivered to the host environment to be written to the file; any unread
 * buffered data are discarded. The stream is disassociated from the file.
 * If the associated buffer was automatically allocated, it is deallocated.
 * @Returns: zero if the stream was succesfully closed, or nonzero if any
 *          errors were detected or if the stream was already closed.
 */
/** Sledge implementation NOTICE
 * @return: 0 if the stream was succesfully closed, 
 * 			>0 if the stream was already closed,
 * 			<0 if any error occours
 */
int fclose(FILE * f)
{
	int ret=0;
	FRESULT fr;
	
	if( !f )  return -1;
	
	/*if( strcmp( f->path, "/dev/uart0" ) == 0 )
	{
		///\ToDo deinit UART.
		ret = 0xFFFFFFFF;  // not implemented yet
	} 
	else if( strncmp( f->path, "sd:/", 4 ) == 0 ){
	//else if( strchr( f->path, ':' ) == 0 ){  // If file path contains ':', pass it to FatFS module
		fr = f_close(f->handle);
		if( fr != FR_OK )
			ret = -1;
		free( f->handle );
		free( f->path );
		free( f );
	}
	else if( f->path[0] == '/' ){
		fs_close( f->handle );
		free( f->path );
		free( f );
	}*/
	switch( f->type ){
		case Other_FILE:
		case Hw_FILE: 
			///\ToDo deinit UART.
			ret = 0xFFFFFFFF;  // not implemented yet
		case RawFs_FILE: 
			fs_close( f->handle );
			free( f->path );
			free( f );
			break;
		case FatFS_FILE: 
			fr = f_close( f->handle );
			if( fr != FR_OK )
				ret = -1;
			free( f->handle );
			free( f->path );
			free( f );
			break;
	}
	return ret;
}

/**
 * Tests the end-of-file indicator for the stream pointed to by stream.
 * \Returns: nonzero iff the end-of-file indicator is set for stream.
 */
/*extern _ARMABI*/ int feof( FILE *f ) 
{
	switch( f->type ){
		case Other_FILE:
		case Hw_FILE: 
			//if(f->getc)  ret = f->gets(); 
			//break;
			return 0;
		case RawFs_FILE: 
			return fs_eof( f->handle );
		case FatFS_FILE: 
			return f_eof( (FIL*)f->handle );
	}
	return 1;
}
   

/***
 * If the stream points to an output or update stream in which the most
 * recent operation was output, the fflush function causes any unwritten
 * data for that stream to be delivered to the host environment to be
 * written to the file. If the stream points to an input or update stream,
 * the fflush function undoes the effect of any preceding ungetc operation
 * on the stream.
 * @Returns: nonzero if a write error occurs.
 */
//int fflush(FILE * /*stream*/);


/**
 * Tests the error indicator for the stream pointed to by stream.
 * @Returns: nonzero iff the error indicator is set for stream.
 */
/*
int ferror(FILE *f) {
	/// Your implementation of ferror
	return EOF;
}


void _ttywrch(int c) {
	//sendchar(c);
}


void _sys_exit(int return_code) 
{
	label:  goto label;  /// endless loop
}
*/


#ifdef __MICROLIB /*__CC_ARM*/
/** To suppress MDK-ARM Linker Error: L6218E: Undefined symbol __fread_bytes_avail (referred from ios.o).
 * 	This function is unimplemented in microlib.
 */
size_t __fread_bytes_avail(void * __restrict ptr,
                    size_t count, FILE * __restrict stream) //__attribute__((__nonnull__(1,3)))
{
	return (size_t)-1;
}


#include <wchar.h>
/// To suppress MDK-ARM Linker Error: L6218E: Undefined symbol mbsinit (referred from ios.o).
int mbsinit(const mbstate_t * ps)
{
	return -1;
}

/// To suppress MDK-ARM Linker Error: L6218E: Undefined symbol mbsinit (referred from ios.o).
wchar_t *wmemmove(wchar_t * __restrict s1,
                      const wchar_t * __restrict s2, size_t n) //__attribute__((__nonnull__(1,2)))
{
	return NULL;
}
#endif /*__MICROLIB*/ /*__CC_ARM*/
