/**
 * Sledge - wide functionality library, suitable for embedded systems, 
 * mainly for ARM Cortex and more particularly for STM32 family
 * 
 * Copyright (c) 2015, Ivan Kuvaldin. All rights reserved.
 * 
 * Licensed under BSD or MIT. 
 * Please refer to Licence.BSD.txt or Licence.MIT.txt provided with these sources.
 * You may obtain a copy of the Licenses at
 *      http://opensource.org/licenses/bsd-license.php
 *      http://opensource.org/licenses/mit-license.php
 */
/**
 * \file 	Sledge/retarget/retarget_stdlib.c
 * \brief	'Retarget' layer for target-dependent low level functions
 * 
 */

#include "mainConfig.h"  // For USE_RTOS
#include <stdlib.h>
#include "../drv/stm32/sledge_stm32_rng.h"

#if USE_RTOS == 1
#include "FreeRTOS.h"
#endif


#ifndef RETARGET_RAND_SIGNED
#define RETARGET_RAND_SIGNED  0
#endif


#if USE_RTOS == 1
void * malloc(size_t size)
{
	return pvPortMalloc(size);  //return pvPortMallocAligned(size);
}

/**
 * Allocates space for an array of nmemb objects, each of whose size is
 * 'size'. The space is initialised to all bits zero.
 * \Returns: either a null pointer or a pointer to the allocated space.
 */
void * calloc(size_t nmemb, size_t size)
{
	size_t siz = nmemb * size;
	void *ret = pvPortMalloc( siz );
	memset( ret, 0, siz );
	return ret;
}
   

void free(void *ptr)
{
	vPortFree(ptr);  //vPortFreeAligned(ptr);
}
#endif //USE_RTOS == 1


/**
 * Stdlib says that rand() must return 0 to RAND_MAX.
 * STM32 RNG module provides 32-bit value. So we can return signed 32-bit value.
 * To provide signed random value RETARGET_RAND_SIGNED must be defined as 1.
 */
int rand(void)
{
 #if RETARGET_RAND_SIGNED
	union{ uint32_t u; int i; } uni;
	uni.u = drv_RNG_getNext();
	return uni.i;
 #else
	return (RAND_MAX & drv_RNG_getNext());
 #endif
}

/**
 * Initiatize random generator. (Randomize)
 */
void srand(unsigned int seed)
{
	(void) seed;
	
}
