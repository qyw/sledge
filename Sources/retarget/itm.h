/**
 * @file	itm.h
 * 
 */
#ifndef __ITM_H__
#define __ITM_H__

#ifdef __cplusplus
extern "C" {
#endif


int itm_putc( int c );
int itm_getc( void );


#ifdef __cplusplus
}
#endif

#endif /*__ITM_H__*/
