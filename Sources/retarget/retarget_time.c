/**
 * Sledge - wide functionality library, suitable for embedded systems, 
 * mainly for ARM Cortex and more particularly STM32 family
 * 
 * Copyright (c) 2015, Ivan Kuvaldin. All rights reserved.
 * 
 * Licensed under BSD or MIT. 
 * Please refer to Licence.BSD.txt or Licence.MIT.txt provided with these sources.
 * You may obtain a copy of the Licenses at
 *      http://opensource.org/licenses/bsd-license.php
 *      http://opensource.org/licenses/mit-license.php
 */
/**
 * @file 	retarget_time.c
 * @author 	Ivan Kuvaldin <i.Kyb[2]ya,ru>
 * @version	1.0.0
 * @date 	2015-05-24
 * @brief 	Retarget layer from stadart C library to STM32 real-time clock hardware.
 * 
 */

#include <time.h>
//#include <rt_misc.h>	/// MDK-ARM specific
 #include "Sledge/drv/stm32/sledge_stm32_rtc.h"

#if USE_RTOS == 1
 #include "FreeRTOS.h"
 #include "task.h"
#endif


/**
 * It is not part of the C library standard, but the ARM C library supports it as an extension.
 * http://www.keil.com/support/man/docs/ARMLIB/armlib_chr1359122848748.htm
 */
//void _clock_init(void);


#if USE_RTOS == 1
/**
 * Determines the processor time used.
 * \Return 	the number of clock ticks elapsed since the start of the program. 
 * 			On failure, the function returns a value of -1.
 * 			
 * 			the implementation's best approximation to the processor time
 *          used by the program since program invocation. The time in
 *          seconds is the value returned divided by the value of the macro
 *          CLK_TCK. The value (clock_t)-1 is returned if the processor time
 *          used is not available.
 */
clock_t clock(void)
{
	return xTaskGetTickCount();
	//return (clock_t)-1;
}
#endif


/**
 * Determines the current calendar time. The encoding of the value is unspecified.
 * \Returns	the implementations best approximation to the current calendar
 *          time. The value (time_t)-1 is returned if the calendar time is
 *          not available. If timer is not a null pointer, the return value
 *          is also assigned to the object it points to.
 */
extern _ARMABI time_t time( time_t *timer )
{
	time_t t = drv_rtc_get(false);
	if(timer) *timer = t;
	return t;
	//return (time_t)-1;
}

