/**
 * @file	itm.c
 * 
 * ITM может значительно тормозить в связи с количеством прослоек uC->Debugger->USB->PC
 */

#include "itm.h"
#include "stm32f4xx.h"
//#include "stdio.h"

/**
 * Отключить Semihosting в стандартной библиотеке Си.
 * Если библиотека, например Redlib, включена с Semihosting, то возникнут конфликты.
 */
#pragma import(__use_no_semihosting_swi)

/// Для инлайновой функции ITM_ReceiveChar;
volatile int32_t ITM_RXBuffer = ITM_RXBUFFER_EMPTY;

int itm_putc( int c )
{
	return ITM_SendChar(c);
}

int itm_getc( void )
{
	//while( ITM_CheckChar() != 1 )
	//	__NOP();
	//return ITM_ReceiveChar();
	return -1;
}
