/** 
 * @file	colors.c 
 * @author	Кувалда (Kyb) <mailto:u.Kyb@ya.ru>
 * @version 1.0.0
 * @date	12-Jan-2014
 * @brief	
 *			
 *			
 */

#include "Sledge/test/sledge_test.h"
#include "Sledge/colors.h"
#include "Sledge/assert.h"
//#include <stdbool.h>


#define RGB_equals( rgb1, rgb2 )	( rgb1.R == rgb2.R && rgb1.G == rgb2.G && rgb1.B == rgb2.B )
#define HSV_equals( hsv1, hsv2 )	( hsv1.H == hsv2.H && hsv1.S == hsv2.S && hsv1.V == hsv2.V )

#define RGB_approx( rgb1, rgb2 )	( 	rgb1.R >= rgb2.R-1 && rgb1.R <= rgb2.R+1  \
									&& 	rgb1.G >= rgb2.G-1 && rgb1.G <= rgb2.G+1  \
									&& 	rgb1.B >= rgb2.B-1 && rgb1.B <= rgb2.B+1  \
									)
#define HSV_approx( hsv1, hsv2 )	( 	hsv1.H >= hsv2.H-1 && hsv1.H <= hsv2.H+1  \
									&& 	hsv1.S >= hsv2.S-1 && hsv1.S <= hsv2.S+1  \
									&& 	hsv1.V >= hsv2.V-1 && hsv1.V <= hsv2.V+1  \
									)


/**
 * Test RGB<->HSV conversation
 * @return true if test passed
 */
bool test_colors( void )
{
	volatile RGB_t rgb, rgb_r;
	volatile HSV_t hsv, hsv_r;
	volatile uint32_t hexColor = 0;

	/// Test RGB->HexColorRef
	STATIC_ASSERT( rgb2hex( 137, 19, 123 ) == 0x89137B );
	STATIC_ASSERT( rgb2hex( 130, 32, 187 ) == 0x8220BB );
	
	/// Test 1
	rgb.R = 31;
	rgb.G = 127;
	rgb.B = 86;
	
	hsv.H = 154;
	hsv.S = 255 * 75 / 100;
	hsv.V = 255 * 50 / 100;
	
	rgb_r = hsv2rgb(hsv);
	assert_amsg( RGB_approx( rgb, rgb_r ) );
	//if( !RGB_approx(rgb, rgb_r) )
	//	return false;
	
	hsv_r = rgb2hsv(rgb);
	assert_amsg( HSV_approx( hsv, hsv_r ) );	
	//if( !HSV_approx(hsv, hsv_r) )
	//	return false;
	
	/// Test 2
	rgb.R = 137;
	rgb.G = 19;
	rgb.B = 123;
	
	hsv.H = 307;
	hsv.S = 255 * 86 / 100;
	hsv.V = 255 * 54 / 100;
	
	rgb_r = hsv2rgb(hsv);
	assert_amsg( RGB_approx( rgb, rgb_r ) );
	hsv_r = rgb2hsv(rgb);
	assert_amsg( HSV_approx( hsv, hsv_r ) );	
	
	/// Test 3
	rgb.R = 0;
	rgb.G = 0;
	rgb.B = 0;
	
	hsv.H = 0;
	hsv.S = 255 * 0 / 100;
	hsv.V = 255 * 0 / 100;
	
	rgb_r = hsv2rgb(hsv);
	assert_amsg( RGB_approx( rgb, rgb_r ) );
	hsv_r = rgb2hsv(rgb);
	assert_amsg( HSV_approx( hsv, hsv_r ) );
	
	return true;
}
