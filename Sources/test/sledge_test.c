/**
 * Sledge - wide functionality library, suitable for embedded systems, 
 * mainly for ARM Cortex and more particularly for STM32 family
 * 
 * Copyright (c) 2015, Ivan Kuvaldin. All rights reserved.
 * 
 * Licensed under BSD or MIT. 
 * Please refer to Licence.BSD.txt or Licence.MIT.txt provided with these sources.
 * You may obtain a copy of the Licenses at
 *      http://opensource.org/licenses/bsd-license.php
 *      http://opensource.org/licenses/mit-license.php
 */
/**
 * @file 	Sledge/src/sledge_test.c
 * @author 	Ivan "Kyb Sledgehammer" Kuvaldin <i.kyb[2]ya,ru>
 * @version	1.0.0
 * @date 	2014-May-25
 * @brief 	Unit test for Sledge library
 */
 
#include "Sledge.h"

#if USE_RTOS == 1
#include "FreeRTOS.h"
#include "task.h"
#endif


STATIC_ASSERT( BIT(3) == 8 );
STATIC_ASSERT( BIT(13) == 0x2000 );
STATIC_ASSERT( BIT(15) == 0x8000 );
STATIC_ASSERT( BIT(16) == 0x10000 );
STATIC_ASSERT( BIT(31) == 0x80000000u );
// STATIC_ASSERT( BIT(32) == 0x100000000 ); 	It is 64-bit.
STATIC_ASSERT( BIT_MASK(3) == 7 );
STATIC_ASSERT( BIT_MASK(15) == 0x7FFF );
STATIC_ASSERT( BIT_MASK(16) == 0xFFFF );
STATIC_ASSERT( BIT_MASK(31) == 0x7FFFFFFF );
///\bug STATIC_ASSERT( BIT_MASK(32) == 0xFFFFFFFF ); 	Should use 64-bit.
STATIC_ASSERT( BIT_MASK32 == 0xFFFFFFFF ); 	///\ToDo

STATIC_ASSERT( BIT_MASK_FROM_TO(0,0) == 0x1 );
STATIC_ASSERT( BIT_MASK_FROM_TO(0,1) == 0x3 );
STATIC_ASSERT( BIT_MASK_FROM_TO(1,0) == 0x3 );
STATIC_ASSERT( BIT_MASK_FROM_TO(1,1) == 0x2 );
STATIC_ASSERT( BIT_MASK_FROM_TO(1,2) == 0x6 );
STATIC_ASSERT( BIT_MASK_FROM_TO(1,4) == 0x1E );
STATIC_ASSERT( BIT_MASK_FROM_TO(4,1) == 0x1E );
STATIC_ASSERT( BIT_MASK_FROM_TO(3,0) == 0xF );
STATIC_ASSERT( BIT_MASK_FROM_TO(15,12) == 0xF000 );
STATIC_ASSERT( BIT_MASK_FROM_TO(12,15) == 0xF000 );


extern bool test_colors(void);
bool c11n_test(void);


bool sledge_test(void)
{
	test_colors();
	c11n_test();
	return true;
}


bool c11n_test()
{
	return true;
}


#if USE_RTOS == 1
void led_blink_test_task(void *leds)
{
	bsp_LedBlinking( LED_Red, s_timer_ms(50), s_timer_ms(500) );
	
	while(1){
		bsp_LedBlink( LED_Blue, s_timer_ms(100), s_timer_ms(1000), 4 );
		vTaskDelay( ms2ticks(10000) );
	}
}

void led_blink_ttest_task_init( void )
{
	xTaskCreate( led_blink_test_task, "led_blink_test_task", configMINIMAL_STACK_SIZE, 
			NULL, LED_TASK_PRIO-1, NULL);
}
#endif
