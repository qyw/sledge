/**
 *
 */


/**	Supress diagnostic warnings about missed constructor in simple C struct
 *	Only for MDK_ARM. Messages 300..399 are compilier dependend.
 */
#ifdef __CC_ARM
 #pragma diag_suppress 368
#endif

#define DEBUG_LEVEL  0//(DEBUG_L1 | DEBUG_L2)
#include "Sledge.h"
//#include "Sledge/bsp.h"
//#include "Sledge/utils.h"
//#include "Sledge/debug.h"
//#include "Sledge/assert.h"
#include "Sledge/sledge_timers.h"

#ifdef __CC_ARM
 #pragma diag_default 368
#endif


//#include <functional>
//#include <typeinfo>
#include <string>
//#include <iostream>
//#include <memory>
//#include <deque>
//#include <queue>
#include <list>
//#include <map>
#include <vector>
//#include "linked_list_cpp/LinkedList.h"

#if s_USE_TIMER_GARBAGE_COLLECTOR_TASK
#include "FreeRTOS.h"
#include "task.h"
//#include "semphr.h"
#endif


static uint64_t ticks_counter;



class Timer
{
public:
	
	Timer( uint32_t repeats, unsigned StatesN, /*struct State* */ ... )
	{
		va_list args;
		va_start(args, StatesN);
		for( int i=0; i<StatesN; i++ ){
			states.push_back( *va_arg(args, SwTimerState*) );
		}
		va_end(args);
		constructor(repeats);
	}
	
	Timer( uint32_t repeats, unsigned statesN, va_list args )
	{
		for( int i=0; i<statesN; i++ ){
			states.push_back( *va_arg(args, SwTimerState*) );
		}
		constructor(repeats);
	}
	
	Timer( uint32_t repeats, std::vector<SwTimerState> &statesV )
		:states(statesV)
	{	constructor(repeats);
	}
	
	//virtual ~Timer() {}  	// Деструктор должен быть виртуальным для наследования.
	
	bool tick()
	{
		if( !started_ ){
			started_ = true;
			debugf2("Timer.tick() started.\n");
			states[0].callback(states[0].cbparams);  //(state_itr)->cb();  //
			return done();
		}
		
		// Возможно сравнивать с ticks_counter будет эффективнее, чем для каждого таймера counter.
		//if( ticks_counter == scheduled )
		debugf3("Timer.tick() counter: %u; curStateN:%u\n", counter, curStateN );
		if( counter > 1 ){
			counter--;
		}else{
			nextState:
			if( ++curStateN == states.size() ){
				curStateN = 0;
				switch( repeats ){
					default:         repeats--;	break;
					case 0xffffffff:            break;
					case 1:          done_ = true; return done();
				}
				debugf3("Cycle end. Repeats left: %u; curStateN:%u\n", repeats, curStateN );
			}
			states[curStateN].callback(states[curStateN].cbparams);  //(state_itr)->cb();  //
			counter = states[curStateN].duration;  //scheduled = ticks_counter + states[curStateN].duration;
			if( counter == 0 )  goto nextState;
		}
		return done();
	}
	
	inline bool done(){
		return done_;
	}
	
private:
	uint32_t counter;//, ticks;  
	///\todo uint64_t scheduled; // Возможно сравнивать с ticks_counter будет эффективнее, чем для каждого таймера counter.
	std::vector<SwTimerState> states;  //std::map<unsigned, Callback_func> callbacks;  //Callback_func callback;
	//volatile std::vector<State>::iterator state_itr;  //State &currentState;
	size_t curStateN;
	uint32_t repeats;
	bool done_;
	bool started_;
	
	void constructor( uint32_t repeats = 1 )
	{
		this->repeats = repeats>0 ? repeats : 1;
		//state_itr = states.begin();
		//counter = state_itr->duration;
		
		uint32_t fullDuration = 0;
		for( int i=0; i<states.size(); i++ ){
			assert_amsg( states[i].callback != NULL );
			//assert_amsg( states[i].cbparams != NULL );
			//assert_amsg( states[i].duration != 0 );
			//if( states[i].duration == 0 )
			fullDuration += states[i].duration;
		}
		if( fullDuration == 0 )
			done_ = true;
		
		started_ = false;
		done_ = false;
		curStateN = 0;
		counter = states[curStateN].duration;
		debugf("Timer initialized at %llu. states:%u; repeats:%u; counter:%u.\n", ticks_counter, states.size(), repeats, counter );  //debugf("Timer initialized at %llu. states.size():%u; repeats:%u; curStateN:%u; counter:%u. sizeof:%ub \n", ticks_counter, repeats, states.size(), curStateN, counter, sizeof(*this) );
	}
};

static std::list<Timer> timers;


static void wait_cb( void *ready );


/** \addtogroup bla-vla  Прослойки для чистого Си
 *	\{
 */

extern "C" int s_timerbase_init( uint32_t frequency )
{
	// default freq is 100MHz, 
	if( frequency == 0 )
		frequency = 100*1000*1000;
	
	drv_tim_init( &bsp_TIMER, frequency, 12 );
	
 #if s_USE_TIMER_GARBAGE_COLLECTOR_TASK	
	xTaskCreate( s_swtimer_collect_garbage_task, "s_swtimer_collect_garbage_task", configMINIMAL_STACK_SIZE, 
			NULL, 1, NULL);
 #endif
	return 0;
}


/**
 * В конструктор Timer'а передаётся va_list
 */
extern "C" int bsp_schedule_timeout4( uint32_t repeats, uint32_t statesN, /*struct State* */ ... )
{
	va_list args;
	va_start(args, statesN);
	timers.push_back( Timer(repeats, statesN, args) );  //timers.push_front( Timer(repeats, statesN, args) );
	va_end(args);
	return 0;
}


/**
 * В конструктор Timer'а передаётся vector состояний (struct State)
 */
extern "C" int s_schedule_timeout3( uint32_t repeats, uint32_t statesN, /*struct State* */ ... )
{
	std::vector<SwTimerState> states;
	va_list args;
	va_start(args, statesN);
	for( int i=0; i<statesN; i++ ){
		states.push_back( *va_arg(args, SwTimerState*) );  // Здесь структура по указателю копируется в vector
	}
	va_end(args);
	timers.push_back( Timer(repeats, states) );
	return 0;
}


// На два состояния
/*extern "C" int bsp_schedule_timeout22( uint32_t repeats, State *s1, State* s2 )
{
	//const int statesN = 2;
	std::vector<State> states;
	states.push_back( *s1 );  // Здесь структура по указателю копируется в vector
	states.push_back( *s2 );  
	timers.push_back( Timer(repeats, states) );
	return 0;
}*/


/**
 * Ожидание без обратного вызова (callback). Блокировка потока(задачи) с последующим возвратом.
 * Основное применение - прецизионная задержка
 * \return 	0 - OK. Operation done.
 * 			-1 - wrong wanted delay. May be it is too small.
 */
extern "C" int wait( unsigned long ticks )
{
	/*volatile*/ bool ready = false;
	if( ticks == 0 )  return -1;
	SwTimerState s = { ticks, wait_cb, &ready };
	timers.push_back( Timer(1, 1, &s) );
	while( !ready );
	return 0;
}

extern "C" int wait_ns( unsigned long ns )
{
	return wait( s_timer_ns(ns) );
}

extern "C" int wait_us( unsigned long us )
{
	return wait( s_timer_us(us) );
}

extern "C" int wait_ms( unsigned long ms )
{
	return wait( s_timer_ms(ms) );
}

/** \} */


static void wait_cb( void *ready )
{
	*(bool*)ready = true;
}


/**
 * Перехват обработки прерыванния через #define
 * \note Если вектор прерывания используется несколькими источниками, 
 * нужно вызвать эту функцию из вашего обработчика.
 * И закомментировать строку "#define s_TIMER_ISR..." в SledgeConfig.h
 * \note Т.к. это прерывание, и может вызываться довольно часто (например, 1 раз в мкс)
 * то оно должно быть максимально лаконичным и не содержать вызовов типа malloc, free, и т.д.
 * Некоторые реализации stdlib могут пытаться рулить памятью, чем вызывать сравнительно большие задержки или hard fault.
 * Поэтому удаление отработавших таймеров возложено на отдельную задачу s_swtimer_collect_garbage_task(), rjnj
 */
extern "C" void s_TIMER_ISR()
{
	if( bsp_TIMER.tim->DIER & bsp_TIMER.tim->SR & TIM_IT_Update )  //TIM_GetITStatus( bsp_TIMER.tim, TIM_IT_Update ) != RESET )
	{
		bsp_TIMER.tim->SR = ~TIM_IT_Update;  //TIM_ClearITPendingBit( bsp_TIMER.tim, TIM_IT_Update );
		
		ticks_counter++;
		
		for( std::list<Timer>::iterator t_itr = timers.begin(); 
				!t_itr->done() && t_itr != timers.end() && timers.size() > 0; 
				++t_itr )
		{
			t_itr->tick();
		 #if s_USE_TIMER_GARBAGE_COLLECTOR_TASK_DELAY_0_or_NOTIFY_1_or_SEMAPHR_2_or_USERTASK_x == 0
			//do nothing
		 #elif s_USE_TIMER_GARBAGE_COLLECTOR_TASK_DELAY_0_or_NOTIFY_1_or_SEMAPHR_2_or_USERTASK_x == 1
			//vTaskNotifyFromISR()
		 #elif s_USE_TIMER_GARBAGE_COLLECTOR_TASK_DELAY_0_or_NOTIFY_1_or_SEMAPHR_2_or_USERTASK_x == 2
			//vSemaphoreGiveFromISR();
		 #else 
			/*if( t_itr->tick() ){
				t_itr = timers.erase( t_itr );
				debugf("Timer done. Erased. timers.size:%u\n", timers.size() );
			}*/
		 #endif
		}
	}
}


/**
 * Отработавшие таймеры нужно очистить, и освободить память, но не делать же это в прерывании сразу.
 * Выбор места и частоты вызова данной функции ложится на плечи пользователя.
 */
void s_swtimer_collect_garbage(void)
{
	for( std::list<Timer>::iterator t_itr = timers.begin(); 
			t_itr != timers.end() && timers.size() > 0; 
			++t_itr )
	{
		if( t_itr->done() ){
			t_itr = timers.erase( t_itr );
			debugf("Timer done. Erased. timers.size:%u\n", timers.size() );
		}
	}
}


#if s_USE_TIMER_GARBAGE_COLLECTOR_TASK
/**
 * Задача по сбору отработвших программных таймеров.
 * Периодически или по семафору в зависимости от дефайна.
 */
static void s_swtimer_collect_garbage_task( void * repeatEveryTicks )
{
	uint32_t delay = repeatEveryTicks ? (uint32_t)repeatEveryTicks : 60;
	while(1){
	 #if s_USE_TIMER_GARBAGE_COLLECTOR_TASK_DELAY_0_or_NOTIFY_1_or_SEMAPHR_2_or_USERTASK_x == 0
		vTaskDelay(delay);
	 #elif s_USE_TIMER_GARBAGE_COLLECTOR_TASK_DELAY_0_or_NOTIFY_1_or_SEMAPHR_2_or_USERTASK_x == 1
		//waitNotify FreeRTOS v8.2+
		#error NOT implemented yet
	 #elif s_USE_TIMER_GARBAGE_COLLECTOR_TASK_DELAY_0_or_NOTIFY_1_or_SEMAPHR_2_or_USERTASK_x == 2
		//xSemaphoreTake( portWait_MAX );
		#error NOT implemented yet
	 #else 
		#error
	 #endif
		s_swtimer_collect_garbage();
	}
}
#endif
