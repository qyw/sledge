/**
 *
 */


/**	Supress diagnostic warnings about missed constructor in simple C struct
 *	Only for MDK_ARM. Messages 300..399 are compilier dependend.
 */
#ifdef __CC_ARM
 #pragma diag_suppress 368
#endif

#include "Sledge/bsp.h"
#include "Sledge/misc/utils.h"
#include "Sledge/events.h"
#include "Sledge/dassel/debug.h"

#ifdef __CC_ARM
 #pragma diag_default 368
#endif


#include <functional>
#include <map>
#include <typeinfo>
#include <string>
//#include <iostream>
#include <memory>
//#include <deque>
//#include <queue>
#include <list>
//#include <map>
#include <vector>


static uint64_t ticks_counter;




struct Event {
public:
	Event( uint32_t ticks, Callback_func cb, void* cbparams, uint32_t repeats = 1 ) : 
		ticks(ticks),
		callback(cb), 
		cbparams(cbparams),
		repeats(repeats),
		done_(false)
	{}
	
	virtual ~Event() {}  	// Деструктор должен быть виртуальным для наследования.
	
	bool tick(){
		if( --counter == 0 ){
			callback(cbparams);
			if( --repeats == 0 ){
				done_ = true;
			}else{
				counter = ticks;
			}
		}
		return done();
	}
	
	/*bool occur(){ 
		if( counter == 0 ){
			//occured++;
			callback(cbparams);
			return true;
		}
		return false;
	}*/
	
	inline bool done(){
		return done_;
	}
	
	/*void operator() (void) {
		callback(cbparams);
	}*/
private:
	uint32_t counter, ticks;
	Callback_func callback;
	void *cbparams;
	uint32_t repeats;
	bool done_;
};


//template< int NumberOfStates >
class Timer
{
public:
	/*struct TimState {
		//TimState(){};
		//TimState(struct State &st) : duration(){};
		uint32_t duration;
		Callback_func callback;
		void* cbparams;
		void cb(){
			callback(cbparams);
		}
	};*/
	
	Timer( uint32_t repeats, unsigned StatesN, /*struct State* */ ... )
	{
		va_list args;
		va_start(args, StatesN);
		for( int i=0; i<StatesN; i++ ){
			states.push_back( *va_arg(args, State*) );
		}
		va_end(args);
		constructor(repeats);
	}
	
	Timer( uint32_t repeats, std::vector<State> &statesV )
		:states(statesV)
	{	constructor(repeats);
	}
	
	Timer( uint32_t repeats, unsigned statesN, va_list args )
	{
		for( int i=0; i<statesN; i++ ){
			states.push_back( *va_arg(args, State*) );
		}
		constructor(repeats);
	}
	
	//virtual ~Timer() {}  	// Деструктор должен быть виртуальным для наследования.
	
	/*bool tick()
	{
		//debugf("Timer.tick() counter: %u; state_itr:0x%x; state:%d\n", counter, (uint32_t)state_itr, state_itr == states.begin() ? 1 : (state_itr == (states.begin()+1) ? 2 : 0) );
		if( counter > 1 ){
			counter--;
		}else{
			state_itr->callback(state_itr->cbparams);  //(state_itr)->cb();  //
			
			++state_itr;
			if( state_itr == states.begin()+2 ){
				state_itr = states.begin();
				switch( repeats ){
					default:         repeats--;	break;
					case 0xffffffff:            break;
					case 1:          done_ = true; return done();
				}
				debugf("Cycle end. Repeats left: %u; state_itr:0x%x\n", repeats, (uint32_t)state_itr );
			}
			counter = state_itr->duration;
		}
		return done();
	}*/
	bool tick()
	{
		debugf("Timer.tick() counter: %u; curStateN:%u\n", counter, curStateN );
		if( counter > 1 ){
			counter--;
		}else{
			states[curStateN].callback(states[curStateN].cbparams);  //(state_itr)->cb();  //
			if( ++curStateN == states.size() ){
				curStateN = 0;
				switch( repeats ){
					default:         repeats--;	break;
					case 0xffffffff:            break;
					case 1:          done_ = true; return done();
				}
				debugf("Cycle end. Repeats left: %u; curStateN:%u\n", repeats, curStateN );
			}
			counter = states[curStateN].duration;
		}
		return done();
	}
	
	/*bool occur(){ 
		if( counter == 0 ){
			//occured++;
			callback(cbparams);
			return true;
		}
		return false;
	}*/
	
	inline bool done(){
		return done_;
	}
	
	/*void operator() (void) {
		callback(cbparams);
	}*/
	
private:
	uint32_t counter;//, ticks;
	std::vector<State> states;  //std::map<unsigned, Callback_func> callbacks;  //Callback_func callback;
	//volatile std::vector<State>::iterator state_itr;  //State &currentState;
	size_t curStateN;
	uint32_t repeats;
	bool done_;
	
	void constructor( uint32_t repeats ){
		this->repeats = repeats>0 ? repeats : 1;
		//state_itr = states.begin();
		//counter = state_itr->duration;
		//curStateN = 0;
		counter = states[curStateN].duration;
		//debugf("Timer initialized at %llu. states.size(): %u; state_itr: 0x%x; counter: %u\n", ticks_counter, states.size(), (uint32_t)state_itr, counter );
		debugf("Timer initialized at %llu. states.size(): %u; curStateN:%u; counter: %u\n", ticks_counter, states.size(), curStateN, counter );
	}
};

std::list<Event> events;  //std::deque<Event> events;
std::list<Timer> timers;

/*typedef struct {
	uint32_t counter;
	Callback_func callback;
	void *cbparams;
} Event_t;*/

//Event_t bsp_timer_events_list[10];
//size_t events_counter=0;







extern "C" int bsp_timer_init( uint32_t frequency )
{
	// default freq is 1MHz, 
	if( frequency == 0 )
		frequency = 100*1000*1000;
	
	drv_tim_init( &bsp_TIMER, frequency, 12 );
	return 0;
}


extern "C" int bsp_schedule_timeout4( uint32_t repeats, uint32_t statesN, /*struct State* */ ... )
{
	va_list args;
	va_start(args, statesN);
	timers.push_back( *(new Timer(repeats, statesN, args)) );
	va_end(args);
	return 0;
}
extern "C" int bsp_schedule_timeout3( uint32_t repeats, uint32_t statesN, /*struct State* */ ... )
{
	std::vector<State> states;  //std::vector<State> *states = new std::vector<State>(statesN)
	va_list args;
	va_start(args, statesN);
	for( int i=0; i<statesN; i++ ){
		states.push_back( *va_arg(args, State*) );  // Здесь структура по указателю копируется в vector
	}
	va_end(args);
	timers.push_back( *(new Timer(repeats, states)) );
	return 0;
}
extern "C" int bsp_schedule_timeout2( 
		uint32_t ticks1, Callback_func cb1, void *params1, 
		uint32_t ticks2, Callback_func cb2, void *params2, uint32_t repeats )
{
	//int bsp_LedBlink( const GPIO_t *led, unsigned on_ms, unsigned period_ms, unsigned times )
	/*Timer::State st1 = {ticks1, cb1, params1},
	             st2 = {ticks2, cb2, params2};
	timers.push_back( *(new Timer(repeats, 2, st1, st2)) );*/
	return 0;
}
extern "C" int bsp_schedule_timeout( uint32_t ticks, Callback_func cb, void *params, uint32_t repeats )
{
	// Add new event
	//Event *e = new Event();
	//events.push_back( *e );
	events.push_back( *(new Event(ticks, cb, params, repeats)) );
	
	return 0;
}
/*extern "C" int bsp_schedule_timeout( uint32_t ticks, Callback_func cb, void *params )
{
	// Add new event
	if( events_counter < sizeofarr(bsp_timer_events_list) ){
//		bsp_timer_events_list[events_counter].counter = ticks;
//		bsp_timer_events_list[events_counter].callback = cb;
//		bsp_timer_events_list[events_counter].cbparams = params;
	} else {
		return -1;
	}
	return 0;
}*/


extern "C" void bsp_timer_ISR()
{
	//Event *e;
	//int i = 0;
	
	if( TIM_GetITStatus( bsp_TIMER.tim, TIM_IT_Update ) != RESET ){
		TIM_ClearITPendingBit( bsp_TIMER.tim, TIM_IT_Update );
		
		ticks_counter++;
		
		/*for( std::list<Event>::iterator e_itr = events.begin(); 
				e_itr != events.end(); 
				++e_itr )
		{			
			if( e_itr->tick() )
				events.erase( e_itr );
		}*/
		for( std::list<Timer>::iterator t_itr = timers.begin(); 
				t_itr != timers.end(); 
				++t_itr )
		{			
			if( t_itr->tick() )
				timers.erase( t_itr );
		}
		/*for( int i=0; i<events.size(); i++ ){
			if( events.at(i).tick() ){
				events.erase()
			}
		}*/
	}
}
/*
extern "C" void bsp_timer_ISR()
{
	static uint64_t ticks;
	Event_t event;
	int i = 0;
	
	if( TIM_GetITStatus( bsp_TIMER.tim, TIM_IT_Update ) != RESET ){
		TIM_ClearITPendingBit( bsp_TIMER.tim, TIM_IT_Update );
		
		ticks++;
		
		while( i < events_counter ){
		    event = bsp_timer_events_list[i];
//			event.counter--;
//			if( event.counter <= 0 ){
//				event.callback(event.cbparams);
//				///\todo event.delete;
//			}
		}
	}
}*/
