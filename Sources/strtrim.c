/**
 *
 *
 */

#include <string.h>
#include "Sledge/strtrim.h"
#include "Sledge/utils.h"


/** Note: This function returns a pointer to a substring of the original string.
 * If the given string was allocated dynamically, the caller must not overwrite
 * that pointer with the returned value, since the original pointer must be
 * deallocated using the same allocator with which it was allocated.  The return
 * value must NOT be deallocated using free() etc.
 */
char *strtrim(char *str)
{
	char *end;
	
	// Trim leading space
	while(isSpace(*str)) 
		str++;
	
	if(*str == 0)  // All spaces?
		return str;
	
	// Trim trailing space
	end = str + strlen(str) - 1;
	while(end > str && isSpace(*end)) 
		end--;
	
	// Write new null terminator
	*(end+1) = 0;
	
	return str;
}


char *strLtrim(char *str)
{
	// Trim leading space
	while(isSpace(*str)) 
		str++;
	
	return str; 
}


/// @note: this function is safe.
char *strRtrim(char *str)
{
	char *end;
	
	// Trim trailing space
	end = str + strlen(str) - 1;
	while(end > str && isSpace(*end)) 
		end--;
	
	// Write new null terminator
	*(end+1) = 0;
	
	return str;
}
