/**
 * Sledge - wide functionality library, suitable for embedded systems, 
 * mainly for ARM Cortex and more particularly for STM32 family
 * 
 * Copyright (c) 2015, Ivan Kuvaldin. All rights reserved.
 * 
 * Licensed under BSD or MIT. 
 * Please refer to Licence.BSD.txt or Licence.MIT.txt provided with these sources.
 * You may obtain a copy of the Licenses at
 *      http://opensource.org/licenses/bsd-license.php  
 *      http://opensource.org/licenses/mit-license.php  
 */
/** 
 * @file	Sledge/colors/colors.c 
 * @author 	Ivan "Kyb Sledgehammer" Kuvaldin <i.kyb[2]ya,ru>
 * @version 1.0.0
 * @date	2015
 * @brief	
 *			
 */

#include "Sledge/colors.h"
#include "Sledge/assert.h"



//#define max(a,b) 		({ 	__typeof__ (a) _a = (a); \
							__typeof__ (b) _b = (b); \
							_a > _b ? _a : _b; \
						})
//#define max3(a,b,c) 		({ 	__typeof__ (a) _a = (a); \
							__typeof__ (b) _b = (b); \
							__typeof__ (c) _c = (c); \
							__typeof__ (a) _m = max(_a,_b); \
							_m > _c ? _m : _c; \
						})
						
//#define min(a,b) 		({ 	__typeof__ (a) _a = (a); \
							__typeof__ (b) _b = (b); \
							_a < _b ? _a : _b; \
						})
//#define min3(a,b,c) 	({ 	__typeof__ (a) _a = (a); \
							__typeof__ (b) _b = (b); \
							__typeof__ (c) _c = (c); \
							__typeof__ (a) _m = min(_a,_b); \
							_m < _c ? _m : _c; \
						})
#define min( a, b )		( (a) < (b) ? (a) : (b) )
#define min3(a,b,c) 	( min(a,b) < min(a,c) ? min(a,b) : min(a,c))

#define max( a, b )		( (a) > (b) ? (a) : (b) )
#define max3(a,b,c) 	( max(a,b) > max(a,c) ? max(a,b) : max(a,c))


/**
 * @brief Преобразование цветовой модели HSV, HSB (ТНЯ) в RGB
 * @param hsv : The instance of ColorHSV_t
 * @return RGB_t
 * @literature https://ru.wikipedia.org/wiki/HSV
 */
ColorRGB_t hsv2rgb( ColorHSV_t hsv )
{
	uint8_t Hi/*region,hh*/, Vmin/*p*/, a, Vinc/*t*/, Vdec/*q*/;
	ColorRGB_t rgb;
	
	//assert_amsg( hsv.H <=359 );
	//assert_amsg( hsv.S <=100 );
	//assert_amsg( hsv.B <=100 );
	if( !(hsv.H <=359) )
		hsv.H = 0;
		//return rgb;
	//if( !(hsv.S <=100) )
	//	hsv.S = 100;
		//return rgb;
	//if( !(hsv.V <=100) )
	//	hsv.V = 100;
		//return rgb;
	
	if( hsv.S == 0 ){	/// Насыщенность==0, только оттенки серого.
		rgb.R = 
		rgb.G = 
		rgb.B = hsv.V;
		return rgb;
	}
	
	//Hi = hsv.H / 60; 	/// делим круг оттенка (hue) на шесть сегментов
	//Vmin = ( (100 - hsv.S) * hsv.V ) / 100;
	//a = (hsv.V - Vmin) * ( hsv.H % 60 ) / 60;	/// Только для дробных
	//Vinc = Vmin + a;
	//Vdec = hsv.V - a;
	
	Hi = hsv.H / 60; 	/// делим круг оттенка (hue) на шесть сегментов
	Vmin = ( (255 - hsv.S) * hsv.V ) / 255;
	a = (hsv.V - Vmin) * ( hsv.H % 60 ) / 60;	/// Только для дробных, а может и нет
	Vinc = Vmin + a;
	Vdec = hsv.V - a;
	
	/// Другой алгоритм (скорректировано) http://stackoverflow.com/questions/3018313/algorithm-to-convert-rgb-to-hsv-and-hsv-to-rgb-in-range-0-255-for-both	
	//remainder = (hsv.H - (Hi * 60)) * 6; 	///Нах на 6 домножать? Почему не написать hsv.H % 60
	//Vmin = (hsv.V * (255 - hsv.S)) /255; //>> 8;
	//Vdec = (hsv.V * (255 - ((hsv.S * remainder) >> 8))) /255; //>> 8;
	//Vinc = (hsv.V * (255 - ((hsv.S * (255 - remainder)) >> 8))) >> 8;
	
	
	switch( Hi ){
		case 0:
			rgb.R = hsv.V;
			rgb.G = Vinc;
			rgb.B = Vmin;
			break;
		case 1:
			rgb.R = Vdec;
			rgb.G = hsv.V;
			rgb.B = Vmin;
			break;			
		case 2:
			rgb.R = Vmin;
			rgb.G = hsv.V;
			rgb.B = Vinc;
			break;	
		case 3:
			rgb.R = Vmin;
			rgb.G = Vdec;
			rgb.B = hsv.V;
			break;	
		case 4:
			rgb.R = Vinc;
			rgb.G = Vmin;
			rgb.B = hsv.V;
			break;	
		case 5:
			rgb.R = hsv.V;
			rgb.G = Vmin;
			rgb.B = Vdec;
			break;
		}
	return rgb;
}


/**
 * @brief Преобразование цветовой модели RGB в HSV.
 * @param rgb : The instance of ColorRGB_t
 * @return ColorHSV_t
 * @literature https://ru.wikipedia.org/wiki/HSV
 */
ColorHSV_t rgb2hsv( ColorRGB_t rgb ) 
//ColorHSV_t RGBtoHSV( ColorRGB_t rgb ) 
//ColorHSV_t RGB2HSV( ColorRGB_t rgb ) 
{
	ColorHSV_t hsv;
	uint8_t maxRGB, minRGB, deltaRGB;
	maxRGB = max3( rgb.R,rgb.G,rgb.B );
	minRGB = min3( rgb.R,rgb.G,rgb.B );
	deltaRGB = (maxRGB - minRGB);
	
	hsv.V = maxRGB;
	if( hsv.V == 0 ){
		hsv.S = 0;
		hsv.H = 0;
		return hsv;
	}
	
	//hsv.S = maxRGB == 0 ? 0 : 1 - minRGB/maxRGB; 	/// Только для дробных
	//hsv.S = maxRGB == 0 
	//		? 0
	//		: (long)255 * (maxRGB - minRGB) / maxRGB;
	hsv.S = 255ul * (maxRGB - minRGB) / maxRGB;
	if( hsv.S == 0 ){
		hsv.H = 0;
		return hsv;
	}
	
	if( maxRGB == minRGB ){
		hsv.H = 0;
	} else if( maxRGB == rgb.R && rgb.G >= rgb.B ){
		hsv.H = 60 * (rgb.G - rgb.B) / deltaRGB + 0;
	} else if( maxRGB == rgb.R && rgb.G < rgb.B ){
		hsv.H = 60 * (rgb.G - rgb.B) / deltaRGB + 360;
	} else if( maxRGB == rgb.G ){
		hsv.H = 60 * (rgb.B - rgb.R) / deltaRGB + 120;
	} else if( maxRGB == rgb.B ){
		hsv.H = 60 * (rgb.R - rgb.G) / deltaRGB + 240;
	}
	
	/// Другой упрощённый алгоритм (скорректировано) http://stackoverflow.com/questions/3018313/algorithm-to-convert-rgb-to-hsv-and-hsv-to-rgb-in-range-0-255-for-both
    //if (rgbMax == rgb.r)
    //    hsv.h = 0 + 43 * (rgb.g - rgb.b) / deltaRGB;
    //else if (rgbMax == rgb.g)
    //    hsv.h = 85 + 43 * (rgb.b - rgb.r) / deltaRGB;
    //else
    //    hsv.h = 171 + 43 * (rgb.r - rgb.g) / deltaRGB;
	

	
	return hsv;
}



/// ---------------------
/// Представление цветов double или float
/// ----------------------

/**
 * Преобразование цветовой модели HSV, HSB(ОНЯ) в RGB
 * Цвета представлены в double
 * @param rgb : The instance of ColorRGB_t
 * @return ColorHSV_t
 * @literature https://ru.wikipedia.org/wiki/HSV
 */
/*ColorHSV_t rgb2hsv_double( ColorRGB_t in ) 
{
    ColorHSV_t  out;
    double      min, max, delta;

    min = in.r < in.g ? in.r : in.g;
    min = min  < in.b ? min  : in.b;

    max = in.r > in.g ? in.r : in.g;
    max = max  > in.b ? max  : in.b;

    out.v = max;                                // v
    delta = max - min;
    if( max > 0.0 ) { // NOTE: if Max is == 0, this divide would cause a crash
        out.s = (delta / max);                  // s
    } else {
        // if max is 0, then r = g = b = 0              
            // s = 0, v is undefined
        out.s = 0.0;
        out.h = NAN;                            // its now undefined
        return out;
    }
    if( in.r >= max )                           // > is bogus, just keeps compilor happy
        out.h = ( in.g - in.b ) / delta;        // between yellow & magenta
    else
    if( in.g >= max )
        out.h = 2.0 + ( in.b - in.r ) / delta;  // between cyan & yellow
    else
        out.h = 4.0 + ( in.r - in.g ) / delta;  // between magenta & cyan

    out.h *= 60.0;                              // degrees

    if( out.h < 0.0 )
        out.h += 360.0;

    return out;
}


rgb hsv2rgb_double(hsv in)
{
    double      hh, p, q, t, ff;
    long        i;
    rgb         out;

    if(in.s <= 0.0) {       // < is bogus, just shuts up warnings
        out.r = in.v;
        out.g = in.v;
        out.b = in.v;
        return out;
    }
    hh = in.h;
    if(hh >= 360.0) hh = 0.0;
    hh /= 60.0;
    i = (long)hh;
    ff = hh - i;
    p = in.v * (1.0 - in.s);
    q = in.v * (1.0 - (in.s * ff));
    t = in.v * (1.0 - (in.s * (1.0 - ff)));

    switch(i) {
    case 0:
        out.r = in.v;
        out.g = t;
        out.b = p;
        break;
    case 1:
        out.r = q;
        out.g = in.v;
        out.b = p;
        break;
    case 2:
        out.r = p;
        out.g = in.v;
        out.b = t;
        break;

    case 3:
        out.r = p;
        out.g = q;
        out.b = in.v;
        break;
    case 4:
        out.r = t;
        out.g = p;
        out.b = in.v;
        break;
    case 5:
    default:
        out.r = in.v;
        out.g = p;
        out.b = q;
        break;
    }
    return out;     
}
*/
