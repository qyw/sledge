/**
 * Sledge - wide functionality library, suitable for embedded systems, 
 * mainly for ARM Cortex and more particularly for STM32 family
 * 
 * Copyright (c) 2015, Ivan Kuvaldin. All rights reserved.
 * 
 * Licensed under BSD or MIT. 
 * Please refer to Licence.BSD.txt or Licence.MIT.txt provided with these sources.
 * You may obtain a copy of the Licenses at
 *      http://opensource.org/licenses/bsd-license.php  
 *      http://opensource.org/licenses/mit-license.php  
 */
/**
 * \file 	Sledge/bsp/bsp.c
 * \author 	Ivan "Kyb Sledgehammer" Kuvaldin <i.kyb[2]ya,ru>
 * \brief 	Look at ./bsp.h for more information
 */

#include "Sledge/bsp.h"
#include "Sledge/assert.h"
#include "Sledge/colors.h"
#include "Sledge/drv/stm32/sledge_stm32_timers.h"
#include "Sledge/sledge_timers.h"

#include "stm32f4xx_rcc.h"
#include "stm32f4xx_syscfg.h"
#include "misc.h"


#define NUMBER_OF_GPORTS  11 // up to K // зависит от камня
static volatile uint16_t bsp_used_gpios[NUMBER_OF_GPORTS];

inline static bool bsp_PinIsUsed( const GPIO_t* gp ){
	return bsp_used_gpios[gp->portname-'A'] & gp->pin ? true : false;
}


/*	┌———————————————————————————————————————————————————————————————————————┐
	│						Outouts & LEDs control							│  
	└———————————————————————————————————————————————————————————————————————┘ */
/** \addtogroup Outouts_n_LEDs_control Outouts & LEDs control
	@{ */

/** 
 * Configure uC's pin as output. Does all the same as bsp_GpioOut_Init(),
 * but allows to define GPIO_InitStructure with users params.
 * @param      gp  uC's pin described as GPIO_t
 * @param *extGis  Pointer to GPIO_InitTypeDef instance. ONLY GPIO_Speed, 
 *		           GPIO_OType, GPIO_PuPd fields should be set, other does not metter.
 *                 NULL means use default settings: GPIO_Speed_100MHz, GPIO_OType_PP, GPIO_PuPd_NOPULL.
 * \return 	0 | error(negative) | warning(positive) code
 * 			0  OK
 * 		   -1  Invalid argument. Nothing to configure.
 *         -2  pin is already in use. Must be deinited before new configuration
 */
int bsp_GpioOut_InitStruct( const GPIO_t* gp, GPIO_InitTypeDef *extGis ) 
{
	GPIO_InitTypeDef gis, *p; 	// Структура регистров для конфигурации портов
	
	if(gp == 0) return -1; //assert_amsg( gp != NULL);
	if( bsp_PinIsUsed(gp) )  return -2;
	
	if( extGis ) {
		p = extGis;
		assert_amsg(IS_GPIO_SPEED( p->GPIO_Speed ));
		assert_amsg(IS_GPIO_OTYPE( p->GPIO_OType ));
		assert_amsg(IS_GPIO_PUPD(  p->GPIO_PuPd  ));
	} else {
		// Задать структуру конфирурации порта, если ее не предоставили как параметр extgis
		gis.GPIO_Speed = GPIO_Speed_100MHz; 
		gis.GPIO_OType = GPIO_OType_PP; 
		gis.GPIO_PuPd  = GPIO_PuPd_NOPULL;
		p = &gis;
	}
	
	// Пользователь может задавать только GPIO_Speed, GPIO_OType, GPIO_PuPd
	p->GPIO_Pin  = gp->pin;
	p->GPIO_Mode = GPIO_Mode_OUT;

	// Port clock enable
	RCC_AHB1PeriphClockCmd(gp->clk, ENABLE);
 	// Инициализировать структуой порт
	GPIO_Init( (GPIO_TypeDef*)gp->port, p);
	
	//gp->used = true;
	return 0;
}


/**
 * Configure uC's pin as output. Does all the same as bsp_GpioOut_Init(),
 * but allows to define GPIO_InitStructure with users params.
 * \param *extGis  Pointer to GPIO_InitTypeDef instance. ONLY GPIO_Speed, 
 *		           GPIO_OType, GPIO_PuPd fields should be set, other does not metter.
 *                 NULL means use default settings: GPIO_Speed_100MHz, GPIO_OType_PP, GPIO_PuPd_NOPULL.
 * \param 	n  Number of GPIOs to be coonfigured
 * \param   gp  uC's pin described as GPIO_t*
 * \return 	0 | error(negative) | warning(positive) code
 * 			0 - OK
 * 			positive - number of pins which did not pass check for null-pointer and is-used
 * 			negative - ?TODO error
 */
int bsp_GpioOut_InitStruct_va( GPIO_InitTypeDef *extGis, size_t n, /*GPIO_t* */... ) 
{
	GPIO_InitTypeDef gis, *p; 	// Структура регистров для конфигурации портов
	GPIO_t *gp;
	va_list vl;
	int i;
	int ret;
	
	if( extGis ) {
		p = extGis;
		assert_amsg(IS_GPIO_SPEED( p->GPIO_Speed ));
		assert_amsg(IS_GPIO_OTYPE( p->GPIO_OType ));
		assert_amsg(IS_GPIO_PUPD(  p->GPIO_PuPd  ));
	} else {
		// Задать структуру конфирурации порта, если ее не предоставили как параметр extgis
		gis.GPIO_Speed = GPIO_Speed_100MHz; 
		gis.GPIO_OType = GPIO_OType_PP; 
		gis.GPIO_PuPd  = GPIO_PuPd_NOPULL;
		p = &gis;
	}
	// Пользователь может задавать только GPIO_Speed, GPIO_OType, GPIO_PuPd
	p->GPIO_Mode = GPIO_Mode_OUT;
	
	va_start( vl, n );
	for( i=0; i<n; i++ ){
		gp = va_arg( vl, GPIO_t* );
		p->GPIO_Pin  = gp->pin;

		if( gp == NULL || bsp_PinIsUsed(gp)){
			ret++;
			continue;
		}
		
		// Port clock enable
		RCC_AHB1PeriphClockCmd( gp->clk, ENABLE );
		// Инициализировать структуой порт
		GPIO_Init( (GPIO_TypeDef*)gp->port, p );
		
		//gp->used = true;
	}

	va_end(vl);
	return ret;
}


/** 
 * Initialize the led with adjustable brightness
 * @Note you need to make sure led may be internally connected to TIM's channel.
 * @param led
 * @param timhandler
 */
void bsp_LedPwmInit( const GPIO_t* led /*, timhandler*/ )
{
	GPIO_InitTypeDef gis;
	
	gis.GPIO_Speed = GPIO_Speed_2MHz;
	gis.GPIO_OType = GPIO_OType_PP;
	gis.GPIO_PuPd = GPIO_PuPd_NOPULL; 
	
	bsp_GpioAf_InitStruct( led, LEDS_PWM_TIM_HANDLER.af, &gis );
	
	timPwmConfig( &(LEDS_PWM_TIM_HANDLER), LEDS_PWM_TIM_FREQ );
}

/** 
 * @brief  Turns selected LED On. 
 */
void bsp_LedPwmInitAll( /*, timhandler*/ )
{
	uint8_t i=0; 
	while( i < numOfLEDS_PWM )
		bsp_LedPwmInit(LEDS[i++]);
}

/**
 * @brief 	Turns selected LED On. 
 * @param 	brightness From 0 to 1000‰ 
 */
void bsp_LedPwmSetBrightness( const GPIO_t* led, const uint16_t brightness )
{
	uint16_t ccr;
	ccr = (uint16_t) (((uint32_t) brightness * (LEDS_PWM_TIM_PERIOD - 1)) / 1000);
	
	if( led == LEDS[0] ) {
		LEDS_PWM_TIM_HANDLER.tim->CCR1 = ccr;
	} else if( led == LEDS[1] ) {
		LEDS_PWM_TIM_HANDLER.tim->CCR2 = ccr;
	} else if( led == LEDS[2] ) {
		LEDS_PWM_TIM_HANDLER.tim->CCR3 = ccr;
	} else if( led == LEDS[3] ) {
		LEDS_PWM_TIM_HANDLER.tim->CCR4 = ccr;
	}
}


/** 
 * Initialize the RGB Led with adjustable brightness
 * @note you need to make sure led may be internally connected to TIM's channels.
 * @param led
 */
void bsp_LedRgbInit( /*const*/ LedRGB_t* led )
{
	GPIO_InitTypeDef gis;
	uint8_t ch, alt;
	volatile uint32_t* tempCCRp;
	
	if( led == NULL )
		return;
	//if( led->R == 0 && led->G == 0 && led->B == 0 )
	//	return;
	///\TODO проверка что таймер иммеет 3 канала.
	
	///\TODO проверка принадлежности пинов таймеру
	
	gis.GPIO_Speed = GPIO_Speed_2MHz;
	gis.GPIO_OType = GPIO_OType_PP;
	gis.GPIO_PuPd = GPIO_PuPd_NOPULL;
	
	bsp_GpioAf_InitStruct( led->R, led->timhandler->af, &gis );
	bsp_GpioAf_InitStruct( led->G, led->timhandler->af, &gis );
	bsp_GpioAf_InitStruct( led->B, led->timhandler->af, &gis );
	
	// Задать соответствия пинов, каналов и регистров таймера
	/// @note 4*ch - вариант не супер. Неизвестно куда перекочует CCR2 на другом камне
	for( ch=0; ch<4; ch++ ){ 	// Iterate through channals
		// Iterate through pin alternatives
		for( alt=0; alt<3 && led->timhandler->ch_Pins[ch][alt]; alt++ )
		{
			tempCCRp = & led->timhandler->tim->CCR1 + ch; //+ch*4
			if( led->timhandler->ch_Pins[ch][alt] == led->R ){
				led->R_CCR = tempCCRp;
			} else if( led->timhandler->ch_Pins[ch][alt] == led->G ){
				led->G_CCR = tempCCRp;
			} else if( led->timhandler->ch_Pins[ch][alt] == led->B ){
				led->B_CCR = tempCCRp;
			}
		}
	}
	
	// Проверка, что соответствия пинов каналам таймера назначены корректно.
	assert_amsg( led->R_CCR != 0 );
	assert_amsg( led->G_CCR != 0 );
	assert_amsg( led->B_CCR != 0 );
	
	timPwmConfig( led->timhandler, 500000/*LEDS_PWM_TIM_FREQ*/ );
	//bsp_LedRGBSetColor( led, 0, 0, 0 )
}


/**
 * Set the RGB LED color
 * @param 	led   : pointer to LedRGB_t
 * @param 	r,g,b : brightness of red,gree,blue LED from 0 to 255 
 */
void bsp_LedRGBSetColor( const LedRGB_t* led, const uint8_t r, const uint8_t g, const uint8_t b )
{
	uint16_t ccrR, ccrG, ccrB;
	
	if( led == NULL )  return;
	
	ccrR = (uint16_t) (((uint32_t) r * led->timhandler->tim->ARR) / UINT8_MAX);
	ccrG = (uint16_t) (((uint32_t) g * led->timhandler->tim->ARR) / UINT8_MAX);
	ccrB = (uint16_t) (((uint32_t) b * led->timhandler->tim->ARR) / UINT8_MAX);
	//ccrG = (uint16_t) (((uint32_t) g * (LEDS_PWM_TIM_PERIOD-1)) / 1000);
	//ccrB = (uint16_t) (((uint32_t) b * (LEDS_PWM_TIM_PERIOD-1)) / 1000);
	
	*led->R_CCR = ccrR;
	*led->G_CCR = ccrG;
	*led->B_CCR = ccrB;
}


/**
 * Set the RGB LED color
 * @param 	led : pointer to LedRGB_t
 * @param 	rgb : color 
 */
void bsp_LedRGBSetRGB( const LedRGB_t* led, ColorRGB_t rgb )
{
	bsp_LedRGBSetColor(led,rgb.R,rgb.G,rgb.B);
	/*uint16_t ccrR, ccrG, ccrB;
	
	assert_amsg( led != NULL );
	
	ccrR = (uint16_t) (((uint32_t) rgb.R * led->timhandler->tim->ARR) / UINT8_MAX);
	ccrG = (uint16_t) (((uint32_t) rgb.G * led->timhandler->tim->ARR) / UINT8_MAX);
	ccrB = (uint16_t) (((uint32_t) rgb.B * led->timhandler->tim->ARR) / UINT8_MAX);
	
	*led->R_CCR = ccrR;
	*led->G_CCR = ccrG;
	*led->B_CCR = ccrB;*/
}



/** \addtogroup LEDs_blinking_control  LEDs blinking control
	@{ */

/*static void callback_LedBlinking(void *led)
{
	bsp_LedToggle(led);
}*/

/**
 * Blink with LED
 * \param led  GPIO, e.g. PD12
 * \param on_ms  time in milliseconds, when LED is on
 * \param period_ms  blinking period
 * \param times  How much times LED will blink
 */
int bsp_LedBlink( const GPIO_t *led, unsigned on_ms, unsigned period_ms, unsigned times )
{
//	///\todo Make sure timer is configured
//	///\todo Make sure GPIO is configured as output
//	assert_amsg( led != NULL );
//	
//	unsigned off_ms = period_ms>on_ms ? period_ms-on_ms : 0;
//	
//	struct State s_on  = { on_ms,  (Callback_func)bsp_OutHi,  (void*)led };
//    struct State s_off = { off_ms, (Callback_func)bsp_OutLow, (void*)led };
//
//	s_schedule_timeout3( times, 2, &s_on, &s_off );
//	//s_schedule_timeout( on_ms*1000/*us*/, (Callback_func) bsp_OutLow, (void*)led, times );  //через время
//	/*s_schedule_timeout2( times,
//			on_ms,             (Callback_func)callback_LedBlinking, (void*)led, 
//			(period_ms-on_ms), (Callback_func)callback_LedBlinking, (void*)led );*/
//	//bsp_schedule_timeout22( times, &s_on, &s_off );
//	
	//assert_msg("Unimplemented yet!");
	return 0;
}


#define bsp_LedBlinkOnce( led, on_ms )  bsp_LedBlink( led, on_ms, 0, 1 )
//int bsp_LedBlinkOnce( GPIO_t *led, unsigned on_ms ){}

#define bsp_LedBlinking( led, on_ms, period_ms )  bsp_LedBlink( led, on_ms, period_ms, 0xffffffff )
//int bsp_LedBlinking( GPIO_t *led, unsigned on_ms, unsigned period_ms ){}

#define bsp_LedBlinkingOff( led )  bsp_LedBlink( led, 0, 0, 0 )
//int bsp_LedBlinkingOff( GPIO_t *led ){}


/** @} */
/** @} */




/*	┌———————————————————————————————————————————————————————————————————————┐
	│				Inputs, Buttons and joystick control					│  
	└———————————————————————————————————————————————————————————————————————┘ */
/** \addtogroup Inputs_Buttons_and_joystick_control Inputs, Buttons and joystick control
	@{ */

/**	
 * Configures GPIO as input and EXTI Line if mode setted.
 * @param *gp : Specifies the GPIO to be configured.
 * @param *extGis: External GPIO_InitStruct. 0-pointer to use default parameters:
 * 		  GPIO_Speed_100MHz, GPIO_PuPd_NOPULL.
 * @param mode: Specifies input mode.
 *   This parameter can be one of following parameters:   
 *     @arg eBUTTON_MODE_GPIO: Button will be used as simple IO 
 *     @arg eBUTTON_MODE_EXTI: Button will be connected to EXTI line with interrupt
 *          generation capability
 * @param *extEis: External EXTI_InitStructure. 0-pointer to use default parameters:
 *  	  EXTI_Mode_Interrupt, EXTI_Trigger_Rising
 * @param intprio: Interrupt priority if eBUTTON_MODE_EXTI. 
 * 		  Only NVIC_PriorityGroup_4 supported - pre-emption priority levels
 * @retval 0 if OK, negative value on error.
 * 		   extGis and extEis fields will be set as they are sent to registers.
 */
int bsp_Input_InitGeneral( 
	const GPIO_t* gp, 
	GPIO_InitTypeDef *extGis, 
	ButtonMode_e buttonMode, 
	EXTI_InitTypeDef *extEis, 
	uint8_t intprio )
{
	GPIO_InitTypeDef gis, *p; // Структура регистров для конфигурации портов
	EXTI_InitTypeDef eis, *peis;
	NVIC_InitTypeDef nvic;
	
	assert_amsg( gp != NULL);
	//if( gp != NULL)  return -1;
		
	if( extGis ) {
		assert_amsg(IS_GPIO_SPEED( extGis->GPIO_Speed ));
		assert_amsg(IS_GPIO_PUPD( extGis->GPIO_PuPd  ));
		p = extGis;
	} else {
		// Задать структуру конфирурации порта, если ее не предоставили как параметр extGis
		gis.GPIO_Speed = GPIO_Speed_100MHz; 
		gis.GPIO_PuPd  = GPIO_PuPd_NOPULL;
		p = &gis;
	}
	
	p->GPIO_Pin = gp->pin;
	p->GPIO_Mode = GPIO_Mode_IN;
	p->GPIO_OType = GPIO_OType_PP;  // Не имеет значения, т.к. порт настроен как вход. Однако значение по умолчанию должно быть задано.
	
	// Enable GPIO clock
	RCC_AHB1PeriphClockCmd( gp->clk, ENABLE );
	
	// Configure Button pin as input
	GPIO_Init( (GPIO_TypeDef*)gp->port, p );
	
	// Configure external interrupt source
	if( buttonMode == eBUTTON_MODE_EXTI )
	{
		if( extEis ) {
			peis = extEis;
		} else {
			// Задать структуру конфирурации порта, если ее не предоставили как параметр extEis
			eis.EXTI_Mode = EXTI_Mode_Interrupt;
			eis.EXTI_Trigger = EXTI_Trigger_Rising;
			peis = &eis;
		}
		
		peis->EXTI_Line = gp->extiLine;
		peis->EXTI_LineCmd = ENABLE;
		EXTI_Init( peis );
				
		// Enable SYSCFG clock and connect button's EXTI Line to pin
		RCC_APB2PeriphClockCmd( RCC_APB2Periph_SYSCFG, ENABLE );
		SYSCFG_EXTILineConfig( gp->extiPortSrc, gp->extiPinSrc );
		
		// Enable and set Button EXTI Interrupt to the lowest priority
		nvic.NVIC_IRQChannel = gp->irq;
		nvic.NVIC_IRQChannelPreemptionPriority = IS_NVIC_PREEMPTION_PRIORITY(intprio) ? intprio : 0xF;
		nvic.NVIC_IRQChannelSubPriority = 0;
		nvic.NVIC_IRQChannelCmd = ENABLE;
		NVIC_Init( &nvic ); 
	}
	return 0;
}

/** @} */


/*	┌———————————————————————————————————————————————————————————————————————┐
	│						Alternative functions							│  
	└———————————————————————————————————————————————————————————————————————┘ */
/** \addtogroup Alternative_functions  Alternative functions
	@{ */

/** 
 * @brief	Set GPIO to Alternative function. Does all the same as bsp_GpioAf,
 *			but allows to define GPIO_InitStructure with users params.
 * @param 	gp	uC's pin described as GPIO_t
 * @param 	af	Alternative function @ref GPIO_Alternat_function_selection_define
 * @param 	extgis Pointer to GPIO_InitTypeDef instance. ONLY GPIO_Speed, 
				GPIO_OType, GPIO_PuPd fields should be set, other does not metter.
				NULL means use default settings: GPIO_Speed_100MHz, GPIO_OType_PP, GPIO_PuPd_NOPULL.
 **/
void bsp_GpioAf_InitStruct( const GPIO_t* gp, uint8_t af, GPIO_InitTypeDef* extgis ) 
{
	GPIO_InitTypeDef gis, *p; // Структура регистров для конфигурации портов

	assert_amsg(IS_GPIO_AF(af));
	///\todo return -1;
	
	if( extgis ) {
		p = extgis;
		assert_amsg(IS_GPIO_SPEED( p->GPIO_Speed ));
		assert_amsg(IS_GPIO_OTYPE( p->GPIO_OType ));
		assert_amsg(IS_GPIO_PUPD(  p->GPIO_PuPd  ));
	} else {
		// Задать структуру конфирурации порта, если ее не предоставили как параметр extgis
		gis.GPIO_Speed = GPIO_Speed_100MHz; 
		gis.GPIO_OType = GPIO_OType_PP; 
		gis.GPIO_PuPd  = GPIO_PuPd_NOPULL;
		p = &gis;
	}
	
	// Пользователь может задавать только GPIO_Speed, GPIO_OType, GPIO_PuPd
	p->GPIO_Pin  = gp->pin;
	p->GPIO_Mode = GPIO_Mode_AF;	

	//  Port clock enable
	RCC_AHB1PeriphClockCmd(gp->clk, ENABLE);
 	// Инициализировать структуой порт
	GPIO_Init( (GPIO_TypeDef*)gp->port, p);
	GPIO_PinAFConfig( (GPIO_TypeDef*)gp->port, gp->pinsource, af );
}


/** 
 * Configures MCO pin. MCO1 (PA8) configured to out HSE, MCO2 (PC9) configured to out SysClk/4.
 * Main purposes are 1) look at internal clocks of uC and 2) synchronize several uCs
 * 
 * \param  gp	uC's pin described as GPIO_t
 * \retval -1 if input param is invalid or unsupported
 */
int bsp_MCO_Config( const GPIO_t *gp )
{	
	if( !(gp == PA8 || gp == PC9) ) 
		return -1;
	
	bsp_GpioAf( gp, GPIO_AF_MCO );

	if( gp == PA8 ){
		RCC_MCO1Config( RCC_MCO1Source_HSE, RCC_MCO2Div_1 );
	} else if( gp == PC9 ){
		RCC_MCO2Config( RCC_MCO2Source_SYSCLK, RCC_MCO2Div_5 );
	}
	
	return 0;
}

/** @} */


/*	┌———————————————————————————————————————————————————————————————————————┐
	│						COM ports & UART/USART							│  
	└———————————————————————————————————————————————————————————————————————┘ */
/** \addtogroup COM_ports_n_UART_USART  COM ports & UART/USART
	@{ */

/** 
 * @brief  Configures GPIO for COM port.
 * @param  com: Specifies the COM port to be configured.
 *   This parameter can be one of following parameters:    
 *     @arg COM_debug
 *     @arg COMS[1]
 * 	or may be more...
 * @retval None
 */
void bsp_COMInit( const COMPort_t* com )
{
	GPIO_InitTypeDef GPIO_InitStructure;
	
	//RCC_AHB1PeriphClockCmd( com->tx->clk | com->rx->clk, ENABLE );
	
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_UP;
	
	bsp_GpioAf_InitStruct( com->tx, com->af, &GPIO_InitStructure);
	bsp_GpioAf_InitStruct( com->rx, com->af, &GPIO_InitStructure);
}

/** @} */


/*	┌———————————————————————————————————————————————————————————————————————┐
	│						Analog GPIO functions							│  
	└———————————————————————————————————————————————————————————————————————┘ */
/** \addtogroup Analog_GPIO_functions  Analog GPIO functions
	@{ */

/**
 * Initialize GPIO as analog I/O.
 * @param gp	 uC's pin described as GPIO_t, e.g. PA0
 * @param extGis Pointer to GPIO_InitTypeDef instance. 
 *		ONLY GPIO_Speed, GPIO_OType, GPIO_PuPd fields should be set, other does not metter.
 *		NULL means use default settings: GPIO_Speed_100MHz, GPIO_OType_PP, GPIO_PuPd_NOPULL.
 */
void bsp_AnalogInitGeneral( const GPIO_t* gp, GPIO_InitTypeDef* extgis )
{
	GPIO_InitTypeDef gis, *p; // Структура регистров для конфигурации портов

	if( extgis ){
		p = extgis;
		assert_amsg(IS_GPIO_SPEED( p->GPIO_Speed ));
		assert_amsg(IS_GPIO_OTYPE( p->GPIO_OType ));
		assert_amsg(IS_GPIO_PUPD(  p->GPIO_PuPd  ));
	} else {
		// Задать структуру конфирурации порта, если ее не предоставили как параметр extgis
		gis.GPIO_Speed = GPIO_Speed_100MHz; 
		gis.GPIO_OType = GPIO_OType_PP; 
		gis.GPIO_PuPd  = GPIO_PuPd_NOPULL;
		p = &gis;
	}
	
	// Пользователь может задавать только GPIO_Speed, GPIO_OType, GPIO_PuPd
	p->GPIO_Pin  = gp->pin;
	p->GPIO_Mode = GPIO_Mode_AN;	

	// Port clock enable
	RCC_AHB1PeriphClockCmd(gp->clk, ENABLE);
 	// Инициализировать структуой порт
	GPIO_Init( (GPIO_TypeDef*)gp->port, p);
}


/*void bsp_Dac_init( void ){
	
}*/


/** @} */
