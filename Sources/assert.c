/**
 * 
 * TODO проапгрейдить свой код из "хитрого примера"
 */

#include "Sledge/assert.h"
#include "Sledge/bsp.h"
#include "Sledge/utils.h"
#include <stdio.h>



volatile const char *pcMessage;
volatile const char *pcFileName;
volatile uint32_t ulLineNumber;
//volatile const char *pcFuncName;

/**
  * @brief  Reports the name of the source file and the source line number
  * 		where the assert_param error has occurred.
  * 		Volatile variables pcFileName and ulLineNumber allows to know where was assertion via debugger.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  */
void assert_failed( const char* msg, const char* file, /*const*/ uint32_t line /*, const char* funcName*/)
{
	pcMessage = msg;
	pcFileName = file;
	ulLineNumber = line;
	//pcFuncName = funcName;
	
	//(void) pcFileName; 	/// To prevent compile warnings.
	
	/* User can add his own implementation to report the file name and line number,
	ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
	
	/** Прежде чем использовать bsp_LedOn(), bsp_ledinit() должна быть выполнена.
		Прежде чем использовать printf() comport должен быть инициализирован. */
	//bsp_LedOn(LED_Red);
	//printf( "\n" "Assert FAILED in %s:%d" NEWLINE, file, line /*, funcName*/ );
	printf("(A) %s FAILED in %s:%d"NEWLINE,  msg, file, line /*, funcName*/ );
	
	slg_abort();  //abort();
}


/**
 * Stop program execution
 */
void slg_abort(void)
{
	/** The BASEPRI register defines the minimum priority for exception processing. 
		When BASEPRI is set to a nonzero value, it prevents the activation of all 
		exceptions with the same or lower priority level as the BASEPRI value. */
	// Disable interrupts with priority lower than 0x5 (bigger by number).
	//__set_BASEPRI( 0x5 << __NVIC_PRIO_BITS );
	// FAULTMASK register: 1 = prevents the activation of all exceptions except for Non-Maskable Interrupt (NMI).
	//__set_FAULTMASK( 1 );
	// Priority Mask Register: 1 - prevents the activation of all exceptions with configurable priority.
	//__set_PRIMASK( 1 );
	__disable_irq();
	
	//bsp_LedOff(NULL)  // Off all leds
	bsp_LedOn(LED_Red);
	
	/**	Set i to 0 in the debugger to break out of this loop and
		return to the line that triggered the assert. */
	volatile int i = 1;
	while(i)
		__asm volatile( "NOP" );
	
	/// Or reboot after sometime. It will be better to use watchdog.
	//for( int i = INT_MAX; i!=0; i-- );
	//NVIC_SystemReset();
}



/**
 * Retarget
 */
void __aeabi_assert(const char *expr, const char *file, int line)
{
	assert_failed( expr, file, line );
} 

/**
 * Retarget stdlib abort() function.
 */
void abort(void)
{
	slg_abort();
}
