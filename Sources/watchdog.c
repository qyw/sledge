
///\todo WWDG
///\todo Проверить как оно работает в режиме отладки. Нужно ли переконфигурировать 


#include "Sledge/hal/watchdog.h"
//#include "Sledge.h"
#include "stm32f4xx_iwdg.h"


/**	
 * Настройка независимого сторожевого таймера IWDG
 * Мин время 125 мкс, макс 32,760 с.
 * \return	 0  OK
 * 			 1  Out of range. Corrected to min or max.
 * 			-2  
 * 			-3  
 * 			other negative value on error.
 * 			positive value - passed with warnings
 */
int watchdog_init( int timeout_us )
{
	int ret = 0;
	uint32_t reload;
	uint8_t prescaler = IWDG_Prescaler_4;
	
	// check timeout value
	if( timeout_us < 125 ){
		timeout_us = 125;
		ret = 1;
	}
	if( timeout_us > 32768000 ){
		timeout_us = 32768000;
		ret = 1;
	}
		
	//timeout = 31.25 * prescaler * (reload+1)
	reload = timeout_us / 31.25 / (4 << prescaler);
	while( reload > (0xfff) ){
		reload /= 2;
		prescaler ++;
	}
	
	/*	Enable write access to IWDG_PR and IWDG_RLR registers */
	IWDG_WriteAccessCmd(IWDG_WriteAccess_Enable);  //IWDG->KR = IWDG_WriteAccess_Enable;
	
	/*	Configure the IWDG prescaler using IWDG_SetPrescaler() function */
	IWDG_SetPrescaler( prescaler );  //IWDG->PR = IWDG_Prescaler_X;
	// 4 диапазон 125.. 127968.75
	// 8 диапазрн 250..1023750
	// 256 биапазон 8000..32760000
	
	/*	Configure the IWDG counter value using IWDG_SetReload() function.
		This value will be loaded in the IWDG counter each time the counter
		is reloaded, then the IWDG will start counting down from this value. */
	IWDG_SetReload( reload );  //IWDG->RLR = Reload;  // 0xFFF max
			
	/*  Start the IWDG using IWDG_Enable() function, when the IWDG is used
		in software mode (no need to enable the LSI, it will be enabled
		by hardware) */
	IWDG_Enable();  //IWDG->KR = KR_KEY_ENABLE;
			
	/*	Then the application program must reload the IWDG counter at regular
		intervals during normal operation to prevent an MCU reset, using
		IWDG_ReloadCounter() function. */
	IWDG_ReloadCounter();  //IWDG->KR = KR_KEY_RELOAD;
	
	
	return ret;
}


/**	
 * Отключение сторожевого таймера.
 */
void watchdog_stop( int timeout_us )
{
	///\todo //IWDG_Disable();
}


/**
 * Сброс сторожевого таймера
 */
void watchdog_reload(){
	IWDG_ReloadCounter();  //IWDG->KR = KR_KEY_RELOAD;
}
