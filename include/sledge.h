/**
 * Sledge - wide functionality library, suitable for embedded systems, 
 * mainly for ARM Cortex and more particularly for STM32 family
 * 
 * Copyright (c) 2015, Ivan Kuvaldin. All rights reserved.
 * 
 * Licensed under BSD or MIT. 
 * Please refer to Licence.BSD.txt or Licence.MIT.txt provided with these sources.
 * You may obtain a copy of the Licenses at
 *      http://opensource.org/licenses/bsd-license.php
 *      http://opensource.org/licenses/mit-license.php
 */


#ifndef __Sledge_H
#define __Sledge_H


#include "SledgeConfig.h"

#include "./Sledge/utils.h"
//#include "./Sledge/hal.h"
#include "./Sledge/hal/uid.h"
#include "./Sledge/hal/watchdog.h"
//#include "./Sledge/hal/rtc.h"
#include "./Sledge/bsp.h"
#include "Sledge/sledge_timers.h"

#include "./Sledge/colors.h"
#include "Sledge/c11n/c11n.h"
#include "Sledge/cli/cli.h"

//#include "Sledge/drv.h"
//#include "Sledge/drv/stm32/sledge_stm32_RNG.h"
//#include "Sledge/drv/stm32/sledge_stm32_timers.h"
#include "Sledge/drv/stm32/sledge_stm32_RTC.h"
//#include "Sledge/drv/stm32/sledge_stm32_ADC.h"
//#include "Sledge/drv/stm32/sledge_stm32_DMA.h"
//#include "Sledge/drv/stm32/sledge_stm32_USART.h"

#include "./Sledge/assert.h"
#include "./Sledge/debug.h"
//#include "./Sledge/stacktrace.h"

//#include <stdint.h>
//#include <stdbool.h>
//#include <math.h>



#ifdef __cplusplus
 extern "C" {
#endif


__STATIC_INLINE int Sledge_init()
{
	//core_debug_manager_init();
	//drv_debug_usart_init();
	//bsp_LedInitAll();
	//drv_tim_init( &bsp_TIMER, bsp_TIMER_UPD_FREQUENCY, 12 );  // for Sledge_timers
	//drv_rtc_init();
	//watchdog_init( 10*1000*1000/*us*/ );  // Не перестаёт работать во время отладки. См. Ref.Man. DBG manager.
	return 0;
}


#ifdef __cplusplus
 }
#endif



#endif /*__Sledge_H*/
