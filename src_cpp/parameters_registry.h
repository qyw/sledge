

namespace Sledge {
namespace ParametersRegistry {

template<typename T>
class Parameter {
	std::string name;
	
	
public:
	void set(T && v);
	T& get();
	
}


} // namespace ParametersRegistry
} // namespace Sledge