/**
 * @file    segment_indicator.c
 * @author  Kyb <i.kybߛya.ru>
 * @version 1.1.0
 * @date    2015-mar-30
 * 
 * DO NOT INCLUDE THIS FILE IN THE PROJECT BUILD. It is included in segment_indicator.c
 */ 

#include <stdint.h>


#if si_SEGMENTS_COUNT<=8
 typedef uint8_t si_type;
#elif si_SEGMENTS_COUNT<=16
 typedef uint16_t si_type;
#else
 #error si_SEGMENTS_COUNT more than 16 is unsupported at the time
#endif

/// Определение сегментов для битового_поля-шрифта
#define A 	(1<<0)
#define B 	(1<<1)
#define C 	(1<<2)
#define D 	(1<<3)
#define E 	(1<<4)
#define F 	(1<<5)
#define G 	(1<<6)
#define H 	(1<<7)

#define _SPACE 	0
#define _QUOTE 	A
#define _COMMA	_POINT
#define _MINUS 	G
#define _POINT 	H
#define _0		A | B | C | D | E | F
#define _1		    B | C                           
#define _2		A | B     | D | E     | G
#define _3		A | B | C | D         | G
#define _4		    B | C         | F | G
#define _5		A     | C | D     | F | G
#define _6		A     | C | D | E | F | G
#define _7		A | B | C                                
#define _8		A | B | C | D | E | F | G
#define _9		A | B | C | D     | F | G
#define _A		A | B | C     | E | F | G
#define _B		_b
#define _C		A         | D | E | F  
#define _D		_d
#define _E		A         | D | E | F | G
#define _F		A             | E | F | G
#define _G		A     | C | D | E | F       
#define _H		    B | C     | E | F | G
#define _I		                E | F        
#define _J		    B | C | D | E        
#define _K		0
#define _L		            D | E | F
#define _M		0
#define _N		        C     | E     | G
#define _O		A | B | C | D | E | F
#define _P		A | B         | E | F | G
#define _Q		        C | D | E     | G
#define _R		                E     | G
#define _S		A     | C | D     | F | G
#define _T		            D | E | F | G
#define _U		    B | C | D | E | F
#define _V		        C | D | E        
#define _W		0
#define _X		0
#define _Y		    B         | E | F | G
#define _Z		A | B     | D | E     | G
#define _UNDERLINE          D                
#define _a 		_A
#define _b 		        C | D | E | F | G
#define _c		            D | E     | G
#define _d		    B | C | D | E     | G
#define _e		_E // A | B     | D | E | F | G
#define _h		        C     | E | F | G
#define _i		                E
#define _n		_N
#define _o		        C | D | E     | G
#define _r		_R
#define _t		_T
#define _u		        C | D | E        
#define _v		_V
#define _w		_W
#define _x		_X
#define _y		_Y
#define _z		_Z
#define _NONE 	A


/// Шрифт. Постараемся соответствовать ASCII
/// @note Для точки хоть и предусмотрено отдельное место, она должна объединяться в один символ вместе с предыдущим
/// @note Для 7-8-сегментника достаточно uint8_t, но ради универсальности экономить не будем
static const si_type si_FONT[] = {
	_SPACE, 0, _QUOTE, _NONE,  _NONE, _NONE, _NONE, _NONE,  _NONE, _NONE, _NONE, _NONE,  _COMMA, _MINUS, _POINT, 0,	
	_0, _1, _2, _3,    _4, _5, _6, _7,    _8, _9, 0, 0,      0, 0, 0, 0,
 	 0, _A, _B, _C,    _D, _E, _F, _G,    _H, _I, _J, _K,    _L, _M, _N, _O,
	_P, _Q, _R, _S,    _T, _U, _V, _W,    _X, _Y, _Z, 0,     0, 0, 0, _UNDERLINE,
	 0, _a, _b, _c,    _d, _e, _F, _G,    _h, _i, _J, _K,    _L, _M, _n, _o,
	_P, _Q, _r, _S,    _t, _u, _V, _W,    _X, _Y, _Z, 0,     0, 0, 0, _NONE,
};

#define siFONT_SIZE  sizeofarr(si_FONT)   //static const uint8_t siFONT_SIZE = sizeofarr(si_FONT);

//#define si_font(c) 	si_FONT[ (c) - ' ' ]
//#define si_font(c) 	( ' '<(c) && (c)-' '<siFONT_SIZE ? si_FONT[(c) - ' '] : _NONE )
//#define si_font(c) 	{ char c_=(c)-' '; 0<(c_) && (c_)<siFONT_SIZE ? si_FONT[(c_)] : _NONE }
inline static si_type si_font( char c ){
	unsigned char c_ = (c)-' ';
	return  (c_) < siFONT_SIZE 
			? si_FONT[(c_)] 
			: _NONE;
}
