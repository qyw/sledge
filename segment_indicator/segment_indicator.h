/**
 * Sledge - wide functionality library, suitable for embedded systems, 
 * mainly for ARM Cortex and more particularly for STM32 family
 * 
 * Copyright (c) 2015, Ivan Kuvaldin. All rights reserved.
 * 
 * Licensed under BSD or MIT. 
 * Please refer to Licence.BSD.txt or Licence.MIT.txt provided with these sources.
 * You may obtain a copy of the Licenses at
 *      http://opensource.org/licenses/bsd-license.php  
 *      http://opensource.org/licenses/mit-license.php  
 */
/**
 * @file    segment_indicator.h
 * @author  Kyb Sledgehammer <i.kybߛya,ru>
 * @version 1.2.2
 * @date    2015-mar-31
 * 
 * Индикация динамическая. Все сегменты соединены параллельно. Общий анод или катод.
 * Если общий анод si_COMMON_ANODE_CATHODE ставим 0, катод - 1.
 *
 * @Usage
 
	si_init();
	si_puts("02.3b");
	
	/// Если нет RTOS делаем задержку в основном потоке
	#if USE_RTOS
	#else
	while(1) si_refresh_all_with_delay( 3 );
	#endif
 
 * @Changes
 *  ver.1.2.2  date.2015-mar-31
 *   + поддержка вкл/выкл ОС
 *  ver.1.1.1  date.2015-mar-30
 * 
 * @TODO 
 *  оптимизировать дефайны
 *  возможно оптимизировать код если насильно привязиать группы пинов к одному порту
 *  si_SegDP_ALONG
 *  динамика посегментная а не посимвольная.
 *  привязать таймер
 *  оптимизировать delay
 *  вынести выбор RTOS во внешний модуль
 *  для совершенно офигенной совметимости между ОСРВ можно привязать к CMSIS-RTOS
 */
 
/** СХЕМА.
 * 
 * 1. Питание:
 * 1.1) 3В (Vcc)
 * 1.2) 5В (возможно 3В не хватает).
 * 		У STM32 многие ноги/порты допускают 5В (FT - 5 volt tolerant). Другие использовать опасно для МК. Подробнее -> см. даташит.
 * 		База(затвор) pnp(p)-транзистора(ов) подтягивается к +5V - закрыт. Порт МК настроен в режиме открытый коллектор. Никаких подтяжек!
 * 
 * 2. Общий анод или катод:
 * 2.1) Общий анод (удобнее)
 * 	1 pnp(p)-транзистор на каждый знак.
 *  1 пин на сегмент, без внешнего npn(n)-транзистора, в режиме открытого коллектора (ОК, open drain OD). '0' - открыт.
 * 		Ток 25мА макс. На весь камень не более 240мА для STM32F4, 150мА для STM32F100. Подробнее -> см. даташит.
 * 	[не обязательно] 1 npn(n)-транзистор на сегмент. Выход в режиме push-pull, подтяжка к '0' - закрыт.
 * 	
 * 2.2) Общий катод 
 * 	1 npn(n)-транзистор на знак. Если суммарный ток индикатора не превысит 25мА можно обойтись внутренним. Не рекомендуется.
 * 	1 пин на сегмент без внешнего pnp(p)-транзистора Если 3В@25мА достаточно для сегмента, подключаем напрямую к ногам МК.
 * 	[не обязательно] 1 pnp(p)-транзистор на сегмент.
 * 
 *
 * РЕКОМЕНДУЕТСЯ базы(затворы) всех pnp(p)-транзисторов подтягивать к +. В случае +5В - обязательно.
 *               базы(затворы) всех npn(n)-транзисторов подтягивать к 0.
 *
 * 3) Резисторы. 1 резистор на каждый сегмент + 1 на общий (необязательно)
 * 	@note Для FYS-15011 на точку(сегмент H или DP) резитор нужен в 4..5 разбольше, потому как там 1 диод и послабее, а не 2 как в основных сегментах.
 *
 * 	Если общий вывод - анод, то транзистор при нём PNP (или P-канальный) и эмитер(исток) присоединён к +.
 * 	Выводы сегментов (катоды) включаются через транзисторы обратной полярности на землю
 *	Если общий вывод - катод,то транзистор при нём NPN (или N-канальный) и коллектор(сток) присоединён к катоду.
 * 	Выводы сегментов (аноды) включаются через транзисторы обратной полярности на питание.
 *  
 * 	Не забывать про подтяжки для защиты от наводок.
 * 	
 
	Общий анод (ОА). Без внешних транзисторов "снизу". Резисторы и ток указаны для FYS15011. Этот индикатор не работает от 3V
	
                                         +5V(Vcc)
                                           ↑
                                      ┌────┤
                                     ┌─┐   │
                                  10K│ │   │
                                     └─┘   │ Э(И)
                               ┌────┐ │  │∕
                         Pxx———└────┘—┴——│   pnp(p)-транзистор
                                     Б(З)│\
                                           │ К(С)
                                       	   │
                                        ▬▬▬▬▬▬▬
                                       ▌   A   ▐
                                       ▌F     B▐
                                       ▌       ▐
                                        ▬▬▬▬▬▬▬  
                                       ▌   G   ▐
                                       ▌E    C ▐
                                       ▌   D   ▐  H
                                        ▬▬▬▬▬▬▬   ■
                                  	     ║        │
                                        ┌─┐       ┌─┐ 
                                      7x│ │10     │ │100
                                        └─┘       └─┘
                                         ║         │
                                   ~12мА ↓         ↓ ~6мА
                                         ╟─────────┘
                        8x Pxx           ║
                       ╔═════════════════╝
                       │        
                     │∕ 
                  ┌——│  Внутренний n-транзистор x8. 25мА макс.
                  │  │\
                 ┌─┐   │
        Pull-down│ │   │
                 └─┘   │
                  └────┴─────────────────┐
                                         ┴
                                        GND
		Снизу резисторы 100Ом, на сегмент H 400..500Ом, потом  на ноги МК обязательно 5V-tolerant(FT), настроенные как открытый коллектор.
		Без транзисторов через МК можно прокачивать ток общей суммой до 240мА, и не более 25 на пин.
 */
 
#ifndef __SEGMENT_INDICATOR_H
#define __SEGMENT_INDICATOR_H


#include "Sledge_impl/bsp_pcb.h"
#if defined(_SegmentGpios_) && defined(_CommonGpios_)

#define siUSE_INDICATORS

#include "si_config.h"
#include "Sledge/bsp.h"  //"Sledge.h"


#ifndef si_VOLTAGE
 #define si_VOLTAGE  3
#elif  !(si_VOLTAGE==3 || si_VOLTAGE==5)
 #error si_VOLTAGE must be 3 or 5
#endif

#if !( si_EXTERNAL_TRANSISTORS>=0 && si_EXTERNAL_TRANSISTORS<=2 )
 #error si_EXTERNAL_TRANSISTORS must be defined properly
#endif


#ifdef __cplusplus
 extern "C" {
#endif


/** Список пинов, управляющих общими анодами(катодами). Определяет количество индикаторов
 */
extern const GPIO_t* CommonGpios[/*si_INDICATORS_COUNT*/];
/** Количество сегментных индикаторов */
extern const uint8_t numOfSiCommonGpios;
#define si_INDICATORS_COUNT numOfSiCommonGpios

/** Список ножек, управляющих сегментами. Определяет количество сегментов
 * 	7 в обычном, 8 с точкой, 14 с Ж внутри, 16 + по 2 сегмента сверху и снизу
 */
extern const GPIO_t* SegmentGpios[/*si_SEGMENTS_COUNT*/];
/** Количество сегментов */
extern const uint8_t numOfSiSegmentGpios;


/**
 * Инициализация портов и создание задачи RTOS, если она есть. 
 * Будет отображаться знак '-' на всех индикаторах.
 */
void si_init(void);

/**
 * Инициализация портов. Включена в si)init()
 */
void si_periphInit( void );

/**
 * Задача для RTOS. Запускает в безконечном цикле si_refresh_all_with_delay()
 */
void si_task(void *opts);

/**
 * Put string to segment indicator
 * @param str
 * @return number of symbols putted
 */
int si_puts( const char *str );
 
/**
 * Обновить показания на одном индикаторе
 */
void si_refresh( uint8_t/*size_t*/ ind );

/**
 * Обновить все индикаторы с задержкой.
 * Эту функцию можно запускать в безконечном цикле
 */
void si_refresh_all_with_delay( uint32_t ms );


#else 

#define si_init()
#define si_puts(str)

#endif // defined(_SegmentGpios_) || defined(_CommonGpios_)



#ifdef __cplusplus
}
#endif

#endif /*__SEGMENT_INDICATOR_H */
