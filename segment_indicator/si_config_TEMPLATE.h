/**
 * @file    si_config.h
 * @author  Kyb <i.kybߛya.ru>
 * @version 1.1.0
 * @date    2015-mar-30
 * 
 * 
 */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __SI_CONFIG_H
#define __SI_CONFIG_H

/*#ifdef __cplusplus
extern "C" {
#endif*/

/** Общий анод 0 или общий катод 1
 */
#define si_COMMON_ANODE_CATHODE  0

/** Напряжение питатия индикатора 3 или 5 Вольт
 */
#define si_VOLTAGE 	5

/** Внешние транзисторы. 0-нет, 1-общий, 2-общий и сегменты, 3-общий и сегменты через ULN2003(MC1413, ...)
 */
#define si_EXTERNAL_TRANSISTORS  2 	//1

/**
 * При включении сегментов через транзисторы/ULN2003, а точку напрямую в МК.
 */
#define si_SegDP_ALONG 	0

/** Список пинов, управляющих общими анодами(катодами). Определяет количество индикаторов
 *  @note если работаем с 5V внимательно смотреть в даташит на предмет совместимости ноги с 5В.
 */
#ifndef si_COMMON_GPIOS
#define si_COMMON_GPIOS { PG2, PG3, PG4, PG5, PG6, }
#endif

/** Список ножек, управляющих сегментами. Определяет количество сегментов
 * 	7 в обычном, 8 с точкой, 14 с Ж внутри, 16 + по 2 сегмента сверху и снизу
 *  @note если работаем с 5V внимательно смотреть в даташит на предмет совместимости ноги с 5В.
 */
#ifndef si_SEGMENT_GPIOS
#define si_SEGMENT_GPIOS  { PF1, PF2, PF3, PF4, PF5, PF6, PF7, PF0, }
#endif

/** Частота или период обновления индикаторов.
 *  Можно установить одно из двух на вкус.  */
//#define si_REFRESH_FREQ_HZ 	  333
#define si_REFRESH_PERIOD_MS  1

/** Таймер, который будет генерировать перерывания, по которым 
 *  
 */
#define SegInd_Timer 			TIMS[12]
#define SegInd_TimerIRQHandler 	TIM8_BRK_TIM12_IRQHandler
#define si_configTIMER_IRQ_PRIO 13



#ifndef USE_RTOS
 #define USE_RTOS  1 //FreeRTOS
#endif

#if USE_RTOS == 0
 //#define delayMs(ms)  userDelayMs(ms);  
 #define delayMs(ms)  dummySwDelayMs(ms);  
#elif USE_RTOS == 1  ///FreeRTOS
 #include "FreeRTOS.h"
 #include "task.h"
 #define delayMs(ms)  vTaskDelay( ms / portTICK_PERIOD_MS );
//#elif USE_RTOS == 2
#else
 #error Only FreeRTOS implemented. You should add/change few rows of code.
#endif


/*#ifdef __cplusplus
}
#endif*/

#endif /*__SI_CONFIG_H */
