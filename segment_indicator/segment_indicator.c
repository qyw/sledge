/**
 * Sledge - wide functionality library, suitable for embedded systems, 
 * mainly for ARM Cortex and more particularly for STM32 family
 * 
 * Copyright (c) 2015, Ivan Kuvaldin. All rights reserved.
 * 
 * Licensed under BSD or MIT. 
 * Please refer to Licence.BSD.txt or Licence.MIT.txt provided with these sources.
 * You may obtain a copy of the Licenses at
 *      http://opensource.org/licenses/bsd-license.php  
 *      http://opensource.org/licenses/mit-license.php  
 */
/**
 * @file    segment_indicator.c
 * @author  Kyb Sledgehammer <i.kybߛya,ru>
 */

#include "./segment_indicator.h"
#ifdef siUSE_INDICATORS //#if defined(_SegmentGpios_) && defined(_CommonGpios_)

//#undef DEBUG_LEVEL
#define DEBUG_LEVEL  0
#include "Sledge.h"
//#include "Sledge/drv/stm32/sledge_stm32_timers.h"
#include "./si_font.c"


// Управление транзисторами
#if si_EXTERNAL_TRANSISTORS == 0		/// нет внешних
  
  #if si_VOLTAGE == 5
    #error Configuration is not supported. Контроллер не может подать 5В.
  #elif si_VOLTAGE == 3
	
	/// Все без подтяжек
    #define commonGPIO_PuPd 	GPIO_PuPd_NOPULL
    #define segmentGPIO_PuPd 	GPIO_PuPd_NOPULL
	
    #if si_COMMON_ANODE_CATHODE == 0	/// ОА
      /// common - push-pull, сегменты - открытый сток
      #define commonGPIO_OType  	GPIO_OType_PP
      #define segmentGPIO_OType 	GPIO_OType_OD
	  
      #define openCommon(gpio)  	bsp_OutHi(gpio)
      #define closeCommon(gpio) 	bsp_OutLow(gpio)
      #define openSegment(gpio) 	bsp_OutLow(gpio)
      #define closeSegment(gpio) 	bsp_OutHi(gpio)
    
	#elif si_COMMON_ANODE_CATHODE == 1  /// ОК
      /// common - open drain, сегменты - push-pull
      #define commonGPIO_OType  	GPIO_OType_OD
      #define segmentGPIO_OType 	GPIO_OType_PP
	  
      #define openCommon(gpio)  	bsp_Out0(gpio)
      #define closeCommon(gpio) 	bsp_Out1(gpio)
      #define openSegment(gpio) 	bsp_Out1(gpio)
      #define closeSegment(gpio) 	bsp_Out0(gpio)
    #endif
	
  #endif

#elif si_EXTERNAL_TRANSISTORS == 1		/// только общий
 
  #if si_COMMON_ANODE_CATHODE == 0	/// ОА
    
	#define segmentGPIO_OType 	GPIO_OType_OD
    #define segmentGPIO_PuPd 	GPIO_PuPd_NOPULL
	
	#define openCommon(gpio)  	bsp_Out0(gpio)
    #define closeCommon(gpio) 	bsp_Out1(gpio)
    #define openSegment(gpio) 	bsp_Out0(gpio)
    #define closeSegment(gpio) 	bsp_Out1(gpio)
	  
	#if si_VOLTAGE == 5
	  /// все порты - открытый сток. Базу(затвор) общего транзистора обязательно подлянуть к 5В.
	  #define commonGPIO_OType  	GPIO_OType_OD
      #define commonGPIO_PuPd   	GPIO_PuPd_NOPULL
	  
    #elif si_VOLTAGE == 3
      /// common - push-pull (можно открытый сток), желательно подтяжку; сегменты - открытый сток
	  #define commonGPIO_OType  	GPIO_OType_PP
      #define commonGPIO_PuPd   	GPIO_PuPd_UP
    #endif
  
  #elif si_COMMON_ANODE_CATHODE == 1  /// ОК
    #if si_VOLTAGE == 5
      #error Configuration is not supported. Контроллер не может подать 5В.
    #elif si_VOLTAGE == 3
      /// common - push-pull, подтянут к 0, сегменты - push-pull
	  #define commonGPIO_OType  	GPIO_OType_PP
      #define commonGPIO_PuPd   	GPIO_PuPd_DOWN
	  #define segmentGPIO_OType 	GPIO_OType_PP
      #define segmentGPIO_PuPd  	GPIO_PuPd_NOPULL
	  
      #define openCommon(gpio)  	bsp_Out1(gpio)
      #define closeCommon(gpio) 	bsp_Out0(gpio)
      #define openSegment(gpio) 	bsp_Out1(gpio)
      #define closeSegment(gpio) 	bsp_Out0(gpio)
    #endif
  #endif
  
#elif si_EXTERNAL_TRANSISTORS == 2 	/// транзитор общий и сегменты

  #if si_COMMON_ANODE_CATHODE == 0	/// ОА
    
	/// Сегменты в любом случае управляются через транзисторы.
	#define segmentGPIO_OType 	GPIO_OType_PP
    #define segmentGPIO_PuPd 	GPIO_PuPd_DOWN
	#if si_SegDP_ALONG
	 #define segmentDP_GPIO_OType 	GPIO_OType_OD
     #define segmentDP_GPIO_PuPd 	GPIO_PuPd_NOPULL
	 #define openSegmentDP(gpio) 	bsp_Out0(gpio)
     #define closeSegmentDP(gpio) 	bsp_Out1(gpio)
	#endif
	
	#define openCommon(gpio)  	bsp_Out0(gpio)
    #define closeCommon(gpio) 	bsp_Out1(gpio)
    #define openSegment(gpio) 	bsp_Out1(gpio)
    #define closeSegment(gpio) 	bsp_Out0(gpio)
	  
	#if si_VOLTAGE == 5
	  /// все порты - открытый сток. Базу(затвор) общего транзистора обязательно подлянуть к 5В.
	  #define commonGPIO_OType  	GPIO_OType_OD
      #define commonGPIO_PuPd   	GPIO_PuPd_NOPULL
	  
    #elif si_VOLTAGE == 3
      /// common - push-pull (можно открытый сток), желательно подтяжку; сегменты - открытый сток
	  #define commonGPIO_OType  	GPIO_OType_PP
      #define commonGPIO_PuPd   	GPIO_PuPd_UP
    #endif
  
  #elif si_COMMON_ANODE_CATHODE == 1  /// ОК
    
	/// common управляет n(npn)-транзистором
	#define commonGPIO_OType  	GPIO_OType_PP
    #define commonGPIO_PuPd   	GPIO_PuPd_DOWN
	
    #define openCommon(gpio)  	bsp_Out1(gpio)
    #define closeCommon(gpio) 	bsp_Out0(gpio)
    #define openSegment(gpio) 	bsp_Out0(gpio)
    #define closeSegment(gpio) 	bsp_Out1(gpio)
    
	/// segment управляет p(pnp)-транзистором
	#if si_VOLTAGE == 5
      #define segmentGPIO_OType 	GPIO_OType_OD
      #define segmentGPIO_PuPd  	GPIO_PuPd_NOPULL
    #elif si_VOLTAGE == 3
      #define segmentGPIO_OType 	GPIO_OType_PP
      #define segmentGPIO_PuPd  	GPIO_PuPd_UP
    #endif
  #endif
  
#endif


#ifndef  delayMs
 #define delayMs(ms)  dummySwDelayMs(ms);
 void dummySwDelayMs(uint32_t ms);
#endif

#ifndef  si_REFRESH_PERIOD_MS
 #define si_REFRESH_PERIOD_MS  (1000/si_REFRESH_FREQ_HZ)
#endif


/** Список пинов, управляющих общими анодами(катодами)
 *  @note если работаем с 5V внимательно смотреть в даташит на предмет совместимости ноги с 5В.
 */
//#ifndef CommonGpios
//const GPIO_t* CommonGpios[/*si_INDICATORS_COUNT*/] = si_COMMON_GPIOS;
////#define si_INDICATORS_COUNT  sizeofarr(CommonGpios)
//const uint8_t si_INDICATORS_COUNT = sizeofarr(CommonGpios);
//#endif

/** Список пинов для управления сегментами
 *  @note если работаем с 5V внимательно смотреть в даташит на предмет совместимости ноги с 5В.
 */
//#ifndef SegmentGpios
//const GPIO_t* SegmentGpios[/*si_SEGMENTS_COUNT*/] = si_SEGMENT_GPIOS;
////#define si_SEGMENTS_COUNT  si_INDICATORS_COUNT(SegmentGpios)
//const uint8_t si_SEGMENTS_COUNT = sizeofarr(SegmentGpios);
//#endif

/// Буффер - массив индикаторов, содержащий битовые поля - символы
//static si_type si_buffer[si_INDICATORS_COUNT];
static si_type si_buffer[6];
/// Позиция каретки для putc(). 
static uint8_t si_carriage = 0;
/// Кэш строки, из которой образован буффер
//static char strCach[si_INDICATORS_COUNT*2+1];



/**
 * Put string to segment indicator
 * @param str
 * @return number of symbols was put
 */
int si_puts( const char *str )
{
	uint8_t/*size_t*/ i=0, j=0, wasput;
	//strlcpy( strCach, str, sizeofarr(strCach) );
	
	while( 	str[i] != '\0' 
			&& j < numOfSiCommonGpios )
	{
		// если символ ',' или '.' присоединить его к предыдущему
		if( str[i] == '.' || str[i] == ',' ){
			if(j==0) si_buffer[j]=0; 	// опустошить символ буффера, если точка первый символ в строке.
			si_buffer[j?j-1:j++] |= si_font('.');
		
		// просто добавить символ в буффер
		} else {
			si_buffer[j++] = si_font( str[i] );
		}
		i++;
	}
	wasput = j;
	
	// Проверка строки на точтку в конце, а si_buffer уже заполнен
	if( str[i] == '.' || str[i] == ',' ){
		si_buffer[j-1] |= si_font('.');
	}
	
	// опустошить остальные знаки
	for(; j < numOfSiCommonGpios; j++ )
		si_buffer[j]=0;
	
	return wasput;
}


/**
 * Put float number to segment indicator
 * @param float
 * @return number of symbols putted
 */
int si_putf( float f )
{
	
	return 0;
}


static int powerInt( int i, unsigned p )
{
	if(p==0) return 1;
	if(p==1) return i;
	if(p==2) return i*i;
	return i * powerInt( i, p-1 );
}


/**
 * Put integer number to segment indicator
 * @param integer
 * @return number of symbols putted
 */
int si_puti( int in )
{
	int/*uint8_t*/ i;
	
	for( i=numOfSiCommonGpios-1; i>=0; i-- ){
		si_buffer[numOfSiCommonGpios-i] = si_font( in / powerInt(10,i) + '0');
	}
	
	return i;
}


/**
 * Put char to Segment Indicator
 * @param c : char
 * @return number of symbols putted
 */
int si_putc( char c )
{
	si_buffer[si_carriage] = si_font( c	);
	return 1;
}


/**
 * Задача для RTOS
 */
void si_task(void *opts)
{
	//uint8_t/*size_t*/ i;
	
	while(1) 
		si_refresh_all_with_delay(si_REFRESH_PERIOD_MS);
		
		/*i=0;
		while( i < numOfSiCommonGpios ){
			si_refresh(i);
			vTaskDelay( 1 / portTICK_PERIOD_MS );
			i++;
		}
	}*/
}


/**
 * Эту функцию можно запускать в безконечном цикле
 */
void si_refresh_all_with_delay( uint32_t ms )
{
	/*uint8_t i=0;
	while( i < numOfSiCommonGpios ){
		si_refresh(i);
		delayMs(ms);
		i++;
	}*/
	for( uint8_t i=0; i < numOfSiCommonGpios; i++) {
		si_refresh(i);
		delayMs(ms);
	}
}


/**
 * Обновить показания на одном индикаторе и сделать его активным.
 */
void si_refresh( uint8_t/*size_t*/ ind )
{
	uint8_t/*size_t*/ i=0;
	
	/// Выключить все индикаторы (COMMON пины)
	for( i=0; i<numOfSiCommonGpios; i++) {
		closeCommon( CommonGpios[i] );
	}
	///\TODO выключить только текущий светящийся, потому как только один индикатор работает единовременно
	
	/// Включить, выключить сегменты, какие нужны
	for( i=0; i<numOfSiSegmentGpios; i++ ){
		if( si_buffer[ind] & BIT(i) ) 			//si_buffer[ind] & (1<<i) 
			openSegment(SegmentGpios[i]);       //		? openSegment(SegmentGpios[i]) 
		else                                    //		: closeSegment(SegmentGpios[i]);
			closeSegment(SegmentGpios[i]);
	}
  #if si_SegDP_ALONG
	if( si_buffer[ind] & BIT(7) )	//if( si_buffer[ind] & (1<<7) )
		openSegmentDP(SegmentGpios[7]);
	else
		closeSegmentDP(SegmentGpios[7]);
  #endif
	
	///Включить индикатор
	openCommon( CommonGpios[ind] );
}
 

/**
 * Инициализация портов
 */
void si_periphInit( void )
{
	uint8_t/*size_t*/ i=0;
	GPIO_InitTypeDef gis;
	
	gis.GPIO_Speed = GPIO_Speed_2MHz;
	
	/// Настройка портов, управляющих общими анодами/катодами индикаторов
	gis.GPIO_OType = commonGPIO_OType;
	gis.GPIO_PuPd = commonGPIO_PuPd;

	for( i=0; i<numOfSiCommonGpios; i++ ){
		bsp_GpioOut_InitStruct( CommonGpios[i], &gis );
		closeCommon( CommonGpios[i] );
	}
	
	/// Настройка портов, управляющих сегментами
	gis.GPIO_OType = segmentGPIO_OType;
	gis.GPIO_PuPd = segmentGPIO_PuPd;
	
	for( i=0; i<numOfSiSegmentGpios; i++ ){
		bsp_GpioOut_InitStruct( SegmentGpios[i], &gis );
		closeSegment( SegmentGpios[i] );
	}
	
	/// При включении сегментов через транзисторы/ULN2003, а точку напрямую в МК.
#if si_SegDP_ALONG
	gis.GPIO_OType = segmentDP_GPIO_OType;
	gis.GPIO_PuPd = segmentDP_GPIO_PuPd;
	bsp_GpioOut_InitStruct( SegmentGpios[7], &gis );
	closeSegmentDP( SegmentGpios[7] );
#endif
}


/**
 * Инициализация портов и создание задачи RTOS, если она есть.
 * Будет отображаться знак '-' на всех индикаторах.
 */
void si_init( void )
{
	si_periphInit();
	//si_puts("Init..."); //si_puts( "--------" ); 	///\TODO а если индикаторов больше? Это врядли.
	
 #ifdef SegInd_Timer
	// Инициализация таймера
	drv_tim_init( &SegInd_Timer, 500/*Hz*/, si_configTIMER_IRQ_PRIO/*13*/ );
 #else
  #if USE_RTOS == 0
	///\ToDo
  #elif USE_RTOS == 1
	/*errTask =*/ xTaskCreate( si_task, "si_task", configMINIMAL_STACK_SIZE * 1, 
			/*opts*/NULL, SI_TASK_PRIO, NULL);
  #else
   #error Only FreeRTOS implemented. You should add/change few rows of code.
  #endif
 #endif	/*SegInd_Timer*/
}


///\todo si_deinit
void si_deinit()
{}


#if defined(SegInd_Timer) && defined(SegInd_TimerIRQHandler)
void SegInd_TimerIRQHandler()
{
	static uint8_t ind=0;
	
	// Проверить прерывание
	if( drv_TIM_GetITStatus( SegInd_Timer.tim, TIM_IT_Update ) ){
		SegInd_Timer.tim->SR &= ~TIM_SR_UIF;	// Сбросить флаг прерывания
		si_refresh(ind);
		if( ++ind >= numOfSiCommonGpios )  
			ind = 0;
		//(++ind >= numOfSiCommonGpios) ? ind=0 : ind;
	}
}
#endif


#ifndef delayMs
#warning Using dummySwDelayMs() depricated! Only for demo.

extern uint32_t SystemCoreClock;

/** Функция задержка - заглушка. НЕ РЕКОМЕНДУЕТСЯ использовать.
 */
void dummySwDelayMs( uint32_t ms )
{
	uint32_t nopsMs = SystemCoreClock / 1000 /2;
	
	while( ms-- ){
		while( nopsMs-- ){
			__NOP();
		}
	}
}
#endif



#else 
#warning segment_indicator.c not compiled due to no pins defined for indicators.
#endif /*siUSE_INDICATORS*/ // defined(_SegmentGpios_) || defined(_CommonGpios_)
