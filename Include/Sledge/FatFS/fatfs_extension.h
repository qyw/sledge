/**
 * Sledge - wide functionality library, suitable for embedded systems, 
 * mainly for ARM Cortex and more particularly for STM32 family
 * 
 * Copyright (c) 2015, Ivan Kuvaldin. All rights reserved.
 * 
 * Licensed under BSD or MIT. 
 * Please refer to Licence.BSD.txt or Licence.MIT.txt provided with these sources.
 * You may obtain a copy of the Licenses at
 *      http://opensource.org/licenses/bsd-license.php
 *      http://opensource.org/licenses/mit-license.php
 */

/**
 * @file	fatfs_extension.h
 * @author	Kyb <i.kyb[2]ya,ru>
 * @version	1.0.0
 * @date	2015-feb-25
 * @brief	
 */ 

/** Define to prevent recursive inclusion -------------------------------------*/
#ifndef __FATFS_EXTENSION_H
#define __FATFS_EXTENSION_H

#ifdef __cplusplus
 extern "C" {
#endif

#include <stdint.h>
#include <stdbool.h>
//#include <time.h>
//#include "ff.h"
#ifndef _FATFS
 #error #include "ff.h" must appear before this header
#endif


/**
 * Conversations bentween STL broken-down time and FATFS timestamp field
 * struct tm counts years from 1900, FatFs from 1980. We may not convert date earlier than 1980.
 */ 
#define CONVERT_structTm2FatfsTime(tm,ftimedate)  \
								ftimedate = ((DWORD)( (tm).tm_year>=80 ? (tm).tm_year-80 : 0) << 25)  \
								| ((DWORD)( (tm).tm_mon + 1  ) << 21)  \
								| ((DWORD)( (tm).tm_mday     ) << 16)  \
								| ((DWORD)( (tm).tm_hour     ) << 11)  \
								| ((DWORD)( (tm).tm_min      ) << 5)   \
								| ((DWORD)( (tm).tm_sec      ) >> 1)
/**
 * Conversations bentween STL broken-down time and FATFS timestamp field
 */ 
#define CONVERT_structTm2FatfsTime_(tm) ( ((DWORD)( (tm).tm_year>=80 ? (tm).tm_year-80 : 0) << 25)  \
										| ((DWORD)( (tm).tm_mon + 1  ) << 21)  \
										| ((DWORD)( (tm).tm_mday     ) << 16)  \
										| ((DWORD)( (tm).tm_hour     ) << 11)  \
										| ((DWORD)( (tm).tm_min      ) << 5)   \
										| ((DWORD)( (tm).tm_sec      ) >> 1) )

/**
 * Conversations bentween STL broken-down time and FATFS timestamp field
 */ 
#define CONVERT_FatfsTime2structTm(ftimedate,tm)  \
							do{	(tm).tm_year = (((ftimedate) >> 25) & BIT_MASK(32-25))+80; \
								(tm).tm_mon  = ((ftimedate) >> 21) & BIT_MASK(25-21) -1; \
								(tm).tm_mday = ((ftimedate) >> 16) & BIT_MASK(21-16)   ; \
								(tm).tm_hour = ((ftimedate) >> 11) & BIT_MASK(16-11)   ; \
								(tm).tm_min  = ((ftimedate) >>  5) & BIT_MASK(11- 5)   ; \
								(tm).tm_sec  = ((ftimedate) <<  1) & BIT_MASK( 5- 1)   ; \
							}while(0)


#define f_umount(path, opt) 	f_mount(0, path, opt)				/* Umount a logical drive. by Kyb */

/// Array of FatFs objects. Index matches drive number.
extern FATFS fatFses[_VOLUMES];
//#define sdFatFs  fatFses[0];

FRESULT f_getDriveSize(char *drivename, uint32_t *total_sectors, uint32_t *free_sectors);

/**
 * Централизованное монтирование дисков
 */
int/*FRESULT*/ f_imount(
	FATFS** fs,			/* [OUT] Pointer to pointer the file system object. Returns the pointer to instance of FATFS object from fatFses array */
	const TCHAR* path,	/* [IN]  Logical drive to be mounted/unmounted. May be autoresolved from filename */
	const BYTE opt		/* [IN]  0:Do not mount (delayed mount), 1:Mount immediately */
);


bool f_isClosed( FIL *fil );

/** 
 * Если файл закрыт, то говорить не о чем
 */
inline static bool f_isEof(FIL *fil){
	return !f_isClosed(fil) && f_eof(fil);
}


#ifdef __cplusplus
}
#endif

#endif /* __FATFS_EXTENSION_H */
