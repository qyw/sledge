

#include "stm32f4xx_iwdg.h"

/**	
 * Настройка независимого сторожевого таймера IWDG
 * Мин время 125 мкс, макс 32,760 с.
 * \param	timeout_us  время 
 * \return	 0  OK
 * 			 1  Out of range. Corrected to min or max.
 * 			-2  
 * 			-3  
 * 			other negative value on error.
 * 			positive value - passed with warnings
 */
int watchdog_init( int timeout_us );


/**
 * Сброс сторожевого таймера
 */
void watchdog_reload();
//#define watchdog_reload() 	IWDG_ReloadCounter()
