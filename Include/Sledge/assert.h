/**
 *
 */
 
#ifndef __KYB_ASSERT_H
#define __KYB_ASSERT_H

#ifdef __cplusplus
extern "C" {
#endif


#include <stdint.h>
#include <stdio.h>
//#include "../misc/utils.h"


/** Конкатинация во время компиляции. Подробнее в интернете */
#define CONCAT(First, Second) 			CONCAT_HELPER(First, Second)
#define CONCAT_HELPER(First, Second) 	(First ## Second)


/**	Выдает ошибку во время компиляции, если @arg expr ложно. */
#define STATIC_ASSERT(expr) 	typedef char CONCAT(static_assert_failed_at_line_, __LINE__) [(expr) ? 1 : -1]


#ifdef USE_FULL_ASSERT
 /// If assertion failed Report about trouble and Stop program execution

// /// with user provided message to stdout
// #define assert_msg(msg,expr)	( (expr) ? (void)0 : ((void)printf("(A) \"%s\" FAILED in %s:%d"NEWLINE,  msg, __FILE__, __LINE__), abort()) )
// /// with message that contains failed expression to stdout
// #define assert_amsg(expr)		( (expr) ? (void)0 : ((void)printf("(A) \"%s\" FAILED. File %s: %d"NEWLINE,#expr, __FILE__, __LINE__), abort()) )
// /// without any message
// #define assert_silent(expr)	( (expr) ? (void)0 : abort() )
 
 /// with user provided message to stdout
 #define assert_msg(msg,expr)	( (expr) ? (void)0 : assert_failed(msg, __FILE__, __LINE__) )
 /// with message that contains failed expression to stdout
 #define assert_amsg(expr)		( (expr) ? (void)0 : assert_failed(#expr, __FILE__, __LINE__) )
 /// without any message
 #define assert_silent(expr)	( (expr) ? (void)0 : assert_failed("", __FILE__, __LINE__) )
 
 #define error(msg) 			assert_failed(msg, __FILE__, __LINE__)
 #define fatal_error(msg) 		assert_failed(msg, __FILE__, __LINE__)
 
 /**
   * @brief  Reports the name of the source file and the source line number
   * 		 where the assert_param error has occurred.
   * 		 Volatile variables pcFileName and ulLineNumber allows to know where was assertion via debugger.
   * @param  file: pointer to the source file name
   * @param  line: assert_param error line source number
   */
 void assert_failed( const char* msg, const char* file, uint32_t line /*, const char* funcName*/);
 
 /**
  *
  */
 //void fatal_error(char *msg);
 //void fatal_error_silent(void);
 
#else /* USE_FULL_ASSERT */

 #define assert_msg(msg,expr)	((void)0)
 #define assert_amsg(expr)	    ((void)0)
 #define assert_silent(expr)	((void)0)

#endif /* USE_FULL_ASSERT */


void slg_abort(void);

/** 
 * @brief	Called if assertion failed.
 * 			Also this function shoud be called, if any fatal fault occurs,
 * 			e.g. on hard fault or bus fault. 
 * 	@note	This function may also be included with <assert.h> from STL.
 */
//void abort(void);
/** 
 * Print message to STDOUT before abort().
 */
//#define abort_msg(msg)	{ printf("%s",msg); abort(); }
//#define abort_msg(msg)	{ puts(msg); abort(); }
#define abort_msg(msg)	assert_failed(msg, __FILE__, __LINE__)


#ifdef __cplusplus
}
#endif

#endif /*__KYB_ASSERT_H*/
