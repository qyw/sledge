

#ifndef sledge_timers_H
#define sledge_timers_H

//#include "Sledge/bsp.h"
#include "Sledge/drv/stm32/sledge_stm32_timers.h"


#if !defined(bsp_TIMER) || !defined(s_TIMER_ISR)
 #undef  bsp_TIMER
 #define bsp_TIMER     	TIMS[14]
 #undef  s_TIMER_ISR
 #define s_TIMER_ISR 	TIM8_TRG_COM_TIM14_IRQHandler
#endif

#ifndef bsp_TIMER_UPD_FREQUENCY
 #define bsp_TIMER_UPD_FREQUENCY  (uint32_t)(1*1000*1000)
#endif

#ifndef bsp_TIMER_ISR_PRIO
 #define bsp_TIMER_ISR_PRIO  12
#endif


#define s_timer_ms(ms)  ((ms)*bsp_TIMER_UPD_FREQUENCY/1000)
#define s_timer_us(us)  ((us)*bsp_TIMER_UPD_FREQUENCY/1000000)
#define s_timer_ns(ns)  ((ns)*bsp_TIMER_UPD_FREQUENCY/1000000000)

#define s_ms2ticks(ms)  ((ms)*bsp_TIMER_UPD_FREQUENCY/1000)
#define s_us2ticks(us)  ((us)*bsp_TIMER_UPD_FREQUENCY/1000000)
#define s_ns2ticks(ns)  ((ns)*bsp_TIMER_UPD_FREQUENCY/1000000000)


#ifdef __cplusplus
extern "C" {
#endif


typedef void(*Callback_func)(void*);
typedef struct SwTimerState {
	uint32_t duration;
	Callback_func callback;  /// \note Важно помнить, что эта функция будет вызвана из прерывания, и должна быть лаконичной, соответствовать всем правилам проектирования обработчиков прерываний.
	void* cbparams;
	//void cb(){ callback(cbparams); }	// Убрано ради совместимости с Си
} SwTimerState;


int s_timerbase_init( uint32_t frequency );
int s_schedule_timeout( uint32_t ticks, Callback_func cb, void *params, uint32_t times );
int s_schedule_timeout2( uint32_t repeats, 
		uint32_t ticks1, Callback_func cb1, void *params1, 
		uint32_t ticks2, Callback_func cb2, void *params2 );
int s_schedule_timeout3( uint32_t repeats, uint32_t statesN, /*struct State*/ ... );
		
#ifndef s_TIMER_ISR
void s_TIMER_ISR(void);
#endif

void s_swtimer_collect_garbage(void);
		
#if s_USE_TIMER_GARBAGE_COLLECTOR_TASK
/**
 * Задача по сбору отработвших программных таймеров.
 * Периодически или по семафору в зависимости от дефайна.
 */
static void s_swtimer_collect_garbage_task( void * repeatEveryTicks );
#endif



#ifdef __cplusplus
}
#endif


#endif /* sledge_timers_H */
