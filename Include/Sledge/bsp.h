/**
 * Sledge - wide functionality library, suitable for embedded systems, 
 * mainly for ARM Cortex and more particularly for STM32 family
 * 
 * Copyright (c) 2015, Ivan Kuvaldin. All rights reserved.
 * 
 * Licensed under BSD or MIT. 
 * Please refer to Licence.BSD.txt or Licence.MIT.txt provided with these sources.
 * You may obtain a copy of the Licenses at
 *      http://opensource.org/licenses/bsd-license.php
 *      http://opensource.org/licenses/mit-license.php
 */

/**
 * \file 	"Sledge/bsp.h"
 * \author 	Ivan "Kyb Sledgehammer" Kuvaldin <i.kyb[2]ya,ru>
 * \version	2.8.0
 * \date 	2015-Jun-5
 * \brief 	Board support package. This module is a part of Sledgehammer Library aka Sledge.
 * 
 * This file provides:
 * 		- set of firmware functions to manage LEDs, push-buttons and COM ports
 *		- low level initialization functions for SD card (on SDIO) and
 *			serial EEPROM (sEE) 
 * 
 * Available on EVK407I, STM32F4-Discovery and STM324xG-EVAL evaluation board(MB786) RevB 
 * from STMicroelectronics. Easy portable for any other board.
 * 
 * Restrictions:
 * 		Since FreeRTOS supports only preemption NVIC priorities, Sledge library too.
 * 
 * Функции инициализации исполняются 1 раз при запуске МК или крайне редко.
 * Посему их оптимизация особого смысла не имеет, сделано для макс удобства.
 * Функции, используемые во время выполнения, например переключение выхода, 
 * реализованы в виде define'ов или inline-функций.
 * 
 * \Changes
 *  All next changes logged in git repository.
 * 	ver.2.8.0  date.2015-Jun-5
 * 	 + bsp_pcb.h and bsp_pcb.c. Concrete board definitions now are in PCBs/pcb_<boardname>.h
 *   * overall upgrades
 *  ver.2.6.0  date.2015-Apr-30
 *   * overall upgrades
 * 	 * bsp_pcb sub-module upgraged and integrated
 *  ver.2.5.0  date.2015-Mar-31
 *   + Better documentation
 *  ver.2.3.0  date.2015-Jan-13
 *  ver.2.0.0  date.2014-August-23
 *
 *
 * \TODO
 * bsp_initialize
 * bsp_LedBlink(led, on_ms, period_ms, times), bsp_LedBlinkOnce(led, on_ms), bsp_LedBlinking(led, on_ms, period_ms), bsp_LedBlinkingOff(led)
 * различать входы и выходы которые могут включатся/выключаться логическим 0.
 */



#ifndef __BSP_H
#define __BSP_H 260

#ifdef __cplusplus
 extern "C" {
#endif


/** Allow working with stupid HAL driver
 */
#ifdef USE_HAL_DRIVER
 #define GPIO_Mode_OUT  	GPIO_Mode_OUTPUT
 #define GPIO_Mode_OUT_OD  	GPIO_MODE_OUTPUT_OD	 
 #define GPIO_Mode_OUT_PP  	GPIO_MODE_OUTPUT_PP
 
 //#define 
 
#endif


/// STD C lib
#include <stdbool.h>
#include <stdarg.h>
/// Sledge library includes
#include "Sledge/drv/stm32/sledge_stm32_gpio.h"
#include "Sledge/drv/stm32/sledge_stm32_timers.h"
#include "Sledge/drv/stm32/sledge_stm32_dma.h"
#include "Sledge/drv/stm32/sledge_stm32_usart.h"
#include "Sledge/colors.h"
#include "Sledge/assert.h"
/// User provided includes
#include "Sledge_impl/bsp_pcb.h"
/// STM32 includes
//#include "stm32f4xx.h"
#include "stm32f4xx_gpio.h"  //allready in sledge_stm32_gpio.h
#include "stm32f4xx_exti.h"
#include "stm32f4xx.h"


/** ┌———————————————————————————————————————————————————————————————————————┐
	│						Type definitions								│  
	└———————————————————————————————————————————————————————————————————————┘ */
/** Useful while configuring GPIO inputs (e.g. buttons) */
typedef enum {
	eBUTTON_MODE_GPIO = 0,
	eBUTTON_MODE_EXTI = 1,
	//eBUTTON_FORCE_BYTE = 0xff 
} ButtonMode_e;
//STATIC_ASSERT( sizeof(ButtonMode_e) == 1 ); 	/// Check the size of enum.

typedef enum {
	eNONINVERTED = false,
	eINVERTED = true
} Inverted_e;
//STATIC_ASSERT( sizeof(Inverted_e) == 1 ); 	/// Check the size of enum.

typedef struct Button_t {
	GPIO_t* gp;
	Inverted_e/*bool*/ inverted; 	/// false: Hi input level means button is pressed,
									/// true: Low input level is pressed
	ButtonMode_e mode;
} Button_t;

/** COM port control typedefs */
typedef struct COMPort_t {
	USART_TypeDef 		*usart;	//e.g. USART3
	const uint32_t 		clk;	//e.g. RCC_APB1Periph_USART3
	const IRQn_Type		irq;	//e.g. USART3_IRQn
	const GPIO_t		*tx;	//e.g. PD5
	const GPIO_t		*rx;	//e.g. PD6
	const uint8_t 		af;		//e.g. GPIO_AF_USART3
	const DMA_BSP_t		dmaBsp;	
	//const DMA_BSP_t		dmaTx;	
	//const DMA_BSP_t		dmaRx;	
	//void (*irqHandler)(void);	//pointer to USARTx_IRQHandler()
} COMPort_t;

typedef struct LedRGB_t {
	const GPIO_t* R;
	const GPIO_t* G;
	const GPIO_t* B;
	const uint8_t common; 	/// 1 is common anode(Vcc), 0 is common cathode(GND)
	const TIM_handler_t* timhandler;
	volatile uint32_t * R_CCR;	/// Указатели на регистры таймера,
	volatile uint32_t * G_CCR;	/// соответствующие каждому цвету.
	volatile uint32_t * B_CCR;	/// Задаются автоматически в функции bsp_LedRGB_Init()
} LedRGB_t;


/*	┌———————————————————————————————————————————————————————————————————————┐
	│				Lists of LEDs, buttons, COMs, etc.						│  
	└———————————————————————————————————————————————————————————————————————┘ */
/** Imported from bsp_pcb.c */
extern const GPIO_t* LEDS[];
extern const uint8_t numOfLEDS;

/*extern const GPIO_t * const LED_Green;
extern const GPIO_t * const LED_Orange;
extern const GPIO_t * const LED_Red;
extern const GPIO_t * const LED_Blue;*/


extern const GPIO_t* LEDS_PWM[];
extern const uint8_t numOfLEDS_PWM;

extern LedRGB_t *const LEDS_RGB[];
extern /*const*/ LedRGB_t _LEDS_RGB[];
extern const uint8_t numOfLEDS_RGB;

extern const GPIO_t* BUTTONS[];
extern const uint8_t numOfBUTTONS;

//extern const COMPort_t COMS[];
//extern const uint8_t numOfCOMS;
extern const COMPort_t COM_debug;



//#define NUMBER_OF_GPORTS  11 // up to K
//extern volatile uint16_t bsp_used_gpios[NUMBER_OF_GPORTS];



/*	┌———————————————————————————————————————————————————————————————————————┐
	│						General functions								│  
	└———————————————————————————————————————————————————————————————————————┘ */
//int bsp_init(void); 
__STATIC_INLINE int bsp_init(void){
	//s_timerbase_init();
	return 0;
}

///\todo Universal Peripheral Clock Swither, based on spicial structs
//#define bsp_PeriphClockCmd(periph, AHBx, cmd)		do{ RCC->APB1ENR = (cmd) ? RCC->APB1ENR |  (periph).rcc  \
																			 : RCC->APB1ENR & ~(periph).rcc; \
													}while(0)


/*	┌———————————————————————————————————————————————————————————————————————┐
	│						Outouts & LEDs control							│  
	└———————————————————————————————————————————————————————————————————————┘ */
/** \addtogroup Outouts_n_LEDs_control Outouts & LEDs control
	@{ */

/** 
 * @brief 	Configures uC's pin as output with default settings: GPIO_Speed_100MHz, GPIO_OType_PP, GPIO_PuPd_NOPULL.
 * @param 	out  uC's pin described as GPIO_t
 */
#define bsp_GpioOut_Init(out) 	bsp_GpioOut_InitStruct( (out), NULL )

/** 
 * Configures uC's pin as output. Does all the same as bsp_GpioOut_Init(),
 * but allows to define GPIO_InitStructure with users params.
 * @param      gp  uC's pin described as GPIO_t
 * @param *extGis  Pointer to GPIO_InitTypeDef instance. ONLY GPIO_Speed, 
 *		           GPIO_OType, GPIO_PuPd fields should be set, other does not metter.
 *                 NULL means use default settings: GPIO_Speed_100MHz, GPIO_OType_PP, GPIO_PuPd_NOPULL.
 * \return 	0 | error(negative) | warning(positive) code
 * 			0  OK
 * 		   -1  Invalid argument. Nothing to configure.
 */
int bsp_GpioOut_InitStruct( const GPIO_t* gp, GPIO_InitTypeDef *extGis );

/** 
 * Turns the output Hi, Low or toggle.
 */
__STATIC_INLINE void bsp_OutHi(const GPIO_t* out) //__attribute__((__always_inline))
{
	if(!out) return; //assert_amsg(out);
	out->port->BSRRL = out->pin;
}

__STATIC_INLINE void bsp_OutLow(const GPIO_t* out)
{
	if(!out) return; //assert_amsg(out);
	out->port->BSRRH = out->pin;
}

__STATIC_INLINE void bsp_OutToggle(const GPIO_t* out)
{
	if(!out) return; //assert_amsg(out);
	out->port->ODR ^= out->pin;
	//if( out->port->ODR & out->pin )
	//	bsp_OutLow(out);
	//else
	//	bsp_outHi(out);
}

/// Additional aliases
#define bsp_OutSet(out, state) 	( (state) ? bsp_OutHi(out) : bsp_OutLow(out) )
#define bsp_OutOn(out)  		bsp_OutHi(out)
#define bsp_OutOff(out) 		bsp_OutLow(out)
#define bsp_Out1(out)  			bsp_OutHi(out)
#define bsp_Out0(out) 			bsp_OutLow(out)
#define bsp_OutHigh(out)  		bsp_OutHi(out)


/**
 * Configure GPIO connected to led.
 * @param  led: Specifies the LED to be configured. 
 *   This parameter can be one of following parameters:
 *     @arg ledsGreen	aka	LEDS[0]
 *     @arg ledsOrange	aka	LEDS[1]
 *     @arg ledsRed		aka	LEDS[2]
 *     @arg ledsBlue	aka	LEDS[3]
 * @retval None
 */
//#define bsp_LedInit(gpio) 	bsp_GpioOut_InitStruct( gpio, {} )
__STATIC_INLINE void bsp_LedInit( const GPIO_t* led )
{
	GPIO_InitTypeDef gis = {
		/*pin*/0, GPIO_Mode_OUT, GPIO_Speed_2MHz, GPIO_OType_PP, GPIO_PuPd_NOPULL
	};
	if(!led) return; //assert_amsg( isLed(led) );
	bsp_GpioOut_InitStruct( led, &gis );
}

/** 
 * Initialize all LEDs defined in LEDS[]
 */
__STATIC_INLINE void bsp_LedInitAll(void)
{
	uint32_t i=0; 
	while( i < numOfLEDS )
		bsp_LedInit(LEDS[i++]);
}

/** 
 * Turn the LED on, off or toggle. 
 */
/// TODO добавить возможность включения светика если он подключен наоборот(катодом)
#define bsp_LedOn(led)		bsp_OutOn(led)
#define bsp_LedOff(led)		bsp_OutOff(led)
//#define bsp_LedOn(led)		led->inverted ? bsp_OutHi(led)  : bsp_OutLow(led)
//#define bsp_LedOff(led)		led->inverted ? bsp_OutLow(led) : bsp_OutHi(led)
#define bsp_LedToggle(led) 	bsp_OutToggle(led)

/*__INLINE*/ void bsp_LedPwmInit( const GPIO_t* led /*, timhandler*/ );
/*__INLINE*/ void bsp_LedPwmInitAll( void/*, timhandler*/ );
/*__INLINE*/ void bsp_LedPwmSetBrightness( const GPIO_t* led, const uint16_t brightness );
#define bsp_LedPwmOn( led ) 	bsp_LedPwmSetBrightness( led, 100 )
#define bsp_LedPwmOff( led ) 	bsp_LedPwmSetBrightness( led, 0 )

//void bsp_LedRGB_color( const GPIO_t* led, const uint16_t r, const uint16_t g, const uint16_t b );


/** 
 * Initialize the RGB Led with adjustable brightness
 * @Note you need to make sure led may be internally connected to TIM's channels.
 * @param led
 */
void bsp_LedRgbInit( /*const*/ LedRGB_t* led );

/**
 * Set the RGB LED color
 * @param r,g,b: Brightness from 0 to 255 
 */
void bsp_LedRGBSetColor( const LedRGB_t* led, const uint8_t r, const uint8_t g, const uint8_t b );

/**
 * Set the RGB LED color
 * @param 	led : pointer to LedRGB_t
 * @param 	rgb : color 
 */
void bsp_LedRGBSetRGB( const LedRGB_t* led, ColorRGB_t rgb);


/** \addtogroup LEDs_blinking_control  LEDs blinking control
	@{ */

/**
 * Blink with LED
 * \param 
 */
int bsp_LedBlink( const GPIO_t *led, unsigned on_ms, unsigned period_ms, unsigned times );

#define bsp_LedBlinkOnce( led, on_ms )  bsp_LedBlink( led, on_ms, 0, 1 )
//int bsp_LedBlinkOnce( GPIO_t *led, unsigned on_ms ){}

#define bsp_LedBlinking( led, on_ms, period_ms )  bsp_LedBlink( led, on_ms, period_ms, 0xffffffff )
//int bsp_LedBlinking( GPIO_t *led, unsigned on_ms, unsigned period_ms ){}

#define bsp_LedBlinkingOff( led )  bsp_LedBlink( led, 0, 0, 0 )
//int bsp_LedBlinkingOff( GPIO_t *led ){}


/** @} */
/** @} */



/*	┌———————————————————————————————————————————————————————————————————————┐
	│				Inputs, Buttons and joystick control					│  
	└———————————————————————————————————————————————————————————————————————┘ */
/** \addtogroup Inputs_Buttons_and_joystick_control Inputs, Buttons and joystick control
	@{ */

/**	
 * Configures GPIO as input and EXTI Line if mode setted.
 * @param *gp : Specifies the GPIO to be configured.
 * @param *extGis: External instance of GPIO_InitTypeDef.
 * 				   Only .GPIO_Speed and .GPIO_PuPd fields should be set, others will be changed.
 * 		  		   NULL-pointer to use default parameters: GPIO_Speed_100MHz, GPIO_PuPd_NOPULL.
 * @param mode: Specifies input mode.
 *   This parameter can be one of following parameters:   
 *     @arg eBUTTON_MODE_GPIO: Button will be used as simple IO 
 *     @arg eBUTTON_MODE_EXTI: Button will be connected to EXTI line with interrupt
 *          generation capability
 * @param *extEis: External instance of EXTI_InitTypeDef. 0-pointer to use default parameters:
 *  	  EXTI_Mode_Interrupt, EXTI_Trigger_Rising
 * @param intprio: Interrupt priority if eBUTTON_MODE_EXTI. 
 * 		  Only NVIC_PriorityGroup_4 supported - pre-emption priority levels
 * @retval 0 if OK, negative value on error.
 * 		   extGis and extEis fields will be set as they are sent to registers.
 */
int bsp_Input_InitGeneral( const GPIO_t* gp, 
	GPIO_InitTypeDef *gis, 
	ButtonMode_e buttonMode, 
	EXTI_InitTypeDef *eis, 
	uint8_t intprio
);

/**
 * Configure uC's pin as output. Does all the same as bsp_GpioOut_Init(),
 * but allows to define GPIO_InitStructure with users params.
 * \param *extGis  Pointer to GPIO_InitTypeDef instance. ONLY GPIO_Speed, 
 *		           GPIO_OType, GPIO_PuPd fields should be set, other does not metter.
 *                 NULL means use default settings: GPIO_Speed_100MHz, GPIO_OType_PP, GPIO_PuPd_NOPULL.
 * \param 	n  Number of GPIOs to be coonfigured
 * \param   gp  uC's pin described as GPIO_t*
 * \return 	0 | error(negative) | warning(positive) code
 * 			0 - OK
 * 			positive - number of pins which did not pass check for null-pointer and is-used
 * 			negative - ?TODO error
 */
int bsp_Input_InitStruct_va( GPIO_InitTypeDef *extGis, size_t n, /*GPIO_t* */ ... );

/**
 * Configures GPIO as input.
 * @param *gp : Specifies the GPIO to be configured.
 * @param *extGis: External instance of GPIO_InitTypeDef.
 * 				   Only .GPIO_Speed and .GPIO_PuPd fields should be set, others will be changed.
 * 		  		   NULL-pointer to use default parameters: GPIO_Speed_100MHz, GPIO_PuPd_NOPULL.
 * @retval 0 if OK, negative value on error.
 * 		   extGis and extEis fields will be set as they are sent to registers.
 */
#define bsp_Input_InitStruct( gp, gis ) 	bsp_Input_InitGeneral( (gp), (gis), eBUTTON_MODE_GPIO, 0, 0 )

/**
 * Configures GPIO as input.
 * @param *gp : Specifies the GPIO to be configured.
 * @retval 0 if OK, negative value on error.
 * 		   extGis and extEis fields will be set as they are sent to registers.
 */
#define bsp_Input_Init( gp ) 	bsp_Input_InitGeneral( (gp), 0, eBUTTON_MODE_GPIO, 0, 0 )

/**	
 * Configures button's GPIO and EXTI Line if needed.
 * @param  button: Specifies the button's GPIO to be configured.
 * @param  buttonMode: Specifies Button mode.
 *   This parameter can be one of following parameters:   
 *     @arg BUTTON_MODE_GPIO: Button will be used as simple IO 
 *     @arg BUTTON_MODE_EXTI: Button will be connected to EXTI line with interrupt
 *          generation capability
 * @retval None
 */
#define bsp_ButtonInit( button, buttonMode, intprio ) 	bsp_Input_InitGeneral( (button), 0, (buttonMode), 0, (intprio) )

/**	
 * Configures button's GPIO and EXTI Line if needed.
 * @param  button: Specifies the button's GPIO to be configured.
 * @param  buttonMode: Specifies Button mode.
 *   This parameter can be one of following parameters:   
 *     @arg BUTTON_MODE_GPIO: Button will be used as simple IO 
 *     @arg BUTTON_MODE_EXTI: Button will be connected to EXTI line with interrupt
 *          generation capability
 * @param *extEis: External instance of EXTI_InitTypeDef. 
 * 	 NULL-pointer to use default parameters: EXTI_Mode_Interrupt, EXTI_Trigger_Rising
 * @param intprio: Interrupt priority if eBUTTON_MODE_EXTI. 
 * 	 Only NVIC_PriorityGroup_4 supported - pre-emption priority levels
 * @retval 0 if OK, negative value on error.
 * 	 	   extGis and extEis fields will be set as they are sent to registers.
 */
#define bsp_ButtonInitStruct( button, buttonMode, extEis, intprio ) 	bsp_Input_InitGeneral( (button), 0, (buttonMode), (extEis), intprio )

/** 
 * Initialize all buttons defined in BUTTONS[]
 */
__STATIC_INLINE void bsp_ButtonInitAll( ButtonMode_e buttonMode, uint8_t intprio )
{
	uint32_t i=0; 
	while( i < numOfBUTTONS )
		bsp_ButtonInit(BUTTONS[i++], buttonMode, intprio);
}


/**
 * Returns the level on selected GPIO.
 * @param  *gp: Specifies the GPIO to be checked.
 * @retval true(1) if hi, false(0) if low.
 */
__STATIC_INLINE bool bsp_InputRead( const GPIO_t* gp) {
	return (gp->port->IDR & gp->pin) ? true : false;
}
#define bsp_ReadInput(gp) 	bsp_InputRead(gp)

/**
 * Returns state of the button.
 * @param  *button: Specifies the Button to be checked.
 * @retval true if pressed.
 */
__STATIC_INLINE bool bsp_ButtonGetState( const Button_t* button) {
	bool ret = bsp_InputRead(button->gp);
	return button->inverted ? ret : !ret;
}


/**
 * Returns true if button is pressed. Button shunts pin to GND.
 * @param  *button: Specifies the Button to be checked.
 * @retval true if pressed.
 */
__STATIC_INLINE bool bsp_ButtonIsPressed( const GPIO_t* button) {
	bool ret = bsp_InputRead(button);
	return !ret;
}

/** @} */


/*	┌———————————————————————————————————————————————————————————————————————┐
	│						Alternative functions							│  
	└———————————————————————————————————————————————————————————————————————┘ */
/** \addtogroup Alternative_functions Alternative functions
	@{ */
/**
 * Set GPIO to Alternative function with default parameters: GPIO_Speed_100MHz, GPIO_OType_PP, GPIO_PuPd_NOPULL.
 * @param 	gp				uC's pin described as GPIO_t
 * @param 	af				Alternative function @ref GPIO_Alternat_function_selection_define
 */
#define bsp_GpioAf(gp, af) 			bsp_GpioAf_InitStruct((gp), (af), 0)
#define bsp_GpioAf_Init(gp, af) 	bsp_GpioAf((gp), (af))

/** 
 * Set GPIO to Alternative function. Does all the same as bsp_GpioAf,
 * but allows to define GPIO_InitStructure with user's parameters.
 * @param 	gp				uC's pin described as GPIO_t
 * @param 	af				Alternative function @ref GPIO_Alternat_function_selection_define
 * @param 	extGpioInitSt  	Pointer to GPIO_InitTypeDef instance. 
 * 		ONLY GPIO_Speed, GPIO_OType, GPIO_PuPd fields should be set, other does not metter.
 *		NULL means use default settings: GPIO_Speed_100MHz, GPIO_OType_PP, GPIO_PuPd_NOPULL.
 */
void bsp_GpioAf_InitStruct( const GPIO_t* gp, uint8_t af, GPIO_InitTypeDef* GPIO_InitStructure );

/** 
 * Configures MCO pin. MCO1 (PA8) configured to out HSE, MCO2 (PC9) configured to out SysClk/4.
 * Main purposes are 1) look at internal clocks of uC and 2) synchronize several uCs
 * 
 * \param  gp	uC's pin described as GPIO_t
 * \retval -1 if input param is invalid or unsupported
 */
int bsp_MCO_Config( const GPIO_t *gp );

/** 
 * @brief  Configures GPIO for COM port.
 * @param  com: Specifies the COM port to be configured.
 *   This parameter can be one of following parameters:    
 *     @arg coms[SERIAL_DEBUG_COMx]
 *     @arg COM_debug
 *     @arg coms[1]
 * 	or may be more...
 * @retval None
 */
void bsp_COMInit( const COMPort_t* com );

/** @} */


/*	┌———————————————————————————————————————————————————————————————————————┐
	│						COM ports & UART/USART							│  
	└———————————————————————————————————————————————————————————————————————┘ */
/** \addtogroup COM_ports_n_UART_USART COM ports & UART/USART
	@{ */

/** @} */



/** @} */


/*	┌———————————————————————————————————————————————————————————————————————┐
	│						Analog GPIO functions							│  
	└———————————————————————————————————————————————————————————————————————┘ */
/** \addtogroup Analog_GPIO_functions Analog GPIO functions
	@{ */

/**
 * Initialize GPIO as analog I/O.
 * @param gp	 uC's pin described as GPIO_t, e.g. PA0
 * @param extGis Pointer to GPIO_InitTypeDef instance. 
 *		ONLY GPIO_Speed, GPIO_OType, GPIO_PuPd fields should be set, other does not metter.
 *		NULL means use default settings: GPIO_Speed_100MHz, GPIO_OType_PP, GPIO_PuPd_NOPULL.
 */
void bsp_AnalogInitGeneral( const GPIO_t* gp, GPIO_InitTypeDef* extGis );

/**
 * Initialize GPIO as analog I/O with default parameters: GPIO_Speed_100MHz, GPIO_OType_PP, GPIO_PuPd_NOPULL.
 * @param gp uC's pin described as GPIO_t, e.g. PA0
 */
#define bsp_AnalogInit( gp ) 	bsp_AnalogInitGeneral( (gp), 0 )

/** @} */


/** ┌———————————————————————————————————————————————————————————————————————┐
	│							Debug & Assert								│  
	└———————————————————————————————————————————————————————————————————————┘ */
/** \addtogroup Debug_n_Assert	Debug & Assert	
	@{ */

/**
 * Check if pin is in array. 
 * Recommended to use with assert_amsg().
 * This function is prototype for isButton(), isLed(), isLedPwm(), ...
 * @param 	pin		
 * @param 	array	
 * @param 	size	size of array
 * @return	true if pin is button, else false
 */
__STATIC_INLINE bool isPinInArray( const GPIO_t* pin, GPIO_t* array[], uint32_t/*size_t*/ size ){
	uint32_t/*size_t*/ i;
	for( i=0; i<size; i++ ) {
		if( (pin) == array[i] )
			return true;
	}
	return false;
}

#define isButton(pin) 	isPinInArray( (pin), buttons, numOfButtons )
#define isLed(pin) 		isPinInArray( (pin), LEDS, 	  numOfLeds )
#define isLedPwm(pin) 	isPinInArray( (pin), ledsPwm, numOfLEDS_PWM )

/** @} */


#ifdef __cplusplus
}
#endif

#endif /* __BSP_H */
