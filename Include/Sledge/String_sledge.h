/**
 * @file    String.h
 * \author 	Ivan "Kyb Sledgehammer" Kuvaldin <i.kyb[2]ya,ru>
 * @version V1.0.0
 * @date    2014-09-01
 * @brief   This file contains all the functions prototypes for the telnet.c 
 *          file.
 */ 

/** Define to prevent recursive inclusion -------------------------------------*/
#ifndef __KYBS_STRING_H
#define __KYBS_STRING_H

#ifdef __cplusplus
 extern "C" {
#endif



/** Includes ------------------------------------------------------------------*/
#include <stdint.h>
	 

/** Private define ------------------------------------------------------------*/
#define MAX_STRING_SIZE 	96

/** Private typedef -----------------------------------------------------------*/
typedef struct String {
	int length;
	char bytes[MAX_STRING_SIZE];
} String;



#ifdef __cplusplus
}
#endif

#endif /* __KYBS_STRING_H */
