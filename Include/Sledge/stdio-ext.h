/** 
 * @author 	������� (Kyb)
 * @version	1.0.0
 * @date 	18-Apr-2015
 * 
 */ 

#ifndef __sledge_stdio_ext_H
#define __sledge_stdio_ext_H	100


#include <stdio.h>


/**
 * Get size of the file.
 */
size_t fsize( FILE * __restrict f );


#endif // __sledge_stdio_ext_H
