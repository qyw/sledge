/** 
 * @file 	Sledge/colors/colors.h
 * @author 	Кувалда (Kyb)
 * @version	1.0.0
 * @date 	18-Apr-2015
 * 
 * 
 */ 

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __sledge_colors_H
#define __sledge_colors_H	100

#ifdef __cplusplus
 extern "C" {
#endif



#include <stdint.h>


typedef struct ColorRGB_t {
	uint8_t R,
			G,
			B;
} ColorRGB_t, RGB_t;

/// Финт ушами чтоб обеспечить совместимость с Web Color Reference
/*typedef union Color_t {
	uint32_t colorRef;
	struct {
		const uint8_t ALIGN; 	/// allways must be 0x0
		uint8_t R;
		uint8_t G;
		uint8_t B;
	} rgb;
} Color_t; /
/// Финт ушами чтоб обеспечить совместимость с Web Color Reference. 
/// TODO должно быть зависит от little-big-endian
/// Не очень-то удачный способ конверсации. Не универсальный
typedef union Color_t {
	uint32_t 	hex; //colorRef;
	ColorRGB_t 	rgb;
} Color_t;*/

typedef struct ColorHSV_t {
	uint16_t H; /* Hue 				 0..359 */
	uint8_t  S; /* Saturation 		 0..255 */
	uint8_t  V; /* Brightness(Value) 0..255 */
} ColorHSV_t, HSV_t;


/**
 * Собрать все цвета в ColorRef
 */
#define rgb2hex( r,g,b ) 	( (uint32_t)(uint8_t)r<<16 | (uint8_t)g<<8 | (uint8_t)b )
//#define rgb2hex( r,g,b ) 	( (uint32_t)r<<16 | g<<8 | b )
#define RGBtoHEX( r,g,b ) 	rgb2hex( r,g,b )

/**
 * @brief Преобразование цветовой модели RGB в HSV.
 * @param rgb : The instance of ColorRGB_t
 * @return ColorHSV_t
 * @literature https://ru.wikipedia.org/wiki/HSV
 */
ColorHSV_t rgb2hsv( ColorRGB_t rgb );
#define RGBtoHSV( rgb ) 	rgb2hsv( rgb )

/**
 * @brief Преобразование цветовой модели HSV, HSB (ТНЯ) в RGB
 * @param hsv : The instance of ColorHSV_t
 * @return RGB_t
 * @literature https://ru.wikipedia.org/wiki/HSV
 */
ColorRGB_t hsv2rgb( ColorHSV_t hsv );
#define HSVtoRGB( hsv ) 	hsv2rgb( hsv )



#ifdef __cplusplus
}
#endif

#endif /* __sledge_colors_H */
