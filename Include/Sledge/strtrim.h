/**
 * 
 * 
 * 
 */


#ifndef __STRTRIM_H
#define __STRTRIM_H

#ifdef __cplusplus
extern "C" {
#endif


//#define isspace(ch) 	(ch == ' ')
//#define isspace(ch) 	((ch == ' ') || (ch == '\r') || (ch == '\n'))
//#define isspace(c)           (c == ' ' || c == '\f' || c == '\n' || c == '\r' || c == '\t' || c == '\v')


// Note: This function returns a pointer to a substring of the original string.
// If the given string was allocated dynamically, the caller must not overwrite
// that pointer with the returned value, since the original pointer must be
// deallocated using the same allocator with which it was allocated.  The return
// value must NOT be deallocated using free() etc.
char *strtrim(char *str);
char *strLtrim(char *str);
char *strRtrim(char *str);


#ifdef __cplusplus
}
#endif

#endif /*__STRTRIM_H*/
