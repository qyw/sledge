/**
 * Sledge - wide functionality library, suitable for embedded systems, 
 * mainly for ARM Cortex and more particularly for STM32 family
 * 
 * Copyright (c) 2015, Ivan Kuvaldin. All rights reserved.
 * 
 * Licensed under BSD or MIT. 
 * Please refer to Licence.BSD.txt or Licence.MIT.txt provided with these sources.
 * You may obtain a copy of the Licenses at
 *      http://opensource.org/licenses/bsd-license.php  
 *      http://opensource.org/licenses/mit-license.php  
 */
/**
 * @file    debug.h
 * @author  Kyb <i.kybߛya.ru>
 * @version 1.1.0
 * @date    2015-mar-20
 *
 * @Usage 
 * 	@note Don't include this header file in other user's header files! Only in .c modules.
 * 
 * GLOBAL DEBUG LEVEL.
 * 	GLOBAL_DEBUG_LEVEL should be defined in project configuration. 0 is recommended for Release.
 * 	Default value is 7 (0b111) means all 3 debug levels enabled.
 * 	GLOBAL_DEBUG_LEVEL is used as mask for DEBUG_LEVEL. So if GLOBAL_DEBUG_LEVEL==0 no any debugf() will be compilied.
 * 
 * LOCAL DEBUG LEVEL.
 *	DEBUG_LEVEL is local debug level for current .c module.
 * 	Default value is GLOBAL_DEBUG_LEVEL.
 *	It is recommended to redefine DEBUG_LEVEL before including this file.
 *	
 *	Add the following code after all includes, 
 * 	and change 'mymodule' to target module name:
 
	~~~{.c}
	/// ==== DEBUG ====
	#include "debugConfig.h"			/// Contains DEBUG_LEVELs for modules	
	#undef DEBUG_LEVEL					/// Redefiniton of DEBUG_LEVEL
	#define DEBUG_LEVEL  mymodule_DEBUG_LEVEL
	#include "Sledge/debug.h"
	#include "Sledge/assert.h"
	~~~

 * Optionally you may add following rows after #include "debugConfig.h"
	~~~{.c}
	#ifndef mymodule_DEBUG_LEVEL		/// if have not defined yet, define it here
	 #define mymodule_DEBUG_LEVEL  (DEBUG_LEVEL_1 | DEBUG_LEVEL_2 | DEBUG_LEVEL_3)
	#endif
	~~~

 * Optionally the number instead of mymodule_DEBUG_LEVEL may be used. 
 * Don't forget DEBUG_LEVEL is bit-field.
	~~~{.c}
	#define DEBUG_LEVEL  3
	~~~

 * Sledge/utils.h also may be useful
	~~~{.c}
	#include "Sledge/utils.h"
	#define DEBUG_LEVEL  BIT(0) | BIT1
	~~~
	
 * # Behavior
 * 	The message type indicator is added before the string:
 *	(D)  debug					for debugf()
 *	(V)  verbose debug			for debugf2()
 *	(VV) very verbose debug		for debugf3()

 * # Changes
 *  ver.1.1.0 date.2015-mar-20
 *	  + GLOBAL_DEBUG_LEVEL support as bit-mask
 *	  + DEBUG_LEVEL is bit-field
 *	  +	local debug level support
 *	  + added usage manual for local debug level
 *	ver.1.0.0 date.2015-feb-20
 *		works fine
 * 	ver.0.1.0 date.2015-jan-20
 *		initial implementation
 */

#include "Sledge/utils.h"


#ifdef __DEBUG_H

 #warning debugf() functions REDEFINED! Reason: "Sledge/debug.h" has been already included.
 #undef __DEBUG_H
 #undef debugf
 #undef debugf2
 #undef debugf3

#endif /* __DEBUG_H */


#define __DEBUG_H	110

#include "debugConfig.h"

#define DEBUG_L1 	(1<<0)	//BIT(0)
#define DEBUG_L2 	(1<<1)	//BIT(1)
#define DEBUG_L3 	(1<<2)	//BIT(2)


#ifndef GLOBAL_DEBUG_LEVEL
#define GLOBAL_DEBUG_LEVEL  (DEBUG_L1 | DEBUG_L2 | DEBUG_L3)	//7
#endif

#ifndef DEBUG_LEVEL
#define DEBUG_LEVEL  (GLOBAL_DEBUG_LEVEL)
#endif

#if ((DEBUG_LEVEL) & (GLOBAL_DEBUG_LEVEL) & DEBUG_L1)
 #define debugf(...) \
		 printf("(D) "__VA_ARGS__)
#else
 #define debugf(...)
#endif

#if ((DEBUG_LEVEL) & (GLOBAL_DEBUG_LEVEL) & DEBUG_L2)
 #define debugf2(...) \
		 printf("(V) "__VA_ARGS__)
#else                         
 #define debugf2(...)
#endif

#if ((DEBUG_LEVEL) & (GLOBAL_DEBUG_LEVEL) & DEBUG_L3)
 #define debugf3(...) \
		 printf("(VV) "__VA_ARGS__)
#else
 #define debugf3(...)
#endif

