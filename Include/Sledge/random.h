/**
 * Sledge - wide functionality library, suitable for embedded systems, 
 * mainly for ARM Cortex and more particularly for STM32 family
 * 
 * Copyright (c) 2015, Ivan Kuvaldin. All rights reserved.
 * 
 * Licensed under BSD or MIT. 
 * Please refer to Licence.BSD.txt or Licence.MIT.txt provided with these sources.
 * You may obtain a copy of the Licenses at
 *      http://opensource.org/licenses/bsd-license.php
 *      http://opensource.org/licenses/mit-license.php
 */
/**
 * \file 	Sledge/misc/random.h
 * \author 	Ivan "Kyb Sledgehammer" Kuvaldin <i.kyb[2]ya,ru>
 * \brief	convenient API for STM32 random number generator
 */


/// Define to prevent recursive inclusion -------------------------------------
#ifndef __Sledge_random_H
#define __Sledge_random_H

#ifdef __cplusplus
extern "C" {
#endif

	
#include "../drv/stm32/sledge_stm32_RNG.h"


/** 
 * @brief 	40 periods of the PLL48CLK clock signal between two consecutive random numbers.
 * 			So the rate is 1.2MHz. And if your core frequency is 168MHz, the time between 
 *			two consecutive random numbers will equal 140 core pulses.
 *	 @note	USUALLY when we call this function the random value is ready.
 */
__STATIC_INLINE uint32_t random(void) {
	return drv_RNG_getNext();
}
	

#ifdef __cplusplus
}
#endif

#endif /*__Sledge_random_H */
