/**
 * Sledge - wide functionality library, suitable for embedded systems, 
 * mainly for ARM Cortex and more particularly for STM32 family
 * 
 * Copyright (c) 2015, Ivan Kuvaldin. All rights reserved.
 * 
 * Licensed under BSD or MIT. 
 * Please refer to Licence.BSD.txt or Licence.MIT.txt provided with these sources.
 * You may obtain a copy of the Licenses at
 *      http://opensource.org/licenses/bsd-license.php  
 *      http://opensource.org/licenses/mit-license.php  
 */

/**
 * \file 	"Sledge/bsp.h"
 * \author 	Ivan "Kyb Sledgehammer" Kuvaldin <i.kyb[2]ya,ru>
 * \link	Sledge.herokuapp
 * \version	0.1.0
 * \date 	2015-Mar-31
 * \license BSD 2-clause or MIT
 * \brief 	ADC library for STM32. This module is a part of `Sledge` Library.
 */

#ifndef Sledge_adc_H
#define Sledge_adc_H 010


#include "stm32f4xx_adc.h"
#include "Sledge/bsp.h"


typedef struct ADC_t {
	ADC_TypeDef* regs;
	uint32_t 	 rcc;
	
} ADC_t;

typedef struct Channel_t {
	ADC_t adc;
	GPIO_t gp;
	
} Channel_t;


/**
 * Enable|disable Vbat channel.
 */
__STATIC_INLINE void adc_Vbat(bool state) {
	if( state )
		ADC->CCR |= ADC_CCR_VBATE;
	else
		ADC->CCR &= ~ADC_CCR_VBATE;		
}

/**
 * Read data from ADC channel manually.
 */
__STATIC_INLINE uint16_t adc_Read( ADC_TypeDef* ADCx, uint8_t channel ) 
{
	ADC_RegularChannelConfig( ADCx, channel, 1, ADC_SampleTime_15Cycles );
	/// Start software conversion
	ADCx->CR2 |= (uint32_t)ADC_CR2_SWSTART;
	/// Wait till done
	while (!(ADCx->SR & ADC_SR_EOC));
	return ADCx->DR;
}

/**
 * 
 */
__STATIC_INLINE void adc_init_channel( Channel_t* ch ){
	//bsp_
}


#endif /* Sledge_adc_H */
