/**
 * \file    Sledge/drv/stm32/stm32_rtc_sledge.h
 * \author 	Ivan "Kyb Sledgehammer" Kuvaldin <i.kyb[2]ya,ru>
 * @version V1.0
 * @date    21-Aug-2014
 * @brief   Real-time clock. 
 */

#ifndef __MYRTC_H
#define __MYRTC_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <stdbool.h>
#include <time.h>
//#include "stm32f4xx_rtc.h"


#define secondsPast1970to2000   	946684800
#define TIME_FORMAT_STRING  		"%02d.%02d.%02d %02d:%02d:%02d"
#define TIME_FORMAT_structtm(tm) 	(tm)->tm_mday, (tm)->tm_mon+1, (tm)->tm_year+1900, \
									(tm)->tm_hour, (tm)->tm_min, (tm)->tm_sec


int drv_rtc_init(void);


uint32_t drv_rtc_get(bool waitForSyncro);
uint32_t drv_rtc_get_sec(bool waitForSyncro);

/**
 * Always < 1000000
 * \note SS can be larger than PREDIV_S only after a shift operation. In that case, the correct
 * 		 time/date is one second less than as indicated by RTC_TR/RTC_DR
 * \return 	microseconds
 *  		-1 on error
 */
int32_t drv_rtc_get_us(bool waitForSyncro);

/**
 * \param *ptm poiter to struct tm instanse, which will contain broken-down time and date.
 *        Can be NULL, than internal struct tm used. MUST be non-null for reentrancy.
 * \return pointer to broken-down time.
 */
struct tm* drv_rtc_getToStructTM(bool waitForSyncro, struct tm *ptm);


int drv_rtc_set(uint32_t seconds);
int drv_rtc_set_sec(uint32_t seconds);
int drv_rtc_set_us(uint32_t seconds);

int drv_rtc_SetByStructTM(struct tm *tm);


/// ======================================
/// Fuck the ST STANDART PERIPHERAL DRIVER 
/// ======================================
/**
 * @brief  Enables or disables the RTC registers write protection.
 * @note   All the RTC registers are write protected except for RTC_ISR[13:8], 
 *         RTC_TAFCR and RTC_BKPxR.
 * @note   Writing a wrong key reactivates the write protection.
 * @note   The protection mechanism is not affected by system reset.  
 */
void drv_rtc_WriteEnable(void);
void drv_rtc_WriteDisable(void);

/**
 * \return  0  OK
 *         -1  error. too much data.
 */
int drv_rtc_WriteToBackupRegs( const void *data, uint8_t dwords );

/**
 *
 */
int drv_rtc_ReadFromBackupRegs( void *data, uint8_t dwords );



#ifdef __cplusplus
}
#endif
	
#endif /*__MYRTC_H */
