/**
 * Sledge - wide functionality library, suitable for embedded systems, 
 * mainly for ARM Cortex and more particularly for STM32 family
 * 
 * Copyright (c) 2015, Ivan Kuvaldin. All rights reserved.
 * 
 * Licensed under BSD or MIT. 
 * Please refer to Licence.BSD.txt or Licence.MIT.txt provided with these sources.
 * You may obtain a copy of the Licenses at
 *      http://opensource.org/licenses/bsd-license.php
 *      http://opensource.org/licenses/mit-license.php
 */
 
/**
 * \file	Sledge/bsp/stm32_gpio_new.h
 * \author 	Ivan "Kyb Sledgehammer" Kuvaldin <i.kyb[2]ya,ru>
 * \version	1.0.1
 * \date	2015-feb-18
 * \brief	This file contains definitions for GPIOs of STM32. New representation
 *			
 *			Новое представление	и способ работы с GPIO. Константы, дефайны, 
 *			адреса портов объединены в структуры. 
 * 			Маленький шаг в сторону ООП. Удобнее.
 *			Данная реализация поглощает больше flash-памяти.
 *
 * \Changes
 *	ver.1.0.1, date.2015-feb-18
 *	 *	#define _Pxx_ moved to .c file.
 * 	ver.1.0.0, date.2014-aug-23
 *	 + 	Initial implementation.
 *
 * \ToDo
 *	+ Добавить #ifdef <модель> для разных серий котроллеров. Исключить порты, 
 *	  которых нет в данной модели во время компиляции.
 */
  
#ifndef __stm32_USART_sledge_H
#define __stm32_USART_sledge_H

#ifdef __cplusplus
 extern "C" {
#endif


#include "stm32f4xx.h"
#if defined( USE_STDPERIPH_DRIVER )
 #include "stm32f4xx_dma.h"
#elif defined( USE_HAL_DRIVER)
 #include "stm32f4xx_hal_dma.h"
#endif
//#include <stdbool.h>


typedef struct DMA_BSP_Flags_t {
	uint32_t FEIF;		//FifoError;
	uint32_t DMEIF;		//DirectModeError;
	uint32_t TEIF;		//TransferError ;
	uint32_t HTIF;		//HalfTransfer ;
	uint32_t TCIF;		//TransferComplete;
} DMA_BSP_Flags_t, DMA_BSP_ITs_t;

typedef struct DMA_BSP_t {
	const DMA_TypeDef* 		dma;
	const uint32_t			rcc;
	const uint32_t			channel;
	DMA_Stream_TypeDef*		streamTx;
	DMA_Stream_TypeDef*		streamRx;
	DMA_BSP_Flags_t			flagsTx;
	DMA_BSP_Flags_t			flagsRx;
	DMA_BSP_ITs_t 			ITsTx;
	DMA_BSP_ITs_t 			ITsRx;
	IRQn_Type/*enum IRQn*/ 	irqTx;
	IRQn_Type/*enum IRQn*/	irqRx;
} DMA_BSP_t;



///\todo
typedef struct {
	const DMA_TypeDef 	*dma;
	const uint32_t		rcc;
} BSP_DMA_desc_t;
//STATIC_ASSERT

///\todo
typedef struct {
	const DMA_Stream_TypeDef *stream;
	const DMA_BSP_Flags_t 	flags;
	const DMA_BSP_ITs_t	  	ITs;
	const IRQn_Type/*enum IRQn*/   	irqn;
} BSP_DMA_Stream_t;

///\todo
typedef struct {
	const BSP_DMA_desc_t 	dma_desc;
	const uint32_t			channel;
	BSP_DMA_Stream_t		tx;
	BSP_DMA_Stream_t		rx;
} BSP_DMA_t_new;





/** Комбинации параметров DMA для разных USART. Их немного и удобнее один раз записать тут, 
 *	чем каждый раз переписывать код и обращаться в Ref.Man. 
 */
						/// +-dma-+---------rcc--------+----channel---+---streamTx--+---streamRx--+------FlagsTx------+------FlagsRx-------+
#define USART1_DMA_BSP		{ DMA2, RCC_AHB1Periph_DMA2, DMA_Channel_4, DMA2_Stream7, DMA2_Stream2, DMAy_STREAM7_FLAGS, DMAy_STREAM2_FLAGS }
#define USART1_DMA_BSP_2	{ DMA2, RCC_AHB1Periph_DMA2, DMA_Channel_4, DMA2_Stream7, DMA2_Stream5, DMAy_STREAM7_FLAGS, DMAy_STREAM5_FLAGS }

#define USART2_DMA_BSP		{ DMA1, RCC_AHB1Periph_DMA1, DMA_Channel_4, DMA1_Stream6, DMA1_Stream5, DMAy_STREAM6_FLAGS, DMAy_STREAM5_FLAGS }
#define USART3_DMA_BSP		{ DMA1, RCC_AHB1Periph_DMA1, DMA_Channel_4, DMA1_Stream3, DMA1_Stream1, DMAy_STREAM3_FLAGS, DMAy_STREAM1_FLAGS }
#define UART4_DMA_BSP		{ DMA1, RCC_AHB1Periph_DMA1, DMA_Channel_4, DMA1_Stream4, DMA1_Stream2, DMAy_STREAM4_FLAGS, DMAy_STREAM2_FLAGS }
#define UART5_DMA_BSP		{ DMA1, RCC_AHB1Periph_DMA1, DMA_Channel_4, DMA1_Stream7, DMA1_Stream0, DMAy_STREAM7_FLAGS, DMAy_STREAM0_FLAGS }

#define USART6_DMA_BSP 		{ DMA2, RCC_AHB1Periph_DMA2, DMA_Channel_4, DMA2_Stream6, DMA2_Stream1, DMAy_STREAM6_FLAGS, DMAy_STREAM1_FLAGS }
#define USART6_DMA_BSP_2 	{ DMA2, RCC_AHB1Periph_DMA2, DMA_Channel_4, DMA2_Stream6, DMA2_Stream2, DMAy_STREAM6_FLAGS, DMAy_STREAM2_FLAGS }
#define USART6_DMA_BSP_3 	{ DMA2, RCC_AHB1Periph_DMA2, DMA_Channel_4, DMA2_Stream7, DMA2_Stream1, DMAy_STREAM7_FLAGS, DMAy_STREAM1_FLAGS }
#define USART6_DMA_BSP_4 	{ DMA2, RCC_AHB1Periph_DMA2, DMA_Channel_4, DMA2_Stream7, DMA2_Stream2, DMAy_STREAM7_FLAGS, DMAy_STREAM2_FLAGS }


/** Комбинации параметров DMA для разных SPI. Их немного и удобнее один раз записать тут, 
 *	чем каждый раз переписывать код и обращаться в Ref.Man. 
 */
						 // +-dma-+---------rcc--------+----channel---+---streamTx--+---streamRx--+------FlagsTx------+------FlagsRx------+------ITsTx------+------ITsRx------+-------irqTx-------+------irqRx-------+
#define BSP_SPI1_DMA  		{ DMA2, RCC_AHB1Periph_DMA2, DMA_Channel_3, DMA2_Stream3, DMA2_Stream0, DMAy_STREAM3_FLAGS, DMAy_STREAM0_FLAGS, DMAy_STREAM3_ITs, DMAy_STREAM0_ITs, DMA2_Stream3_IRQn, DMA2_Stream0_IRQn }
#define BSP_SPI1_DMA_2		{ DMA2, RCC_AHB1Periph_DMA2, DMA_Channel_3, DMA2_Stream3, DMA2_Stream2, DMAy_STREAM3_FLAGS, DMAy_STREAM2_FLAGS, DMAy_STREAM3_ITs, DMAy_STREAM2_ITs, DMA2_Stream3_IRQn, DMA2_Stream2_IRQn }
#define BSP_SPI1_DMA_3		{ DMA2, RCC_AHB1Periph_DMA2, DMA_Channel_3, DMA2_Stream5, DMA2_Stream0, DMAy_STREAM5_FLAGS, DMAy_STREAM0_FLAGS, DMAy_STREAM5_ITs, DMAy_STREAM0_ITs, DMA2_Stream5_IRQn, DMA2_Stream0_IRQn }
#define BSP_SPI1_DMA_4		{ DMA2, RCC_AHB1Periph_DMA2, DMA_Channel_3, DMA2_Stream5, DMA2_Stream2, DMAy_STREAM5_FLAGS, DMAy_STREAM2_FLAGS, DMAy_STREAM5_ITs, DMAy_STREAM2_ITs, DMA2_Stream5_IRQn, DMA2_Stream2_IRQn }
                                                                                                                                           
#define BSP_SPI2_DMA		{ DMA1, RCC_AHB1Periph_DMA1, DMA_Channel_0, DMA1_Stream4, DMA1_Stream3, DMAy_STREAM4_FLAGS, DMAy_STREAM3_FLAGS, DMAy_STREAM4_ITs, DMAy_STREAM3_ITs, DMA1_Stream4_IRQn, DMA1_Stream3_IRQn }
                                                                                                                                           
#define BSP_SPI3_DMA  		{ DMA1, RCC_AHB1Periph_DMA1, DMA_Channel_0, DMA1_Stream5, DMA1_Stream0, DMAy_STREAM5_FLAGS, DMAy_STREAM0_FLAGS, DMAy_STREAM5_ITs, DMAy_STREAM0_ITs, DMA1_Stream5_IRQn, DMA1_Stream0_IRQn }
#define BSP_SPI3_DMA_2		{ DMA1, RCC_AHB1Periph_DMA1, DMA_Channel_0, DMA1_Stream5, DMA1_Stream2, DMAy_STREAM5_FLAGS, DMAy_STREAM2_FLAGS, DMAy_STREAM5_ITs, DMAy_STREAM2_ITs, DMA1_Stream5_IRQn, DMA1_Stream2_IRQn }
#define BSP_SPI3_DMA_3		{ DMA1, RCC_AHB1Periph_DMA1, DMA_Channel_0, DMA1_Stream7, DMA1_Stream0, DMAy_STREAM7_FLAGS, DMAy_STREAM0_FLAGS, DMAy_STREAM7_ITs, DMAy_STREAM0_ITs, DMA1_Stream7_IRQn, DMA1_Stream0_IRQn }
#define BSP_SPI3_DMA_4		{ DMA1, RCC_AHB1Periph_DMA1, DMA_Channel_0, DMA1_Stream7, DMA1_Stream2, DMAy_STREAM7_FLAGS, DMAy_STREAM2_FLAGS, DMAy_STREAM7_ITs, DMAy_STREAM2_ITs, DMA1_Stream7_IRQn, DMA1_Stream2_IRQn }

///\todo SPI4,5,6 for STM32F42xxx, STM32F43xxx


/** Все флаги DMA собраны в одну структуру, чтобы избежать жестких зависимостей в коде 
 *	и позволить себе менять только одно число при необходимости. Использутся, например, в USARTx_DMA_BSP. 
*	Маски флагов используются... \todo Где?
 */
						/// +---FifoError---+-DirectModeError-+-TransferError-+-HalfTransfer--+-TransferComplete-+
#define DMAy_STREAM0_FLAGS 	{ DMA_FLAG_FEIF0, DMA_FLAG_DMEIF0,  DMA_FLAG_TEIF0, DMA_FLAG_HTIF0, DMA_FLAG_TCIF0   }
#define DMAy_STREAM1_FLAGS 	{ DMA_FLAG_FEIF1, DMA_FLAG_DMEIF1,  DMA_FLAG_TEIF1, DMA_FLAG_HTIF1, DMA_FLAG_TCIF1   }
#define DMAy_STREAM2_FLAGS 	{ DMA_FLAG_FEIF2, DMA_FLAG_DMEIF2,  DMA_FLAG_TEIF2, DMA_FLAG_HTIF2, DMA_FLAG_TCIF2   }
#define DMAy_STREAM3_FLAGS 	{ DMA_FLAG_FEIF3, DMA_FLAG_DMEIF3,  DMA_FLAG_TEIF3, DMA_FLAG_HTIF3, DMA_FLAG_TCIF3   }
#define DMAy_STREAM4_FLAGS 	{ DMA_FLAG_FEIF4, DMA_FLAG_DMEIF4,  DMA_FLAG_TEIF4, DMA_FLAG_HTIF4, DMA_FLAG_TCIF4   }
#define DMAy_STREAM5_FLAGS 	{ DMA_FLAG_FEIF5, DMA_FLAG_DMEIF5,  DMA_FLAG_TEIF5, DMA_FLAG_HTIF5, DMA_FLAG_TCIF5   }
#define DMAy_STREAM6_FLAGS 	{ DMA_FLAG_FEIF6, DMA_FLAG_DMEIF6,  DMA_FLAG_TEIF6, DMA_FLAG_HTIF6, DMA_FLAG_TCIF6   }
#define DMAy_STREAM7_FLAGS 	{ DMA_FLAG_FEIF7, DMA_FLAG_DMEIF7,  DMA_FLAG_TEIF7, DMA_FLAG_HTIF7, DMA_FLAG_TCIF7   }


/** Все прерывания DMA собраны в одну структуру, чтобы избежать жестких зависимостей в коде 
 *	и позволить себе менять только одно число при необходимости. Использутся, например, в USARTx_DMA_BSP. 
*	Маски прерываний используются... \todo Где?
 */
						/// +--FifoError--+-DirectModeError-+-TransferError-+-HalfTransfer-+-TransferComplete-+
#define DMAy_STREAM0_ITs 	{ DMA_IT_FEIF0,    DMA_IT_DMEIF0,   DMA_IT_TEIF0,  DMA_IT_HTIF0,   DMA_IT_TCIF0   }
#define DMAy_STREAM1_ITs 	{ DMA_IT_FEIF1,    DMA_IT_DMEIF1,   DMA_IT_TEIF1,  DMA_IT_HTIF1,   DMA_IT_TCIF1   }
#define DMAy_STREAM2_ITs 	{ DMA_IT_FEIF2,    DMA_IT_DMEIF2,   DMA_IT_TEIF2,  DMA_IT_HTIF2,   DMA_IT_TCIF2   }
#define DMAy_STREAM3_ITs 	{ DMA_IT_FEIF3,    DMA_IT_DMEIF3,   DMA_IT_TEIF3,  DMA_IT_HTIF3,   DMA_IT_TCIF3   }
#define DMAy_STREAM4_ITs 	{ DMA_IT_FEIF4,    DMA_IT_DMEIF4,   DMA_IT_TEIF4,  DMA_IT_HTIF4,   DMA_IT_TCIF4   }
#define DMAy_STREAM5_ITs 	{ DMA_IT_FEIF5,    DMA_IT_DMEIF5,   DMA_IT_TEIF5,  DMA_IT_HTIF5,   DMA_IT_TCIF5   }
#define DMAy_STREAM6_ITs 	{ DMA_IT_FEIF6,    DMA_IT_DMEIF6,   DMA_IT_TEIF6,  DMA_IT_HTIF6,   DMA_IT_TCIF6   }
#define DMAy_STREAM7_ITs 	{ DMA_IT_FEIF7,    DMA_IT_DMEIF7,   DMA_IT_TEIF7,  DMA_IT_HTIF7,   DMA_IT_TCIF7   }


/* === FUNCTIONS and FUNCTION-like DEFINES ===
*/
#define DMAx_AHB1ClockCmd(dma, cmd)		do{ RCC->AHB1ENR = (cmd) ? RCC->AHB1ENR |=  (dma).rcc  \
																 : RCC->AHB1ENR &= ~(dma).rcc; \
										}while(0)
#define DMAx_AHB1ClockEnable(dma)		( RCC->AHB1ENR |=  (dma).rcc )
#define DMAx_AHB1ClockDisable(dma)		( RCC->AHB1ENR &= ~(dma).rcc )

#define IS_DMA_stream_enabled(stream) 	((bool)( (stream)->CR & DMA_SxCR_EN ))

///\todo добавить IS_RCC_DMA_ENABLED
#define IS_DMA_stream_configured(stream)  (bool)( ((stream)->CR != 0x0) || ((stream)->FCR != 0x21) )
//#define IS_DMA_stream_configured(stream)  ((bool)( DMA_Stream_belongs_to(stream) == DMA1 ? ((stream)->CR != 0x0) || ((stream)->FCR != 0x21) : \
												 ( DMA_Stream_belongs_to(stream) == DMA2 ? ((stream)->CR != 0x0) || ((stream)->FCR != 0x0 ) : false ) ))

/** Determine the DMA to which belongs the stream */
//#define DMA_Stream_belongs_to(stream)  	( (DMA1_Stream0<=(stream) && (stream)<=DMA1_Stream7) ? DMA1 : \
										 ((DMA2_Stream0<=(stream) && (stream)<=DMA2_Stream7) ? DMA2 : 0 ) )

#define DMA_Stream_belongs_to(stream)  	( (DMAy_Streamx < DMA2_Stream0) ? DMA1 : DMA2 )



#ifdef __cplusplus
}
#endif

#endif /* __stm32_USART_sledge_H */
