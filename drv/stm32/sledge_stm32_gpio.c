/**
 * Sledge - wide functionality library, suitable for embedded systems, 
 * mainly for ARM Cortex and more particularly for STM32 family
 * 
 * Copyright (c) 2015, Ivan Kuvaldin. All rights reserved.
 * 
 * Licensed under BSD or MIT. 
 * Please refer to Licence.BSD.txt or Licence.MIT.txt provided with these sources.
 * You may obtain a copy of the Licenses at
 *      http://opensource.org/licenses/bsd-license.php
 *      http://opensource.org/licenses/mit-license.php
 */

/**
 * @file	stm32_gpio_sledge.с
 * @author	Kyb
 * @version	1.0.1
 * @date	2015-feb-18
 * @brief	This file contains definitions for GPIOs of STM32. New representation
 *			
 *			Новое представление	и способ работы с GPIO. Константы, дефайны, 
 *			адреса портов объединены в структуры. 
 * 			Маленький шаг в сторону ООП. Удобнее.
 *			Данная реализация поглощает больше flash-памяти.
 *
 * @changes
 *	ver.1.0.1, date.2015-feb-18
 *	 *	#define _Pxx_ moved to .c file.
 * 	ver.1.0.0, date.2014-aug-23
 *	 + 	Initial implementation.
 *
 * @ToDo
 *	+ Добавить #ifdef <модель> для разных серий котроллеров. Исключить порты, 
 *	  которых нет в данной модели во время компиляции.
 *  + Нет необходимости в EXTI_LineX. Она совпадает с GPIO_Pin_x
 */

#include "./sledge_stm32_gpio.h"

//#include "stm32f4xx.h"
#include "stm32f4xx_rcc.h"
#include "stm32f4xx_gpio.h"
#include "stm32f4xx_exti.h"
#include "stm32f4xx_syscfg.h"  // for EXTI_PortSourceGPIOx


/** Defines of GPIOs pins */
#define _PA0_	{ RCC_AHB1Periph_GPIOA, /*(GPIO_Regs_t*)*/GPIOA, GPIO_Pin_0 , GPIO_PinSource0 , EXTI_Line0 , EXTI_PortSourceGPIOA, EXTI_PinSource0 , EXTI0_IRQn }
#define _PA1_	{ RCC_AHB1Periph_GPIOA, /*(GPIO_Regs_t*)*/GPIOA, GPIO_Pin_1 , GPIO_PinSource1 , EXTI_Line1 , EXTI_PortSourceGPIOA, EXTI_PinSource1 , EXTI1_IRQn }
#define _PA2_	{ RCC_AHB1Periph_GPIOA, /*(GPIO_Regs_t*)*/GPIOA, GPIO_Pin_2 , GPIO_PinSource2 , EXTI_Line2 , EXTI_PortSourceGPIOA, EXTI_PinSource2 , EXTI2_IRQn }
#define _PA3_	{ RCC_AHB1Periph_GPIOA, /*(GPIO_Regs_t*)*/GPIOA, GPIO_Pin_3 , GPIO_PinSource3 , EXTI_Line3 , EXTI_PortSourceGPIOA, EXTI_PinSource3 , EXTI3_IRQn }
#define _PA4_	{ RCC_AHB1Periph_GPIOA, /*(GPIO_Regs_t*)*/GPIOA, GPIO_Pin_4 , GPIO_PinSource4 , EXTI_Line4 , EXTI_PortSourceGPIOA, EXTI_PinSource4 , EXTI4_IRQn }
#define _PA5_	{ RCC_AHB1Periph_GPIOA, /*(GPIO_Regs_t*)*/GPIOA, GPIO_Pin_5 , GPIO_PinSource5 , EXTI_Line5 , EXTI_PortSourceGPIOA, EXTI_PinSource5 , EXTI9_5_IRQn }
#define _PA6_	{ RCC_AHB1Periph_GPIOA, /*(GPIO_Regs_t*)*/GPIOA, GPIO_Pin_6 , GPIO_PinSource6 , EXTI_Line6 , EXTI_PortSourceGPIOA, EXTI_PinSource6 , EXTI9_5_IRQn }
#define _PA7_	{ RCC_AHB1Periph_GPIOA, /*(GPIO_Regs_t*)*/GPIOA, GPIO_Pin_7 , GPIO_PinSource7 , EXTI_Line7 , EXTI_PortSourceGPIOA, EXTI_PinSource7 , EXTI9_5_IRQn }
#define _PA8_	{ RCC_AHB1Periph_GPIOA, /*(GPIO_Regs_t*)*/GPIOA, GPIO_Pin_8 , GPIO_PinSource8 , EXTI_Line8 , EXTI_PortSourceGPIOA, EXTI_PinSource8 , EXTI9_5_IRQn }
#define _PA9_	{ RCC_AHB1Periph_GPIOA, /*(GPIO_Regs_t*)*/GPIOA, GPIO_Pin_9 , GPIO_PinSource9 , EXTI_Line9 , EXTI_PortSourceGPIOA, EXTI_PinSource9 , EXTI9_5_IRQn }
#define _PA10_	{ RCC_AHB1Periph_GPIOA, /*(GPIO_Regs_t*)*/GPIOA, GPIO_Pin_10, GPIO_PinSource10, EXTI_Line10, EXTI_PortSourceGPIOA, EXTI_PinSource10, EXTI15_10_IRQn }
#define _PA11_	{ RCC_AHB1Periph_GPIOA, /*(GPIO_Regs_t*)*/GPIOA, GPIO_Pin_11, GPIO_PinSource11, EXTI_Line11, EXTI_PortSourceGPIOA, EXTI_PinSource11, EXTI15_10_IRQn }
#define _PA12_	{ RCC_AHB1Periph_GPIOA, /*(GPIO_Regs_t*)*/GPIOA, GPIO_Pin_12, GPIO_PinSource12, EXTI_Line12, EXTI_PortSourceGPIOA, EXTI_PinSource12, EXTI15_10_IRQn }
#define _PA13_	{ RCC_AHB1Periph_GPIOA, /*(GPIO_Regs_t*)*/GPIOA, GPIO_Pin_13, GPIO_PinSource13, EXTI_Line13, EXTI_PortSourceGPIOA, EXTI_PinSource13, EXTI15_10_IRQn }
#define _PA14_	{ RCC_AHB1Periph_GPIOA, /*(GPIO_Regs_t*)*/GPIOA, GPIO_Pin_14, GPIO_PinSource14, EXTI_Line14, EXTI_PortSourceGPIOA, EXTI_PinSource14, EXTI15_10_IRQn }
#define _PA15_	{ RCC_AHB1Periph_GPIOA, /*(GPIO_Regs_t*)*/GPIOA, GPIO_Pin_15, GPIO_PinSource15, EXTI_Line15, EXTI_PortSourceGPIOA, EXTI_PinSource15, EXTI15_10_IRQn }
                                        /*              */
#define _PB0_	{ RCC_AHB1Periph_GPIOB, /*(GPIO_Regs_t*)*/GPIOB, GPIO_Pin_0 , GPIO_PinSource0 , EXTI_Line0 , EXTI_PortSourceGPIOB, EXTI_PinSource0 , EXTI0_IRQn }
#define _PB1_	{ RCC_AHB1Periph_GPIOB, /*(GPIO_Regs_t*)*/GPIOB, GPIO_Pin_1 , GPIO_PinSource1 , EXTI_Line1 , EXTI_PortSourceGPIOB, EXTI_PinSource1 , EXTI1_IRQn }
#define _PB2_	{ RCC_AHB1Periph_GPIOB, /*(GPIO_Regs_t*)*/GPIOB, GPIO_Pin_2 , GPIO_PinSource2 , EXTI_Line2 , EXTI_PortSourceGPIOB, EXTI_PinSource2 , EXTI2_IRQn }
#define _PB3_	{ RCC_AHB1Periph_GPIOB, /*(GPIO_Regs_t*)*/GPIOB, GPIO_Pin_3 , GPIO_PinSource3 , EXTI_Line3 , EXTI_PortSourceGPIOB, EXTI_PinSource3 , EXTI3_IRQn }
#define _PB4_	{ RCC_AHB1Periph_GPIOB, /*(GPIO_Regs_t*)*/GPIOB, GPIO_Pin_4 , GPIO_PinSource4 , EXTI_Line4 , EXTI_PortSourceGPIOB, EXTI_PinSource4 , EXTI4_IRQn }
#define _PB5_	{ RCC_AHB1Periph_GPIOB, /*(GPIO_Regs_t*)*/GPIOB, GPIO_Pin_5 , GPIO_PinSource5 , EXTI_Line5 , EXTI_PortSourceGPIOB, EXTI_PinSource5 , EXTI9_5_IRQn }
#define _PB6_	{ RCC_AHB1Periph_GPIOB, /*(GPIO_Regs_t*)*/GPIOB, GPIO_Pin_6 , GPIO_PinSource6 , EXTI_Line6 , EXTI_PortSourceGPIOB, EXTI_PinSource6 , EXTI9_5_IRQn }
#define _PB7_	{ RCC_AHB1Periph_GPIOB, /*(GPIO_Regs_t*)*/GPIOB, GPIO_Pin_7 , GPIO_PinSource7 , EXTI_Line7 , EXTI_PortSourceGPIOB, EXTI_PinSource7 , EXTI9_5_IRQn }
#define _PB8_	{ RCC_AHB1Periph_GPIOB, /*(GPIO_Regs_t*)*/GPIOB, GPIO_Pin_8 , GPIO_PinSource8 , EXTI_Line8 , EXTI_PortSourceGPIOB, EXTI_PinSource8 , EXTI9_5_IRQn }
#define _PB9_	{ RCC_AHB1Periph_GPIOB, /*(GPIO_Regs_t*)*/GPIOB, GPIO_Pin_9 , GPIO_PinSource9 , EXTI_Line9 , EXTI_PortSourceGPIOB, EXTI_PinSource9 , EXTI9_5_IRQn }
#define _PB10_	{ RCC_AHB1Periph_GPIOB, /*(GPIO_Regs_t*)*/GPIOB, GPIO_Pin_10, GPIO_PinSource10, EXTI_Line10, EXTI_PortSourceGPIOB, EXTI_PinSource10, EXTI15_10_IRQn }
#define _PB11_	{ RCC_AHB1Periph_GPIOB, /*(GPIO_Regs_t*)*/GPIOB, GPIO_Pin_11, GPIO_PinSource11, EXTI_Line11, EXTI_PortSourceGPIOB, EXTI_PinSource11, EXTI15_10_IRQn }
#define _PB12_	{ RCC_AHB1Periph_GPIOB, /*(GPIO_Regs_t*)*/GPIOB, GPIO_Pin_12, GPIO_PinSource12, EXTI_Line12, EXTI_PortSourceGPIOB, EXTI_PinSource12, EXTI15_10_IRQn }
#define _PB13_	{ RCC_AHB1Periph_GPIOB, /*(GPIO_Regs_t*)*/GPIOB, GPIO_Pin_13, GPIO_PinSource13, EXTI_Line13, EXTI_PortSourceGPIOB, EXTI_PinSource13, EXTI15_10_IRQn }
#define _PB14_	{ RCC_AHB1Periph_GPIOB, /*(GPIO_Regs_t*)*/GPIOB, GPIO_Pin_14, GPIO_PinSource14, EXTI_Line14, EXTI_PortSourceGPIOB, EXTI_PinSource14, EXTI15_10_IRQn }
#define _PB15_	{ RCC_AHB1Periph_GPIOB, /*(GPIO_Regs_t*)*/GPIOB, GPIO_Pin_15, GPIO_PinSource15, EXTI_Line15, EXTI_PortSourceGPIOB, EXTI_PinSource15, EXTI15_10_IRQn }
                                        /*              */                                                        
#define _PC0_	{ RCC_AHB1Periph_GPIOC, /*(GPIO_Regs_t*)*/GPIOC, GPIO_Pin_0 , GPIO_PinSource0 , EXTI_Line0 , EXTI_PortSourceGPIOC, EXTI_PinSource0 , EXTI0_IRQn }
#define _PC1_	{ RCC_AHB1Periph_GPIOC, /*(GPIO_Regs_t*)*/GPIOC, GPIO_Pin_1 , GPIO_PinSource1 , EXTI_Line1 , EXTI_PortSourceGPIOC, EXTI_PinSource1 , EXTI1_IRQn }
#define _PC2_	{ RCC_AHB1Periph_GPIOC, /*(GPIO_Regs_t*)*/GPIOC, GPIO_Pin_2 , GPIO_PinSource2 , EXTI_Line2 , EXTI_PortSourceGPIOC, EXTI_PinSource2 , EXTI2_IRQn }
#define _PC3_	{ RCC_AHB1Periph_GPIOC, /*(GPIO_Regs_t*)*/GPIOC, GPIO_Pin_3 , GPIO_PinSource3 , EXTI_Line3 , EXTI_PortSourceGPIOC, EXTI_PinSource3 , EXTI3_IRQn }
#define _PC4_	{ RCC_AHB1Periph_GPIOC, /*(GPIO_Regs_t*)*/GPIOC, GPIO_Pin_4 , GPIO_PinSource4 , EXTI_Line4 , EXTI_PortSourceGPIOC, EXTI_PinSource4 , EXTI4_IRQn }
#define _PC5_	{ RCC_AHB1Periph_GPIOC, /*(GPIO_Regs_t*)*/GPIOC, GPIO_Pin_5 , GPIO_PinSource5 , EXTI_Line5 , EXTI_PortSourceGPIOC, EXTI_PinSource5 , EXTI9_5_IRQn }
#define _PC6_	{ RCC_AHB1Periph_GPIOC, /*(GPIO_Regs_t*)*/GPIOC, GPIO_Pin_6 , GPIO_PinSource6 , EXTI_Line6 , EXTI_PortSourceGPIOC, EXTI_PinSource6 , EXTI9_5_IRQn }
#define _PC7_	{ RCC_AHB1Periph_GPIOC, /*(GPIO_Regs_t*)*/GPIOC, GPIO_Pin_7 , GPIO_PinSource7 , EXTI_Line7 , EXTI_PortSourceGPIOC, EXTI_PinSource7 , EXTI9_5_IRQn }
#define _PC8_	{ RCC_AHB1Periph_GPIOC, /*(GPIO_Regs_t*)*/GPIOC, GPIO_Pin_8 , GPIO_PinSource8 , EXTI_Line8 , EXTI_PortSourceGPIOC, EXTI_PinSource8 , EXTI9_5_IRQn }
#define _PC9_	{ RCC_AHB1Periph_GPIOC, /*(GPIO_Regs_t*)*/GPIOC, GPIO_Pin_9 , GPIO_PinSource9 , EXTI_Line9 , EXTI_PortSourceGPIOC, EXTI_PinSource9 , EXTI9_5_IRQn }
#define _PC10_	{ RCC_AHB1Periph_GPIOC, /*(GPIO_Regs_t*)*/GPIOC, GPIO_Pin_10, GPIO_PinSource10, EXTI_Line10, EXTI_PortSourceGPIOC, EXTI_PinSource10, EXTI15_10_IRQn }
#define _PC11_	{ RCC_AHB1Periph_GPIOC, /*(GPIO_Regs_t*)*/GPIOC, GPIO_Pin_11, GPIO_PinSource11, EXTI_Line11, EXTI_PortSourceGPIOC, EXTI_PinSource11, EXTI15_10_IRQn }
#define _PC12_	{ RCC_AHB1Periph_GPIOC, /*(GPIO_Regs_t*)*/GPIOC, GPIO_Pin_12, GPIO_PinSource12, EXTI_Line12, EXTI_PortSourceGPIOC, EXTI_PinSource12, EXTI15_10_IRQn }
#define _PC13_	{ RCC_AHB1Periph_GPIOC, /*(GPIO_Regs_t*)*/GPIOC, GPIO_Pin_13, GPIO_PinSource13, EXTI_Line13, EXTI_PortSourceGPIOC, EXTI_PinSource13, EXTI15_10_IRQn }
#define _PC14_	{ RCC_AHB1Periph_GPIOC, /*(GPIO_Regs_t*)*/GPIOC, GPIO_Pin_14, GPIO_PinSource14, EXTI_Line14, EXTI_PortSourceGPIOC, EXTI_PinSource14, EXTI15_10_IRQn }
#define _PC15_	{ RCC_AHB1Periph_GPIOC, /*(GPIO_Regs_t*)*/GPIOC, GPIO_Pin_15, GPIO_PinSource15, EXTI_Line15, EXTI_PortSourceGPIOC, EXTI_PinSource15, EXTI15_10_IRQn }
                                        /*              */                                                        
#define _PD0_	{ RCC_AHB1Periph_GPIOD, /*(GPIO_Regs_t*)*/GPIOD, GPIO_Pin_0 , GPIO_PinSource0 , EXTI_Line0 , EXTI_PortSourceGPIOD, EXTI_PinSource0 , EXTI0_IRQn }
#define _PD1_	{ RCC_AHB1Periph_GPIOD, /*(GPIO_Regs_t*)*/GPIOD, GPIO_Pin_1 , GPIO_PinSource1 , EXTI_Line1 , EXTI_PortSourceGPIOD, EXTI_PinSource1 , EXTI1_IRQn }
#define _PD2_	{ RCC_AHB1Periph_GPIOD, /*(GPIO_Regs_t*)*/GPIOD, GPIO_Pin_2 , GPIO_PinSource2 , EXTI_Line2 , EXTI_PortSourceGPIOD, EXTI_PinSource2 , EXTI2_IRQn }
#define _PD3_	{ RCC_AHB1Periph_GPIOD, /*(GPIO_Regs_t*)*/GPIOD, GPIO_Pin_3 , GPIO_PinSource3 , EXTI_Line3 , EXTI_PortSourceGPIOD, EXTI_PinSource3 , EXTI3_IRQn }
#define _PD4_	{ RCC_AHB1Periph_GPIOD, /*(GPIO_Regs_t*)*/GPIOD, GPIO_Pin_4 , GPIO_PinSource4 , EXTI_Line4 , EXTI_PortSourceGPIOD, EXTI_PinSource4 , EXTI4_IRQn }
#define _PD5_	{ RCC_AHB1Periph_GPIOD, /*(GPIO_Regs_t*)*/GPIOD, GPIO_Pin_5 , GPIO_PinSource5 , EXTI_Line5 , EXTI_PortSourceGPIOD, EXTI_PinSource5 , EXTI9_5_IRQn }
#define _PD6_	{ RCC_AHB1Periph_GPIOD, /*(GPIO_Regs_t*)*/GPIOD, GPIO_Pin_6 , GPIO_PinSource6 , EXTI_Line6 , EXTI_PortSourceGPIOD, EXTI_PinSource6 , EXTI9_5_IRQn }
#define _PD7_	{ RCC_AHB1Periph_GPIOD, /*(GPIO_Regs_t*)*/GPIOD, GPIO_Pin_7 , GPIO_PinSource7 , EXTI_Line7 , EXTI_PortSourceGPIOD, EXTI_PinSource7 , EXTI9_5_IRQn }
#define _PD8_	{ RCC_AHB1Periph_GPIOD, /*(GPIO_Regs_t*)*/GPIOD, GPIO_Pin_8 , GPIO_PinSource8 , EXTI_Line8 , EXTI_PortSourceGPIOD, EXTI_PinSource8 , EXTI9_5_IRQn }
#define _PD9_	{ RCC_AHB1Periph_GPIOD, /*(GPIO_Regs_t*)*/GPIOD, GPIO_Pin_9 , GPIO_PinSource9 , EXTI_Line9 , EXTI_PortSourceGPIOD, EXTI_PinSource9 , EXTI9_5_IRQn }
#define _PD10_	{ RCC_AHB1Periph_GPIOD, /*(GPIO_Regs_t*)*/GPIOD, GPIO_Pin_10, GPIO_PinSource10, EXTI_Line10, EXTI_PortSourceGPIOD, EXTI_PinSource10, EXTI15_10_IRQn }
#define _PD11_	{ RCC_AHB1Periph_GPIOD, /*(GPIO_Regs_t*)*/GPIOD, GPIO_Pin_11, GPIO_PinSource11, EXTI_Line11, EXTI_PortSourceGPIOD, EXTI_PinSource11, EXTI15_10_IRQn }
#define _PD12_	{ RCC_AHB1Periph_GPIOD, /*(GPIO_Regs_t*)*/GPIOD, GPIO_Pin_12, GPIO_PinSource12, EXTI_Line12, EXTI_PortSourceGPIOD, EXTI_PinSource12, EXTI15_10_IRQn }
#define _PD13_	{ RCC_AHB1Periph_GPIOD, /*(GPIO_Regs_t*)*/GPIOD, GPIO_Pin_13, GPIO_PinSource13, EXTI_Line13, EXTI_PortSourceGPIOD, EXTI_PinSource13, EXTI15_10_IRQn }
#define _PD14_	{ RCC_AHB1Periph_GPIOD, /*(GPIO_Regs_t*)*/GPIOD, GPIO_Pin_14, GPIO_PinSource14, EXTI_Line14, EXTI_PortSourceGPIOD, EXTI_PinSource14, EXTI15_10_IRQn }
#define _PD15_	{ RCC_AHB1Periph_GPIOD, /*(GPIO_Regs_t*)*/GPIOD, GPIO_Pin_15, GPIO_PinSource15, EXTI_Line15, EXTI_PortSourceGPIOD, EXTI_PinSource15, EXTI15_10_IRQn }
                                        /*              */                                                    
#define _PE0_	{ RCC_AHB1Periph_GPIOE, /*(GPIO_Regs_t*)*/GPIOE, GPIO_Pin_0 , GPIO_PinSource0 , EXTI_Line0 , EXTI_PortSourceGPIOE, EXTI_PinSource0 , EXTI0_IRQn }
#define _PE1_	{ RCC_AHB1Periph_GPIOE, /*(GPIO_Regs_t*)*/GPIOE, GPIO_Pin_1 , GPIO_PinSource1 , EXTI_Line1 , EXTI_PortSourceGPIOE, EXTI_PinSource1 , EXTI1_IRQn }
#define _PE2_	{ RCC_AHB1Periph_GPIOE, /*(GPIO_Regs_t*)*/GPIOE, GPIO_Pin_2 , GPIO_PinSource2 , EXTI_Line2 , EXTI_PortSourceGPIOE, EXTI_PinSource2 , EXTI2_IRQn }
#define _PE3_	{ RCC_AHB1Periph_GPIOE, /*(GPIO_Regs_t*)*/GPIOE, GPIO_Pin_3 , GPIO_PinSource3 , EXTI_Line3 , EXTI_PortSourceGPIOE, EXTI_PinSource3 , EXTI3_IRQn }
#define _PE4_	{ RCC_AHB1Periph_GPIOE, /*(GPIO_Regs_t*)*/GPIOE, GPIO_Pin_4 , GPIO_PinSource4 , EXTI_Line4 , EXTI_PortSourceGPIOE, EXTI_PinSource4 , EXTI4_IRQn }
#define _PE5_	{ RCC_AHB1Periph_GPIOE, /*(GPIO_Regs_t*)*/GPIOE, GPIO_Pin_5 , GPIO_PinSource5 , EXTI_Line5 , EXTI_PortSourceGPIOE, EXTI_PinSource5 , EXTI9_5_IRQn }
#define _PE6_	{ RCC_AHB1Periph_GPIOE, /*(GPIO_Regs_t*)*/GPIOE, GPIO_Pin_6 , GPIO_PinSource6 , EXTI_Line6 , EXTI_PortSourceGPIOE, EXTI_PinSource6 , EXTI9_5_IRQn }
#define _PE7_	{ RCC_AHB1Periph_GPIOE, /*(GPIO_Regs_t*)*/GPIOE, GPIO_Pin_7 , GPIO_PinSource7 , EXTI_Line7 , EXTI_PortSourceGPIOE, EXTI_PinSource7 , EXTI9_5_IRQn }
#define _PE8_	{ RCC_AHB1Periph_GPIOE, /*(GPIO_Regs_t*)*/GPIOE, GPIO_Pin_8 , GPIO_PinSource8 , EXTI_Line8 , EXTI_PortSourceGPIOE, EXTI_PinSource8 , EXTI9_5_IRQn }
#define _PE9_	{ RCC_AHB1Periph_GPIOE, /*(GPIO_Regs_t*)*/GPIOE, GPIO_Pin_9 , GPIO_PinSource9 , EXTI_Line9 , EXTI_PortSourceGPIOE, EXTI_PinSource9 , EXTI9_5_IRQn }
#define _PE10_	{ RCC_AHB1Periph_GPIOE, /*(GPIO_Regs_t*)*/GPIOE, GPIO_Pin_10, GPIO_PinSource10, EXTI_Line10, EXTI_PortSourceGPIOE, EXTI_PinSource10, EXTI15_10_IRQn }
#define _PE11_	{ RCC_AHB1Periph_GPIOE, /*(GPIO_Regs_t*)*/GPIOE, GPIO_Pin_11, GPIO_PinSource11, EXTI_Line11, EXTI_PortSourceGPIOE, EXTI_PinSource11, EXTI15_10_IRQn }
#define _PE12_	{ RCC_AHB1Periph_GPIOE, /*(GPIO_Regs_t*)*/GPIOE, GPIO_Pin_12, GPIO_PinSource12, EXTI_Line12, EXTI_PortSourceGPIOE, EXTI_PinSource12, EXTI15_10_IRQn }
#define _PE13_	{ RCC_AHB1Periph_GPIOE, /*(GPIO_Regs_t*)*/GPIOE, GPIO_Pin_13, GPIO_PinSource13, EXTI_Line13, EXTI_PortSourceGPIOE, EXTI_PinSource13, EXTI15_10_IRQn }
#define _PE14_	{ RCC_AHB1Periph_GPIOE, /*(GPIO_Regs_t*)*/GPIOE, GPIO_Pin_14, GPIO_PinSource14, EXTI_Line14, EXTI_PortSourceGPIOE, EXTI_PinSource14, EXTI15_10_IRQn }
#define _PE15_	{ RCC_AHB1Periph_GPIOE, /*(GPIO_Regs_t*)*/GPIOE, GPIO_Pin_15, GPIO_PinSource15, EXTI_Line15, EXTI_PortSourceGPIOE, EXTI_PinSource15, EXTI15_10_IRQn }
                                        /*              */                                                        
#define _PF0_	{ RCC_AHB1Periph_GPIOF, /*(GPIO_Regs_t*)*/GPIOF, GPIO_Pin_0 , GPIO_PinSource0 , EXTI_Line0 , EXTI_PortSourceGPIOF, EXTI_PinSource0 , EXTI0_IRQn }
#define _PF1_	{ RCC_AHB1Periph_GPIOF, /*(GPIO_Regs_t*)*/GPIOF, GPIO_Pin_1 , GPIO_PinSource1 , EXTI_Line1 , EXTI_PortSourceGPIOF, EXTI_PinSource1 , EXTI1_IRQn }
#define _PF2_	{ RCC_AHB1Periph_GPIOF, /*(GPIO_Regs_t*)*/GPIOF, GPIO_Pin_2 , GPIO_PinSource2 , EXTI_Line2 , EXTI_PortSourceGPIOF, EXTI_PinSource2 , EXTI2_IRQn }
#define _PF3_	{ RCC_AHB1Periph_GPIOF, /*(GPIO_Regs_t*)*/GPIOF, GPIO_Pin_3 , GPIO_PinSource3 , EXTI_Line3 , EXTI_PortSourceGPIOF, EXTI_PinSource3 , EXTI3_IRQn }
#define _PF4_	{ RCC_AHB1Periph_GPIOF, /*(GPIO_Regs_t*)*/GPIOF, GPIO_Pin_4 , GPIO_PinSource4 , EXTI_Line4 , EXTI_PortSourceGPIOF, EXTI_PinSource4 , EXTI4_IRQn }
#define _PF5_	{ RCC_AHB1Periph_GPIOF, /*(GPIO_Regs_t*)*/GPIOF, GPIO_Pin_5 , GPIO_PinSource5 , EXTI_Line5 , EXTI_PortSourceGPIOF, EXTI_PinSource5 , EXTI9_5_IRQn }
#define _PF6_	{ RCC_AHB1Periph_GPIOF, /*(GPIO_Regs_t*)*/GPIOF, GPIO_Pin_6 , GPIO_PinSource6 , EXTI_Line6 , EXTI_PortSourceGPIOF, EXTI_PinSource6 , EXTI9_5_IRQn }
#define _PF7_	{ RCC_AHB1Periph_GPIOF, /*(GPIO_Regs_t*)*/GPIOF, GPIO_Pin_7 , GPIO_PinSource7 , EXTI_Line7 , EXTI_PortSourceGPIOF, EXTI_PinSource7 , EXTI9_5_IRQn }
#define _PF8_	{ RCC_AHB1Periph_GPIOF, /*(GPIO_Regs_t*)*/GPIOF, GPIO_Pin_8 , GPIO_PinSource8 , EXTI_Line8 , EXTI_PortSourceGPIOF, EXTI_PinSource8 , EXTI9_5_IRQn }
#define _PF9_	{ RCC_AHB1Periph_GPIOF, /*(GPIO_Regs_t*)*/GPIOF, GPIO_Pin_9 , GPIO_PinSource9 , EXTI_Line9 , EXTI_PortSourceGPIOF, EXTI_PinSource9 , EXTI9_5_IRQn }
#define _PF10_	{ RCC_AHB1Periph_GPIOF, /*(GPIO_Regs_t*)*/GPIOF, GPIO_Pin_10, GPIO_PinSource10, EXTI_Line10, EXTI_PortSourceGPIOF, EXTI_PinSource10, EXTI15_10_IRQn }
#define _PF11_	{ RCC_AHB1Periph_GPIOF, /*(GPIO_Regs_t*)*/GPIOF, GPIO_Pin_11, GPIO_PinSource11, EXTI_Line11, EXTI_PortSourceGPIOF, EXTI_PinSource11, EXTI15_10_IRQn }
#define _PF12_	{ RCC_AHB1Periph_GPIOF, /*(GPIO_Regs_t*)*/GPIOF, GPIO_Pin_12, GPIO_PinSource12, EXTI_Line12, EXTI_PortSourceGPIOF, EXTI_PinSource12, EXTI15_10_IRQn }
#define _PF13_	{ RCC_AHB1Periph_GPIOF, /*(GPIO_Regs_t*)*/GPIOF, GPIO_Pin_13, GPIO_PinSource13, EXTI_Line13, EXTI_PortSourceGPIOF, EXTI_PinSource13, EXTI15_10_IRQn }
#define _PF14_	{ RCC_AHB1Periph_GPIOF, /*(GPIO_Regs_t*)*/GPIOF, GPIO_Pin_14, GPIO_PinSource14, EXTI_Line14, EXTI_PortSourceGPIOF, EXTI_PinSource14, EXTI15_10_IRQn }
#define _PF15_	{ RCC_AHB1Periph_GPIOF, /*(GPIO_Regs_t*)*/GPIOF, GPIO_Pin_15, GPIO_PinSource15, EXTI_Line15, EXTI_PortSourceGPIOF, EXTI_PinSource15, EXTI15_10_IRQn }
                                        /*              */
#define _PG0_	{ RCC_AHB1Periph_GPIOG, /*(GPIO_Regs_t*)*/GPIOG, GPIO_Pin_0 , GPIO_PinSource0 , EXTI_Line0 , EXTI_PortSourceGPIOG, EXTI_PinSource0 , EXTI0_IRQn }
#define _PG1_	{ RCC_AHB1Periph_GPIOG, /*(GPIO_Regs_t*)*/GPIOG, GPIO_Pin_1 , GPIO_PinSource1 , EXTI_Line1 , EXTI_PortSourceGPIOG, EXTI_PinSource1 , EXTI1_IRQn }
#define _PG2_	{ RCC_AHB1Periph_GPIOG, /*(GPIO_Regs_t*)*/GPIOG, GPIO_Pin_2 , GPIO_PinSource2 , EXTI_Line2 , EXTI_PortSourceGPIOG, EXTI_PinSource2 , EXTI2_IRQn }
#define _PG3_	{ RCC_AHB1Periph_GPIOG, /*(GPIO_Regs_t*)*/GPIOG, GPIO_Pin_3 , GPIO_PinSource3 , EXTI_Line3 , EXTI_PortSourceGPIOG, EXTI_PinSource3 , EXTI3_IRQn }
#define _PG4_	{ RCC_AHB1Periph_GPIOG, /*(GPIO_Regs_t*)*/GPIOG, GPIO_Pin_4 , GPIO_PinSource4 , EXTI_Line4 , EXTI_PortSourceGPIOG, EXTI_PinSource4 , EXTI4_IRQn }
#define _PG5_	{ RCC_AHB1Periph_GPIOG, /*(GPIO_Regs_t*)*/GPIOG, GPIO_Pin_5 , GPIO_PinSource5 , EXTI_Line5 , EXTI_PortSourceGPIOG, EXTI_PinSource5 , EXTI9_5_IRQn }
#define _PG6_	{ RCC_AHB1Periph_GPIOG, /*(GPIO_Regs_t*)*/GPIOG, GPIO_Pin_6 , GPIO_PinSource6 , EXTI_Line6 , EXTI_PortSourceGPIOG, EXTI_PinSource6 , EXTI9_5_IRQn }
#define _PG7_	{ RCC_AHB1Periph_GPIOG, /*(GPIO_Regs_t*)*/GPIOG, GPIO_Pin_7 , GPIO_PinSource7 , EXTI_Line7 , EXTI_PortSourceGPIOG, EXTI_PinSource7 , EXTI9_5_IRQn }
#define _PG8_	{ RCC_AHB1Periph_GPIOG, /*(GPIO_Regs_t*)*/GPIOG, GPIO_Pin_8 , GPIO_PinSource8 , EXTI_Line8 , EXTI_PortSourceGPIOG, EXTI_PinSource8 , EXTI9_5_IRQn }
#define _PG9_	{ RCC_AHB1Periph_GPIOG, /*(GPIO_Regs_t*)*/GPIOG, GPIO_Pin_9 , GPIO_PinSource9 , EXTI_Line9 , EXTI_PortSourceGPIOG, EXTI_PinSource9 , EXTI9_5_IRQn }
#define _PG10_	{ RCC_AHB1Periph_GPIOG, /*(GPIO_Regs_t*)*/GPIOG, GPIO_Pin_10, GPIO_PinSource10, EXTI_Line10, EXTI_PortSourceGPIOG, EXTI_PinSource10, EXTI15_10_IRQn }
#define _PG11_	{ RCC_AHB1Periph_GPIOG, /*(GPIO_Regs_t*)*/GPIOG, GPIO_Pin_11, GPIO_PinSource11, EXTI_Line11, EXTI_PortSourceGPIOG, EXTI_PinSource11, EXTI15_10_IRQn }
#define _PG12_	{ RCC_AHB1Periph_GPIOG, /*(GPIO_Regs_t*)*/GPIOG, GPIO_Pin_12, GPIO_PinSource12, EXTI_Line12, EXTI_PortSourceGPIOG, EXTI_PinSource12, EXTI15_10_IRQn }
#define _PG13_	{ RCC_AHB1Periph_GPIOG, /*(GPIO_Regs_t*)*/GPIOG, GPIO_Pin_13, GPIO_PinSource13, EXTI_Line13, EXTI_PortSourceGPIOG, EXTI_PinSource13, EXTI15_10_IRQn }
#define _PG14_	{ RCC_AHB1Periph_GPIOG, /*(GPIO_Regs_t*)*/GPIOG, GPIO_Pin_14, GPIO_PinSource14, EXTI_Line14, EXTI_PortSourceGPIOG, EXTI_PinSource14, EXTI15_10_IRQn }
#define _PG15_	{ RCC_AHB1Periph_GPIOG, /*(GPIO_Regs_t*)*/GPIOG, GPIO_Pin_15, GPIO_PinSource15, EXTI_Line15, EXTI_PortSourceGPIOG, EXTI_PinSource15, EXTI15_10_IRQn }
                                        /*              */
#define _PH0_	{ RCC_AHB1Periph_GPIOH, /*(GPIO_Regs_t*)*/GPIOH, GPIO_Pin_0 , GPIO_PinSource0 , EXTI_Line0 , EXTI_PortSourceGPIOH, EXTI_PinSource0 , EXTI0_IRQn }
#define _PH1_	{ RCC_AHB1Periph_GPIOH, /*(GPIO_Regs_t*)*/GPIOH, GPIO_Pin_1 , GPIO_PinSource1 , EXTI_Line1 , EXTI_PortSourceGPIOH, EXTI_PinSource1 , EXTI1_IRQn }
#define _PH2_	{ RCC_AHB1Periph_GPIOH, /*(GPIO_Regs_t*)*/GPIOH, GPIO_Pin_2 , GPIO_PinSource2 , EXTI_Line2 , EXTI_PortSourceGPIOH, EXTI_PinSource2 , EXTI2_IRQn }
#define _PH3_	{ RCC_AHB1Periph_GPIOH, /*(GPIO_Regs_t*)*/GPIOH, GPIO_Pin_3 , GPIO_PinSource3 , EXTI_Line3 , EXTI_PortSourceGPIOH, EXTI_PinSource3 , EXTI3_IRQn }
#define _PH4_	{ RCC_AHB1Periph_GPIOH, /*(GPIO_Regs_t*)*/GPIOH, GPIO_Pin_4 , GPIO_PinSource4 , EXTI_Line4 , EXTI_PortSourceGPIOH, EXTI_PinSource4 , EXTI4_IRQn }
#define _PH5_	{ RCC_AHB1Periph_GPIOH, /*(GPIO_Regs_t*)*/GPIOH, GPIO_Pin_5 , GPIO_PinSource5 , EXTI_Line5 , EXTI_PortSourceGPIOH, EXTI_PinSource5 , EXTI9_5_IRQn }
#define _PH6_	{ RCC_AHB1Periph_GPIOH, /*(GPIO_Regs_t*)*/GPIOH, GPIO_Pin_6 , GPIO_PinSource6 , EXTI_Line6 , EXTI_PortSourceGPIOH, EXTI_PinSource6 , EXTI9_5_IRQn }
#define _PH7_	{ RCC_AHB1Periph_GPIOH, /*(GPIO_Regs_t*)*/GPIOH, GPIO_Pin_7 , GPIO_PinSource7 , EXTI_Line7 , EXTI_PortSourceGPIOH, EXTI_PinSource7 , EXTI9_5_IRQn }
#define _PH8_	{ RCC_AHB1Periph_GPIOH, /*(GPIO_Regs_t*)*/GPIOH, GPIO_Pin_8 , GPIO_PinSource8 , EXTI_Line8 , EXTI_PortSourceGPIOH, EXTI_PinSource8 , EXTI9_5_IRQn }
#define _PH9_	{ RCC_AHB1Periph_GPIOH, /*(GPIO_Regs_t*)*/GPIOH, GPIO_Pin_9 , GPIO_PinSource9 , EXTI_Line9 , EXTI_PortSourceGPIOH, EXTI_PinSource9 , EXTI9_5_IRQn }
#define _PH10_	{ RCC_AHB1Periph_GPIOH, /*(GPIO_Regs_t*)*/GPIOH, GPIO_Pin_10, GPIO_PinSource10, EXTI_Line10, EXTI_PortSourceGPIOH, EXTI_PinSource10, EXTI15_10_IRQn }
#define _PH11_	{ RCC_AHB1Periph_GPIOH, /*(GPIO_Regs_t*)*/GPIOH, GPIO_Pin_11, GPIO_PinSource11, EXTI_Line11, EXTI_PortSourceGPIOH, EXTI_PinSource11, EXTI15_10_IRQn }
#define _PH12_	{ RCC_AHB1Periph_GPIOH, /*(GPIO_Regs_t*)*/GPIOH, GPIO_Pin_12, GPIO_PinSource12, EXTI_Line12, EXTI_PortSourceGPIOH, EXTI_PinSource12, EXTI15_10_IRQn }
#define _PH13_	{ RCC_AHB1Periph_GPIOH, /*(GPIO_Regs_t*)*/GPIOH, GPIO_Pin_13, GPIO_PinSource13, EXTI_Line13, EXTI_PortSourceGPIOH, EXTI_PinSource13, EXTI15_10_IRQn }
#define _PH14_	{ RCC_AHB1Periph_GPIOH, /*(GPIO_Regs_t*)*/GPIOH, GPIO_Pin_14, GPIO_PinSource14, EXTI_Line14, EXTI_PortSourceGPIOH, EXTI_PinSource14, EXTI15_10_IRQn }
#define _PH15_	{ RCC_AHB1Periph_GPIOH, /*(GPIO_Regs_t*)*/GPIOH, GPIO_Pin_15, GPIO_PinSource15, EXTI_Line15, EXTI_PortSourceGPIOH, EXTI_PinSource15, EXTI15_10_IRQn }
                                        /*              */
#define _PI0_	{ RCC_AHB1Periph_GPIOI, /*(GPIO_Regs_t*)*/GPIOI, GPIO_Pin_0 , GPIO_PinSource0 , EXTI_Line0 , EXTI_PortSourceGPIOI, EXTI_PinSource0 , EXTI0_IRQn }
#define _PI1_	{ RCC_AHB1Periph_GPIOI, /*(GPIO_Regs_t*)*/GPIOI, GPIO_Pin_1 , GPIO_PinSource1 , EXTI_Line1 , EXTI_PortSourceGPIOI, EXTI_PinSource1 , EXTI1_IRQn }
#define _PI2_	{ RCC_AHB1Periph_GPIOI, /*(GPIO_Regs_t*)*/GPIOI, GPIO_Pin_2 , GPIO_PinSource2 , EXTI_Line2 , EXTI_PortSourceGPIOI, EXTI_PinSource2 , EXTI2_IRQn }
#define _PI3_	{ RCC_AHB1Periph_GPIOI, /*(GPIO_Regs_t*)*/GPIOI, GPIO_Pin_3 , GPIO_PinSource3 , EXTI_Line3 , EXTI_PortSourceGPIOI, EXTI_PinSource3 , EXTI3_IRQn }
#define _PI4_	{ RCC_AHB1Periph_GPIOI, /*(GPIO_Regs_t*)*/GPIOI, GPIO_Pin_4 , GPIO_PinSource4 , EXTI_Line4 , EXTI_PortSourceGPIOI, EXTI_PinSource4 , EXTI4_IRQn }
#define _PI5_	{ RCC_AHB1Periph_GPIOI, /*(GPIO_Regs_t*)*/GPIOI, GPIO_Pin_5 , GPIO_PinSource5 , EXTI_Line5 , EXTI_PortSourceGPIOI, EXTI_PinSource5 , EXTI9_5_IRQn }
#define _PI6_	{ RCC_AHB1Periph_GPIOI, /*(GPIO_Regs_t*)*/GPIOI, GPIO_Pin_6 , GPIO_PinSource6 , EXTI_Line6 , EXTI_PortSourceGPIOI, EXTI_PinSource6 , EXTI9_5_IRQn }
#define _PI7_	{ RCC_AHB1Periph_GPIOI, /*(GPIO_Regs_t*)*/GPIOI, GPIO_Pin_7 , GPIO_PinSource7 , EXTI_Line7 , EXTI_PortSourceGPIOI, EXTI_PinSource7 , EXTI9_5_IRQn }
#define _PI8_	{ RCC_AHB1Periph_GPIOI, /*(GPIO_Regs_t*)*/GPIOI, GPIO_Pin_8 , GPIO_PinSource8 , EXTI_Line8 , EXTI_PortSourceGPIOI, EXTI_PinSource8 , EXTI9_5_IRQn }
#define _PI9_	{ RCC_AHB1Periph_GPIOI, /*(GPIO_Regs_t*)*/GPIOI, GPIO_Pin_9 , GPIO_PinSource9 , EXTI_Line9 , EXTI_PortSourceGPIOI, EXTI_PinSource9 , EXTI9_5_IRQn }
#define _PI10_	{ RCC_AHB1Periph_GPIOI, /*(GPIO_Regs_t*)*/GPIOI, GPIO_Pin_10, GPIO_PinSource10, EXTI_Line10, EXTI_PortSourceGPIOI, EXTI_PinSource10, EXTI15_10_IRQn }
#define _PI11_	{ RCC_AHB1Periph_GPIOI, /*(GPIO_Regs_t*)*/GPIOI, GPIO_Pin_11, GPIO_PinSource11, EXTI_Line11, EXTI_PortSourceGPIOI, EXTI_PinSource11, EXTI15_10_IRQn }
#define _PI12_	{ RCC_AHB1Periph_GPIOI, /*(GPIO_Regs_t*)*/GPIOI, GPIO_Pin_12, GPIO_PinSource12, EXTI_Line12, EXTI_PortSourceGPIOI, EXTI_PinSource12, EXTI15_10_IRQn }
#define _PI13_	{ RCC_AHB1Periph_GPIOI, /*(GPIO_Regs_t*)*/GPIOI, GPIO_Pin_13, GPIO_PinSource13, EXTI_Line13, EXTI_PortSourceGPIOI, EXTI_PinSource13, EXTI15_10_IRQn }
#define _PI14_	{ RCC_AHB1Periph_GPIOI, /*(GPIO_Regs_t*)*/GPIOI, GPIO_Pin_14, GPIO_PinSource14, EXTI_Line14, EXTI_PortSourceGPIOI, EXTI_PinSource14, EXTI15_10_IRQn }
#define _PI15_	{ RCC_AHB1Periph_GPIOI, /*(GPIO_Regs_t*)*/GPIOI, GPIO_Pin_15, GPIO_PinSource15, EXTI_Line15, EXTI_PortSourceGPIOI, EXTI_PinSource15, EXTI15_10_IRQn }


/** GPIO pins CONSTANTS */
const GPIO_t
		_PA0  = _PA0_
	  , _PA1  = _PA1_
	  , _PA2  = _PA2_	
	  , _PA3  = _PA3_	
	  , _PA4  = _PA4_	
	  , _PA5  = _PA5_	
	  , _PA6  = _PA6_	
	  , _PA7  = _PA7_	
	  , _PA8  = _PA8_	
	  , _PA9  = _PA9_	
	  , _PA10 = _PA10_
	  , _PA11 = _PA11_
	  , _PA12 = _PA12_
	  , _PA13 = _PA13_
	  , _PA14 = _PA14_
	  , _PA15 = _PA15_
        
	  , _PB0  = _PB0_
	  , _PB1  = _PB1_
	  , _PB2  = _PB2_
	  , _PB3  = _PB3_
	  , _PB4  = _PB4_
	  , _PB5  = _PB5_
	  , _PB6  = _PB6_
	  , _PB7  = _PB7_
	  , _PB8  = _PB8_
	  , _PB9  = _PB9_
	  , _PB10 = _PB10_
	  , _PB11 = _PB11_
	  , _PB12 = _PB12_
	  , _PB13 = _PB13_
	  , _PB14 = _PB14_
	  , _PB15 = _PB15_

	  , _PC0  = _PC0_
	  , _PC1  = _PC1_
	  , _PC2  = _PC2_
	  , _PC3  = _PC3_
	  , _PC4  = _PC4_
	  , _PC5  = _PC5_
	  , _PC6  = _PC6_
	  , _PC7  = _PC7_
	  , _PC8  = _PC8_
	  , _PC9  = _PC9_
	  , _PC10 = _PC10_
	  , _PC11 = _PC11_
	  , _PC12 = _PC12_
	  , _PC13 = _PC13_
	  , _PC14 = _PC14_
	  , _PC15 = _PC15_

	  , _PD0  = _PD0_
	  , _PD1  = _PD1_
	  , _PD2  = _PD2_	
	  , _PD3  = _PD3_	
	  , _PD4  = _PD4_	
	  , _PD5  = _PD5_	
	  , _PD6  = _PD6_	
	  , _PD7  = _PD7_	
	  , _PD8  = _PD8_	
	  , _PD9  = _PD9_	
	  , _PD10 = _PD10_
	  , _PD11 = _PD11_
	  , _PD12 = _PD12_
	  , _PD13 = _PD13_
	  , _PD14 = _PD14_
	  , _PD15 = _PD15_

	  , _PE0  = _PE0_
	  , _PE1  = _PE1_
	  , _PE2  = _PE2_
	  , _PE3  = _PE3_
	  , _PE4  = _PE4_
	  , _PE5  = _PE5_
	  , _PE6  = _PE6_
	  , _PE7  = _PE7_
	  , _PE8  = _PE8_
	  , _PE9  = _PE9_
	  , _PE10 = _PE10_
	  , _PE11 = _PE11_
	  , _PE12 = _PE12_
	  , _PE13 = _PE13_
	  , _PE14 = _PE14_
	  , _PE15 = _PE15_

	  , _PF0  = _PF0_
	  , _PF1  = _PF1_
	  , _PF2  = _PF2_
	  , _PF3  = _PF3_
	  , _PF4  = _PF4_
	  , _PF5  = _PF5_
	  , _PF6  = _PF6_
	  , _PF7  = _PF7_
	  , _PF8  = _PF8_
	  , _PF9  = _PF9_
	  , _PF10 = _PF10_
	  , _PF11 = _PF11_
	  , _PF12 = _PF12_
	  , _PF13 = _PF13_
	  , _PF14 = _PF14_
	  , _PF15 = _PF15_

	  , _PG0  = _PG0_
	  , _PG1  = _PG1_
	  , _PG2  = _PG2_	
	  , _PG3  = _PG3_	
	  , _PG4  = _PG4_	
	  , _PG5  = _PG5_	
	  , _PG6  = _PG6_	
	  , _PG7  = _PG7_	
	  , _PG8  = _PG8_	
	  , _PG9  = _PG9_	
	  , _PG10 = _PG10_
	  , _PG11 = _PG11_
	  , _PG12 = _PG12_
	  , _PG13 = _PG13_
	  , _PG14 = _PG14_
	  , _PG15 = _PG15_
         
	  , _PH0  = _PH0_
	  , _PH1  = _PH1_
	  , _PH2  = _PH2_
	  , _PH3  = _PH3_
	  , _PH4  = _PH4_
	  , _PH5  = _PH5_
	  , _PH6  = _PH6_
	  , _PH7  = _PH7_
	  , _PH8  = _PH8_
	  , _PH9  = _PH9_
	  , _PH10 = _PH10_
	  , _PH11 = _PH11_
	  , _PH12 = _PH12_
	  , _PH13 = _PH13_
	  , _PH14 = _PH14_
	  , _PH15 = _PH15_

	  , _PI0  = _PI0_
	  , _PI1  = _PI1_
	  , _PI2  = _PI2_
	  , _PI3  = _PI3_
	  , _PI4  = _PI4_
	  , _PI5  = _PI5_
	  , _PI6  = _PI6_
	  , _PI7  = _PI7_
	  , _PI8  = _PI8_
	  , _PI9  = _PI9_
	  , _PI10 = _PI10_
	  , _PI11 = _PI11_
	  , _PI12 = _PI12_
	  , _PI13 = _PI13_
	  , _PI14 = _PI14_
	  , _PI15 = _PI15_
	  ;
