/**
 * Sledge - wide functionality library, suitable for embedded systems, 
 * mainly for ARM Cortex and more particularly for STM32 family
 * 
 * Copyright (c) 2015, Ivan Kuvaldin. All rights reserved.
 * 
 * Licensed under BSD or MIT. 
 * Please refer to Licence.BSD.txt or Licence.MIT.txt provided with these sources.
 * You may obtain a copy of the Licenses at
 *      http://opensource.org/licenses/bsd-license.php <br/>
 *      http://opensource.org/licenses/mit-license.php <br/>
 */
/**
 * @file 	Sledge/misc/uid.h
 * @author 	Ivan "Kyb Sledgehammer" Kuvaldin <i.kyb[2]ya,ru>
 * @version	1.2.1
 * @date 	2015-Mar-31
 * @brief 	STM32 Unique ID functions.
 *			This file contains definitions and functions, which allows to get
 *			96-bit UID, its packed 32-bit, 16-bit and 8-bit variations.
 * 			This module is part of Sledgehammer library abbr. Sledge.
 *
 * @Changes
 *  ver.1.2.1  date.2015-Mar-31
 *    * Code optimizations
 *    * Better documentation
 *  ver.1.1.0  date.2015-Jan-13
 * 	  + Added uid96toAll() funtion, and its alias uidInit()
 *    * Some refactoring.
 *  ver.1.0.0  date.2014-July-31
 *    + Initial implementation
 *
 * @TODO
 *  * Перейти на __STATIC_INLINE
 *
 * BSD license
 */

#ifndef __UID_H
#define __UID_H

#ifdef __cplusplus
extern "C" {
#endif

	 
//#include "stm32f4xx.h"

/// Address in STM32 memory
#define UNIQUE_ID_BASE 		((uint32_t)0x1FFF7A10)

typedef struct {
	const uint32_t uidArr[3];
} UniqueID_t;

  
#ifdef __cplusplus
}
#endif
#endif /*__UID_H */
