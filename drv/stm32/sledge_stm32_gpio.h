/**
 * Sledge - wide functionality library, suitable for embedded systems, 
 * mainly for ARM Cortex and more particularly for STM32 family
 * 
 * Copyright (c) 2015, Ivan Kuvaldin. All rights reserved.
 * 
 * Licensed under BSD or MIT. 
 * Please refer to Licence.BSD.txt or Licence.MIT.txt provided with these sources.
 * You may obtain a copy of the Licenses at
 *      http://opensource.org/licenses/bsd-license.php
 *      http://opensource.org/licenses/mit-license.php
 */
 
/**
 * \file	Sledge/bsp/stm32_gpio_new.h
 * \author 	Ivan "Kyb Sledgehammer" Kuvaldin <i.kyb[2]ya,ru>
 * \version	1.0.1
 * \date	2015-feb-18
 * \brief	This file contains definitions for GPIOs of STM32. New representation
 *			
 *			Новое представление	и способ работы с GPIO. Константы, дефайны, 
 *			адреса портов объединены в структуры. 
 * 			Маленький шаг в сторону ООП. Удобнее.
 *			Данная реализация поглощает больше flash-памяти.
 *
 * \Changes
 *	ver.1.0.1, date.2015-feb-18
 *	 *	#define _Pxx_ moved to .c file.
 * 	ver.1.0.0, date.2014-aug-23
 *	 + 	Initial implementation.
 *
 * \ToDo
 *	+ Добавить #ifdef <модель> для разных серий котроллеров. Исключить порты, 
 *	  которых нет в данной модели во время компиляции.
 */
  
#ifndef __sledge_stm32_gpio_H
#define __sledge_stm32_gpio_H

#ifdef __cplusplus
 extern "C" {
#endif


#include <stdbool.h>
#include "stm32f4xx.h"
#include "stm32f4xx_gpio.h"
#include "Sledge/utils.h"


//typedef GPIO_TypeDef GPIO_Regs_t; 
#pragma anon_unions  /// allow anonimous unions
/**
 * Completly compatible with GPIO_TypeDef from both ST's SPD and HAL drivers.
 * Main feature of this struct is unioned BSRR and BSRRL(H) register respresentation.
 */
typedef struct GPIO_Regs_t
{
	volatile uint32_t MODER;    /*!< GPIO port mode register,               Address offset: 0x00      */
	volatile uint32_t OTYPER;   /*!< GPIO port output type register,        Address offset: 0x04      */
	volatile uint32_t OSPEEDR;  /*!< GPIO port output speed register,       Address offset: 0x08      */
	volatile uint32_t PUPDR;    /*!< GPIO port pull-up/pull-down register,  Address offset: 0x0C      */
	volatile uint32_t IDR;      /*!< GPIO port input data register,         Address offset: 0x10      */
	volatile uint32_t ODR;      /*!< GPIO port output data register,        Address offset: 0x14      */
	union {
		volatile uint32_t BSRR; /// GPIO port bit set/reset register. Address offset: 0x18. Reset value: 0x0000 0000
		struct {
			volatile uint16_t BSRRL;    /*!< GPIO port bit set register,   Address offset: 0x18      */
			volatile uint16_t BSRRH;    /*!< GPIO port bit reset register, Address offset: 0x1A      */
		};
	};
	volatile uint32_t LCKR;     /*!< GPIO port configuration lock register, Address offset: 0x1C      */
	volatile uint32_t AFR[2];   /*!< GPIO alternate function registers,     Address offset: 0x20-0x24 */
} GPIO_Regs_t;
STATIC_ASSERT( sizeof(GPIO_Regs_t) == sizeof(GPIO_TypeDef) );


typedef struct GPIO_t {
	const uint32_t 		clk;			/// e.g. RCC_AHB1Periph_GPIOA
	GPIO_TypeDef *const port;			/// e.g. GPIOA
	const uint16_t 		pin;			/// e.g. GPIO_Pin_0, or BIT(0)
	const uint8_t 		pinsource;		/// e.g. GPIO_PinSource0
	const uint32_t		extiLine;		/// e.g. EXTI_Line0
	const uint8_t		extiPortSrc;	/// e.g. EXTI_PortSourceGPIOA
	const uint8_t		extiPinSrc;		/// e.g. EXTI_PinSource0
	const IRQn_Type		irq;			/// e.g. EXTI0_IRQn
	const char 		    portname;		/// e.g. 'A', 'B', etc.
} GPIO_t;


#define PA0 	(&_PA0 )
#define PA1 	(&_PA1 )
#define PA2 	(&_PA2 )
#define PA3 	(&_PA3 )
#define PA4 	(&_PA4 )
#define PA5 	(&_PA5 )
#define PA6 	(&_PA6 )
#define PA7 	(&_PA7 )
#define PA8 	(&_PA8 )
#define PA9 	(&_PA9 )
#define PA10	(&_PA10)
#define PA11	(&_PA11)
#define PA12	(&_PA12)
#define PA13	(&_PA13)
#define PA14	(&_PA14)
#define PA15	(&_PA15)
			
#define PB0 	(&_PB0 )	
#define PB1 	(&_PB1 )
#define PB2 	(&_PB2 )
#define PB3 	(&_PB3 )
#define PB4 	(&_PB4 )
#define PB5 	(&_PB5 )
#define PB6 	(&_PB6 )
#define PB7 	(&_PB7 )
#define PB8 	(&_PB8 )
#define PB9 	(&_PB9 )
#define PB10	(&_PB10)
#define PB11	(&_PB11)
#define PB12	(&_PB12)
#define PB13	(&_PB13)
#define PB14	(&_PB14)
#define PB15	(&_PB15)
			
#define PC0 	(&_PC0 )	
#define PC1 	(&_PC1 )	
#define PC2 	(&_PC2 )	
#define PC3 	(&_PC3 )	
#define PC4 	(&_PC4 )	
#define PC5 	(&_PC5 )	
#define PC6 	(&_PC6 )	
#define PC7 	(&_PC7 )	
#define PC8 	(&_PC8 )	
#define PC9 	(&_PC9 )	
#define PC10	(&_PC10)
#define PC11	(&_PC11)
#define PC12	(&_PC12)
#define PC13	(&_PC13)
#define PC14	(&_PC14)
#define PC15	(&_PC15)
			
#define PD0 	(&_PD0 )
#define PD1 	(&_PD1 )
#define PD2 	(&_PD2 )
#define PD3 	(&_PD3 )
#define PD4 	(&_PD4 )
#define PD5 	(&_PD5 )
#define PD6 	(&_PD6 )
#define PD7 	(&_PD7 )
#define PD8 	(&_PD8 )
#define PD9 	(&_PD9 )
#define PD10	(&_PD10)
#define PD11	(&_PD11)
#define PD12	(&_PD12)
#define PD13	(&_PD13)
#define PD14	(&_PD14)
#define PD15	(&_PD15)
			
#define PE0 	(&_PE0 )
#define PE1 	(&_PE1 )
#define PE2 	(&_PE2 )
#define PE3 	(&_PE3 )
#define PE4 	(&_PE4 )
#define PE5 	(&_PE5 )
#define PE6 	(&_PE6 )
#define PE7 	(&_PE7 )
#define PE8 	(&_PE8 )
#define PE9 	(&_PE9 )
#define PE10	(&_PE10)
#define PE11	(&_PE11)
#define PE12	(&_PE12)
#define PE13	(&_PE13)
#define PE14	(&_PE14)
#define PE15	(&_PE15)
			
#define PF0 	(&_PF0 )
#define PF1 	(&_PF1 )
#define PF2 	(&_PF2 )
#define PF3 	(&_PF3 )
#define PF4 	(&_PF4 )
#define PF5 	(&_PF5 )
#define PF6 	(&_PF6 )
#define PF7 	(&_PF7 )
#define PF8 	(&_PF8 )
#define PF9 	(&_PF9 )
#define PF10	(&_PF10)
#define PF11	(&_PF11)
#define PF12	(&_PF12)
#define PF13	(&_PF13)
#define PF14	(&_PF14)
#define PF15	(&_PF15)
	
#define PG0 	(&_PG0 )
#define PG1 	(&_PG1 )
#define PG2 	(&_PG2 )
#define PG3 	(&_PG3 )
#define PG4 	(&_PG4 )
#define PG5 	(&_PG5 )
#define PG6 	(&_PG6 )
#define PG7 	(&_PG7 )
#define PG8 	(&_PG8 )
#define PG9 	(&_PG9 )
#define PG10	(&_PG10)
#define PG11	(&_PG11)
#define PG12	(&_PG12)
#define PG13	(&_PG13)
#define PG14	(&_PG14)
#define PG15	(&_PG15)
			
#define PH0 	(&_PH0 )
#define PH1 	(&_PH1 )
#define PH2 	(&_PH2 )
#define PH3 	(&_PH3 )
#define PH4 	(&_PH4 )
#define PH5 	(&_PH5 )
#define PH6 	(&_PH6 )
#define PH7 	(&_PH7 )
#define PH8 	(&_PH8 )
#define PH9 	(&_PH9 )
#define PH10	(&_PH10)
#define PH11	(&_PH11)
#define PH12	(&_PH12)
#define PH13	(&_PH13)
#define PH14	(&_PH14)
#define PH15	(&_PH15)
			
#define PI0 	(&_PI0 )
#define PI1 	(&_PI1 )
#define PI2 	(&_PI2 )
#define PI3 	(&_PI3 )
#define PI4 	(&_PI4 )
#define PI5 	(&_PI5 )
#define PI6 	(&_PI6 )
#define PI7 	(&_PI7 )
#define PI8 	(&_PI8 )
#define PI9 	(&_PI9 )
#define PI10	(&_PI10)
#define PI11	(&_PI11)
#define PI12	(&_PI12)
#define PI13	(&_PI13)
#define PI14	(&_PI14)
#define PI15	(&_PI15)


/** Exported GPIO pins CONSTANTS */
extern const GPIO_t 
		_PA0, _PA1, _PA2, _PA3, _PA4, _PA5, _PA6, _PA7, _PA8, _PA9, _PA10, _PA11, _PA12, _PA13, _PA14, _PA15, 
		_PB0, _PB1, _PB2, _PB3, _PB4, _PB5, _PB6, _PB7, _PB8, _PB9, _PB10, _PB11, _PB12, _PB13, _PB14, _PB15, 
		_PC0, _PC1, _PC2, _PC3, _PC4, _PC5, _PC6, _PC7, _PC8, _PC9, _PC10, _PC11, _PC12, _PC13, _PC14, _PC15,
		_PD0, _PD1, _PD2, _PD3, _PD4, _PD5, _PD6, _PD7, _PD8, _PD9, _PD10, _PD11, _PD12, _PD13, _PD14, _PD15,
		_PE0, _PE1, _PE2, _PE3, _PE4, _PE5, _PE6, _PE7, _PE8, _PE9, _PE10, _PE11, _PE12, _PE13, _PE14, _PE15,
		_PF0, _PF1, _PF2, _PF3, _PF4, _PF5, _PF6, _PF7, _PF8, _PF9, _PF10, _PF11, _PF12, _PF13, _PF14, _PF15,
		_PG0, _PG1, _PG2, _PG3, _PG4, _PG5, _PG6, _PG7, _PG8, _PG9, _PG10, _PG11, _PG12, _PG13, _PG14, _PG15,
		_PH0, _PH1, _PH2, _PH3, _PH4, _PH5, _PH6, _PH7, _PH8, _PH9, _PH10, _PH11, _PH12, _PH13, _PH14, _PH15,
		_PI0, _PI1, _PI2, _PI3, _PI4, _PI5, _PI6, _PI7, _PI8, _PI9, _PI10, _PI11, _PI12, _PI13, _PI14, _PI15;


#ifdef __cplusplus
}
#endif

#endif // __sledge_stm32_gpio_H
