/**
 * Sledge - wide functionality library, suitable for embedded systems, 
 * mainly for ARM Cortex and more particularly for STM32 family
 * 
 * Copyright (c) 2015, Ivan Kuvaldin. All rights reserved.
 * 
 * Licensed under BSD or MIT. 
 * Please refer to Licence.BSD.txt or Licence.MIT.txt provided with these sources.
 * You may obtain a copy of the Licenses at
 *      http://opensource.org/licenses/bsd-license.php
 *      http://opensource.org/licenses/mit-license.php
 */

/**
 * \file 	Sledge/drv/stm32/stm32_rng_sledge.h
 * \author 	Ivan "Kyb Sledgehammer" Kuvaldin <i.Kyb[2]ya,ru> * 
 * \brief	Convenient API for STM32 random number generator
 * 
 * @changelog
 * ver.2.0 date.2015-feb-09
 * 	+ RNG_Check_Fault()	- function which checks if any troubles were happened. 
 *						  MUST be called from HASH_RNG_IRQHandler()
 *	+ added information from ref.man.
 *	* API ported to __STATIC_INLINE. So functions have gone to randomGen.h file.
 *
 * ver1.0 date.2014-aug-01
 *	.first base implementation
 */


/// Define to prevent recursive inclusion -------------------------------------
#ifndef __RANDOM_GEN_H
#define __RANDOM_GEN_H

#ifdef __cplusplus
extern "C" {
#endif

	
#include <stdint.h>
//#include "stm32f4xx.h"
#include "stm32f4xx_rcc.h"
#include "stm32f4xx_rng.h"
#include "Sledge/assert.h"



void drv_RNG_init(void);
//#define randomGen_getNext()		drv_RNG_getNext();


/** 
 * @brief 	40 periods of the PLL48CLK clock signal between two consecutive random numbers.
 * 			So the rate is 1.2MHz, or 833 ns. And if your core frequency is 168MHz, 
 * 			the time between two consecutive random numbers will equal 140 core pulses.
 * 						140 ticks / 168 MHz = 833 ns.
 * @note	USUALLY when we call this function the random value is ready.
 * @Return 	Pseudo-random hardware generated 32-bit number.
 */
__STATIC_INLINE uint32_t drv_RNG_getNext(void)
{
	// иницализировать, если еще не сделано. Пара лишних тактов проца за удобство.
	if( !(RCC->AHB2ENR & RCC_AHB2Periph_RNG) )
		drv_RNG_init();
	// Ждём окончания работы генератора.
	while( !(RNG->SR & RNG_FLAG_DRDY) ); 	//RNG_GetFlagStatus(RNG_FLAG_DRDY) == RESET );
	return RNG->DR;		//return RNG_GetRandomNumber();
}


/** 
 * Check if STM32 Random number generator failed
 * This function should be called from interrupt HASH_RNG_IRQHandler() only.
 * Purpose is to handle errors. I don't know how often are they met.
 *	
 *	24.3.2  ERROR MANAGEMENT
 *	If the CEIS bit is read as ‘1’ (clock error)
 *	In the case of a clock, the RNG is no more able to generate random numbers because the
 *	PLL48CLK clock is not correct. Check that the clock controller is correctly configured to
 *	provide the RNG clock and clear the CEIS bit. The RNG can work when the CECS bit is ‘0’.
 *	The clock error has no impact on the previously generated random numbers, and the
 *	RNG_DR register contents can be used.
 *
 *	If the SEIS bit is read as ‘1’ (seed error)
 *	In the case of a seed error, the generation of random numbers is interrupted for as long as
 *	the SECS bit is ‘1’. If a number is available in the RNG_DR register, it must not be used
 *	because it may not have enough entropy.
 *	What you should do is clear the SEIS bit, then clear and set the RNGEN bit to reinitialize
 *	and restart the RNG. 
 */
void RNG_IRQHandler();  //RNG_Check_Fault()
	

#ifdef __cplusplus
}
#endif

#endif /*__RANDOM_GEN_H */
