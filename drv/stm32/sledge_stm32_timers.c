/**
 * Sledge - wide functionality library, suitable for embedded systems, 
 * mainly for ARM Cortex and more particularly for STM32 family
 * 
 * Copyright (c) 2015, Ivan Kuvaldin. All rights reserved.
 * 
 * Licensed under BSD or MIT. 
 * Please refer to Licence.BSD.txt or Licence.MIT.txt provided with these sources.
 * You may obtain a copy of the Licenses at
 *      http://opensource.org/licenses/bsd-license.php
 *      http://opensource.org/licenses/mit-license.php
 */

/**
 * @file	stm32_timers_sledge.c 
 * @author 	Ivan "Kyb Sledgehammer" Kuvaldin <i.kyb[2]ya,ru>
 * @brief	Look at ./stm32_timers_sledge.h
 */

#include "Sledge/bsp.h"
#include "./sledge_stm32_timers.h"
#include "Sledge/utils.h"

#include "stm32f4xx_rcc.h"
#include "stm32f4xx_tim.h"
#include "misc.h"


/** This defines represents pin arrays for TIM_handler_t constant
 *	Usage: TIMS[x].ch_Pins[channel-1][alternative]. Channel may be 1-4, alternative 0 or 1 or 2.
 *	For example TIMS[1].ch_Pins[0][0] 
 */
#define TIM1ChPins 		{ { PA8,  PE9  }	\
						 ,{ PA9,  PE11 } 	\
						 ,{ PA10, PE13 }	\
						 ,{ PA11, PE14 }	\
						}				 
#define TIM2ChPins		{ { PA0,  PA5, PA15 } \
						 ,{ PA1,  PB3  }  	\
						 ,{ PA2,  PB10 }	\
						 ,{ PA3,  PB11 }	\
						}	 
#define TIM3ChPins		{ { PB4,  PC6,	PA6 } \
						 ,{ PB5,  PC7,	PA7 } \
						 ,{ PB0,  PC8 }		\
						 ,{ PB1,  PC9 }		\
						}				 
#define TIM4ChPins		{ { PD12, PB6 }	\
						 ,{ PD13, PB7 }	\
						 ,{ PD14, PB8 }	\
						 ,{ PD15, PB9 }	\
						}	 
#define TIM5ChPins		{ { PA0 , PH10 } \
						 ,{ PA1 , PH11 } \
						 ,{ PA2 , PH12 } \
						 ,{ PA3 , PI0 }	 \
						}				 
#define TIM8ChPins		{ { PC6 , PI5 }	\
						 ,{ PC7 , PI6 }	\
						 ,{ PC8 , PI7 }	\
						 ,{ PC9 , PI2 }	\
						}			 
#define TIM9ChPins		{ { PA2 , PE5 }	\
						 ,{ PA3 , PE6 } \
						}			 
#define TIM10ChPins		{ { PB8 , PF6 } }
#define TIM11ChPins		{ { PB9 , PF7 } }
#define TIM12ChPins		{ { PH6 , PB14 } \
						 ,{ PH9 , PB15 } \
						}			 
#define TIM13ChPins		{ { PA6 , PF8 } }
#define TIM14ChPins		{ { PA7 , PF9 } }

/** External trigger pins for TIM_handler_t */
#define TIM1EtrPins 	{ PE7, PA12 }
#define TIM2EtrPins 	{ PA0, PA5, PA15 }
#define TIM3EtrPins 	{ PD2 }
#define TIM4EtrPins 	{ PE0 }
#define TIM8EtrPins 	{ PA0, PI3 }


const TIM_handler_t TIMS[] = {
	{   0 },	// TIM0 does not exist. Wasting some flash memory is paid for more clear code.
	{  TIM1, RCC_APB2Periph_TIM1,   GPIO_AF_TIM1,      TIM1_UP_TIM10_IRQn,  TIM1ChPins, TIM1EtrPins },
	{  TIM2, RCC_APB1Periph_TIM2,   GPIO_AF_TIM2,               TIM2_IRQn,  TIM2ChPins, TIM2EtrPins },
	{  TIM3, RCC_APB1Periph_TIM3,   GPIO_AF_TIM3,               TIM3_IRQn,  TIM3ChPins, TIM3EtrPins },
	{  TIM4, RCC_APB1Periph_TIM4,   GPIO_AF_TIM4,               TIM4_IRQn,  TIM4ChPins, TIM4EtrPins },
	{  TIM5, RCC_APB1Periph_TIM5,   GPIO_AF_TIM5,               TIM5_IRQn,  TIM5ChPins 				},
	{  TIM6, RCC_APB1Periph_TIM6,           NULL,      	    TIM6_DAC_IRQn,	  						},
	{  TIM7, RCC_APB1Periph_TIM7,           NULL,               TIM7_IRQn,	 						},
	{  TIM8, RCC_APB2Periph_TIM8,   GPIO_AF_TIM8,      TIM8_UP_TIM13_IRQn,  TIM8ChPins, TIM8EtrPins },
	{  TIM9, RCC_APB2Periph_TIM9,   GPIO_AF_TIM9,      TIM1_BRK_TIM9_IRQn,  TIM9ChPins, 			},
	{ TIM10, RCC_APB2Periph_TIM10, GPIO_AF_TIM10,      TIM1_UP_TIM10_IRQn, TIM10ChPins, 			},
	{ TIM11, RCC_APB2Periph_TIM11, GPIO_AF_TIM11, TIM1_TRG_COM_TIM11_IRQn, TIM11ChPins, 			},
	{ TIM12, RCC_APB1Periph_TIM12, GPIO_AF_TIM12,     TIM8_BRK_TIM12_IRQn, TIM12ChPins, 			},
	{ TIM13, RCC_APB1Periph_TIM13, GPIO_AF_TIM13,      TIM8_UP_TIM13_IRQn, TIM13ChPins, 			},
	{ TIM14, RCC_APB1Periph_TIM14, GPIO_AF_TIM14, TIM8_TRG_COM_TIM14_IRQn, TIM14ChPins, 			},
};                                                                                                  


/** 
 * Таблица соответсвия источников триггегов таймеров
 * Значения поля Trigger Selection (TS) в регистре slave mode control register SMCR
 * Usage: TIM_TriggerSources[destination_tim_num][source_tim_num]
 +-----------+-----------------+-----------------+-----------------+-----------------+
 | Slave TIM | ITR0 (TS = 000) | ITR1 (TS = 001) | ITR2 (TS = 010) | ITR3 (TS = 011) |
 |           |   TIM_TS_ITR0   |   TIM_TS_ITR1   |   TIM_TS_ITR2   |   TIM_TS_ITR3   |
 +-----------+-----------------+-----------------+-----------------+-----------------+
 |   TIM1    |      TIM5       |      TIM2       |      TIM3       |      TIM4       |
 |   TIM2    |      TIM1       |      TIM8       |      TIM3       |      TIM4       |
 |   TIM3    |      TIM1       |      TIM2       |      TIM5       |      TIM4       |
 |   TIM4    |      TIM1       |      TIM2       |      TIM3       |      TIM8       |
 |   TIM5    |      TIM2       |      TIM3       |      TIM4       |      TIM8       |
 |    -      |                 |                 |                 |                 |
 |   TIM8    |      TIM1       |      TIM2       |      TIM4       |      TIM5       |
 +-----------+-----------------+-----------------+-----------------+-----------------+*/
const struct {
	const TIM_handler_t *dest;
	const uint8_t src_n[4];
} TIM_TriggerSources[] = 
{	{ &TIMS[1], { 5, 2, 3, 4 } },
    { &TIMS[2], { 1, 8, 3, 4 } },
    { &TIMS[3], { 1, 2, 5, 4 } },
    { &TIMS[4], { 1, 2, 3, 8 } },
    { &TIMS[5], { 2, 3, 4, 8 } },
    { &TIMS[8], { 1, 2, 4, 5 } },
};
STATIC_ASSERT( sizeofarr(TIM_TriggerSources[0].src_n) == 4 );

/// Получить значение поля регистра SMCR_TS, чтобы задать для таймера dest источник триггера таймер src
///\param dest целевой таймер, slave
///\param src таймер источник, master
///\return TIM_TS_ITR0, or TIM_TS_ITR1, TIM_TS_ITR2, TIM_TS_ITR3
///        -1 if cannont resolve
uint16_t TIM_ObtainTriggerSource( 
	const TIM_handler_t *dest, 
	const TIM_handler_t *src 
){
	for( uint8_t i=0; i<sizeofarr(TIM_TriggerSources); i++ ){
		if( dest == TIM_TriggerSources[i].dest ){
			for( uint8_t j=0; j<sizeofarr(TIM_TriggerSources[i].src_n); j++ ){
				if( src == &TIMS[TIM_TriggerSources[i].src_n[j]] )
					return j<<4;
			}
		}
	}
	return (uint16_t)-1;
}


/** Таблица соответствия фильтров для таймеров с разной базовой частотой APB1, APB2. 84MHz и 168MHz соответственно.
  * Цель: 2 таймера должны тратить одинаковое время на фильтрацию вх. сигнала.
  *	Представлено в виде массива пар значений фильтров для таймеров APB1 и APB2 
  *	соответсвенно, т.е. для STM32F4 первое значение соответствует 84МГц таймеру, второе - 168МГц.
  * или другие в зависимости от контроллера 
  * @note Значения ClockDivision должны быть равны для пары таймеров
  * @literarure Ref.Man.p623 Input Capture 1 Filter IC1F */ 
const uint16_t timFiltersTable[][2] = { {  0,  0 },
                                        {  1,  2 },
                                        {  2,  3 },
                                        {  2,  3 },
                                        {  4,  6 },
                                        {  5,  7 },
                                        {  6,  8 },
                                        {  7,  9 },
                                        {  8, 11 },
                                        {  9, 12 },
                                        { 10, 13 },
                                        { 11, 14 },
                                        { 12, 15 },
                                        { 12, 15 },
                                        { 12, 15 },
                                        { 12, 15 },
                                      };

/**
 * Make
 * @param pwmfreq_hz[IN/OUT]  Задать частоту ШИМ, значение будет изменено на принятое, пересчитанное //TODO
 */
void timPwmConfig( const TIM_handler_t *timhandler, uint32_t pwmfreq_hz ) 
{
	TIM_TimeBaseInitTypeDef  timebase;
	TIM_OCInitTypeDef  outcap;
	uint16_t TimerPeriod = 0;
	//uint16_t ch1Pulse = 0, ch2Pulse = 0, ch3Pulse = 0, ch4Pulse = 0;
	
	/* TIM pins Configuration */
	//TIM_Config_pins();
	
	/** TIM1 Configuration ---------------------------------------------------
	 * 	Generate 7 PWM signals with 4 different duty cycles:
	 * 	TIM1 input clock (TIM1CLK) is set to 2 * APB2 clock (PCLK2), since APB2 
	 * 		prescaler is different from 1.	 
	 * 		TIM1CLK = 2 * PCLK2	
	 * 		PCLK2 = HCLK / 2 
	 * 		=> TIM1CLK = 2 * (HCLK / 2) = HCLK = SystemCoreClock
	 * 	TIM1CLK = SystemCoreClock, Prescaler = 0, TIM1 counter clock = SystemCoreClock
	 * 	SystemCoreClock is set to 168 MHz for STM32F4xx devices
	 * 	
	 * 	The objective is to generate 7 PWM signal at 17.57 KHz:
	 * 		 - TIM1_Period = (SystemCoreClock / 17570) - 1
	 * 	The channel 1 and channel 1N duty cycle is set to 50%
	 * 	The channel 2 and channel 2N duty cycle is set to 37.5%
	 * 	The channel 3 and channel 3N duty cycle is set to 25%
	 * 	The channel 4 duty cycle is set to 12.5%
	 * 	The Timer pulse is calculated as follows:
	 * 		 - ChannelxPulse = DutyCycle * (TIM1_Period - 1) / 100
	 * 	
	 * 	Note: 
	 * 		SystemCoreClock variable holds HCLK frequency and is defined in system_stm32f4xx.c file.
	 * 		Each time the core clock (HCLK) changes, user had to call SystemCoreClockUpdate()
	 * 		function to update SystemCoreClock variable value. Otherwise, any configuration
	 * 		based on this variable will be incorrect. 
	 *---------------------------------------------------------------------- */
	/// Compute the value to be set in ARR regiter to generate signal frequency at 17.57 Khz */
	TimerPeriod = (SystemCoreClock / pwmfreq_hz/*17570*/ ) - 1;
	/*// Compute CCR1 value to generate a duty cycle at 50% for channel 1 and 1N
	ch1Pulse = (uint16_t) (((uint32_t) 5 * (TimerPeriod - 1)) / 10);
	/// Compute CCR2 value to generate a duty cycle at 37.5%	for channel 2 and 2N
	ch2Pulse = (uint16_t) (((uint32_t) 375 * (TimerPeriod - 1)) / 1000);
	/// Compute CCR3 value to generate a duty cycle at 25%	for channel 3 and 3N
	ch3Pulse = (uint16_t) (((uint32_t) 25 * (TimerPeriod - 1)) / 100);
	/// Compute CCR4 value to generate a duty cycle at 12.5%	for channel 4
	ch4Pulse = (uint16_t) (((uint32_t) 125 * (TimerPeriod- 1)) / 1000);
	*/
	
	//assert_msg("Timer already initialized.", TIMx_APBxClockIsEnabled( *timhandler ));
	
	/* TIM1 clock enable */
	//RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM1 , ENABLE);
	RCC_TIMx_APBxClockCmd( *timhandler, ENABLE );
	
	/* Time Base configuration */
	timebase.TIM_Prescaler = 0;
	timebase.TIM_CounterMode = TIM_CounterMode_Up;
	timebase.TIM_Period = TimerPeriod;
	timebase.TIM_ClockDivision = 0;
	timebase.TIM_RepetitionCounter = 0;

	TIM_TimeBaseInit( timhandler->tim, &timebase);
	
	/// Channel 1,2,3 and 4 Configuration in PWM mode
	outcap.TIM_OCMode = TIM_OCMode_PWM2;
	outcap.TIM_OutputState = TIM_OutputState_Enable;
	outcap.TIM_OutputNState = TIM_OutputNState_Enable;
	outcap.TIM_OCPolarity = TIM_OCPolarity_Low;
	outcap.TIM_OCNPolarity = TIM_OCNPolarity_High;
	outcap.TIM_OCIdleState = TIM_OCIdleState_Set;
	outcap.TIM_OCNIdleState = TIM_OCIdleState_Reset;
	
	//outcap.TIM_Pulse = ch1Pulse;
	outcap.TIM_Pulse = 0;
	TIM_OC1Init( timhandler->tim, &outcap );
	//outcap.TIM_Pulse = ch2Pulse;
	TIM_OC2Init( timhandler->tim, &outcap );
	// If timer has 4 channels
	if( IS_TIM_LIST3_PERIPH(timhandler->tim) ){
		//outcap.TIM_Pulse = ch3Pulse;
		TIM_OC3Init( timhandler->tim, &outcap );
		//outcap.TIM_Pulse = ch4Pulse;
		TIM_OC4Init( timhandler->tim, &outcap );
	}
	
	// TIMx counter enable
	timhandler->tim->CR1 |= TIM_CR1_CEN;	//TIM_Cmd( timhandler->tim, ENABLE );
	
	//if( IS_TIM_LIST4_PERIPH( timhandler->tim )) {
	if( IS_TIM_LIST_ADVANCED_CONTROL( timhandler->tim )) {
		// TIM1/TIM8 Main Output Enable
		TIM_CtrlPWMOutputs( timhandler->tim, ENABLE);
	}
}


/**
 * Make
 * 
 * @param pwmfreq_hz[IN/OUT] : Задать частоту ШИМ, значение будет изменено на принятое, пересчитанное //TODO
 */
void drv_tim_pwm( const TIM_handler_t *timhandler, uint32_t pwmfreq_hz ) 
{
	TIM_TimeBaseInitTypeDef  timebase;
	TIM_OCInitTypeDef  outcap;
	//uint16_t TimerPeriod = 0;
	//uint16_t ch1Pulse = 0, ch2Pulse = 0, ch3Pulse = 0, ch4Pulse = 0;
	
	if( !timhandler )  return;
	///\todo 
	assert_msg("Timer already initialized.\n", !TIMx_APBxClockIsEnabled( timhandler ));
	//assert_msg("Timer already initialized.\n", !(RCC->APB1ENR & timhandler->rcc) );
	RCC_TIMx_APBxClockCmd( *timhandler, ENABLE );
	
	// Задать частоту ШИМ. Период обновления таймера
	timebase.TIM_ClockDivision = TIM_CKD_DIV1;
	drv_tim_AutoFillTimeBase( timhandler, pwmfreq_hz, &timebase );
	timebase.TIM_CounterMode = TIM_CounterMode_Up;
	timebase.TIM_RepetitionCounter = 0;
	TIM_TimeBaseInit( timhandler->tim, &timebase);
	
	/* TIM1 Configuration ---------------------------------------------------
	 * The objective is to generate 7 PWM signal at 17.57 KHz:
	 * 	 - TIM1_Period = (SystemCoreClock / 17570) - 1
	 * The Timer pulse is calculated as follows:
	 * 	 - ChannelxPulse = DutyCycle * (TIM1_Period - 1) / 100
	 *-------------------------------------------------------------------- */
	
	/// Channel 1,2,3 and 4 Configuration in PWM mode
	outcap.TIM_OCMode = TIM_OCMode_PWM2;
	outcap.TIM_OutputState = TIM_OutputState_Enable;
	outcap.TIM_OutputNState = TIM_OutputNState_Enable;
	outcap.TIM_OCPolarity = TIM_OCPolarity_Low;
	outcap.TIM_OCNPolarity = TIM_OCNPolarity_High;
	outcap.TIM_OCIdleState = TIM_OCIdleState_Set;
	outcap.TIM_OCNIdleState = TIM_OCIdleState_Reset;
	
	//outcap.TIM_Pulse = ch1Pulse;
	outcap.TIM_Pulse = 0;
	TIM_OC1Init( timhandler->tim, &outcap );
	
	if( IS_TIM_LIST2_PERIPH(timhandler->tim) ){
		//outcap.TIM_Pulse = ch2Pulse;
		TIM_OC2Init( timhandler->tim, &outcap );
	}
	// If timer has 4 channels
	if( IS_TIM_LIST3_PERIPH(timhandler->tim) ){
		//outcap.TIM_Pulse = ch3Pulse;
		TIM_OC3Init( timhandler->tim, &outcap );
		//outcap.TIM_Pulse = ch4Pulse;
		TIM_OC4Init( timhandler->tim, &outcap );
	}
	
	// TIMx counter enable
	timhandler->tim->CR1 |= TIM_CR1_CEN;	//TIM_Cmd( timhandler->tim, ENABLE );
	
	if( IS_TIM_LIST_ADVANCED_CONTROL( timhandler->tim )) {
		// TIM1/TIM8 Main Output Enable
		TIM_CtrlPWMOutputs( timhandler->tim, ENABLE);
	}
}


/**
 * Установить скважность выхода ШИМ
 */
void drv_tim_pwm_set_out(
	const TIM_handler_t *timhandler, 
	uint8_t ch,							/// Канал 1..4
	uint16_t duty_cycle					/// parts per 10000
){
	uint32_t TimerPeriod;
	volatile uint32_t* ccr;
	TimerPeriod = timhandler->tim->ARR;
	ccr = &(timhandler->tim->CCR1) + (ch-1);
	//ccr += ch-1;
	*ccr = (duty_cycle * (TimerPeriod - 1)) / 10000;
	//timhandler->tim->CCR2 = (uint16_t)((duty_cycle * (TimerPeriod - 1)) / 10000);
}


/**
 * Установаить частоту ШИМ. Влияет на все каналы таймера.
 * @param pwmfreq_hz[IN/OUT] : Задать частоту ШИМ, значение будет изменено на принятое, пересчитанное //TODO
 */
void drv_tim_pwm_set_freq( const TIM_handler_t *timhandler, uint32_t pwmfreq_hz ) 
{
	TIM_TimeBaseInitTypeDef  timebase;
	// Задать частоту ШИМ. Период обновления таймера
	timebase.TIM_ClockDivision = TIM_CKD_DIV1;
	drv_tim_AutoFillTimeBase( timhandler, pwmfreq_hz, &timebase );
	timebase.TIM_CounterMode = TIM_CounterMode_Up;
	timebase.TIM_RepetitionCounter = 0;
	TIM_TimeBaseInit( timhandler->tim, &timebase );
}


/*float round(float f, unsigned n){
}*/

/**
 * Установаить частоту ШИМ. Влияет на все каналы таймера.
 * @param pwmfreq_hz[IN/OUT] : Задать частоту ШИМ, значение будет изменено на принятое, пересчитанное //TODO
 */
void drv_tim_pwm_setFreqFloat( const TIM_handler_t *timhandler, float pwmfreq_hz ) 
{
	TIM_TimeBaseInitTypeDef  timebase;
	// Задать частоту ШИМ. Период обновления таймера
	timebase.TIM_ClockDivision = TIM_CKD_DIV1;
	drv_tim_AutoFillTimeBaseFloat( timhandler, pwmfreq_hz, &timebase );
	timebase.TIM_CounterMode = TIM_CounterMode_Up;
	timebase.TIM_RepetitionCounter = 0;
	TIM_TimeBaseInit( timhandler->tim, &timebase );
}


/**
 * Check if GPIO belongs to HW Timer
 */
inline static bool is_GPIO_belongs_Tim( 
	const TIM_handler_t *timhandler, 
	GPIO_t *gp
){	
	int ch, a;
	for( ch=0; ch<4; ch++ ){
		for( a=0; a<3; a++ ){
			if( gp == timhandler->ch_Pins[ch][a] )
				return true;
		}
	}
	return false;
}


/**
 * Configure GPIOs for Timer.
 * \param 	*timhandler	Таймер, описанный структорой TIM_handler_t.
 * \param 	n    Number of GPIOs to be coonfigured
 * \param 	...  pointers to GPIOs
 * \return 	0 | error(negative) | warning(positive) code
 * 			0 - OK
 * 			positive - number of pins which do not belong to Timer
 * 			negative - ?TODO error
 */
int drv_timer_gpio_va( 
	const TIM_handler_t *timhandler, 
	int n,                            	/// Number of GPIOs
	/*GPIO_t* */ ... 
){	
	va_list vl;
	GPIO_t *gp;
	int i;
	GPIO_InitTypeDef gis;// = {0, GPIO_Mode_AF, GPIO_Speed_100MHz, GPIO_OType_PP, GPIO_PuPd_NOPULL};
	int ret = 0;
	
	gis.GPIO_Speed = GPIO_Speed_100MHz;
	gis.GPIO_OType = GPIO_OType_PP;
	gis.GPIO_PuPd = GPIO_PuPd_NOPULL;
	
	va_start( vl, n );
	for( i=0; i<n; i++ ){
		// Get GPIO from va_list
		gp = va_arg( vl, GPIO_t* );
		
		// Check pin belongs to timer
		//assert_asmg( is_GPIO_belongs_Tim( timhandler, gp ) );
		if( !is_GPIO_belongs_Tim( timhandler, gp ) ){
			ret++;
			continue;
		}
		
		bsp_GpioAf_InitStruct( gp, timhandler->af, &gis );
	}
	va_end(vl);
	
	return ret;
}


/** 
 * Настроить таймер, задающий частоту дискретизации. 
 * Параметры Period, Prescaler, ClockDivision будут выбраны 
 * для обеспечения макс. детализации
 * @param  *timhandler	Таймер, описанный структорой TIM_handler_t.
 * @param  updFreq		Частота, с которой будут происходить события переполнения (Update).
 * @param  irqPrio		Приоритет прерывания события переполнения (Update). Если отрицательное значение прерывание отключено.
 * @note Only pre-emption priorities allowed for this driver.
 */
void drv_tim_init( 
	const TIM_handler_t * timhandler, 
	uint32_t updFreq,
	int8_t updIrqPrio
){	
	TIM_TimeBaseInitTypeDef	timebase = TIM_TimeBaseInitStruct_Default;
	//TIM_ICInitTypeDef		incap;	// Input Capture management structure
	//TIM_OCInitTypeDef		outcap;	// Output Capture management structure
	//TIM_BDTRInitTypeDef 	bdtr;
	NVIC_InitTypeDef nvic; 			// Структура для конфигурации регистров прерываний
	
	assert_amsg(timhandler);
	
	// Enable peripheral clock
	RCC_TIMx_APBxClockCmd(*timhandler, ENABLE);
	TIM_DeInit( timhandler->tim );
	
	// Time base configuration 
	//TIM_TimeBaseStructInit(&timebase);
	drv_tim_AutoFillTimeBase( timhandler, updFreq, &timebase );
	//timebase.TIM_Prescaler = 2-1; //prescaler(timhandler.tim, freq);
	//timebase.TIM_Period = 84000000 / (timebase.TIM_Prescaler+1) / updFreq -1;
	//timebase.TIM_CounterMode = TIM_CounterMode_Up;
	//timebase.TIM_RepetitionCounter = 0; 	/// only for advanced-control timers TIM1 and TIM8
	TIM_TimeBaseInit( timhandler->tim, &timebase );
	
	// Настроить прерывание по переполнению. 
	if( updIrqPrio > 0 ){
		if( updIrqPrio > 0xF /*BIT_MASK(__NVIC_PRIO_BITS_)*/)
			updIrqPrio = 0xF;
		
		TIM_ClearITPendingBit( timhandler->tim, TIM_IT_Update );
		TIM_ITConfig( timhandler->tim, TIM_IT_Update, ENABLE);
		
		nvic.NVIC_IRQChannel = timhandler->updIrqN;
		nvic.NVIC_IRQChannelPreemptionPriority = updIrqPrio;
		nvic.NVIC_IRQChannelSubPriority = 0;
		nvic.NVIC_IRQChannelCmd = ENABLE;
		NVIC_Init(&nvic);
	}
	
	timhandler->tim->CR1 |= TIM_CR1_CEN;
	//drv_TIM_Cmd(timhandler->tim, ENABLE); 	// Enable counting
}


/**
 * Механизм вычисления оптимальных Prescaler и Period.  
 * Prescaler будет минимальным. Так точнее. 
 * Calculate prescaler и period for given timer. Fill the timebase struct.
 * \note algorithm tries to make prescaler as small as posible, to achive max accuracy.
 * \param[IN]      *timhandler   pointer to HW Timer
 * \param[IN]       updFreq      Desired timer update event frequency
 * \param[IN/OUT]  *timebase     TIM_ClockDivision is input value, always read-only,
 * 							     TIM_Prescaler and TIM_Period will be filled with calculated values.
 */
void drv_tim_AutoFillTimeBase( 
	const TIM_handler_t * timhandler, 
	uint32_t updFreq, 
	TIM_TimeBaseInitTypeDef *timebase 
){
	/*volatile*/ uint32_t basefrq, period, division =1;
	/*volatile*/ uint16_t prescaler = 1;
	/*volatile*/ uint8_t round = 0;
	
	division =   timebase->TIM_ClockDivision == TIM_CKD_DIV4 ? 4 : 
				(timebase->TIM_ClockDivision == TIM_CKD_DIV2 ? 2 :
															   1 );
	basefrq = TIM_MAX_FREQUENCY(timhandler->tim) / division;
	period = basefrq / updFreq;
	
	if( TIM_HAS_16bit_COUNTER(timhandler->tim) 
		&& period > UINT16_MAX //timebase->TIM_Period > UINT16_MAX 
	){
		prescaler = period / UINT16_MAX + 1;
		round = (period % prescaler) >= (prescaler/2) ? 1 : 0;
		period = period / prescaler + round;
	}
	
	timebase->TIM_Prescaler = prescaler -1;
	timebase->TIM_Period = period>0 ? period-1 : 0;
}


#include <math.h>
/**
 * Выч
 * Механизм вычисления оптимальных Prescaler и Period. Prescaler должен 
 * быть минимальным. Так точнее. 
 * Calculate prescaler и period for given timer. Fill the timebase struct.
 * \note algorithm tries to make prescaler as small as posible, to achive max accuracy.
 * \param[IN]      *timhandler   pointer to HW Timer
 * \param[IN]       updFreq      Desired timer update event frequency
 * \param[IN/OUT]  *timebase     TIM_ClockDivision is input value read-only,
 * 							     TIM_Prescaler and TIM_Period will be filled with calculated values.
 */
void drv_tim_AutoFillTimeBaseFloat( 
	const TIM_handler_t * timhandler, 
	float updFreq, 
	TIM_TimeBaseInitTypeDef *timebase 
){
	float basefrq, period; float division =1;
	uint16_t prescaler = 1;
	
	division =   timebase->TIM_ClockDivision == TIM_CKD_DIV4 ? 4 : 
				(timebase->TIM_ClockDivision == TIM_CKD_DIV2 ? 2 :
															   1 );
	basefrq = (float)TIM_MAX_FREQUENCY(timhandler->tim) / division;
	period = basefrq / updFreq;
	
	if( TIM_HAS_16bit_COUNTER(timhandler->tim) 
		&& period > UINT16_MAX 
	){
		prescaler = ceil( period / UINT16_MAX );
		period /= prescaler;
	}
	
	timebase->TIM_Prescaler = prescaler -1;
	timebase->TIM_Period = roundf(period); //period>0 ? period-1 : 0;
}


/// Расчет предделителя для конкретного таймера по заданной частоте
/** @brief  Compute the prescaler value for Timer.
  * @param  tim		The timer. For example TIM1.
  * @param  tim		The base frequency of tim. Must not be more than tim's max allowed base frequency.
  * @retval Computed prescaler value.
  */
uint16_t drv_tim_prescaler( TIM_TypeDef* tim, uint32_t freq) 
{
	if( IS_TIM_LIST_APB2_168MHz( tim ) ) {
		assert_amsg( freq <= 1680000000);
		return (uint16_t)( (SystemCoreClock) / freq) - 1;		// TIM1,8,9,10,11
	} else {
		assert_amsg( freq <= 1680000000 /2 );
		return (uint16_t)( (SystemCoreClock /2) / freq) - 1;	// TIM2,3,4,5,6,7,12,13,14
	}
}

