/** 
 * \file    Sledge/drv/stm32/stm32_rtc_sledge.c
 * \author 	Ivan "Kyb Sledgehammer" Kuvaldin <i.kyb[2]ya,ru>
 * \version	0.1
 */

#include "./sledge_stm32_rtc.h"

#include <time.h>
//#include <stdbool.h>

#include "mainConfig.h"

#include "Sledge/utils.h"
#define DEBUG_LEVEL 0
#include "Sledge/debug.h"

#include "stm32f4xx_rcc.h"
#include "stm32f4xx_rtc.h"
#include "stm32f4xx_pwr.h"


/* Private macros */
/* Internal status registers for RTC. This is selected by us, not by ST */
#if defined(STM32F7xx)
 #define RTC_STATUS_REG      			RTC_BKP_DR31 /* Status Register */
#else
 #define RTC_STATUS_REG      			RTC_BKP_DR19 /* Status Register */
#endif

#define RTC_STATUS_INIT_OK              0x1234       /* RTC initialised */
#define RTC_STATUS_TIME_OK              0x4321       /* RTC time OK */
#define	RTC_STATUS_ZERO                 0x0000


enum rtc_status {
	rtc_status_UNDEFINED, 
	rtc_status_WAS_RESET, // часы были сброшены и все настройки тоже.
	rtc_status_WORKING, 
};
volatile enum rtc_status rtc_status = rtc_status_UNDEFINED;


/** Conversation macros between STM32 Standart Peripheral Driver time&date and struct tm from <time.h> */
#define CONVERT_STTimeDate2structTM(time, date, tm)	 	do{ (tm).tm_sec  = (time).RTC_Seconds;   \
                                                            (tm).tm_min  = (time).RTC_Minutes;   \
                                                            (tm).tm_hour = (time).RTC_Hours;     \
                                                            (tm).tm_wday = (date).RTC_WeekDay-1; \
                                                            (tm).tm_mday = (date).RTC_Date;      \
                                                            (tm).tm_mon  = (date).RTC_Month - 1; \
                                                            (tm).tm_year = (date).RTC_Year+100;  \
															/*(tm).tm_isdst = (date).*/ \
														}while(0)
#define CONVERT_structTM2STTimeDate(tm, time, date)	 	do{ (time).RTC_Seconds = (tm).tm_sec;	  \
															(time).RTC_Minutes = (tm).tm_min;	  \
															(time).RTC_Hours   = (tm).tm_hour;	  \
															(date).RTC_WeekDay = (tm).tm_wday+1;  \
															(date).RTC_Date    = (tm).tm_mday;	  \
															(date).RTC_Month   = (tm).tm_mon + 1; \
															(date).RTC_Year    = (tm).tm_year-100;	  \
														}while(0)


/**
 * \return 	1 already initialized
 * 			0 just intialized
 */
int drv_rtc_init(void)
{	
	int ret = -1;
	RTC_InitTypeDef rtcis = { RTC_HourFormat_24, /*asynch*/127, /*synch*/255 };  // initialize with default values for LSE
	struct tm default_time = {DEFAULT_TIME};
	//uint32_t status;
	
	RCC_APB1PeriphClockCmd( RCC_APB1Periph_PWR, ENABLE );  //__HAL_RCC_PWR_CLK_ENABLE();
	// Allow access to configurations registers of BKP(Backup) module. Must be done before RCC_RTCCLKConfig().
	PWR_BackupAccessCmd(ENABLE);  //HAL_PWR_EnableBkUpAccess();
	
  #ifdef NO_LSE
	// Always initialize LSI, if no LSE, because it may fail after uC reset
	// Enable LSI (low speed internal 32kHz clock)
	RCC_LSICmd(ENABLE);
	// Wait on LSIRDY flag to be set indicating that LSI clock is stable
	while( RCC_GetFlagStatus(RCC_FLAG_LSIRDY) == RESET );
	// Set LSI as clock for RTC, start RTC
	RCC_RTCCLKConfig(RCC_RTCCLKSource_LSI);
	// Set frequency predeviders (refer to AN3711 p.9)?
	rtcis.RTC_AsynchPrediv = 127;
	rtcis.RTC_SynchPrediv = 249;
  #endif
	
	// Check if RTC already initialized
	/* After a system reset, the application can read the INITS flag in the RTC_ISR register 
	 * to check if the calendar has been initialized or not. If this flag equals 0, the calendar 
	 * has not been initialized since the year field is set at its backup domain reset default value (0x00). */
	// Oblique way to determine if rtc initialied
	/* if( (RCC->BDCR & RCC_BDCR_RTCEN)  // RCC Backup domain control register -> RCC enabled
			&& !( (RTC->TR == 0) && (RTC->DR == 0x2101) )
	 		&& !(RTC->ISR & RTC_ISR_INITS) ){ */
	/*status = RTC_ReadBackupRegister(RTC_STATUS_REG); //HAL_RTCEx_BKUPRead(&hRTC, RTC_STATUS_REG);
	if( status != RTC_STATUS_TIME_OK )*/
	if( !(RTC->ISR & RTC_ISR_INITS) )
	{
		rtc_status = rtc_status_WAS_RESET;
		// Full RTC module reset
		//RCC_BackupResetCmd(ENABLE);
		//RCC_BackupResetCmd(DISABLE);
		
	  #ifndef NO_LSE
		/* Enable LSE (low speed external 32768Hz clock), set LSE as clock for RTC, start RTC */
		RCC_LSEConfig(RCC_LSE_ON);
		/* Wait on LSERDY flag to be set indicating that LSE clock is stable */
		while( RCC_GetFlagStatus(RCC_FLAG_LSERDY) == RESET );
		// Set LSE as clock for RTC, start RTC
		RCC_RTCCLKConfig(RCC_RTCCLKSource_LSE);
		
		rtcis.RTC_AsynchPrediv = 0x7f;	//127
		rtcis.RTC_SynchPrediv = 0xff;	//255
	  #endif
		
  		RCC_RTCCLKCmd(ENABLE);
		
		mktime(&default_time); // normalize, fill calculateable fields
		drv_rtc_SetByStructTM(&default_time);
		PWR_BackupAccessCmd(ENABLE); {
		assert_amsg( RTC_Init(&rtcis) ); //RTC_Init(&rtcis);
		RTC_WriteBackupRegister( RTC_STATUS_REG, RTC_STATUS_TIME_OK );
		} PWR_BackupAccessCmd(DISABLE);
		ret = 0;
	} else {
		rtc_status = rtc_status_WORKING;
		// Wait for RTC APB registers synchronisation (needed after start-up from Reset)
		RTC_WaitForSynchro();
		/* Clear reset flags */
		//__HAL_RCC_CLEAR_RESET_FLAGS();
		ret = 1;
	}
	//RTC_BypassShadowCmd(ENABLE);
	PWR_BackupAccessCmd(DISABLE);
	return ret;
}


/**
 * Set RTC time
 * \param Calendar time as described in STL.
 * \return -1 error. Because struct tm counts from 1900 and RTC counts from 2000 year.
 */
int drv_rtc_set( time_t seconds )
{
	struct tm tm;
	localtime_r(&seconds, &tm);
	if( tm.tm_year < 100 )
		return -1;
	return drv_rtc_SetByStructTM(&tm);
}


/**
 * Set RTC time with microseconds.
 * \see Ref.Man. 26.3.8 RTC Sync
 * \param Calendar time as described in STL.
 * \return -1 error. Because struct tm counts from 1900 and RTC counts from 2000 year.
 */
int drv_rtc_set_sus( time_t seconds, unsigned us )
{
	struct tm tm;
	localtime_r(&seconds, &tm);
	if( tm.tm_year < 100 )
		return -1;
	drv_rtc_SetByStructTM(&tm);
	return 0;
}


int drv_rtc_SetByStructTM(struct tm *tm)
{
	RTC_TimeTypeDef time;
	RTC_DateTypeDef date;

	CONVERT_structTM2STTimeDate( *tm, time, date);

debugf("---drv_rtc_SetByStructTM()\n");
debugf("struct tm: "TIME_FORMAT_STRING"\n", tm->tm_mday, tm->tm_mon+1, tm->tm_year+1900, tm->tm_hour, tm->tm_min, tm->tm_sec);
//debugf("SPD date-time: "TIME_FORMAT_STRING"\n", date.RTC_Date, date.RTC_Month, date.RTC_Year+2000, \
												 time.RTC_Hours, time.RTC_Minutes, time.RTC_Seconds );
	
	PWR_BackupAccessCmd(ENABLE);
	RTC_EnterInitMode(); //assert_amsg( RTC_EnterInitMode() );
	{
	RTC_SetTime(RTC_Format_BIN, &time); 
	RTC_SetDate(RTC_Format_BIN, &date);
	RTC->CR |= RTC_CR_ADD1H | RTC_CR_BCK;  // Set DST
	}
	RTC_ExitInitMode();
	
	/** debug */
/*RTC_WaitForSynchro();*/
RTC_GetTime(RTC_Format_BIN, &time);
RTC_GetDate(RTC_Format_BIN, &date);
debugf("RTC was set: "TIME_FORMAT_STRING".%02d\n", date.RTC_Date, date.RTC_Month, date.RTC_Year+2000, 
												 time.RTC_Hours, time.RTC_Minutes, time.RTC_Seconds, RTC_GetSubSecond());

	PWR_BackupAccessCmd(DISABLE);
	return 0;
}


/**
 * \return 
 * \todo here should be strange and stupid behavior. \see Ref.Man 26.3.6
 */
uint32_t drv_rtc_get(bool waitForSyncro)
{
	return mktime(drv_rtc_getToStructTM(waitForSyncro, NULL));
}

/**
 * \param *ptm poiter to struct tm instanse, which will contain broken-down time and date.
 *        Can be NULL, than internal struct tm used. MUST be non-null for reentrancy.
 * \return pointer to broken-down time.
 */
struct tm* drv_rtc_getToStructTM(bool waitForSyncro, struct tm *ptm)
{
	RTC_TimeTypeDef time;
	RTC_DateTypeDef date;
	struct tm tm;
	
	if( !ptm )
		ptm = &tm;
	
	//PWR_BackupAccessCmd(ENABLE); {
	
	if( waitForSyncro )
		assert_amsg( RTC_WaitForSynchro() );
	
	// Time must be read first
	RTC_GetTime(RTC_Format_BIN, &time);
	RTC_GetDate(RTC_Format_BIN, &date);
	tm.tm_isdst = (RTC->CR & RTC_CR_ADD1H) && (RTC->CR & RTC_CR_BCK); // Daylight saving time. Летнее время
	
	//} PWR_BackupAccessCmd(DISABLE);
	
	CONVERT_STTimeDate2structTM( time, date, *ptm );
	return ptm;
}


#define RTC_PRER_PREDIVS_MASK  BIT_MASK_FROM_TO(0,14) //BIT_MASK(15)
/**
 * Always < 1000000
 * \note SS can be larger than PREDIV_S only after a shift operation. In that case, the correct
 * 		 time/date is one second less than as indicated by RTC_TR/RTC_DR
 * \return 	microseconds
 *  		-1 on error
 */
int32_t drv_rtc_get_us(bool waitForSyncro)
{
	int32_t us = -1;
	uint32_t synchPrediv = RTC->PRER & RTC_PRER_PREDIVS_MASK;
	
	PWR_BackupAccessCmd(ENABLE); {  // надо ли при чтении?
	
	if( waitForSyncro )
		assert_amsg( RTC_WaitForSynchro() );
	
	us = 1000000 * (synchPrediv - RTC_GetSubSecond()) / (synchPrediv + 1);
	
	} PWR_BackupAccessCmd(DISABLE);
	
	return us;
}






/// ======================================
/// Fuck the ST STANDART PERIPHERAL DRIVER 
/// ======================================
/**
  * @brief  Enables or disables the RTC registers write protection.
  * @note   All the RTC registers are write protected except for RTC_ISR[13:8], 
  *         RTC_TAFCR and RTC_BKPxR.
  * @note   Writing a wrong key reactivates the write protection.
  * @note   The protection mechanism is not affected by system reset.  
  * @param  NewState: new state of the write protection.
  *          This parameter can be: ENABLE or DISABLE.
  * @retval None
  */
void drv_rtc_WriteEnable(){
	/* Disable the write protection for RTC registers */
	RTC->WPR = 0xCA;
	RTC->WPR = 0x53;
}

void drv_rtc_WriteDisable() {
	/* Enable the write protection for RTC registers */
	RTC->WPR = 0xFF;
}


//#define RTC_BKPR_OFFSET  0x50
/**
 * \return  0  OK
 *         -1  error. too much data.
 */
int drv_rtc_WriteToBackupRegs( const void *data, uint8_t dwords )
{
	volatile uint32_t *bkp_addr = &(RTC->BKP0R);
	const uint32_t *data32 = data;
	
	// Check the parameters
	if( dwords > /*19*/RTC_STATUS_REG ) 
		return -1;
	
	PWR_BackupAccessCmd(ENABLE);//drv_rtc_WriteEnable();
	for( uint8_t i=0; i<dwords; i++ ){
		//*(&(RTC->BKP0R)+i) = (uint32_t)*data;
		*bkp_addr = *data32;
		bkp_addr++; data32++;
	}
	PWR_BackupAccessCmd(DISABLE);//drv_rtc_WriteDisable();
	return 0;
}


/**
 *
 */
int drv_rtc_ReadFromBackupRegs( void *data, uint8_t dwords )
{
	volatile uint32_t *bkp_addr = &(RTC->BKP0R);
	uint32_t *data32 = data;
	assert_amsg(data != NULL);
	
	if( rtc_status == rtc_status_WAS_RESET )
		return 1;  // nothing to read
	
	// Check the parameters
	if( dwords > /*19*/RTC_STATUS_REG ) 
		return -1;
	
	PWR_BackupAccessCmd(ENABLE);//drv_rtc_WriteEnable();
	for( uint8_t i=0; i<dwords; i++ ){
		*data32 = *bkp_addr;
		bkp_addr++; data32++;
	}
	PWR_BackupAccessCmd(DISABLE);//drv_rtc_WriteDisable();
	return 0;
}

