/**
 * Sledge - wide functionality library, suitable for embedded systems, 
 * mainly for ARM Cortex and more particularly for STM32 family
 * 
 * Copyright (c) 2015, Ivan Kuvaldin. All rights reserved.
 * 
 * Licensed under BSD or MIT. 
 * Please refer to Licence.BSD.txt or Licence.MIT.txt provided with these sources.
 * You may obtain a copy of the Licenses at
 *      http://opensource.org/licenses/bsd-license.php
 *      http://opensource.org/licenses/mit-license.php
 */
 
/**
 * \file	Sledge/bsp/stm32_gpio_new.h
 * \author 	Ivan "Kyb Sledgehammer" Kuvaldin <i.kyb[2]ya,ru>
 * \version	1.0.1
 * \date	2015-feb-18
 * \brief	This file contains definitions for GPIOs of STM32. New representation
 *			
 *			Новое представление	и способ работы с GPIO. Константы, дефайны, 
 *			адреса портов объединены в структуры. 
 * 			Маленький шаг в сторону ООП. Удобнее.
 *			Данная реализация поглощает больше flash-памяти.
 *
 * \Changes
 *	ver.1.0.1, date.2015-feb-18
 *	 *	#define _Pxx_ moved to .c file.
 * 	ver.1.0.0, date.2014-aug-23
 *	 + 	Initial implementation.
 *
 * \ToDo
 *	+ Добавить #ifdef <модель> для разных серий котроллеров. Исключить порты, 
 *	  которых нет в данной модели во время компиляции.
 */
  
#ifndef __stm32_USART_sledge_H
#define __stm32_USART_sledge_H

#ifdef __cplusplus
 extern "C" {
#endif


#include "stm32f4xx.h"
//#include <stdbool.h>
	 




#ifdef __cplusplus
}
#endif

#endif /* __stm32_USART_sledge_H */
