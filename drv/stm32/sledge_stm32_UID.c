/**
 * Sledge - wide functionality library, suitable for embedded systems, 
 * mainly for ARM Cortex and more particularly for STM32 family
 * 
 * Copyright (c) 2015, Ivan Kuvaldin. All rights reserved.
 * 
 * Licensed under BSD or MIT. 
 * Please refer to Licence.BSD.txt or Licence.MIT.txt provided with these sources.
 * You may obtain a copy of the Licenses at
 *      http://opensource.org/licenses/bsd-license.php <br/>
 *      http://opensource.org/licenses/mit-license.php <br/>
 */
/**
 * @file    Sledge/drv/stm32/stm32_uid_sledge.c
 * @author 	Ivan "Kyb Sledgehammer" Kuvaldin <i.kyb[2]ya,ru>
 * Look at ./uid.h for more information.
 */

#include "Sledge/hal//uid.h"
//#include <stdint.h>


/// Address in STM32 memory
#define UNIQUE_ID_BASE_STM32 	((uint32_t)0x1FFF7A10)

/** Once written. READ-OLNY */
const UniqueID_t * const UID96 = (UniqueID_t*)UNIQUE_ID_BASE_STM32;
static /*volatile const*/ uint32_t _uid32 = 0;
static /*volatile const*/ uint16_t _uid16 = 0;
static /*volatile const*/ uint8_t  _uid8  = 0;


/**
 * Calculates and stores all UIDs.
 */
void uid96toAll(void) {
	uid96to32();
	uid96to16();
	uid96to8();
}


/**
 * Calculates UID 96bit to 32bit constant using XOR, if not calculated yet.
 * Function stores calculated value to _uid32. It is global and may be used directly.
 * @retval 32-bit UID.
 */
uint32_t uid96to32() 
{
	if (_uid32==0) 
		_uid32 = UID96->uidArr[0] ^ UID96->uidArr[1] ^ UID96->uidArr[2];
	return _uid32;
}


/**
 * Calculates and stores UID 96bit to 16bit constant using XOR.
 * @retval 16-bit UID.
 */
uint16_t uid96to16() 
{
	const uint16_t* p = (uint16_t*)UNIQUE_ID_BASE_STM32;
	uint8_t i=1; 
	
	if (_uid16==0) {
		_uid16 = *p;
		for (; i < sizeof(UniqueID_t)/sizeof(_uid16); i++)
			_uid16 ^= *(p+i);
	}
	return _uid16;
}


/**
 * Get calculated value of 8-bit UID. 
 * Calculates 8bit UID from and 96bit UID using XOR and stores it.  
 * @note 	Used as last byte of IP-addr
 * @retval	8-bit UID
 */
/*const*/ uint8_t uid96to8() 
{
	const uint8_t* p = (uint8_t*)UNIQUE_ID_BASE_STM32;
	uint8_t i=1; 
	
	if (_uid8==0) {
		_uid8 = *p;
		for (; i < sizeof(UniqueID_t)/sizeof(_uid8); i++)
			_uid8 ^= *(p+i);
	}
	return _uid8;
}
