/**
 * Sledge - wide functionality library, suitable for embedded systems, 
 * mainly for ARM Cortex and more particularly for STM32 family
 * 
 * Copyright (c) 2015, Ivan Kuvaldin. All rights reserved.
 * 
 * Licensed under BSD or MIT. 
 * Please refer to Licence.BSD.txt or Licence.MIT.txt provided with these sources.
 * You may obtain a copy of the Licenses at
 *      http://opensource.org/licenses/bsd-license.php
 *      http://opensource.org/licenses/mit-license.php
 */

/**
 * @file 	Sledge/drv/stm32/stm32_adc_sledge.c
 * @author 	Ivan "Kyb Sledgehammer" Kuvaldin <i.kyb[2]ya,ru>
 * @brief 	Look at ./adc.h for more information
 */

//#include "stm32f4xx_adc.h"
#include "./sledge_stm32_adc.h"
#include "Sledge/bsp.h"
#include "Sledge/assert.h"


#define adc_APB2ClockCmd(adc, cmd) 		RCC->APB2ENR = (cmd) ? RCC->APB2ENR | (adc)->rcc  \
															 : RCC->APB2ENR & ~(adc)->rcc

const ADC_t ADCs[3] = {
	{ ADC1, RCC_APB2ENR_ADC1EN },
	{ ADC2, RCC_APB2ENR_ADC2EN },
	{ ADC3, RCC_APB2ENR_ADC3EN },
};


/** ┌———————————————————————————————————————————————————————————————————————┐
	│                                                                       │  
	└———————————————————————————————————————————————————————————————————————┘ */
/**
 * @brief  Initialize ADCx peripheral
 * @param  *ADCx: ADCx peripheral to initialize
 * @retval None
 */
void adc_init( ADC_t* a ) 
{
	ADC_InitTypeDef ais;
	ADC_CommonInitTypeDef acis;

	/// Init ADC settings
	ais.ADC_Resolution = 0; //ADC_Resolution_12;
	ais.ADC_ContinuousConvMode = DISABLE;
	ais.ADC_DataAlign = ADC_DataAlign_Right;
	ais.ADC_ExternalTrigConv = DISABLE;
	ais.ADC_ExternalTrigConvEdge = ADC_ExternalTrigConvEdge_None;
	ais.ADC_NbrOfConversion = 1;
	ais.ADC_ScanConvMode = DISABLE;

	/// Enable ADC clock
	adc_APB2ClockCmd(a, ENABLE); 	//RCC->APB2ENR |= a->rcc;

	/// Set common ADC settings
	acis.ADC_DMAAccessMode = ADC_DMAAccessMode_Disabled;
	acis.ADC_Mode = ADC_Mode_Independent;
	acis.ADC_Prescaler = ADC_Prescaler_Div4;
	acis.ADC_TwoSamplingDelay = ADC_TwoSamplingDelay_8Cycles;
	ADC_CommonInit( &acis );
	
	/// Initialize ADC
	ADC_Init( a->regs, &ais);
	
	/// Enable ADC
	a->regs->CR2 |= ADC_CR2_ADON;
}


/**
 * @brief  Read Vbat pin voltage
 * @param  *ADCx: ADCx peripheral to use for Vbat measurement
 * @retval voltage in mV
 */
uint16_t adc_ReadVbat(ADC_TypeDef* ADCx) 
{
	uint32_t result;
	
	/// Start conversion
	ADC_RegularChannelConfig(ADCx, ADC_Channel_Vbat, 1, ADC_SampleTime_112Cycles);
	
	/* Start software conversion */
	ADCx->CR2 |= ADC_CR2_SWSTART;
	
	/* Wait till done */
	while (!(ADCx->SR & ADC_SR_EOC));
	
	/* Get result */
	result = ADCx->DR;
	/// Convert to voltage */
	//result = result * ADC_VBAT_MULTI * ADC_SUPPLY_VOLTAGE / 0xFFF;
	
	/* Return value in mV */
	return (uint16_t) result;
}
