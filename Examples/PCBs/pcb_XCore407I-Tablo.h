/**
 * Sledge - wide functionality library, suitable for embedded systems, 
 * mainly for ARM Cortex and more particularly for STM32 family
 * 
 * Copyright (c) 2015, Ivan Kuvaldin. All rights reserved.
 * 
 * Licensed under BSD or MIT. 
 * Please refer to Licence.BSD.txt or Licence.MIT.txt provided with these sources.
 * You may obtain a copy of the Licenses at
 *      http://opensource.org/licenses/bsd-license.php
 *      http://opensource.org/licenses/mit-license.php
 */

/**
 * \file 	pcb_XCore407I.h
 * \author 	Ivan "Kyb Sledgehammer" Kuvaldin <i.Kyb[2]ya,ru>
 * \brief 	This file defines pin lists and their appointments.
 * 			Constatants, pointers, arrays and all other C features 
 * 			are implemented in bsp_pcb.c
 */


#ifndef __pcb_XCore407I_Tablo_H
#define __pcb_XCore407I_Tablo_H


/** ┌———————————————————————————————————————————————————————————————————————┐
	│						Core start up parameters						│  
	└———————————————————————————————————————————————————————————————————————┘ */
#define HSE_Crystal_MHz  8
#define PLL_M 	HSE_Crystal_MHz


/** ┌———————————————————————————————————————————————————————————————————————┐
	│						Outouts & LEDs control							│  
	└———————————————————————————————————————————————————————————————————————┘ */
/** List of regular LEDs */
#define _LEDS_ 	{ PH6, PH7, PH8, PH9, PE14, PE15 }

#define LED_Green 	PH8
#define LED_Orange 	PH6 //PH10
#define LED_Red		PH9
#define LED_Blue 	PH7

/** List of LEDs with controllable brightness. 
 * 	@note Must be connected to TIMx channels. Look at alt. functions of GPIOs
 */
//#define _LEDS_PWM_ 	{ , }
#define LEDS_PWM_TIM_HANDLER	TIMS[4]
#define LEDS_PWM_TIM_FREQ		500000
#define LEDS_PWM_TIM_PERIOD 	( (SystemCoreClock / LEDS_PWM_TIM_FREQ) - 1 )

/** List of RGB LEDs with controllable brightness. 
 * 	@note Must be connected to TIMx channels. Look at alt. functions of GPIOs
 */
#define _LEDS_RGB_ 	{ 	/*Red │Green│ Blue│ common│         */ \
						{ PH10, PH11, PH12,      1, &TIMS[5] }, \
					}


/** ┌———————————————————————————————————————————————————————————————————————┐
	│					Segment indicators GPIOs							│  
	└———————————————————————————————————————————————————————————————————————┘ */
#if PCB_XCORE407I == 3 	/// XCore407I Tablo v1

/** Список пинов, управляющих общими анодами(катодами). Определяет количество индикаторов.
 *  @note если работаем с 5V внимательно смотреть в даташит на предмет совместимости ноги с 5В.
 */
 #define _SegmentGpios_ { PF1, PF2, PF3, PF4, PF5, PF6, PF7, PF0 }

/** Список ножек, управляющих сегментами. Определяет количество сегментов
 * 	7 в обычном, 8 с точкой, 14 с Ж внутри, 16 + по 2 сегмента сверху и снизу
 *  @note если работаем с 5V внимательно смотреть в даташит на предмет совместимости ноги с 5В.
 */
 #define _CommonGpios_ 	{ PG2, PG3, PG4, PG5, PG6, }


#elif PCB_XCORE407I == 32 	/// XCore407I Tablo v2
 
 #define _SegmentGpios_ { PF1, PF0, PF7, PF6, PF5, PF3, PF4, PF2, }
 #define _CommonGpios_	{ PG2, PG3, PG4, PG5, PG6, }
 
#else
 #error
#endif /*PCB_XCORE407I*/

#define SegInd_Timer 			TIMS[12]
#define SegInd_TimerIRQHandler 	TIM8_BRK_TIM12_IRQHandler
 

/** ┌———————————————————————————————————————————————————————————————————————┐
	│				Inputs, Buttons and joystick control					│  
	└———————————————————————————————————————————————————————————————————————┘ */
/** List of buttons */
//#define _BUTTONS_ 	{ PB0 }


/** ┌———————————————————————————————————————————————————————————————————————┐
	│							COM ports									│  
	└———————————————————————————————————————————————————————————————————————┘ */
						/* USART│          RCC         │     IRQn   │ TX │  RX │     GPIO_AF    │      DMA      */
#define _COM_debug_ 	{ USART1, RCC_APB2Periph_USART1, USART1_IRQn, PB6,  PB7,  GPIO_AF_USART1, USART1_DMA_BSP }
#define _COM_RS485_ 	{ USART6, RCC_APB2Periph_USART6, USART6_IRQn, PС6,  PС7,  GPIO_AF_USART6, USART6_DMA_BSP }


/** ┌———————————————————————————————————————————————————————————————————————┐
	│							Ethernet									│  
	└———————————————————————————————————————————————————————————————————————┘ */
#define ETH_LINK_GPIO 	PB14


#endif /* __pcb_XCore407I_Tablo_H */
