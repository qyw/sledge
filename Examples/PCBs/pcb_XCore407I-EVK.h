/**
 * Sledge - wide functionality library, suitable for embedded systems, 
 * mainly for ARM Cortex and more particularly for STM32 family
 * 
 * Copyright (c) 2015, Ivan Kuvaldin. All rights reserved.
 * 
 * Licensed under BSD or MIT. 
 * Please refer to Licence.BSD.txt or Licence.MIT.txt provided with these sources.
 * You may obtain a copy of the Licenses at
 *      http://opensource.org/licenses/bsd-license.php
 *      http://opensource.org/licenses/mit-license.php
 */

/**
 * \file 	pcb_XCore407I.h
 * \author 	Ivan "Kyb Sledgehammer" Kuvaldin <i.Kyb[2]ya,ru>
 * \brief 	This file defines pin lists and their appointments.
 * 			Constatants, pointers, arrays and all other C features 
 * 			are implemented in bsp_pcb.c
 */


#ifndef __pcb_XCore407I_EVK_H
#define __pcb_XCore407I_EVK_H


/** ┌———————————————————————————————————————————————————————————————————————┐
	│						Main start up parameters						│  
	└———————————————————————————————————————————————————————————————————————┘ */
#define HSE_Crystal_MHz  8
#define PLL_M 	HSE_Crystal_MHz


/** ┌———————————————————————————————————————————————————————————————————————┐
	│						Outouts & LEDs control							│  
	└———————————————————————————————————————————————————————————————————————┘ */
/** List of regular LEDs */
#define _LEDS_ 	{ PH2, PH3, PI8, PI10 }

#define LED_Green 	PH2
#define LED_Orange 	PH3
#define LED_Red		PI8
#define LED_Blue 	PI10

#define LedALARM 	LED_Red
#define LedWARNING 	LED_Red
#define LedSD 		LED_Blue
#define LedSENSOR 	LED_Orange

/** List of LEDs with controllable brightness. 
 * 	@note Must be connected to TIMx channels. Look at alt. functions of GPIOs
 */
//#define _LEDS_PWM_ 	{ , }

/** List of RGB LEDs with controllable brightness. 
 * 	@note Must be connected to TIMx channels. Look at alt. functions of GPIOs
 */
//#define _LEDS_RGB_ 	{ 	/*Red │Green│ Blue│ common│         */ \
						{ PH10, PH11, PH12,      1, &TIMS[5] }, \
					}


/** ┌———————————————————————————————————————————————————————————————————————┐
	│					Segment indicators GPIOs							│  
	└———————————————————————————————————————————————————————————————————————┘ */
/** Список пинов, управляющих общими анодами(катодами). Определяет количество индикаторов.
 *  @note если работаем с 5V внимательно смотреть в даташит на предмет совместимости ноги с 5В.
 */
//#define _SegmentGpios_ { PF1, PF2, PF3, PF4, PF5, PF6, PF7, PF0 }

/** Список ножек, управляющих сегментами. Определяет количество сегментов
 * 	7 в обычном, 8 с точкой, 14 с Ж внутри, 16 + по 2 сегмента сверху и снизу
 *  @note если работаем с 5V внимательно смотреть в даташит на предмет совместимости ноги с 5В.
 */
//#define _CommonGpios_ 	{ PG2, PG3, PG4, PG5, PG6, }


/** ┌———————————————————————————————————————————————————————————————————————┐
	│				Inputs, Buttons and joystick control					│  
	└———————————————————————————————————————————————————————————————————————┘ */
/** List of buttons */
#define _BUTTONS_ 	{ PE2, PE3, PE4, PE5, PE6, }
#define JOY_UP 		PE3
#define JOY_DOWN 	PE4
#define JOY_LEFT 	PE2
#define JOY_RIGHT 	PE5
#define JOY_PUSH 	PE6


/** ┌———————————————————————————————————————————————————————————————————————┐
	│							COM ports									│  
	└———————————————————————————————————————————————————————————————————————┘ */
						/* USART│          RCC         │     IRQn   │ TX │  RX │     GPIO_AF    │      DMA       */
#define _COM_debug_ 	{ USART1, RCC_APB2Periph_USART1, USART1_IRQn, PB6,  PB7,  GPIO_AF_USART1, USART1_DMA_BSP }
//#define _COM_RS485_ 	{ USAR41, RCC_APB2Periph_USART4, USART4_IRQn, PС6,  PС7,  GPIO_AF_USART4, USART4_DMA_BSP };


/** ┌———————————————————————————————————————————————————————————————————————┐
	│								LCD										│  
	└———————————————————————————————————————————————————————————————————————┘ */
//#define USE_LCD 	ILI_9129


#endif /* __pcb_XCore407I_EVK_H */
