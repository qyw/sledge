/*******************************************************************************
  * File Name          : sdio.h
  * Date               : 06/09/2014
  * Description        : This file provides code for the configuration
  *                      of the SDIO instances.
  *****************************************************************************/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __sd_H
#define __sd_H
#ifdef __cplusplus
 extern "C" {
#endif


//#include "semphr.h"


#define SD_DISKNAME		"SD:" //"0:"


extern volatile SemaphoreHandle_t writeToSDSemaphore;

void sd_InsertedTaskStart(void);
void sd_InsertedTaskStop(void);
extern int sd_VmpLoggerTaskStart(void);
extern int sd_VmpLoggerTaskStop(void);

	 
#ifdef __cplusplus
}
#endif
#endif /*__sd_H */
