/**
 * 
 *
 *
 *
 *
 */

#include "ff.h"
#include "Sledge/FatFS/fatfs_extension.h"
#include "drivers/fatfs_sd_sdio.h"

/** FreeRTOS includes */
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"

#include "./sd.h"
#include "Sledge/bsp.h"
#include "Sledge/c11n/c11n.h"
#include "Sledge/hal/uid.h"
#include "Sledge/utils.h"
#include "minini_12b/minIni.h"

#include <stdio.h>
#include <string.h>

/** ==== DEBUG ==== */
#define DEBUG_LEVEL  SD_DEBUG_LEVEL
#include "Sledge/debug.h"
#include "Sledge/assert.h"


#define MAX_FILE_SIZE			(uint32_t)( 10 * 1024 * 1024 )		/// 10MB
#define MAX_PACKETS_IN_FILE		(uint32_t)( MAX_FILE_SIZE / sizeof(vmpPackage_t) )
//#define MAX_FILES				10
#define FLUSH_PERIOD_PACKETS	5		// at 250 values in packet and 5000kHz freqM rate this will be about 250ms


volatile SemaphoreHandle_t 	writeToSDSemaphore = NULL,
							sd_InsertedSemaphore = NULL;
static SemaphoreHandle_t vmpLoggerStopSemaphore = NULL;
//static FATFS sdFatFs;
static TaskHandle_t sd_VmpLoggerTaskHandle = NULL;


extern int test_minIni(void);
void /*TaskHandle_t*/ sd_Inserted_task( void* opts );



//#define MAX_LINE_LENGTH  80

static int iniCallback(const char *section, const char *key, const char *value, const void *userdata)
{
	const Parameter_t *parameter;
	int ret;
	//char //response[100],
	//	 cmdline[80];
	(void)userdata; 	/// this parameter is not used in this example
	
	debugf("iniCallback: [%s] %s=%s \t", section, key, value);
	
	parameter = c11n_findParameter( section, key );
	if( !parameter ){
		debugf("Parameter not found.\n");
		return -1;
	}
	
	ret = c11n_setFromString( parameter, value );
	switch(ret){
		case 0:   debugf("Set OK.\n"); break;
		case 1:   debugf("Value reached parameter's limit. Parameter was set.\n"); break;
		case -1:  debugf("F! No valid data found. Parameter was not set.\n"); break;
		case -2:  debugf("F! Parameter is boolean. Use true or false.\n"); break;
		case -3:  debugf("F! Parameter is read-only (has no setter).\n"); break;
		default:  debugf("F! Unknown response.\n"); break;
	}
	
	return 1;
}


int iniReader( void )
{	
	int ret;
	//const char inifile[] = "NCD.ini";
	//char str[100];
	//int s, k;
	//char section[50];
	
	/* section/key enumeration */
	//for (s = 0; ini_getsection(s, section, sizeofarr(section), inifile) > 0; s++) {
	//	printf("    [%s]\n", section);
	//	for (k = 0; ini_getkey(section, k, str, sizeofarr(str), inifile) > 0; k++) {
	//		printf("\t%s\n", str);
	//	}
	//}
	
	/// Browsing through the file
	ret = ini_browse( iniCallback, NULL/*userdata*/, "NCD.ini" );
	if( ret == 1 )
		return 0;
	else{
		debugf("iniReader: cannot read ini file"NEWLINE);
		///\todo Create new default
		return -1;
	}
}


/**
 * Periodically syncronize parameters
 * 
 */
void sd_ConfigFile_task( void* opts /*char* filename*/ )
{
	//const TickType_t/*uint32_t*/ delay = 3000 / portTICK_PERIOD_MS;
	
	while(1){
		//test_minIni();
		iniReader();
		vTaskDelete(NULL);
		//vTaskSuspend( NULL );
		//vTaskDelay( delay );
	}
}


void /*TaskHandle_t*/ sd_Inserted_task( void* opts )
{
	bool inserted, prev_inserted = false;
	int i;
		
	(void) opts;
	//if(!sd_InsertedSemaphore)  sd_InsertedSemaphore = xSemaphoreCreateBinary();
	//assert_amsg( sd_InsertedSemaphore != NULL );
	
	f_SD_ConfigureDetectPin();
	f_SD_ConfigureWriteProtectPin();
	
	while(1){
		inserted = SD_Detect();
		if( inserted != prev_inserted ){
			//prev_inserted = inserted;
			// Make sure that fingers lifted from card. Debounce
			for( i=9; i>0 && inserted == SD_Detect(); --i ){
				vTaskDelay( 33 / portTICK_PERIOD_MS );
			}
			
			if( i == 0 ){
			
				if( inserted ){  //if( SD_Detect() ){  //
					debugf("SD card inserted. Try to mount... \t");
					switch( f_imount( 0, "sd:/", 1/*0*/ ) ){
						case 0:  debugf("OK.\n");
						case -1: debugf("Deferred.\n");
						case -2: debugf("Already.\n");
						default: debugf("ERROR!\n");
					}
					bsp_LedOff( LedSD );
					
					//xTaskCreate( sd_ConfigFile_task, "sd_ConfigFile_task", configMINIMAL_STACK_SIZE * 2, \
							/*opts*/NULL, SD_TASK_PRIO+1, NULL/*&sdioTaskHandle*/ );
					iniReader();
					
					///\TODO if( getVmpLoggerAutoStartOnInsertion() )
					switch( sd_VmpLoggerTaskStart() ){
						case 1:  debugf3("sd_Inserted_task(): sd_VmpLogger_task created."NEWLINE); 		  break; 
						case 2:  debugf2("sd_Inserted_task(): sd_VmpLogger_task already exists."NEWLINE); break;
						default: debugf("sd_Inserted_task(): sd_VmpLogger_task error creating."NEWLINE);  break;
					}
				} else {
					debugf("SD card ejected."NEWLINE);
					switch( sd_VmpLoggerTaskStop() ){
						case 0: debugf3("sd_VmpLoggerTask ALREADY deleted"NEWLINE);	break; 
						case 1: debugf3("sd_VmpLoggerTask normal stop"NEWLINE); 	break;
						case 2: debugf2("sd_VmpLoggerTask FORCED delete"NEWLINE); 	break;
					}
					f_umount( "sd:/", 1 );
					bsp_LedOn( LedSD );
				}
			}
		}
		prev_inserted = inserted;
		// Wait interrupt from SD detect pin - SD card inserted or ejected.
		//xSemaphoreTake( sd_InsertedSemaphore, portMAX_DELAY );
		
		// Periodically check SD presence
		vTaskDelay( 100 / portTICK_PERIOD_MS );
	}
}


void /*TaskHandle_t*/ sd_InsertedTaskStart( void )
{
	//TaskHandle_t sdioTaskHandle;
	xTaskCreate( sd_Inserted_task, "sd_Inserted_task", configMINIMAL_STACK_SIZE * 1, \
			/*opts*/NULL, SD_TASK_PRIO+1, NULL/*&sdioTaskHandle*/ );
}


/**
 * Start sd_VmpLogger_task if not already started
 * \param 	*opts	Options
 * \return 	Code (Error code): 
 * 			  1 			Just created.
 * 			  2 			Already created 
 * 			  -1(ERR_MEM) 	Memory error (errCOULD_NOT_ALLOCATE_REQUIRED_MEMORY)
 */
int /*TaskHandle_t*/ sd_VmpLoggerTaskStart( void )
{
	//TaskHandle_t sdioTaskHandle;
	signed portBASE_TYPE /*err_t*/ task_ret;
	int ret;
		
	if( sd_VmpLoggerTaskHandle ){
		/// Already created.
		return 2;
	}
	
	//task_ret = xTaskCreate( sd_ConfigFile_task, "sd_ConfigFile_task", configMINIMAL_STACK_SIZE * 4, \
			/*opts*/NULL, SD_TASK_PRIO, NULL );
	//task_ret = xTaskCreate( sd_VmpLogger_task, "sd_VmpLogger_task", configMINIMAL_STACK_SIZE * 3, \
			/*opts*/NULL, SD_TASK_PRIO, &sd_VmpLoggerTaskHandle );
	
	// Получилось ли создать таск?
	if( task_ret == pdPASS ){
		// Just created.
		ret = 1;	//ERR_OK;
	} else { // например, errCOULD_NOT_ALLOCATE_REQUIRED_MEMORY
		// Error!
		ret = -1;	//ERR_MEM;
	}
	return ret;
}


/** 
 * Delete sd_VmpLoggerTask and free used resourses.
 * \return 	0  ALREADY deleted
 * 			1  normal stop
 * 			2  FORCED delete
 * 			else if any problems
 */
int sd_VmpLoggerTaskStop( void )
{
	int ret;
	bool forceDelete = false;
	
	if( vmpLoggerStopSemaphore ){
		// Послать сигнал завершения задачи
		xSemaphoreGive( vmpLoggerStopSemaphore );
		vTaskDelay( 10 );
		if( vmpLoggerStopSemaphore ){ 
			vSemaphoreDelete(vmpLoggerStopSemaphore);
			vmpLoggerStopSemaphore = NULL;
			forceDelete = true;
		}
	}
	if( sd_VmpLoggerTaskHandle ){
		if( forceDelete == true ){
			//debugf2("sd_VmpLoggerTask FORCED delete"NEWLINE);
			ret = 2;
		} else {
			//debugf2("sd_VmpLoggerTask normal stop"NEWLINE);
			ret = 1;
		}
		portENTER_CRITICAL();
		vTaskDelete(sd_VmpLoggerTaskHandle);
		sd_VmpLoggerTaskHandle = NULL;
		bsp_LedOff( LedSD );
		portEXIT_CRITICAL();
	} else {
		//debugf2("sd_VmpLoggerTask ALREADY deleted"NEWLINE);
		ret = 0;
	}
	if( writeToSDSemaphore ){
		vSemaphoreDelete(writeToSDSemaphore);
		writeToSDSemaphore = NULL;
	}
	return ret;
}
/*bool multiSenderStop(void)
{
	if( multiSender_taskID != NULL ){
		vTaskDelete(multiSender_taskID);
		multiSender_taskID = NULL;
		vSemaphoreDelete( multiSenderSemaphore );
		multiSenderSemaphore = NULL;
		//rawconndelete		//netconn_delete(connSender);
		//rawbufdelete		//netbuf_delete(bufSender);
		bsp_LedOff(LedMULTISENDER);
		
		return true;	/// Stopped
	}
	return false; 	/// Allready stopped. Nothing to stop
}*/

