/**
 * \file 	indication.c
 */

#include <stdint.h>
#include <stdbool.h>
#include <math.h>

#include "FreeRTOS.h"
#include "task.h"

#include "netconf.h"
#include "netif.h"

#include "Sledge.h"
#include "Sledge/segment_indicator/segment_indicator.h"
//#include "Sledge/bsp.h"
//#include "Sledge/hal/watchdog.h"

#include "./indication.h"

//#define DEBUG_LEVEL 0
//#include "Sledge/debug.h"


enum ShowOnTablo_e {
	showOnTabloHz,
	showOnTabloRPM,
	showOnTabloMmS,
	showOnTabloMS2,
	showOnTabloG,
	showOnTabloMkm,
	showOnTabloMil,
};

volatile enum ShowOnTablo_e indicationShowOnTablo = showOnTabloHz; //showOnTabloHz;


/** 
 * @brief	Toggle Blue LED task, if IP-address is assigned by DHCP or static.
 * @param	pvParameters How much ms to delay. 0 will be replaced with default value
 */
void toggleLed_task(void * pvParameters) 
{
	char sindicatorBuf[si_INDICATORS_COUNT*2+1 /*11*/]; 	/// макс 5 символов и 5 точек + '\0'
	uint32_t ms = (uint32_t) pvParameters;
	if( ms == 0)	ms = 500;
	char *format;
	float value = 32.356;
	int startupPause = 1000;
	
	while(1) {
  #if LedBACKGROUND
		/// Check if IP address assigned
		if (xnetif.ip_addr.addr !=0) {
			bsp_LedToggle( LedBACKGROUND );
		}
  #endif
		/// Регулярно обновлять число на индикаторе
		if( startupPause<=0 ){
			if( true ){	//vmpSTATUS_SENSOR_ERROR_MASK
				if( !isnan(value) && !isinf(value) ){
					sprintf( sindicatorBuf, "%6.2f", value );
					si_puts( sindicatorBuf );
				} else {
					si_puts("----.--");
				}
			} else {
				si_puts("Error.");
			}
		}else{
			startupPause-=ms;
		}
		
		watchdog_reload();
		vTaskDelay( ms2ticks(ms) );
	}
}



void LedsPwm_Play_task( void *opts )
{
	volatile uint16_t brightness[4] = {20, 50, 100, 200};
	bsp_LedPwmInitAll();
	while(1) {
		bsp_LedPwmSetBrightness( LEDS[0], brightness[0] );
		bsp_LedPwmSetBrightness( LEDS[1], brightness[1] );
		bsp_LedPwmSetBrightness( LEDS[2], brightness[2] );
		bsp_LedPwmSetBrightness( LEDS[3], brightness[3] );
		vTaskDelay( 2000 );
		bsp_LedPwmSetBrightness( LEDS[0], brightness[3] );
		bsp_LedPwmSetBrightness( LEDS[1], brightness[2] );
		bsp_LedPwmSetBrightness( LEDS[2], brightness[1] );
		bsp_LedPwmSetBrightness( LEDS[3], brightness[0] );
		vTaskDelay( 2000 );
	}
}


void LedRGB_ColorRondo_task( void/*LedRGB_t*/ *led )
{
	HSV_t hsv = {0,255,255};
	RGB_t rgb;
	
	if( led == NULL )
		vTaskDelete(NULL);
	
	while(1){
		hsv.H +=2;
		hsv.H = hsv.H <360 ? hsv.H : 0;
		rgb = HSVtoRGB(hsv);
		
		bsp_LedRGBSetRGB( led, rgb );
		vTaskDelay(20);
	}
}


void indicationShowOnTablo_set(enum ShowOnTablo_e show)
{
	indicationShowOnTablo = show;
}

uint8_t /*enum ShowOnTablo_e*/ indicationShowOnTablo_get()
{
	return (uint8_t)indicationShowOnTablo;
}



#include "Sledge/c11n/c11n.h"

/// Must be sorted in ASCENDING order
const Parameter_t indication_PARAMETERS[] = {
	{"ShowOnTablo" , NULL,   C11N_UINT8, (getterFunction_t) indicationShowOnTablo_set, (setterFunction_t) indicationShowOnTablo_get, showOnTabloHz },
};
const size_t indication_PARAMETERS_COUNT = sizeofarr(indication_PARAMETERS);
