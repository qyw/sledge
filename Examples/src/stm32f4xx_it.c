/**
 * @file    stm32f4xx_it.c 
 * @author  MCD Application Team
 * @version 1.1.0
 * @date    31-July-2013
 * @brief   Main Interrupt Service Routines.
 *          This file provides template for all exceptions handler and 
 *          peripherals interrupt service routine.
 *******************************************************************************
 * @attention
 *
 * <h2><center>&copy; COPYRIGHT 2013 STMicroelectronics</center></h2>
 *
 * Licensed under MCD-ST Liberty SW License Agreement V2, (the "License");
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at:
 *
 *        http://www.st.com/software_license_agreement_liberty_v2
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the License is distributed on an "AS IS" BASIS, 
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


//#include "mainConfig.h"
//#include "stm32f4xx_it.h"

/** Ethernet includes */
#include "stm32f4x7_eth.h"
#include "stm32f4x7_eth_bsp.h"

/* lwip includes */
#include "lwip/sys.h"

/* Scheduler includes */
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"

/// Sledge includes
#include "Sledge/dassel/logger/comport.h"
#include "Sledge/bsp.h"
#include "Sledge/drv/stm32/sledge_stm32_RNG.h"

#include "Sledge/assert.h"



void HASH_RNG_IRQHandler(void) 
{
	RNG_Check_Fault();
	//HASH_CheckFault();
}


/******************************************************************************/
/*								USB device IRQs								  */
/******************************************************************************/
#include "usb_core.h"
#include "usbd_core.h"
#include "usbd_cdc_core.h"
#include "usbd_cdc_vcp.h"

//extern uint8_t Buffer[6];
//extern __IO uint8_t DemoEnterCondition;
//uint8_t Counter  = 0x00;
extern USB_OTG_CORE_HANDLE USB_OTG_dev;
extern uint32_t USBD_OTG_ISR_Handler (USB_OTG_CORE_HANDLE *pdev);

/**
  * @brief  USB OTG FS Wakeup interrupt request handler
  * @param  None
  * @retval None
  */
void OTG_FS_WKUP_IRQHandler(void)
{
	if( USB_OTG_dev.cfg.low_power ){
		/* Reset SLEEPDEEP and SLEEPONEXIT bits */
		SCB->SCR &= ~(uint32_t)(SCB_SCR_SLEEPDEEP_Msk | SCB_SCR_SLEEPONEXIT_Msk);
		
		/* After wake-up from sleep mode, reconfigure the system clock */
		SystemInit();
		USB_OTG_UngateClock( &USB_OTG_dev );
	}
	
	/// External interrupt line 18 Connected to the USB OTG FS Wakeup from suspend even
	EXTI_ClearITPendingBit( EXTI_Line18 );
}

/**
  * @brief  This function handles OTG_HS Handler.
  * @param  None
  * @retval None
  */
void OTG_FS_IRQHandler(void)
{
	USBD_OTG_ISR_Handler( &USB_OTG_dev );
}


#ifdef USB_OTG_HS_DEDICATED_EP1_ENABLED
/**
  * @brief  This function handles EP1_IN Handler.
  * @param  None
  * @retval None
  */
void OTG_HS_EP1_IN_IRQHandler(void)
{
  USBD_OTG_EP1IN_ISR_Handler (&USB_OTG_dev);
}

/**
  * @brief  This function handles EP1_OUT Handler.
  * @param  None
  * @retval None
  */
void OTG_HS_EP1_OUT_IRQHandler(void)
{
  USBD_OTG_EP1OUT_ISR_Handler (&USB_OTG_dev);
}
#endif



/******************************************************************************/
/*							Serial debug handlers							  */
/******************************************************************************/
/// Look at @ref comLowLevel.c
//void SERIAL_DEBUG_PORT_IRQHANDLER() {}


/*	┌-----------------------------------------------------------------------┐
	|						Ethernet handlers								|  
	└-----------------------------------------------------------------------┘ */
extern __IO xSemaphoreHandle s_xSemaphore;
extern __IO xSemaphoreHandle ETH_link_xSemaphore;

/**
 * @brief  This function handles External lines 15 to 10 interrupt request.
 */
void EXTI15_10_IRQHandler(void)
{
	portBASE_TYPE xHigherPriorityTaskWoken = pdFALSE;
	
	// Check ethernet link
	if(EXTI_GetITStatus(ETH_LINK_EXTI_LINE) != RESET) {
		// Give the semaphore to wakeup LwIP task
		xSemaphoreGiveFromISR( ETH_link_xSemaphore, &xHigherPriorityTaskWoken ); 
	}
	// Clear interrupt pending bit */
	EXTI_ClearITPendingBit(ETH_LINK_EXTI_LINE);
	
	// Switch tasks if necessary.
	if( xHigherPriorityTaskWoken != pdFALSE ) {
		portEND_SWITCHING_ISR( xHigherPriorityTaskWoken );
	}
}

/**
 * @brief  This function handles ethernet DMA interrupt request.
 */
void ETH_IRQHandler(void)
{
	portBASE_TYPE xHigherPriorityTaskWoken = pdFALSE;
	
	/** Frame received */
	if ( ETH_GetDMAFlagStatus(ETH_DMA_FLAG_R) == SET) {
		/** Give the semaphore to wakeup Eth ethernet_input task */
		xSemaphoreGiveFromISR( s_xSemaphore, &xHigherPriorityTaskWoken );
	}

	/** Clear the interrupt flags. */
	/** Clear the Eth DMA Rx IT pending bits */
	ETH_DMAClearITPendingBit(ETH_DMA_IT_R);
	ETH_DMAClearITPendingBit(ETH_DMA_IT_NIS);
	
	/** Switch tasks if necessary. */	
	if( xHigherPriorityTaskWoken != pdFALSE ) {
		portEND_SWITCHING_ISR( xHigherPriorityTaskWoken );
	}
}




#ifdef FATFS_USE_DETECT_PIN_INTERRUPT
//#define DEBOUNCE_MAX 100
/**
 * Handle Interrupt from EXTI3.
 * All Px3 may be connected to this line
 */
void EXTI3_IRQHandler(void)
{
	extern SemaphoreHandle_t sd_InsertedSemaphore;
	portBASE_TYPE xHigherPriorityTaskWoken = pdFALSE;
//	static uint8_t debounce = DEBOUNCE_MAX;
//	int SD_DETECT_status;
	
	// Check SD detect pin. Useful when EXTIx_IRQHandler handles more then 1 EXTI line.
	if( EXTI_GetITStatus( SD_DETECT_GPIO->extiLine ) != RESET ){

		// Следующее прерывание сработает только с обратным фронтом.
		if( bsp_InputRead(SD_DETECT_GPIO) ){
			EXTI->RTSR &= ~SD_DETECT_GPIO->extiLine;  // Rising edge
			EXTI->FTSR |= SD_DETECT_GPIO->extiLine;   // Falling edge
		} else {
			EXTI->RTSR |= SD_DETECT_GPIO->extiLine;
			EXTI->FTSR &= ~SD_DETECT_GPIO->extiLine;
		}
		// Give the semaphore to wakeup SD task
		if( sd_InsertedSemaphore )
			xSemaphoreGiveFromISR( sd_InsertedSemaphore, &xHigherPriorityTaskWoken ); 
	}
	// Clear interrupt pending bit
	EXTI_ClearITPendingBit( SD_DETECT_GPIO->extiLine );
	
	// Switch tasks if necessary.
	if( xHigherPriorityTaskWoken != pdFALSE ) {
		portEND_SWITCHING_ISR( xHigherPriorityTaskWoken );
	}
}
#endif


/******************************************************************************/
/*            Cortex-M4 Processor Exceptions Handlers                         */
/******************************************************************************/

/**
 * @brief	This function handles SysTick Handler. Calls FreeRTOS systick handler.
 *	@note	If systick is not used for other purposes the better solution is 
 * 				#define xPortSysTickHandler	SysTick_Handler
 *			in FreeRTOSConfig.h
 */
/*void SysTick_Handler(void)
{
	extern void xPortSysTickHandler(void);
	xPortSysTickHandler(); 
}*/

/**
  * @brief  This function handles NMI exception.
  * @param  None
  * @retval None
  */
void NMI_Handler(void)
{
}

/**
  * @brief  This function handles Hard Fault exception.
  * @param  None
  * @retval None
  */
void HardFault_Handler(void)
{
	abort_msg(__func__);
	/* Go to infinite loop when Hard Fault exception occurs */
	while (1) {}
}

/**
  * @brief  This function handles Memory Manage exception.
  * @param  None
  * @retval None
  */
void MemManage_Handler(void)
{
	abort_msg(__func__);
	/* Go to infinite loop when Memory Manage exception occurs */
	while (1) {}
}

/**
  * @brief  This function handles Bus Fault exception.
  * @param  None
  * @retval None
  */
void BusFault_Handler(void)
{
	abort_msg(__func__);
	/* Go to infinite loop when Memory Manage exception occurs */
	while (1) {}
}

/**
  * @brief  This function handles Usage Fault exception.
  * @param  None
  * @retval None
  */
void UsageFault_Handler(void)
{
	abort_msg(__func__);
	//assert_failed(__FILE__, __LINE__);
	/// Go to infinite loop when Memory Manage exception occurs
	while(1) {}
}

/**
  * @brief  This function handles Debug Monitor exception.
  * @param  None
  * @retval None
  */
void DebugMon_Handler(void)
{
}


/******************************************************************************/
/*                 STM32F4xx Peripherals Interrupt Handlers                   */
/*  Add here the Interrupt Handler for the used peripheral(s) (PPP), for the  */
/*  available peripheral interrupt handler's name please refer to the startup */
/*  file (startup_stm32f4xx.s).                                               */
/******************************************************************************/
/**
  * @brief  This function handles PPP interrupt request.
  * @param  None
  * @retval None
  */
/*void PPP_IRQHandler(void)
{
}*/

