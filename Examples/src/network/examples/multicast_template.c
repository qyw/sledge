void IntiFunction(void)
{
	struct udp_pcb *pcb;
	err_t err;
	struct ip_addr JoinToGroup, OwnAddress;
	int Retry;
	
#ifndef LWIP_IGMP
  #error "LWIP_IGMP not defined, add it in lwipopts.h file !!!!"
#endif 
	
	// create a new pcb for the server. This is not the pcb used for the connection.
	pcb = udp_new();
	if( pcb != NULL ){
		// bind the listening port to our own IP. Actually opening
		// the socket for listening.
		err = udp_bind(pcb, IP_ADDR_ANY, LISTENING_PORT);
		
		// if binding was OK try to join our own multicast group as well
		// and initialize the rest of the code.
		if(err == ERR_OK) {      
			// send out a join group request:
			JoinToGroup.addr = IP_MCAST_GROUP;
			OwnAddress.addr = IP_ANY_ADDR;
			Retry = IGMP_JOIN_RETRY_COUNT;

			while((Retry--) > 0) {
				err = igmp_joingroup(&OwnAddress, &JoinToGroup);
				if(err == ERR_OK) {
					break;
				}
				vTaskDelay(IGMP_JOIN_RETRY_DELAY / portTICK_RATE_MS);
			} 

			if(err != ERR_OK) {
				// error could not join group. We only can listen to broadcast messages.
			}
			// hook the recv function to the new pcb      
				udp_recv(pcb, recv_func, pcb);

		} else {
			// do something here ??
		}
	} else {
		// abort? output diagnostic?  
	}
}



/*** Important note:
When you first load your system network connection to the router or gateway may take some time. If you try
to join the MC group before the connection is alive the call to igmp_joingroup will fail although it looks like your
stack is running OK.
Let me explain. In the code I use which is based on an example, the main application creates a temporary
task that initializes LwIP and then dies. Part of the initialization process is creating the LwIP own task.
Every task has its own priority.
As the temporary task needs to run before LwIP own task its priority is higher  !
If the temporary task priority is higher than the LwIP own task priority this causes a problem. If you join
the MC group as part of this initialization process igmp_joingroup will fail as the LwIP is not yet running.
To overcome this I have done the following:
TCP initialization task priority is higher than LwIP own task, let’s say 10
LwIP own task priority is 9
I added a long task delay in the temporary task after calling tcpip_init function
The above causes the temporary task to halt, LwIP finishes its own initialization.
The OS scheduler will resume the temporary task and now if you call igmp_joingroup it will not fail.
See the code here:
-----------------------------------------------------------------------------------------------------------------------------*/
//If the above temporary looks something like this:

void vStartEthernetTasks( void* param )
{
	// this function should be called after calling NVIC, GPIO and RCC settings.
	// This function was originally in main but for some reason ETH was not running
	// in stand alone mode. If code was loaded from the debugger and run it was working.
	// After moving it here it started to work
	ETH_BSP_Config();
	// read IP address, port and other definitions from Flash 
	ReadIP_Config();
	// initialize the network netif structure before calling ethernetif_init.
	// once TCP stack has been initalized, set hw and IP parameters, initialize MAC too.
	tcpip_init(prvEthernetConfigureInterface, NULL);
	// the function prvEthernetConfigureInterface must be called before we continue
	// with the initializtion of the rest of the moduls. TCP stack has higher priority
	// than vStartEthernetTasks so yeilding here lets it run first !!!
	vTaskDelay(WAIT_FOR_TCP_TO_INITIALIZE / portTICK_RATE_MS);
	// Initialize HTTP
	httpd_init(GetHttpSrvPort());
	// Nothing else to do.  No point hanging around.
	vTaskDelete( NULL );
}
