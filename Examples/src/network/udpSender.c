/**
 * \author 	Ivan "Kyb Sledgehammer" Kuvaldin <i.kyb[2]ya,ru> 
 */


#include "lwip/opt.h"

#if LWIP_NETCONN

#include "lwip/api.h"
//#include "lwip/sys.h"
//#include "lwip/sockets.h"

/// FreeRTOS includes
//#include "FreeRTOS.h"
//#include "task.h"
#include "semphr.h"

#include <string.h>
#include <stdbool.h>
#include "./udpSender.h"

#include "mainConfig.h"
//#include "task_priorities.h"

///\todo #include "Sledge.h"

#include "Sledge/bsp.h"
#include "Sledge/drv/stm32/sledge_stm32_RTC.h"  
#include "./vmp.h"


/** === DEBUG ===
*/
#include "debugConfig.h"
#undef DEBUG_LEVEL
#define DEBUG_LEVEL  UDP_SENDER_DEBUG_LEVEL
#include "Sledge/debug.h"
#include "Sledge/assert.h"

/*#define MTU_SIZE_DEFAULT	1500
#define IP4_HEADER_SIZE		60
#define UDP_HEADER_SIZE		8
#define UDP_USER_MAX_SIZE	(MTU_SIZE_DEFAULT - IP4_HEADER_SIZE - UDP_HEADER_SIZE)
*/

/** netconn variables */
static struct netconn 	*connSender;
static struct netbuf 	*bufSender;

volatile SemaphoreHandle_t xUdpSenderSemaphore = NULL;
/*__IO*/ static TaskHandle_t udpSender_taskID = NULL;	/// Handler to control task flow 

extern __I uint32_t startTimeSeconds;	//READonly


/**
 *	=== FUNCTIONS ===
 */
static void udpSender_task(void * pvParameters);


/** @brief  Toggle Led3 task
  * @param  pvParameters not used
  * @retval None
  */
static void udpSender_task(void * pvParameters) 
{	
	/*static*/ err_t err;
	SenderOptions_t *opts;
	bool /*int8_t*/ PacketWith0Sent = 0;
	
	
	opts = (SenderOptions_t*) pvParameters;
	//if( opts ){
	//	assert_amsg( opts->destinationIP.addr != 0 );
	//	//debugf( "Error: destinationIP must not be 0"NEWLINE );
	//} else {
	//	debugf( "Error: udpSender_task null poiner to Options"NEWLINE );
	//	goto stopTask;
	//}
	if( opts==NULL || opts->destinationIP.addr==0 ){
		debugf( "udpSender_task: Wrong input opts"NEWLINE );
		goto stopTask;
	}
	
	if( xUdpSenderSemaphore == NULL ) 
		xUdpSenderSemaphore = xSemaphoreCreateBinary(); 
	assert_amsg( xUdpSenderSemaphore );
	
	connSender = netconn_new(NETCONN_UDP);
	if (connSender == NULL) {
		debugf( "Error: connSender cannot be created"NEWLINE );
		goto stopTask;
	}
	
	err = netconn_bind(connSender, IP_ADDR_ANY, UDP_PORT_NUMBER_SENDER/*serverPort*/);
	if( err != ERR_OK ){
		debugf( "Error: cannot bind connSender"NEWLINE );
		goto stopTask;
	}
	
	bufSender = netbuf_new();
	if( bufSender == NULL ){
		debugf( "Error: cannot create bufSender"NEWLINE );
		goto stopTask;
	}
	//TODO try size of 0. Memory for pbuf is not needed.
	if( netbuf_alloc(bufSender, 0 /*0xff*/) == NULL ){
		debugf( "Error: cannot allocate bufSender"NEWLINE );
		goto stopTask;
	}

	for(;;){
		//bufSender->addr.addr = 0;		//clear ip-address in buffer
		xSemaphoreTake( xUdpSenderSemaphore, portMAX_DELAY );
		
		/// Заполнение пакета перед отправкой
		//vmpPackage.timestamp_ms = (uint64_t)startTimeSeconds*1000 + xTaskGetTickCount();
		vmpPackage.rtcSeconds	= myRTC_GetSeconds(false);
		bufSender->p->payload = (void*) &vmpPackage;
		bufSender->p->tot_len = bufSender->p->len = vmp_lengthToSend( (vmpPackage_t*) &vmpPackage, &PacketWith0Sent );
		
		err = netconn_connect(connSender, &(opts->destinationIP), UDP_PORT_NUMBER_SENDER); 	//Connect to ip-address:port
		//if( err = ERR_OK ){
		err = netconn_send(connSender, bufSender);
		//}
		
		bsp_LedToggle(LedUDP_SENDER);
	}
	
	stopTask:
	/// Stop this task
	udpSenderStop();
	//for(;; vTaskDelete(NULL) );
}


/**
 * Start udpSender_task if not already started
 * @param 	*opts	Options
 * @return 	Error code: 
 * 				0 		OK
 * 				2 		already created 
 * 				ERR_MEM memory error
 */
err_t udpSenderStart(SenderOptions_t *opts)
{
	signed portBASE_TYPE /*err_t*/ err;
	err_t ret;
	
	
	if( udpSender_taskID ){
		/// Already created.
		return 2;
	}
	
	// Создать таск, который непрерывно шлёт udp-пакеты.
	err = xTaskCreate( udpSender_task, "udpSender_task", configMINIMAL_STACK_SIZE*2,
			opts, UDPSENDER_THREAD_PRIO, (TaskHandle_t)&udpSender_taskID);
	// Получилось ли создать таск?
	if (err == pdPASS) {
		/// Just created.
		ret = ERR_OK;
	} else { // например, errCOULD_NOT_ALLOCATE_REQUIRED_MEMORY
		/// Error!
		ret = ERR_MEM;
	}
	return ret;
}


/** @brief	Delete udpSender_task and free used resourses.
  * @param	none
  * @retval 0 if OK, else if problems
  */
bool udpSenderStop(void)
{
	if( udpSender_taskID != NULL ){
		/**	FIXED Бывали глюки при 2ой очистке переменных. 
			ПРИЧИНА: xUdpSenderSemaphore был объявлен без volatile, как следствие
			компилятор определял один и тот же адрес под xUdpSenderSemaphore и под connSender->op_completed
			и в функцию vPortFree попадал уже очищенный блок памяти */
		vTaskDelete(udpSender_taskID);
		vSemaphoreDelete( xUdpSenderSemaphore );
		xUdpSenderSemaphore = NULL;
		udpSender_taskID = NULL;
		netconn_delete(connSender);
		netbuf_delete(bufSender);
		bsp_LedOff(LedUDP_SENDER);
		
		return true;	/// Stopped
	}
	return false; 	/// Allready stopped. Nothing to stop
}


#else /* LWIP_NETCONN */
#warn "Module udpSender.c cannot be copilied since LWIP_NETCONN is OFF."
#endif /* LWIP_NETCONN */
