/**
 * @file    udpSender.h
 * \author 	Ivan "Kyb Sledgehammer" Kuvaldin <i.kyb[2]ya,ru>
 * @version V1.0.0
 * @date    2014-09-12
 * @brief   This file contains all the public function prototypes and variables 
 *          for the udpSender.c file.
 */ 

/** Define to prevent recursive inclusion ------------------------------------*/
#ifndef __UDPSENDER_H
#define __UDPSENDER_H

#ifdef __cplusplus
 extern "C" {
#endif

	 
//#ifndef INC_FREERTOS_H
// #error "include FreeRTOS.h" must appear in source files before "include udpSender.h"
//#endif
//#ifndef INC_TASK_H
// #error "include task.h" must appear in source files before "include udpSender.h"
//#endif
//#ifndef SEMAPHORE_H
// #error "include semphr.h" must appear in source files before "include udpSender.h"
//#endif


//#include <stdint.h>

#include "udpSenderOptions.h"
//#include "stm32f4xx_rtc.h"
//#include "siliconSinArray.h" 		/** Synthetic sinuses */

//#include "lwip/err.h"


//extern __IO vmpPackage_t vmpPackage;
//extern /*__IO*/ TaskHandle_t udpSender_taskID;
//extern volatile SemaphoreHandle_t xUdpSenderSemaphore;


//void udpSender_task(void * pvParameters);
int8_t/*err_t*/ udpSenderStart(SenderOptions_t *opts);
bool udpSenderStop(void);



#ifdef __cplusplus
}
#endif

#endif /* __UDPSENDER_H */
