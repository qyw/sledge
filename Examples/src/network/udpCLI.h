/**
 * @file 	udpCLI.h
 * \author 	Ivan "Kyb Sledgehammer" Kuvaldin <i.kyb[2]ya,ru>
 * @version	1.0.0
 * @date 	2015-03-19
 * @brief	
 *           
 *
 * CHANGES
 * ver.1.0.0 date 2015-03-19
 * 	Выделен отдельным модулем из udpSender.c
 *
 * @date 2014-09-12 
 * 	Initial release.
 */

/** Define to prevent recursive inclusion ------------------------------------*/
#ifndef __UDPSENDER_H
#define __UDPSENDER_H

#ifdef __cplusplus
 extern "C" {
#endif


//#include <stdint.h>



#ifdef __cplusplus
}
#endif

#endif /* __UDPSENDER_H */
