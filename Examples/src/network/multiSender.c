/**
 * @file    multiSender.c
 * \author 	Ivan "Kyb Sledgehammer" Kuvaldin <i.kyb[2]ya,ru>
 * @version 1.0.0
 * @date    2015-03-16
 * @brief   
 */


//#include "lwip/opt.h"

//#if LWIP_NETCONN


#include <string.h>
#include <stdbool.h>

/// FreeRTOS includes
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"

//#include "lwip/api.h"	/// netconn API
//#include "lwip/sys.h"
#include "lwip/sockets.h"
#include "lwip/udp.h"
#include "lwip/igmp.h"

#include "./multiSender.h"

#include "mainConfig.h"

#include "Sledge.h"
//#include "Sledge/drv/stm32/stm32_RTC_sledge.h"
#include "./vmp.h"

/** === DEBUG ===
*/
#undef DEBUG_LEVEL
#define DEBUG_LEVEL  MULTI_SENDER_DEBUG_LEVEL
#include "Sledge/debug.h"

/*#define MTU_SIZE_DEFAULT	1500
#define IP4_HEADER_SIZE		60
#define UDP_HEADER_SIZE		8
#define UDP_USER_MAX_SIZE	(MTU_SIZE_DEFAULT - IP4_HEADER_SIZE - UDP_HEADER_SIZE)
*/

#define MULTI_SENDER_ADDRESS	"239.0.43.36"
#define MULTI_SENDER_PORT		4336


volatile QueueHandle_t multiSenderQueue = NULL;
volatile SemaphoreHandle_t multiSenderSemaphore = NULL;
/*__IO*/ static TaskHandle_t multiSender_taskID = NULL;	/// Handler to control task flow 

extern __O uint32_t startTimeSeconds;	//READonly


/**
 *	=== FUNCTIONS ===
 */
static void multiSender_task(void *pvParameters);
static void multiSenderRecvCallback( void *arg, struct udp_pcb *pcb, struct pbuf *p,
									 struct ip_addr *addr, u16_t port);
//static void vmpMulticastListner_task( void *args );


/**
 * @brief  Processing an incoming message on the Event port.
 * @param  arg the user argument
 * @param  pcb the udp_pcb that has received the data
 * @param  p the packet buffer
 * @param  addr the addres of sender
 * @param  port the port number of sender
 * @retval None
 */
static void multiSenderRecvCallback( void *arg, struct udp_pcb *pcb, struct pbuf *p,
									 struct ip_addr *addr, u16_t port)
{
	//extern Vibro_device_mode_e vibro_device_mode;
	vmpPackage_t *vmpp;
	
	// Parse packet
	if( p->len == sizeof(vmpPackage_t) ){
		vmpp = (vmpPackage_t*) p->payload;
	} else {
		debugf("multiSenderRecvCallback(): Wrong packet received!");
	}
	
	/*switch( vibro_device_mode ){
		case VIBRO_DEVICE_MODE_TAXO_SLAVE:
			
			break;
	}*/
}


void multiSender_task( void *pvParameters )
{	
	//struct ip_addr interfaceAddr;
	struct ip_addr netAddr;
	//char addrStr[16];	//NET_ADDRESS_LENGTH
	struct udp_pcb *udpPcb;
	struct pbuf *p;
	bool PacketWith0Sent;
	err_t err;
	
	debugf2("udpVmpMulticastSender"NEWLINE);
	
	if( multiSenderSemaphore == NULL ) 
		multiSenderSemaphore = xSemaphoreCreateBinary(); 
	assert_amsg( multiSenderSemaphore );
	
	if( multiSenderQueue == NULL )
		multiSenderQueue = xQueueCreate( 1, sizeof(vmpPackage_t) );
	assert_amsg( multiSenderQueue != NULL );	
	
	
	/* Open lwIP raw udp interfaces for the event port. */
	udpPcb = udp_new();
	
	if (NULL == udpPcb) {
		debugf("udpVmpMulticastSender: Failed to open udpPcb"NEWLINE);
		goto fail02;
	}
	
	/// Init General multicast IP address
	//memcpy(addrStr, DEFAULT_PTP_DOMAIN_ADDRESS, NET_ADDRESS_LENGTH);
	//if (!inet_aton(addrStr, &netAddr))
	if( !ipaddr_aton(MULTI_SENDER_ADDRESS, &netAddr) ){
		debugf("udpVmpMulticastSender: failed to encode multicast address: %s"NEWLINE, MULTI_SENDER_ADDRESS);
		goto fail04;
	}
	
	/// Join multicast group (for receiving) on default interface
	//igmp_joingroup(&interfaceAddr, &netAddr);		//on specified interface
	igmp_joingroup( &netif_default->ip_addr, &netAddr);
	
	/// Multicast send only on specified interface
	udpPcb->multicast_ip.addr = netAddr.addr;
	
	/// Establish the appropriate UDP bindings/connections for events.
	udp_recv( udpPcb, multiSenderRecvCallback, /*user_arg*/NULL );
	udp_bind( udpPcb, IP_ADDR_ANY, MULTI_SENDER_PORT);
	///  udp_connect( udpPcb, &netAddr, MULTI_SENDER_PORT);
		
	/// Allocate the tx pbuf based on the current size.
    p = pbuf_alloc(PBUF_TRANSPORT, 0/*16*/, PBUF_RAM);
    if (NULL == p) {
        debugf("udpVmpMulticastSender: Failed to allocate Tx Buffer\n");
        goto fail04;
    }
	
	err = udp_connect( udpPcb, &netAddr, MULTI_SENDER_PORT);	
	while(1){
		xSemaphoreTake( multiSenderSemaphore, portMAX_DELAY );
		
		/// Заполнение пакета перед отправкой
		//vmpPackage.timestamp_ms = (uint64_t)startTimeSeconds*1000 + xTaskGetTickCount();
		//vmpPackage.rtcSeconds	= myRTC_GetSeconds();
		p->payload = (void*) &vmpPackage;
		p->tot_len = p->len = vmp_lengthToSend( (vmpPackage_t*) &vmpPackage, &PacketWith0Sent );
		
		//err = udp_connect( udpPcb, &netAddr, MULTI_SENDER_PORT);
		//if( err = ERR_OK ){
		err = udp_send( udpPcb, p );
		//}
		
		bsp_LedBlinkOnce( LedMULTISENDER, bsp_timer_ms(10) );  //bsp_LedToggle(LedMULTISENDER);
	}
		
/*fail05:
	udp_disconnect(netPath->eventPcb);
	udp_disconnect(netPath->generalPcb);
	*/
fail04:
    udp_remove(udpPcb);
fail02:
//fail01:
	/// Stop this task
	(void) err;
	for(;; vTaskDelete(NULL) );
}


/*static void vmpMulticastListner_task( void *args )
{
	int sock, newconn;
	size_t size;
	const int optval = 1;
	struct sockaddr_in address, remotehost;
	
	sock = socket(AF_INET, SOCK_DGRAM, 0);
	if( sock < 0 ){
		debugf("MulticastListener: Cannot create socket");
		goto stopTask;
	}
	
	setsockopt( sock, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof(optval) );
	
	// Bind to port 23 at any interface.
	address.sin_family = AF_INET;
	address.sin_port = htons(MULTI_SENDER_PORT);
	address.sin_addr.s_addr = INADDR_ANY;
	
	if( bind(sock, (struct sockaddr*) &address, sizeof(address)) < 0 ){
		debugf("MulticastListener: cannot bind socket");
		goto stopTask;
	}
	
	// Listen for incoming connections (TCP listen backlog = 1).
	listen(sock, 1);
	
	size = sizeof(remotehost);
	for (;;) {
		newconn = accept(sock, (struct sockaddr*) &remotehost, (socklen_t*) &size);
		if (newconn > 0) {
			//telnet_shell(newconn);
		}
	}
  
	stopTask:
	/// Stop this task
	for( ;; vTaskDelete(NULL) );
}*/


/**
 * Start multiSender_task if not already started
 * @param 	*opts	Options
 * @return 	Code (Error code): 
 * 			  1 			Just created.
 * 			  2 			Already created 
 * 			  -1(ERR_MEM) 	Memory error (errCOULD_NOT_ALLOCATE_REQUIRED_MEMORY)
 */
int8_t multiSenderStart(SenderOptions_t *opts)
{
	signed portBASE_TYPE /*err_t*/ err;
	int8_t ret;
	
	
	if( multiSender_taskID ){
		/// Already created.
		return 2;
	}
	
	// Создать таск, который непрерывно шлёт udp-пакеты.
	err = xTaskCreate( multiSender_task, "multiSender_task", configMINIMAL_STACK_SIZE*2,
			opts, MULTI_SENDER_THREAD_PRIO, &multiSender_taskID);
	// Получилось ли создать таск?
	if (err == pdPASS) {
		// Just created.
		ret = 1;	//ERR_OK;
	} else { // например, errCOULD_NOT_ALLOCATE_REQUIRED_MEMORY
		// Error!
		ret = -1;	//ERR_MEM;
	}
	return ret;
}


/** @brief	Delete udpSender_task and free used resourses.
  * @param	none
  * @retval 0 if OK, else if problems
  */
bool multiSenderStop(void)
{
	if( multiSender_taskID != NULL ){
		vTaskDelete(multiSender_taskID);
		multiSender_taskID = NULL;
		vSemaphoreDelete( multiSenderSemaphore );
		multiSenderSemaphore = NULL;
		//rawconndelete		//netconn_delete(connSender);
		//rawbufdelete		//netbuf_delete(bufSender);
		bsp_LedOff(LedMULTISENDER);
		
		return true;	/// Stopped
	}
	return false; 	/// Allready stopped. Nothing to stop
}

/*-----------------------------------------------------------------------------------*/

//#endif /* LWIP_NETCONN */
