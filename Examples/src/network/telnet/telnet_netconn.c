/*******************************************************************************
  * @file    telnet_netconn.c
  * @author  kyb
  * @version V0.0.1
  * @date    15-September-2014
  * @brief   This file implements telnet interface. Based on lwIP netconn API.
  * 		 Requeres cli.h implementation.
  *
  * @restrict	Only 1 client can connect at the time.
  * @dependency	string.h, cli.h, lwip/sys.h, lwip/api.h
  *****************************************************************************/

/*
 * Copyright (c) 2001-2003 Swedish Institute of Computer Science.
 * All rights reserved. 
 * 
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *	  this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission. 
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT 
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
 *
 * This file is part of the lwIP TCP/IP stack.
 * 
 * Author: Adam Dunkels <adam@sics.se>
 *
 */


#include "lwip/opt.h"

#if LWIP_NETCONN

//#include "telnet.h"

#include "lwip/sys.h"
#include "lwip/api.h"

#include <string.h>

#include "sledge/bsp/bsp.h"
#include "cli/cli.h"


#define TCPECHO_THREAD_PRIO		( tskIDLE_PRIORITY + 5 )
#define TCPECHO_PORT 			7

#define MAX_STRING_SIZE			255

typedef struct String {
	char chars[MAX_STRING_SIZE];
	uint8_t length;
	int8_t	done;
} String;


/*-----------------------------------------------------------------------------------*/
static void telnet_thread(void *arg)
{
	struct netconn *conn, *newconn;
	err_t 	bind_err, accept_err, recv_err;
	struct netbuf *buf;
	void *data;
	u16_t len;
	
	char* c; 
	char responseArr[255], *response = responseArr;
	String* str;
	//String*	listStr[10];
	uint8_t i;
	//int8_t strCompiled = NULL;
	
	
	LWIP_UNUSED_ARG(arg);
	
	/** Create a new connection identifier. */
	conn = netconn_new(NETCONN_TCP);
	
	if (conn!=NULL) {
		/** Bind connection to well known port number 7. */
		bind_err = netconn_bind(conn, IP_ADDR_ANY, TCPECHO_PORT);
		
		if (bind_err == ERR_OK) {
			/** Tell connection to go into listening mode. */
			netconn_listen(conn);
			
			while (1) {
				/** Grab new connection. */
				 accept_err = netconn_accept(conn, &newconn);
				/** Process the new connection. */
				if (accept_err == ERR_OK) {
					/** Write Greeting and Invite to terminal */
					netconn_write(newconn, GREETING, sizeof(GREETING)-1, NETCONN_COPY);
					netconn_write(newconn, CLI_INVITE, sizeof(CLI_INVITE)-1, NETCONN_COPY);
					
					/** Allocate memory for the string which will contain input commands. Initialize str */
					str = pvPortMalloc(sizeof(*str));
					str->length = 0;
					str->done	= 0;
					
					recv_err = netconn_recv(newconn, &buf);
					while ( recv_err == ERR_OK) {
						do {
							//str->done = 0;		//strCompiled = 0;
							netbuf_data(buf, &data, &len);
							
							c = (char*) data;
							for ( i=0; i<len && !str->done; i++ ) {
								if ( (c[i]=='\r') || (c[i]=='\n') || (c[i]=='\0') ) {
									str->done = !NULL;
									if (str->length < MAX_STRING_SIZE)
										str->chars[str->length++] = '\0';
									else 
										str->chars[str->length-1] = '\0';
									break;
								}else if (c[i] == '\b') {		//backspace
									if (str->length > 0) {
										str->length--;
										netconn_write(newconn, " \b", sizeof(" \b")-1, NETCONN_COPY);
									} else {
										netconn_write(newconn, ANSI_ESC_SEQ_CursorRight, sizeof(ANSI_ESC_SEQ_CursorRight)-1, NETCONN_COPY);
									}
								} else if ( strncmp(&c[i], ANSI_ESC_SEQ_CursorUp, sizeof(ANSI_ESC_SEQ_CursorUp)-1 ) == 0) {
									i+=2;
								} else if ( strncmp(&c[i], ANSI_ESC_SEQ_CursorDown, sizeof(ANSI_ESC_SEQ_CursorDown)-1 ) == 0) {
									netconn_write(newconn, ANSI_ESC_SEQ_ScrollDown1Line, sizeof(ANSI_ESC_SEQ_ScrollDown1Line)-1, NETCONN_COPY);
									i+=2;
								} else if ( strncmp(&c[i], ANSI_ESC_SEQ_CursorRight, sizeof(ANSI_ESC_SEQ_CursorRight)-1 ) == 0) {
									netconn_write(newconn, "\x1B[2J", sizeof("\x1B[f")-1, NETCONN_COPY);
									i+=2;
								} else if ( strncmp(&c[i], ANSI_ESC_SEQ_CursorLeft, sizeof(ANSI_ESC_SEQ_CursorLeft)-1 ) == 0) {
									netconn_write(newconn, "\x1B[K", sizeof("\x1B[5;5f")-1, NETCONN_COPY);
									i+=2;
								} else if (c[i] == '\x1B') {
									//netconn_write(newconn, SAY_ESC, sizeof(SAY_ESC)-1, NETCONN_COPY);
								} else if (str->length < MAX_STRING_SIZE) {
									str->chars[str->length++] = c[i];
								} else {
									str->done = !NULL;
									if (str->length < MAX_STRING_SIZE)
										str->chars[str->length++] = '\0';
									else 
										str->chars[str->length-1] = '\0';
									break;
								}
							}
						} while (netbuf_next(buf) >= 0);
						
						if (str->done) {
							processCommand( &response, str->chars, NULL);
							str->chars[0] = '\0';
							str->length = 0;
							str->done = 0;
							
							netconn_write(newconn, response, strlen(response), NETCONN_COPY);
							response = "";
							//netconn_write(newconn, "\x1B[0;72;WOW;p", sizeof("\x1B[0;72;WOW\r\n;")-1, NETCONN_COPY);
							//netconn_write(newconn, "\x1B[46;To4ka;p", sizeof("\x1B[0;72;WOW\r\n;")-1, NETCONN_COPY);
							//netconn_write(newconn, "\x1B[=15h", sizeof("\x1B[0;72;WOW\r\n;")-1, NETCONN_COPY);
							netconn_write(newconn, CLI_INVITE, sizeof(CLI_INVITE)-1, NETCONN_COPY);
						}
						
						netbuf_delete(buf);
						recv_err = netconn_recv(newconn, &buf);
					}
					
					/** Close connection and discard connection identifier. */
					netconn_close(newconn);
					netconn_delete(newconn);
					vPortFree(str);
				}
			}
		} else {
			netconn_delete(newconn);
			printf("can not bind TCP netconn");
		}
	} else {
		printf("can not create TCP netconn");
	}
}
/*-----------------------------------------------------------------------------------*/

void tcpecho_init(void)
{
	sys_thread_new("tcpecho_thread", telnet_thread, NULL, DEFAULT_THREAD_STACKSIZE, TCPECHO_THREAD_PRIO);
}
/*-----------------------------------------------------------------------------------*/

#endif /* LWIP_NETCONN */
