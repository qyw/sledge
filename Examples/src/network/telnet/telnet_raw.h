/*******************************************************************************
  * @file    telnet.h
  * @author  MCD Application Team. Edited by kyb <mailto:i.kyb@ya.ru>
  * @version V1.0.0
  * @date    11/20/2009 | 2014-09-01
  * @brief   This file contains all the functions prototypes for the telnet.c 
  *          file.
  ******************************************************************************/ 

/** Define to prevent recursive inclusion -------------------------------------*/
#ifndef __TELNET_H
#define __TELNET_H

#ifdef __cplusplus
 extern "C" {
#endif




#define TELNET_PORT 	23
#define HELLO 			"Hello "
#define CMD_PROMT		">"
#define CMD_LOGIN		"login as: "
#define CMD_PASSWORD	"password: "

	 
void telnet_init(void);



#ifdef __cplusplus
}
#endif

#endif /* __TELNET_H */

/********************************END OF FILE************************************/
