/**
 * @file    shell.h
 * @author  Mike Thompson
 * @version V1.0.0
 * @date    1-April-2014
 * @brief   Commands for the telnet shell.
 */

#ifndef __SHELL_H__
#define __SHELL_H__

#include <stddef.h>
#include <stdbool.h>
#include <stdint.h>


#define CMDLINE_BUFFER_SIZE 	64
#define ARGV_MAX		8
//#define CMD_NAME_SIZE	16
//#define CMD_HELP_LEN	48


typedef bool (*shell_func)(int argc, char **argv);
//typedef int8_t (*shell_func_r)( char* respBuf, size_t as, int argc, char **argv );

struct shell_command 
{
	const char name[16];			/// The name of command. Must be first to use bsearch() and strcmp().
	const shell_func funcptr; 		/// Pointer to the commnad handler
	//const shell_func_r func_r;		/// Pointer to the reintrant commnad handler
	const char * const help;		/// Pointer to the short help string
	const char * const manual;		/// Pointer to the man string
};


extern const struct shell_command shell_COMMANDS[];
extern const size_t shell_COMMANDS_COUNT;

/**
 * === FUNCTIONS === 
 */

/**
 * Runs the shell by printing the prompt and processing each shell command typed in.
 */
void shell_process(void);

/**
 * Search command by name
 */
struct shell_command * shell_searchCommandByName( const char * cmdname );


#endif /* __SHELL_H__ */
