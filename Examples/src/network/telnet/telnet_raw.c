/*******************************************************************************
  * @file    telnet.c 
  * @author  MCD Application Team, kyb<i.kyb@ya.ru>
  * @version V1.0.0
  * @date    11/20/2009 | 2014.09.01
  * @brief   Telnet server based on raw lwIP.
  *			 The application works as a server which wait for the client request.
  ******************************************************************************/

/** Includes ------------------------------------------------------------------*/
#include "telnet.h"
#include "lwip/tcp.h"
#include <string.h>
#include "network/udpSender.h"
#include "cli/cli.h"
#include "String_kyb.h"
//#include <stdint.h>

/** Private define ------------------------------------------------------------*/
/** Private typedef -----------------------------------------------------------*/
/** Private macro -------------------------------------------------------------*/
/** Private variables ---------------------------------------------------------*/

/** Private function prototypes -----------------------------------------------*/
static err_t telnet_recv(void *arg, struct tcp_pcb *pcb, struct pbuf *p, err_t err);
static err_t telnet_accept(void *arg, struct tcp_pcb *pcb, err_t err);
static void telnet_conn_err(void *arg, err_t err);

/** Private functions ---------------------------------------------------------*/

/** @brief  Called when a data is received on the telnet connection
  * @param  arg	the user argument
  * @param  pcb	the tcp_pcb that has received the data
  * @param  p	the packet buffer
  * @param  err	the error value linked with the received data
  * @retval error value
  */
static err_t telnet_recv(void *arg, struct tcp_pcb *pcb, struct pbuf *p, err_t err)
{
	struct pbuf *q;		//temp pbuf to iterate throught 
	struct String *input = (struct String *)arg;
	int done;			//Indicates the end of string. String is assembled.
	char *c;			//string iterator needed to assemble input from several packets
	size_t i;			//position iterator in char array *c needed to iterate chars throught 1 packet
	
	char response[COMMAND_RESPONSE_MAX_LEN], *buf = response;
//	eCommand eCmd;
 
	/** We perform here any necessary processing on the pbuf */
	if (p != NULL) {
		/** We call this function to tell the LwIp that we have processed the data
		  * This lets the stack advertise a larger window, so more data can be received */
		tcp_recved(pcb, p->tot_len);

		/** Check the input if NULL, no data passed, return withh illegal argument error */
		if (!input) {
			pbuf_free(p);
			return ERR_ARG;
		}
		
		/** Telnet sends 1 char in 1 packet. Assemble chars into one string. */
		done = 0;
		for (q=p; q != NULL; q = q->next) {
			c = q->payload;
			for (i=0; i< q->len && !done; i++) {
				done = ((c[i] == '\r') || (c[i] == '\n'));
				if (c[i] == '\b') {		//backspace
					input->length--;
					tcp_write(pcb, " \b", 2, TCP_WRITE_FLAG_COPY);
				} else if (input->length < MAX_STRING_SIZE) {
					input->bytes[input->length++] = c[i];
				}
			}
		}
		if (done) {
			/** Make last 2 symbols "\r\n" */
			if (input->bytes[input->length-2] != '\r' || input->bytes[input->length-1] != '\n') {
				if ( (input->bytes[input->length-1] == '\r' || input->bytes[input->length-1] == '\n') 
						&& (input->length+1 <= MAX_STRING_SIZE) ) {
					input->length += 1;
				} else if(input->length+2 <= MAX_STRING_SIZE) {
					input->length += 2;
				} else {
					input->length = MAX_STRING_SIZE;
				}
				input->bytes[input->length-2] = '\r';
				input->bytes[input->length-1] = '\n';
			}
			/** replace '\r' with '\0' */
			input->bytes[input->length-2] = '\0';
			
			processCommand( &buf, input->bytes, NULL);
			
			tcp_write(pcb, buf, strlen(buf), TCP_WRITE_FLAG_COPY);
			tcp_write(pcb, "> ", sizeof("> ")-1, TCP_WRITE_FLAG_COPY);
			input->length = 0;
		}
	
		/** End of processing, we free the pbuf */
		pbuf_free(p);
	} else if (err == ERR_OK) {
		/** When the pbuf is NULL and the err is ERR_OK, the remote end is closing the connection.
		  * We free the allocated memory and we close the connection */
		mem_free( (void*)input);
		return tcp_close(pcb);
	} /* (p != NULL) */
	return ERR_OK;
}

/** @brief  This function when the Telnet connection is established
  * @param  arg  user supplied argument 
  * @param  pcb	 the tcp_pcb which accepted the connection
  * @param  err	 error value
  * @retval ERR_OK
  */
static err_t telnet_accept(void *arg, struct tcp_pcb *pcb, err_t err)
{ 
	/** Tell LwIP to associate this structure with this connection. */
	tcp_arg(pcb, mem_calloc(sizeof(struct String), 1));	
	
	/** Configure LwIP to use our call back functions. */
	tcp_err(pcb, telnet_conn_err);
	tcp_recv(pcb, telnet_recv);
	
	/** Send out the first message */	
	tcp_write(pcb, GREETING, strlen(GREETING), TCP_WRITE_FLAG_COPY);
	//log as
	//passwd
	tcp_write(pcb, "> ", sizeof("> ")-1, TCP_WRITE_FLAG_COPY);

	return ERR_OK;
}

/** @brief  Initialize the hello application  
  * @param  None 
  * @retval None 
  */
void telnet_init(void)
{
	struct tcp_pcb *pcb;
	
	/* Create a new TCP control block */
	pcb = tcp_new();

	/* Assign to the new pcb a local IP address and a port number */
	/* Using IP_ADDR_ANY allow the pcb to be used by any local interface */
	tcp_bind(pcb, IP_ADDR_ANY, TELNET_PORT);
	
	/* Set the connection to the LISTEN state */
	pcb = tcp_listen(pcb);				
	
	/* Specify the function to be called when a connection is established */	
	tcp_accept(pcb, telnet_accept);
}

/** @brief  This function is called when an error occurs on the connection 
  * @param  arg
  * @param   err
  * @retval None 
  */
static void telnet_conn_err(void *arg, err_t err)
{
	struct String *input;
	input = (struct String *)arg;

	mem_free(input);
}

/********************************END OF FILE************************************/
