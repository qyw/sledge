/**
 * @author 	Kyb
 * Thanks to Mike P. Thompson
 */

#include <ctype.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
//#include "cmsis_os.h"
#include "ptpd-2.0.0/src/ptpd.h"
#include "shell_mpthompson.h"
#include "telnet_mpthompson.h"

#include "mainConfig.h"
#include "Sledge/cli/cli.h"
#include "Sledge/c11n/c11n.h"
#include "Sledge/utils.h"

/** === DEBUG ===
 */
#undef DEBUG_LEVEL
#define DEBUG_LEVEL 0
#include "Sledge/debug.h"


#define RESPONSE_BUFFER_SIZE  512


/**
 * @param 	argc	Arguments number (count)
 * @param **argv	Arguments array. argv[0] points to command name.
 */
static bool shell_exit(int argc, char **argv);
static bool shell_help(int argc, char **argv);
static bool shell_date(int argc, char **argv);
static bool shell_ptpd(int argc, char **argv);



/// Must be sorted in ascending order.
const struct shell_command commands[] = {
	{"?"	, 	        NULL, /*          cli_help,*/ "The same as help2" },
	{"clear", 	        NULL, /*         cli_clear,*/ "Clear the screen" },
	{"cls"  , 	        NULL, /*         cli_clear,*/ "The same as clear" },
	{"DATE" , 	 shell_date , /*              NULL,*/ "Show current date and time" },
	{"EXIT" , 	 shell_exit , /*              NULL,*/ "Close telnet connection" },
	{"get"  , 	        NULL, /*           cli_get,*/ "Get variable/parameter" },
	{"hello", 	        NULL, /*          cli_info,*/ "The same as info" },
	{"HELP" , 	 shell_help , /*          cli_help,*/ "Show this short help" },
	{"help2", 	        NULL, /*              NULL,*/ "Show this short help from CLI" },	
	{"info", 	        NULL, /*          cli_info,*/ "Print information about device and firmware" },
	{"param",           NULL, /*         cli_param,*/ "Universal get/set command" },
	{"ptpd2" , 	 shell_ptpd , /*              NULL,*/ "Show PTPd2 stats. TODO" },
	{"sd"  , 	        NULL, /*              NULL,*/ "Command for SD card. Info and settings. TODO" },
	{"set"  , 	        NULL, /*           cli_set,*/ "Set variable/parameter" },
	{"show" , 	        NULL, /*           cli_get,*/ "The same as get" },
	{"start", 	        NULL, /*cli_udpSenderStart,*/ "Start udpSender to your IP" },
	{"stats", 	        NULL, /*              NULL,*/ "Statistics" },
	{"stop" , 	        NULL, /* cli_udpSenderStop,*/ "Stop udpSender" },
	{"tasklist",        NULL, /*              NULL,*/ "Show the list of RTOS tasks" },
};

/// Иначе невозможно сделать автоматическое получение размера public
const size_t shell_COMMANDS_COUNT = sizeofarr(commands);


static bool shell_exit(int argc, char **argv)
{
	// Exit the shell interpreter.
	return false;
}


static bool shell_unknown(int argc, char **argv)
{
	char buffer[RESPONSE_BUFFER_SIZE];
	
	cli_unknown( buffer, sizeof(buffer), argc, argv, 0 );
	
	telnet_puts(buffer);
	return true;
}


static bool shell_help(int argc, char **argv)
{
	size_t i;
	
	// Loop over each shell command.
	for (i = 0; i < sizeof(commands) / sizeof(struct shell_command); ++i) {
		telnet_printf("%s\t %s"NEWLINE, commands[i].name, commands[i].help);
	}

	return true;
}


static bool shell_date(int argc, char **argv)
{
	char buffer[32];
	time_t seconds1900;
	struct ptptime_t ptptime;

	// Get the ethernet time values.
	ETH_PTPTime_GetTime(&ptptime);

	// Get the seconds since 1900.
	seconds1900 = (time_t) ptptime.tv_sec;

	// Format into a string.
	strftime(buffer, sizeof(buffer), "%a %b %d %H:%M:%S UTC %Y\n", localtime(&seconds1900));

	// Print the string.
	telnet_puts(buffer);

	return true;
}


static bool shell_ptpd(int argc, char **argv)
{
	char sign;
	const char *s;
	unsigned char *uuid;
	extern PtpClock ptpClock;

	uuid = (unsigned char*) ptpClock.parentDS.parentPortIdentity.clockIdentity;

	/* Master clock UUID */
	telnet_printf("master id: %02x%02x%02x%02x%02x%02x%02x%02x\n",
					uuid[0], uuid[1],
					uuid[2], uuid[3],
					uuid[4], uuid[5],
					uuid[6], uuid[7]);

	switch (ptpClock.portDS.portState)
	{
		case PTP_INITIALIZING:  s = "init";  		break;
		case PTP_FAULTY:        s = "faulty";   	break;
		case PTP_LISTENING:     s = "listening";  	break;
		case PTP_PASSIVE:       s = "passive";  	break;
		case PTP_UNCALIBRATED:  s = "uncalibrated"; break;
		case PTP_SLAVE:         s = "slave";   		break;
		case PTP_PRE_MASTER:    s = "pre master";  	break;
		case PTP_MASTER:        s = "master";   	break;
		case PTP_DISABLED:      s = "disabled"; 	break;
		default:                s = "?";			break;
	}

	/* State of the PTP */
	telnet_printf("state: %s\n", s);

	/* One way delay */
	switch (ptpClock.portDS.delayMechanism)
	{
		case E2E:
			telnet_puts("mode: end to end\n");
			telnet_printf("path delay: %d nsec\n", ptpClock.currentDS.meanPathDelay.nanoseconds);
			break;
		case P2P:
			telnet_puts("mode: peer to peer\n");
			telnet_printf("path delay: %d nsec\n", ptpClock.portDS.peerMeanPathDelay.nanoseconds);
			break;
		default:
			telnet_puts("mode: unknown\n");
			telnet_printf("path delay: unknown\n");
			/* none */
			break;
	}

	/* Offset from master */
	if (ptpClock.currentDS.offsetFromMaster.seconds) {
		telnet_printf("offset: %d sec\n", ptpClock.currentDS.offsetFromMaster.seconds);
	} else {
		telnet_printf("offset: %d nsec\n", ptpClock.currentDS.offsetFromMaster.nanoseconds);
	}

	/* Observed drift from master */
	sign = ' ';
	if (ptpClock.observedDrift > 0) sign = '+';
	if (ptpClock.observedDrift < 0) sign = '-';

	telnet_printf("drift: %c%d.%03d ppm\n", sign, abs(ptpClock.observedDrift / 1000), abs(ptpClock.observedDrift % 1000));

	return true;
}


/** Parse out the next non-space word from a string.
 *	str		Pointer to pointer to the string
 *	word	Pointer to pointer of next word.
 *	Returns	0:Failed, 1:Successful 
 */
static int shell_parse(char **str, char **word)
{
	// Skip leading spaces.
	while (**str && isspace(**str)) (*str)++;

	// Set the word.
	*word = *str;

	// Skip non-space characters.
	while (**str && !isspace(**str)) (*str)++;

	// Null terminate the word.
	if (**str) *(*str)++ = 0;

	return (*str != *word) ? 1 : 0;
}


/** Attempt to execute the shell command.
 * 	@arg cmdline 	The string represnts command line
 * 	@return passes result from executed command. 
 * 			TRUE: continue negotitation. FALSE: close connection
 */
/*static*/ bool shell_command(char *cmdline)
{
	int i;
	char *argv[ARGV_MAX];
	int argc = 0;
	bool rv = true;
	struct shell_command *command;

	// Parse the command and any arguments into an array.
	for (i = 0; i < (sizeof(argv) / sizeof(char *)); ++i)
	{
		shell_parse(&cmdline, &argv[i]);
		if (*argv[i] != 0) ++argc;
	}
	
	if( 0 == argc )
		return rv;
	
	/// Search for a matching command.
	command = shell_searchCommandByName( argv[0] );
	
	/// Call the command if found.
	if (command) {
		/*if( command->func_r )
			rv = command->func_r(0,0,0,0) < 0 ? false : true;
		else*/ if( command->funcptr )
			rv = (command->funcptr)(argc, argv);
		else
			telnet_puts("Command not implemented yet"NEWLINE);
	} else {
		rv = shell_unknown(argc, argv);
	}
	
	return rv;
}


/**
 * Runs the shell by printing the prompt and processing each shell command typed in.
 */
void shell_process() 
{
	char  cmdbuf[CMDLINE_BUFFER_SIZE]
		, respBuf[256];

	// Tell the user the shell is starting.
	telnet_printf("Starting Shell..."NEWLINE);

	/// Send a prompt.
	telnet_puts(CLI_INVITE);
	telnet_flush();

	/// Get a string.
	while( telnet_gets(cmdbuf, sizeof(cmdbuf), 0, true) ){
		
		/// Process the line as a command.
		//if( !shell_command(cmdbuf) ) break;
		
		if( cli_command(respBuf, sizeof(respBuf), cmdbuf, 0) == 1 ) 
			break;	/// Exit shell.
		telnet_puts( respBuf );
		
		// Send a prompt.
		telnet_puts(CLI_INVITE);
		telnet_flush();
	}
}


/**
 * Search command by name
 */
struct shell_command * shell_searchCommandByName( const char * cmdname )
{
	return (struct shell_command *) bsearch( /*key*/  cmdname, 
											 /*base*/ commands, 
											 /*nmemb*/array_length(commands),
											 /*size*/ sizeof(struct shell_command), 
											 /*compareFunction*/(int(*)(const void*, const void*))strcasecmp );
}
