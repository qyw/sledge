/**
 * @file    multiSender.h
 * \author 	Ivan "Kyb Sledgehammer" Kuvaldin <i.kyb[2]ya,ru>
 * @version 1.0.0
 * @date    2015-03-16
 * @brief   
 */

/** Define to prevent recursive inclusion ------------------------------------*/
#ifndef __MULTISENDER_H
#define __MULTISENDER_H

#ifdef __cplusplus
extern "C" {
#endif

	 
//#ifndef INC_FREERTOS_H
// #error "include FreeRTOS.h" must appear in source files before "include udpSender.h"
//#endif
//#ifndef INC_TASK_H
// #error "include task.h" must appear in source files before "include udpSender.h"
//#endif
#ifndef SEMAPHORE_H
 #error "include semphr.h" must appear in source files before "include udpSender.h"
#endif


//#include <stdint.h>
//#include "lwip/err.h"
#include "udpSenderOptions.h"
//#include "vmp.h"


#ifndef LedMULTISENDER
#define LedMULTISENDER  LED_Green
#endif

extern volatile QueueHandle_t multiSenderQueue;
extern volatile SemaphoreHandle_t multiSenderSemaphore;


/** 
 *	=== PUBLIC FUNCTIONS ===
 */
int8_t multiSenderStart(SenderOptions_t *opts);
bool multiSenderStop(void);



#ifdef __cplusplus
}
#endif

#endif /* __MULTISENDER_H */
