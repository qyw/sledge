/**
 * \author 	Ivan "Kyb Sledgehammer" Kuvaldin <i.kyb[2]ya,ru>
 * 
 */


#include "lwip/opt.h"

#if LWIP_NETCONN

#include "lwip/api.h"
#include "lwip/sys.h"
#include "lwip/sockets.h"

/// FreeRTOS includes
//#include "semphr.h"
//#include "task_priorities.h"

/// Kyb's includes
#include "./udpCLI.h"
#include "network/udpSenderOptions.h"
#include "Sledge/cli/cli.h"
#include "mainConfig.h"
#include "task_priorities.h"

#include <string.h>
#include <stdbool.h>

/// ==== DEBUG ====
#include "debugConfig.h"			/// Contains DEBUG_LEVELs for modules
#undef DEBUG_LEVEL					/// Redefiniton of DEBUG_LEVEL
#define DEBUG_LEVEL  0//udpCLI_DEBUG_LEVEL
#include "Sledge/debug.h"
#include "Sledge/assert.h"

	
/*#define MTU_SIZE_DEFAULT	1500
#define IP4_HEADER_SIZE		60
#define UDP_HEADER_SIZE		8
#define UDP_USER_MAX_SIZE	(MTU_SIZE_DEFAULT - IP4_HEADER_SIZE - UDP_HEADER_SIZE)
*/

#define RESPONSE_BUFFER_SIZE  512

/**
 * BUG не хочет работать когда задается в теле функции-задачи. Стека выделял выше крыши 1,5KB 
 * вываливается в HardFault при вызове netconn_delete или где-то там. */
static char buffer[RESPONSE_BUFFER_SIZE];

/**
 *	=== FUNCTIONS ===
 */

/**
 * 
 */
static void udpCLI_thread(void *arg) 
{  	
	struct netconn 	*connCLI;
	struct netbuf 	*buf;
	err_t err;
		//, recv_err;
	ip_addr_t *addr;
	uint16_t port;
	SenderOptions_t opts;
	uint16_t len=0;
	
	LWIP_UNUSED_ARG(arg); //это такой прием, чтобы избежать warning`а о неиспользуемых аргументах функции.
	
	debugf2( "udpCLI_thread"NEWLINE );
	
	/// 1.Create connection
	connCLI = netconn_new(NETCONN_UDP);
	if (connCLI == NULL) {
		debugf("can not create new UDP netconn" NEWLINE);
		goto stopTask;
	}
		
	/// 2.Bind the connection to a local IP adress & port
	err = netconn_bind(connCLI, IP_ADDR_ANY, UDP_PORT_NUMBER_CLI);
	if( err != ERR_OK ){
		netconn_delete(connCLI);
		debugf("can not bind netconn" NEWLINE);
		goto stopTask;
	}

	/// 3.Infinite loop - check if data received
	while(1) {
		//FIXME После первого полученного пакета connCLI привязывается к ip, не принимает пакеты от других хостов.
		err = netconn_recv(connCLI, &buf);
		if (err == ERR_OK) {
			/// Подготовка ответа
			addr = netbuf_fromaddr(buf); 	//Take ip-address and port received packet from.
			port = netbuf_fromport(buf);	//Порт должен быть UDP_PORT_NUMBER_CLI (binded to UDP_PORT_NUMBER_CLI)
			
			ip_addr_copy( opts.destinationIP, *addr );
			
			/// Нуль-терминировать пришендую строку. Последний символ будет удалён.
			( (char*)buf->p->payload )[buf->p->len -1] = '\0';
			
			//TODO Передать длину полученной стороки.
			cli_command( /*response*/ buffer, 
						 /*respLen*/  sizeof(buffer),
						 /*cmdline*/  buf->p->payload, 
						 /*opts*/ /*(void*)*/&opts);
			
			len = strlen( buffer );
		
			if (len) {
				/// Создать новый struct pbuf на отправку. Принятый очистится этой же функцией.
				netbuf_alloc(buf, 0);
				buf->p->payload = buffer;
				
				buf->p->tot_len = buf->p->len = len;
				
				netconn_sendto( connCLI, buf, addr, port );
			}
			
			netbuf_delete(buf); /** Обязательно очищать buffer после использования. 
									netconn_recv() создает свой буффер */
		}
	}
	
	stopTask:
	/// Stop this task
	for( ;; vTaskDelete(NULL) );
}

/*-----------------------------------------------------------------------------------*/
void udpCLI_init(void)
{
	sys_thread_new("udpCLI_thread", udpCLI_thread, NULL, DEFAULT_THREAD_STACKSIZE*1, UDPCLI_THREAD_PRIO );
	//TODO xTaskCreate
}

/*-----------------------------------------------------------------------------------*/

#else /* LWIP_NETCONN */
#warn "Module udpCLI.c cannot be copilied since LWIP_NETCONN is OFF."
#endif /* LWIP_NETCONN */
