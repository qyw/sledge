/**
 * 
 */ 

#ifndef __UDPSENDEROPTIONS_H
#define __UDPSENDEROPTIONS_H

#ifdef __cplusplus
 extern "C" {
#endif

#include "lwip/ip_addr.h"
	 
typedef struct SenderOptions_t {
	struct ip_addr destinationIP;
} SenderOptions_t;


#ifdef __cplusplus
}
#endif

#endif /* __UDPSENDEROPTIONS_H */
