/**
 * @file    debugConfig.h
 * @author  Kyb <i,Kyb[2]ya,ru>
 * @version 1.0.0
 * @date    2015-mar-19
 * @brief   
 */
  
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __DEBUG_CONFIG_H
#define __DEBUG_CONFIG_H	100


/// Don't forget bit-field
#define MULTI_SENDER_DEBUG_LEVEL 	0
#define UDP_SENDER_DEBUG_LEVEL 	0
#define CLI_DEBUG_LEVEL 	3
#define SD_DEBUG_LEVEL  	3



#endif /* __DEBUG_CONFIG_H */  
