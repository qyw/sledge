/**
 * @file 	mainConfig.h
 * @author	Kyb
 * @version	1.2.5
 * @date	2015-Apr-21
 * @brief   This file contains main definitions for the program.
 *			
 */

/** Define to prevent recursive inclusion -----------------------------------*/
#ifndef __MAIN_CONFIG_H
#define __MAIN_CONFIG_H

//#ifdef __cplusplus
// extern "C" {
//#endif


/// 0: No RTOS, 1: FreeRTOS, others: ToDo
#define USE_RTOS 	0

/// New line is: 0=LF (Linux), 1=CR (old Mac), 2=CR+LF (Windows)
#define NEWLINE_OPTION 	0


#define USE_USB_VIRTUAL_COM_PORT	1
/** Uncomment SERIAL_DEBUG to enables retarget of printf to  serial port (COM1 on STM32 evalboard) 
  * for debug purpose */
#define SERIAL_DEBUG				1


/**
 * Назначение светодиодов
 * На каком пине какого цвета светодиод определено в файле bsp_pcb.h
 * тип GPIO_t*
 */
#define LedUDP_SENDER  	LED_Green 
#define LedMULTISENDER 	LED_Green
#define LED_DHCP  		LED_Green
#define LedFAULT  		LED_Red

//#define LedBACKGROUND 	0//LED_Blue

#define LedALARM 	LED_Red
#define LedWARNING 	LED_Red
#define LedSD 		LED_Blue
#define LedSENSOR 	LED_Orange



#define USE_DHCP		/* enable DHCP; if disabled static address is used */
//#define USE_AUTOIP

/**	
 */
#define HOSTNAME_TEMPLATE	"VibroMx%x"

/** MAC ADDRESS */
#define MAC_ADDR0 	02 	// 02 says that this is not Globally registred MAC. Only for local area networks (LANs). So it is free of charge.
#define MAC_ADDR1 	00
#define MAC_ADDR2 	00
#define MAC_ADDR3 	00
#define MAC_ADDR4 	00
#define MAC_ADDR5 	10
#define MAC_ADDR 	"02:00:00:00:00:10"
 
/** Static IP ADDRESS */
#define IP_ADDR0   192
#define IP_ADDR1   168
#define IP_ADDR2   1
#define IP_ADDR3   10
#define IP_ADDR   "192.168.1.10"

/** NETMASK */
#define NETMASK_ADDR0 	255
#define NETMASK_ADDR1 	255
#define NETMASK_ADDR2 	255
#define NETMASK_ADDR3 	0
#define NETMASK_ADDR 	"255.255.255.0"

/** Static Gateway Addres */
#define GW_ADDR0 	192
#define GW_ADDR1 	168
#define GW_ADDR2 	1
#define GW_ADDR3 	1
#define GW_ADDR  	"192.168.1.1"

//#define SNTP_SERVER_ADDRESS

#define MULTICAST_ADDRESS_BASE	  229,9,9,0	/// Array of bytes
#define MULTICAST_ADDRESS_BASE0	  239
#define MULTICAST_ADDRESS_BASE1 	0
#define MULTICAST_ADDRESS_BASE2    43
#define MULTICAST_ADDRESS_BASE3    36
#define MULTICAST_PORT 			 4336

#define UDP_PORT_NUMBER_CLI 	(unsigned short)4334
#define UDP_PORT_NUMBER_SENDER 	(unsigned short)4335

#define FTPD_PORT 	21
#define TFTPD_PORT 	69


/* MII and RMII mode selection, for STM324xG-EVAL Board(MB786) RevB ***********/
#define RMII_MODE	// User have to provide the 50 MHz clock by soldering a 50 MHz
					// oscillator (ref SM7745HEV-50.0M or equivalent) on the U3
					// footprint located under CN3 and also removing jumper on JP5. 
					// This oscillator is not provided with the board. 
					// For more details, please refer to STM3240G-EVAL evaluation
					// board User manual (UM1461).
//#define MII_MODE

/* Uncomment the define below to clock the PHY from external 25MHz crystal (only for MII mode) */
#ifdef 	MII_MODE
 #define PHY_CLOCK_MCO
#endif

	/* STM324xG-EVAL jumpers setting
    +==========================================================================================+
    +  Jumper |       MII mode configuration            |      RMII mode configuration         +
    +==========================================================================================+
    +  JP5    | 2-3 provide 25MHz clock by MCO(PA8)     |  Not fitted                          +
    +         | 1-2 provide 25MHz clock by ext. Crystal |                                      +
    + -----------------------------------------------------------------------------------------+
    +  JP6    |          2-3                            |  1-2                                 +
    + -----------------------------------------------------------------------------------------+
    +  JP8    |          Open                           |  Close                               +
    +==========================================================================================+*/

/** Uncomment if Low Speed External quartz (LSE) is not installed. For RTC */
//#define NO_LSE




/*	┌-----------------------------------------------------------------------┐
	|				SD card, SDIO or SPI interface defines					|  
	└-----------------------------------------------------------------------┘ */
/// Use detect pin
#define FATFS_USE_DETECT_PIN	1
#define SD_DETECT_GPIO			PD3

/** Generete interrupt when card inserted. 
 *  Set EXTIx_IRQHandler function name, 
 * 		0 to switch of feature, 
 * 		negative value if you want to implement IRQHandler yourself. 
 * 		Strongly recommended for ports with number more than 4: Px5, Px6, ..., Px15.
 */
//#define FATFS_USE_DETECT_PIN_INTERRUPT 		 EXTI3_IRQHandler
#define FATFS_USE_DETECT_PIN_IRQHandler 	 EXTI3_IRQHandler
#define FATFS_USE_DETECT_PIN_INTERRUPT_PRIO  12

#define FATFS_USE_WRITEPROTECT_PIN		0
#define SD_WRITEPROTECT_GPIO	PB7

/// SDIO 1-bit mode or 4-bit mode. Default is 4-bit.
#define FATFS_SDIO_4BIT         1


//#include "task_prioriries.h"


//#ifdef __cplusplus
//}
//#endif

#endif /* __MAIN_CONFIG_H */
