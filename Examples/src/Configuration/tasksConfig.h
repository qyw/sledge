/**
 * @file	task_priorities.h
 * @version	1.0.0
 * @date	2015-feb-27
 * 
 * Предназначено для более удобного контроля за приоритетами задач.
 */
 
#ifndef __task_priorities_H
#define __task_priorities_H


#define configMAX_PRIORITIES			10	//((unsigned portBASE_TYPE) 8)

/// tskIDLE_PRIORITY is 0
#define MAIN_TASK_PRIO	(tskIDLE_PRIORITY + 1)
#define DHCP_TASK_PRIO	(tskIDLE_PRIORITY + 3)
#define LED_TASK_PRIO	(tskIDLE_PRIORITY + 2)
#define SI_TASK_PRIO	3

#define VIBRO_TASK_PRIO (configMAX_PRIORITIES - 1)

#define UDPCLI_THREAD_PRIO  		(tskIDLE_PRIORITY + 4)
#define UDPSENDER_THREAD_PRIO		(tskIDLE_PRIORITY + 5)
#define MULTI_SENDER_THREAD_PRIO 	UDPSENDER_THREAD_PRIO

#define SD_TASK_PRIO  4

#define PTP_PRIO	8
	


#endif /*__task_priorities_H*/
