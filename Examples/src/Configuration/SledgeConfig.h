/**
 * Sledge - wide functionality library, suitable for embedded systems, 
 * mainly for ARM Cortex and more particularly for STM32 family
 * 
 * Copyright (c) 2015, Ivan Kuvaldin. All rights reserved.
 * 
 * Licensed under BSD or MIT. 
 * Please refer to Licence.BSD.txt or Licence.MIT.txt provided with these sources.
 * You may obtain a copy of the Licenses at
 *      http://opensource.org/licenses/bsd-license.php
 *      http://opensource.org/licenses/mit-license.php
 */
/**
 * \file 	SledgeConfig.h
 * \author 	
 */
 
#ifndef __SledgeConfig_h
#define __SledgeConfig_h

/** ┌———————————————————————————————————————————————————————————————————————┐
	│				Configuration of c11n module							│  
	└———————————————————————————————————————————————————————————————————————┘ */
/** \addtogroup Configuration_of_c11n_module  Configuration of c11n module
	@{ */

/// Maximum length of parameter name
#define c11n_MAX_PARAMETER_NAME_LEN 	30

/// Maximum length of module name
#define c11n_MAX_MODULE_NAME_LEN 		25

/// Allow c11n_Type contain 64bit data itself, but not pointers. 1:allow, 0:disable.
#define c11n_64BIT_DATA  0

/** @} */


/** ┌———————————————————————————————————————————————————————————————————————┐
	│		Command line interface compilation configuration				│  
	└———————————————————————————————————————————————————————————————————————┘ */
/** \addtogroup Command_line_interface_compilation_configuration  Command line interface compilation configuration 
	@{ */

/// Чувствительность к регистру
#define CLI_REGISTER_SENS	0

/// Максимальное число аргументов с учётом комманды
#define CLI_ARGV_MAX		8

/// Макс. длинна имени комманды
#define CLI_CMD_NAME_SIZE  10

/// Приветствие
#define CLI_GREETING 	"==========================================="NEWLINE \
                        "==== Welcome to SLEDGE Telnet interface ==="NEWLINE \
                        "==========================================="NEWLINE

/// Приглашение коммандной строки
#define CLI_INVITE		"> "

/** @} */


/** ┌———————————————————————————————————————————————————————————————————————┐
	│	Main hardware timer as base for software timers and events			│
	└———————————————————————————————————————————————————————————————————————┘ */
/** \addtogroup Main_hw_timer_as_base_for_sw_timers_and_events  Main hardware timer as base for software timers and events
	@{ */

/// HW timer described as Sledge timer struct
#define bsp_TIMER     	TIMS[14]

/// Interrupt service routine for Update event of bsp_TIMER
#define bsp_TIMER_ISR 	TIM8_TRG_COM_TIM14_IRQHandler
 
/// Update frequency. Ticks per second
#define bsp_TIMER_UPD_FREQUENCY  (uint32_t)(1*1000*1000)

/// ISR priority (NVIC)
#define bsp_TIMER_ISR_PRIO  12

/** @} */


/** ┌———————————————————————————————————————————————————————————————————————┐
	│				RTOS configuration for Sledge							│
	└———————————————————————————————————————————————————————————————————————┘ */
/** \addtogroup RTOS_configuration_for_Sledge  RTOS configuration for Sledge
	\{ */
///\TODO CMSIS-RTOS

/// RTOS integration parameters
//#ifndef USE_RTOS
// #define USE_RTOS  0
//#endif 
//
//#if USE_RTOS == 1 	/// 1 means RTOS is FreeRTOS
// #include "FreeRTOSConfig.h"	//"FreeRTOS.h"
// #ifndef freqmIRQPrio
//  #define freqmIRQPrio	configLIBRARY_MAX_SYSCALL_INTERRUPT_PRIORITY
// #elif freqmIRQPrio < configLIBRARY_MAX_SYSCALL_INTERRUPT_PRIORITY
//  #error "freqmIRQPrio must be equal or lower by priority (higher by number) than configLIBRARY_MAX_SYSCALL_INTERRUPT_PRIORITY. Or its analog in other OS."
// #endif
// 
//#elif freqmRTOS > 1
// #error "Support of another OSes is not implemented yet."
//
//#else /* freqmRTOS */
// #ifndef freqmIRQPrio
//  #define freqmIRQPrio	0
// #endif
//#endif /* freqmRTOS */

/** \} */



#endif /*__SledgeConfig_h*/
