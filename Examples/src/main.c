/**
 * Sledge - wide functionality library, suitable for embedded systems, 
 * mainly for ARM Cortex and more particularly for STM32 family
 * 
 * Copyright (c) 2015, Ivan Kuvaldin. All rights reserved.
 * 
 * Licensed under BSD or MIT. 
 * Please refer to Licence.BSD.txt or Licence.MIT.txt provided with these sources.
 * You may obtain a copy of the Licenses at
 *      http://opensource.org/licenses/bsd-license.php
 *      http://opensource.org/licenses/mit-license.php
 */

/**
 * \file 	main.c
 * \author 	Ivan "Kyb Sledgehammer" Kuvaldin <i.kyb[2]ya,ru>
 * \brief 	
 */

#include "mainConfig.h"

#include <time.h>

/// Ethernet includes & network
//#include "stm32f4x7_eth.h"
//#include "stm32f4x7_eth_bsp.h"
#include "netconf.h"

/// FreeRTOS scheduler includes
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"


#define DEBUG_LEVEL 7
#include "Sledge.h"
#include "Sledge/dassel/logger/comport.h"
#include "Sledge/drv/stm32/sledge_stm32_RTC.h"
#include "Sledge/drv/stm32/sledge_stm32_RNG.h"
#include "Sledge/test/sledge_test.h"
//#include "Sledge/cli/cli.h"

#include "ads1252_driver.h"
#include "sd/sd.h"

//#include "ptpd-2.0.0/src/ptpd.h"
#include "network/telnet/telnet_mpthompson.h" 	//#include "network/telnet/telnet.h"
//#include "network/shell/shell.h"
#include "network/sntp/sntp.h"
#include "network/tftp/tftpserver.h"
#include "network/lwip-ftpd/ftpd.h"
#include "network/multiSender.h"

#include "indication.h"
#include "Sledge/segment_indicator/segment_indicator.h"


/// USB Device includes
#include "usbd_cdc_core.h"
#include "usbd_usr.h"
#include "usbd_desc.h"

///USB defines ?
/*#define TESTRESULT_ADDRESS         0x080FFFFC
#define ALLTEST_PASS               0x00000000
#define ALLTEST_FAIL               0x55555555
*/
#ifdef USB_OTG_HS_INTERNAL_DMA_ENABLED
  #if defined ( __ICCARM__ ) /*!< IAR Compiler */
    #pragma data_alignment = 4   
  #endif
#endif /* USB_OTG_HS_INTERNAL_DMA_ENABLED */
__ALIGN_BEGIN  USB_OTG_CORE_HANDLE  USB_OTG_dev  __ALIGN_END;


volatile uint32_t startTimeSeconds;

/* Private function prototypes -----------------------------------------------*/
void main_task(void * pvParameters);

extern void udpVmpMulticastSender( void *args );
extern void tcpecho_init(void);
extern void udpecho_init(void);
extern void udpCLI_init(void);

extern TaskHandle_t vibro_init( void );


/** @brief  Main program.
  * @param  None
  * @retval None
  */
int main(void)
{
	//SystemInit();  // GCC needs, MDK-ARM not. Look at startup_<uC>.s file
	
//	Sledge_init();
	//watchdog_init( 10*1000*1000/*us*/ );  // Не перестаёт работать во время отладки. См. Ref.Man. DBG manager.
	
	startTimeSeconds = myRTC_GetSeconds(true);//false);
	
	// To look at system clock
	bsp_MCO_Config( PC9 );	// PC9 only - conflict with SDIO 4 Wide Bus. Will be reconfigured if SDIO enabled.
	bsp_MCO_Config( PA8 );	// Conflict with freqM input.
	
	// Configure the priority grouping: 4 bits pre-emption priority. Required by FreeRTOS. Simpier.
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_4);
	
	bsp_LedInitAll();
	//test_spi();
	ads1252_init();
	while(1);
	
	// Traceanalyzer. FreeRTOS+Trace
	vTraceInitTraceData();
	assert_msg("uiTraceStart() fail. Look at RecorderDataPtr->systemInfo"NEWLINE, 
			uiTraceStart() 	);
	
	// Init Main task
	xTaskCreate( main_task, "main_task", configMINIMAL_STACK_SIZE * 5, NULL, MAIN_TASK_PRIO, NULL );
	
	// Start scheduler
	vTaskStartScheduler();
	
	// We should never get here as control is now taken by the scheduler
	for(;;);
}


/** @brief  Main task
  * @param  pvParameters not used
  * @retval None
  */
void main_task(void * pvParameters)
{
	BaseType_t errTask = pdFAIL;
	char timeBuf[40];
	struct tm timeStructure;
	extern volatile bool vcp_Opened;
	
	//bsp_LedInitAll();
	bsp_LedRgbInit( LEDS_RGB[0] );
	//bsp_LedOn(LED_Blue);
	//LCD_LED_Init();	/// Initialize LCD and Leds
	
		
 #if USE_USB_VIRTUAL_COM_PORT
	uint16_t wait = 0; //3000;
	/// Initialize VCP
	USBD_Init(	&USB_OTG_dev,
				USB_OTG_FS_CORE_ID,
				&USR_desc, 
				&USBD_CDC_cb, 		// communications device class (CDC) callbaks
				&USR_cb );			// User's callbacks
  #if GLOBAL_DEBUG_LEVEL
	// Ожидание подключения
	while( !vcp_Opened && wait>=50 ){
		vTaskDelay(50);
		wait-=50;
	}
	if( vcp_Opened )
		bsp_LedOn(LED_Green);
  #endif
 #endif

	fopen( "/dev/uart0", "" );  //DebugComPort_Init();
	printf(NEWLINE NEWLINE);
	printf("Build "__DATE__" "__TIME__ NEWLINE);
	_localtime_r( (const time_t*) &startTimeSeconds, &timeStructure );
	strftime( timeBuf, sizeofarr(timeBuf), "App started on %d.%m.%Y at %H:%M:%S" NEWLINE, &timeStructure );
	printf("%s", timeBuf );
	printf("UIDs: uid96  0x%X.%X.%X" NEWLINE, UID96->uidArr[0], UID96->uidArr[1], UID96->uidArr[2] );
	printf("      uid32  0x%X" NEWLINE, uid32() );
	printf("      uid8   0x%X or 0d%d" NEWLINE, uid8(), uid8() );
	
	si_init();
	si_puts("Init..");
	
	sd_InsertedTaskStart();
	
	// Start toogleLedBlue_task : Toggle led every XXms
	errTask = xTaskCreate(toggleLed_task, "toggleLed_task", configMINIMAL_STACK_SIZE, 
			NULL, LED_TASK_PRIO, NULL);
	assert_amsg( errTask == pdPASS );
	
	errTask = xTaskCreate( LedRGB_ColorRondo_task, "LedRGB_ColorRondo_task", configMINIMAL_STACK_SIZE, 
			LEDS_RGB[0], LED_TASK_PRIO-1, NULL);
	assert_msg("LedRGB_ColorRondo_task creating failed", errTask == pdPASS );
	
	
	LwIP_Init();		/// Initilaize the LwIP stack 
 #if LWIP_NETIF_HOSTNAME == 1
	printf( "Hostname: %s" NEWLINE, xnetif.hostname);
 #endif
 
	/*==============================================
	  === COMMUNICATION protocols initialization ===*/
	//tcpecho_init();			// Initialize tcp echo server on port 7
	//udpecho_init();		// Initialize udp echo server
	udpCLI_init(); 			// Initialize udp CLI
	telnet_shell_init();	/// Initialize telnet shell server.
	//telnet_init();			// telnet server on port 23
	//shell_init();			// shell (cmd line) on port 23
	//tftpd_init();			// TFTP daemon (server)  FAULT! While client tries to get file
	ftpd_init();
	
	/*===================================================================
	  === TIME SYNCRONIZATION protocols initialization via TCP/UDP/IP ===*/
	//if( PTPd_Init() != 0 ){		// PTP daemon ST example for 107
	/*if( ptpd_init() != 0 ){		// PTP daemon mpthompson
		///Handle error
		printf( "PTPd initialization error" NEWLINE );
	}*/
	sntp_init();			// Simple Network Time Protocol initialization
	
 #ifdef USE_DHCP
	// Start DHCPClient
	errTask = xTaskCreate(LwIP_DHCP_task, "LwIP_DHCP_task", configMINIMAL_STACK_SIZE * 2, 
			NULL, DHCP_TASK_PRIO, NULL);
	assert_amsg( errTask == pdPASS );
 #endif
	
	for(;;){
		debugf("_ FreeRTOS heap avaliable: %u of %u\n", xPortGetFreeHeapSize(), configTOTAL_HEAP_SIZE );
		//debugf(LwIP stats);
		vTaskDelay( ms2ticks(60000) );
	}
	//for(;;)	vTaskDelete(NULL); 		/// Delete current task
}
