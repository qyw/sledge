/**
 * Sledge - wide functionality library, suitable for embedded systems, 
 * mainly for ARM Cortex and more particularly for STM32 family
 * 
 * Copyright (c) 2015, Ivan Kuvaldin. All rights reserved.
 * 
 * Licensed under BSD or MIT. 
 * Please refer to Licence.BSD.txt or Licence.MIT.txt provided with these sources.
 * You may obtain a copy of the Licenses at
 *      http://opensource.org/licenses/bsd-license.php  
 *      http://opensource.org/licenses/mit-license.php  
 */
/**
 * @file    Sledge/c11n/c11n.c
 * @author  Kyb Sledgehammer <i.kyb[2]ya,ru>
 * @brief 	Look at ./c11n.h for more information.
 */

#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#include "Sledge.h"
//#include "Sledge/c11n/c11n.h"
//#include "Sledge/dassel/assert.h"


int volatile testParam = 125;


static GETTER_FUNCTION( testParam_get_ );
static SETTER_FUNCTION( testParam_set_ ); 	//static SETTER_FUNCTION( testParam_set_, pValue );


/// Must be sorted in ASCENDING order
const ModulesParameters_t MODULES_PARAMETERS[] = {
	{      "freqM",       /*freqM_PARAMETERS*/ },
	{    "general",                 PARAMETERS },
	{"multiSender", /*multiSender_PARAMETERS*/ },
	{         "sd",          /*sd_PARAMETERS*/ },
	{  "udpSender",   /*udpSender_PARAMETERS*/ },
};
const size_t MODULES_PARAMETERS_COUNT = array_length(MODULES_PARAMETERS);

/// Must be sorted in ASCENDING order
const Parameter_t PARAMETERS[] = {
	{"testParam" ,(void*)&testParam,   C11N_INT32, testParam_get_, (setterFunction_t) testParam_set_,	 125, "" },
};
const size_t PARAMETERS_COUNT = array_length(PARAMETERS);



static GETTER_FUNCTION( testParam_get_ )
{
	c11n_Type ret;
	ret.int32 = testParam;
	return ret;

	//return testParam;
	//return (void*)testParam;
}

static SETTER_FUNCTION( testParam_set_ )//, pValue )
{
	//testParam = pValue;
	testParam = value.int32;//(int)pValue;
}
