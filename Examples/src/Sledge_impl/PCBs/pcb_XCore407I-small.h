/**
 * Sledge - wide functionality library, suitable for embedded systems, 
 * mainly for ARM Cortex and more particularly for STM32 family
 * 
 * Copyright (c) 2015, Ivan Kuvaldin. All rights reserved.
 * 
 * Licensed under BSD or MIT. 
 * Please refer to Licence.BSD.txt or Licence.MIT.txt provided with these sources.
 * You may obtain a copy of the Licenses at
 *      http://opensource.org/licenses/bsd-license.php
 *      http://opensource.org/licenses/mit-license.php
 */

/**
 * \file 	pcb_XCore407I.h
 * \author 	Ivan "Kyb Sledgehammer" Kuvaldin <i.Kyb[2]ya,ru>
 * \brief 	This file defines pin lists and their appointments.
 * 			Constatants, pointers, arrays and all other C features 
 * 			are implemented in bsp_pcb.c
 * Bare XCore407I + Small LEDs board
 */


#ifndef __pcb_XCore407I_small_H
#define __pcb_XCore407I_small_H


/** ┌———————————————————————————————————————————————————————————————————————┐
	│						Main start up parameters						│  
	└———————————————————————————————————————————————————————————————————————┘ */
#define HSE_Crystal_MHz  8
#define PLL_M 	HSE_Crystal_MHz


/** ┌———————————————————————————————————————————————————————————————————————┐
	│						Outouts & LEDs control							│  
	└———————————————————————————————————————————————————————————————————————┘ */
/** List of regular LEDs */
#define _LEDS_ 	{ PH7, PH8, PH9, PH10, }

#define LED_Green 	PH8
#define LED_Orange 	PH10
#define LED_Red		PH9
#define LED_Blue 	PH7

#define LedALARM 	LED_Red
#define LedWARNING 	LED_Red
#define LedSD 		LED_Blue
#define LedSENSOR 	LED_Orange

/** List of LEDs with controllable brightness. 
 * 	@note Must be connected to TIMx channels. Look at alt. functions of GPIOs
 */
//#define _LEDS_PWM_ 	{ PH10, }
#define LEDS_PWM_TIM_HANDLER	TIMS[4]
#define LEDS_PWM_TIM_FREQ		500000
#define LEDS_PWM_TIM_PERIOD 	( (SystemCoreClock / LEDS_PWM_TIM_FREQ) - 1 )

/** List of RGB LEDs with controllable brightness. 
 * 	@note Must be connected to TIMx channels. Look at alt. functions of GPIOs
 */
//#define _LEDS_RGB_ 	{ 	/*Red │Green│ Blue│ common│         */ \
						{ PH10, PH11, PH12,      1, &TIMS[5] }, \
					}
//#define _LEDS_RGB_ {0}


/** ┌———————————————————————————————————————————————————————————————————————┐
	│					Segment indicators GPIOs							│  
	└———————————————————————————————————————————————————————————————————————┘ */
/** Список пинов, управляющих общими анодами(катодами). Определяет количество индикаторов.
 *  @note если работаем с 5V внимательно смотреть в даташит на предмет совместимости ноги с 5В.
 */
//#define _SegmentGpios_ { PF1, PF2, PF3, PF4, PF5, PF6, PF7, PF0 }

/** Список ножек, управляющих сегментами. Определяет количество сегментов
 * 	7 в обычном, 8 с точкой, 14 с Ж внутри, 16 + по 2 сегмента сверху и снизу
 *  @note если работаем с 5V внимательно смотреть в даташит на предмет совместимости ноги с 5В.
 */
//#define _CommonGpios_ 	{ PG2, PG3, PG4, PG5, PG6, }

//#define SegInd_Timer 			TIMS[12]
//#define SegInd_TimerIRQHandler 	TIM8_BRK_TIM12_IRQHandler


/** ┌———————————————————————————————————————————————————————————————————————┐
	│				Inputs, Buttons and joystick control					│  
	└———————————————————————————————————————————————————————————————————————┘ */
/** List of buttons */
/*#define _BUTTONS_ 	{ PE2, PE3, PE4, PE5, PE6, }
#define JOY_UP 		PE3
#define JOY_DOWN 	PE4
#define JOY_LEFT 	PE2
#define JOY_RIGHT 	PE5
#define JOY_PUSH 	PE6*/


/** ┌———————————————————————————————————————————————————————————————————————┐
	│							COM ports									│  
	└———————————————————————————————————————————————————————————————————————┘ */
						/* USART│          RCC         │     IRQn   │ TX │  RX │     GPIO_AF    │      DMA      */
#define _COM_debug_ 	{ USART1, RCC_APB2Periph_USART1, USART1_IRQn, PB6,  PB7,  GPIO_AF_USART1, USART1_DMA_BSP }
#define _COM_RS485_ 	{ USART6, RCC_APB2Periph_USART6, USART6_IRQn, PС6,  PС7,  GPIO_AF_USART6, USART6_DMA_BSP }


/** ┌———————————————————————————————————————————————————————————————————————┐
	│							Ethernet									│  
	└———————————————————————————————————————————————————————————————————————┘ */
#define ETH_LINK_GPIO 	PB14


/** ┌———————————————————————————————————————————————————————————————————————┐
	│			Ethernet PHY MII|RMII pins definitions						│  
	└———————————————————————————————————————————————————————————————————————┘ */
#define ETH_MDIO         	PA2
#define ETH_MDC          	PC1
//#define ETH_PPS_OUT    	PB5  // Uncomment if Eth-PTP Pulse per second output needed
							
#define ETH_MII_CRS      	PH2
#define ETH_MII_COL      	PH3
#define ETH_MII_RX_ER    	PI10  // posible conflict with LED4 on EVK407I
#define ETH_MII_RX_CLK   	PA1
#define ETH_MII_RX_DV    	PA7
#define ETH_MII_RXD0     	PC4
#define ETH_MII_RXD1     	PC5
#define ETH_MII_RXD2     	PH6
#define ETH_MII_RXD3     	PH7
#define ETH_MII_TX_EN    	PB11
#define ETH_MII_TX_CLK   	PC3
#define ETH_MII_TXD0     	PB12
#define ETH_MII_TXD1     	PB13
#define ETH_MII_TXD2     	PC2
#define ETH_MII_TXD3     	PB8
							
#define ETH_RMII_REF_CLK 	PA1
#define ETH_RMII_CRS_DV  	PA7
#define ETH_RMII_RXD0    	PC4
#define ETH_RMII_RXD1    	PC5
#define ETH_RMII_TX_EN   	PG11 //PB11
#define ETH_RMII_TXD0    	PG12 //PB12
#define ETH_RMII_TXD1    	PG13 //PB13


#endif /* __pcb_XCore407I_small_H */
