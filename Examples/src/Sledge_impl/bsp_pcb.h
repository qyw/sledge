/**
 * Sledge - wide functionality library, suitable for embedded systems, 
 * mainly for ARM Cortex and more particularly for STM32 family
 * 
 * Copyright (c) 2015, Ivan Kuvaldin. All rights reserved.
 * 
 * Licensed under BSD or MIT. 
 * Please refer to Licence.BSD.txt or Licence.MIT.txt provided with these sources.
 * You may obtain a copy of the Licenses at
 *      http://opensource.org/licenses/bsd-license.php
 *      http://opensource.org/licenses/mit-license.php
 */

/**
 * \file 	Sledge/bsp/bsp_pcb.h
 * \author 	Ivan "Kyb Sledgehammer" Kuvaldin <i.Kyb[2]ya,ru>
 * \version	2.6.0
 * \date 	2015-Apr-30
 * \brief 	This sub-module is a part of Sledge BSP module.
 * 			This code is to select proper file to include in accordance to defined board.
 * 			The PCB should be defined globally in project.
 */

#ifndef __BSP_BOARD_H
#define __BSP_BOARD_H 260

#ifdef PCB_XCORE407I
 /// Плата обределяется числом одно- или двузначным. 1ая - Тип. 2ая - Ревизия
 #define pcbTemp(x)  ( (PCB_XCORE407I == x) || (PCB_XCORE407I / 10 == x) )
 #if pcbTemp(3)
  #include "PCBs/pcb_XCore407I-Tablo.h"
 #elif pcbTemp(1)
  #include "PCBs/pcb_XCore407I-EVK.h"
 #elif pcbTemp(2)
  #include "PCBs/pcb_XCore407I-small.h"
 #elif pcbTemp(4)
  #include "PCBs/pcb_XCore407I-Crate.h"
 #endif
 #undef pcbTemp

#elif defined(PCB_STM32F4DISCOVERY) || defined(PCB_STM32F4_DISCO) || defined(BOARD_STM32F4DISCOVERY) || defined(BOARD_STM32F4_DISCO)
 #include "./PCBs/pcb_STM32F4-Disco.h"

#else
 #error Unknown PCB or PCB is not defined.
#endif

#endif /* __BSP_BOARD_H */
