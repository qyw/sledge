/**
 * 
 */

///\todo Ожидание Data_Ready. Может по прерыванию, если возможно настроить...


/*
The normal mode of reading data from the ADS1252 is for
the device reading the ADS1252 to latch the data on the
rising edge of SCLK (since data is shifted out of the ADS1252
on the falling edge of SCLK)
*/


#include "ads1252_driver.h"

#include "stm32f4xx.h"
//#include "stm32f4xx_spi.h"
//#include "stm32f4xx_dma.h"

#include "Sledge.h"
//#include "Sledge/bsp.h"
//#include "Sledge/utils.h"
//#include <stdint.h>


// \todo Перенести в драйвер. Писатели ST Peripheral drivers - какие-то индийцы
//#define EXTI_IMR  	(uint32_t)(EXTI_BASE+0x00) 	/// Interrupt Mask Register
//#define EXTI_EMR  	(uint32_t)(EXTI_BASE+0x04) 	/// Event Mask Register
//#define EXTI_RTSR  	(uint32_t)(EXTI_BASE+0x08) 	/// Rising Trigger Register
//#define EXTI_FTSR  	(uint32_t)(EXTI_BASE+0x0C) 	/// Falling Trigger Register
//#define EXTI_SWIER 	(uint32_t)(EXTI_BASE+0x10) 	/// Software Interrupt Event Register
//#define EXTI_PR  	(uint32_t)(EXTI_BASE+0x14) 	/// Pending Register

#define SYSCFG_EXTILineReset(gp) 	SYSCFG_EXTILineConfig( 0x0/*EXTI_PortSourceGPIOA*/, gp->extiPinSrc /*EXTI_PinSource0*/ )



#ifdef BOARD_STM32F4DISCOVERY

#define ADS1252_GPIO_SCK  	PB3//PA5//
#define ADS1252_GPIO_MISO 	PB4//PA6//
#define ADS1252_GPIO_DRdy 	PB5

#define ADS1252_GPIO_AF  	GPIO_AF_SPI1

#define ADS1252_SPI 		SPI1
#define ADS1252_SPI_RCC 	RCC_APB2Periph_SPI1
#define ADS1252_SPI_IRQn 	SPI1_IRQn
#define ADS1252_DMA_BSP 	BSP_SPI1_DMA  	// DMA2 Streams 3 and 0

#define ADS1252_DRdy_IRQHandler 	EXTI9_5_IRQHandler //EXTI4_IRQHandler
#define ADS1252_SPI_IRQHandler   	SPI1_IRQHandler
#define ADS1252_DMA_Tx_IRQHandler  	DMA2_Stream3_IRQHandler
#define ADS1252_DMA_Rx_IRQHandler  	DMA2_Stream0_IRQHandler

#elif PCB_XCORE407I == 2

// PI0 - NSS, PI1 - SCK, PI2 - MISO, PI3 - MOSI
#define ADS1252_GPIO_SCK 	PI1
#define ADS1252_GPIO_MISO 	PI2
#define ADS1252_GPIO_AF 	GPIO_AF_SPI2

#define ADS1252_SPI 		SPI2
#define ADS1252_SPI_RCC 	RCC_APB1Periph_SPI2
#define ADS1252_SPI_IRQn 	SPI2_IRQn
#define ADS1252_DMA_BSP 	BSP_SPI2_DMA  		// DMA1 Streams 4 and 3

#define ADS1252_DRdy_IRQHandler 	EXTI2_IRQHandler
#define ADS1252_SPI_IRQHandler   	SPI2_IRQHandler
#define ADS1252_DMA_Tx_IRQHandler  	DMA1_Stream4_IRQHandler
#define ADS1252_DMA_Rx_IRQHandler  	DMA1_Stream3_IRQHandler

#endif


#define ADS1252_MAIN_CLK_Hz 	(16000000ul)
#define ADS1252_MAIN_CLK_PETIOD_ns 	(1000000000ul/ADS1252_MAIN_CLK_Hz)  // 16MHz ~ 62.5ns, 10MHz ~ 100ns
#define ADS1252_TIMING_Cycle 	(384*ADS1252_CLK_PETIOD_ns)	// tDRDY Conversion Cycle 384•CLK
#define ADS1252_TIMING_DRDY		(36 *ADS1252_CLK_PETIOD_ns)	// DRDY Mode 36•CLK
#define ADS1252_TIMING_DOUT		(348*ADS1252_CLK_PETIOD_ns)	// DOUT Mode 348•CLK
#define ADS1252_TIMING_T1 		(6*ADS1252_CLK_PETIOD_ns) 	// DOR (data output register) write time
#define ADS1252_TIMING_T2 		(6*ADS1252_CLK_PETIOD_ns) 	// DOUT/DRDY LOW Time
#define ADS1252_TIMING_T3 		(6*ADS1252_CLK_PETIOD_ns) 	// DOUT/DRDY HIGH Time (Prior to Data Out)  6•CLK
#define ADS1252_TIMING_T4 		(24*ADS1252_CLK_PETIOD_ns)	// DOUT/DRDY HIGH Time (Prior to Data Ready)  24•CLK


typedef uint8_t ThreeQuarterWord[3];

//static ThreeQuarterWord ADS1252_input_data;//[2];
volatile /*static*/ union {
	char     bytes[3];
	uint32_t ui32;
	int32_t  i32;
} ads1252_data;


static const DMA_BSP_t ads1252_dma = ADS1252_DMA_BSP;


static int ads1252_dma_init( bool use_rx, bool use_tx );


/**
 * ВСЁ ЕЩЕ В ПРЕРЫВАНИИ
 */
int ads1252_process()
{
//	static bool already_here = true;
	
	// Detect !DRDY mode: _↑‾t4‾↓_t2_↑‾t3‾↓_t7(30ns max) → Data MSB→LSB
	///\todo возможно заюзать прерывания
	///\todo проверить возможно ли прочитать состояние ножки если она настроена как Alternative Function
	
	// Reconfigure pin to AF
	//ADS1252_GPIO_MISO->port->MODER &= ~(GPIO_MODER_MODER0 << (ADS1252_GPIO_MISO->pinsource * 2));
    //ADS1252_GPIO_MISO->port->MODER |= GPIO_Mode_AF << (ADS1252_GPIO_MISO->pinsource * 2);
	
	// Listen to DOUT mode. Automaticly by DMA.
	ADS1252_SPI->CR1 |= SPI_CR1_SPE;  //SPI_Cmd( ADS1252_SPI, ENABLE );  //
	SPI_I2S_DMACmd( ADS1252_SPI, SPI_I2S_DMAReq_Rx, ENABLE );  //ADS1252_SPI->CR2 |= SPI_I2S_DMAReq_Rx;
	
	// Принять 3 байта, потом выключить. В прерывании от DMA_Rx_Stream
	
	
//	already_here = false;
	return 0;
}



__STATIC_INLINE bool EXTI_is_rising_edge( uint32_t extiLine )
{
	return (EXTI->RTSR & extiLine) == extiLine;
}

__STATIC_INLINE bool EXTI_is_falling_edge( uint32_t extiLine )
{
	return (EXTI->FTSR & extiLine) == extiLine;
}

__STATIC_INLINE bool EXTI_is_both_edge( uint32_t extiLine )
{
	return EXTI_is_rising_edge && EXTI_is_falling_edge;
}

__STATIC_INLINE void EXTI_reverse_edges( uint32_t extiLine)
{
	// Rising edge enabled?
	if( EXTI_is_rising_edge(extiLine) ){
		EXTI->RTSR &= ~extiLine;  // disable rising
		EXTI->FTSR |= extiLine;   // enable falling edge
	} else {
		EXTI->RTSR |= extiLine;   // enable rising edge
		EXTI->FTSR &= ~extiLine;  // disable falling
	}
}

__STATIC_INLINE void EXTI_rising_edge( uint32_t extiLine )
{
	EXTI->RTSR |=  extiLine;  // enable rising edge
	EXTI->FTSR &= ~extiLine;  // disable falling edge
}

__STATIC_INLINE void EXTI_falling_edge( uint32_t extiLine )
{
	EXTI->RTSR &= ~extiLine;  // disable rising edge
	EXTI->FTSR |=  extiLine;  // enable falling edge
}



volatile uint8_t DRdy_cnt = 0; 
volatile int8_t input = 0, input_etalone = 0x5; 
volatile bool in = false;
// Отлавливает преамбулу 2 импульса по 4 фронтам - 2 нарастающих и 2 спадающих. Срабатывает через раз, но чаще всё же срабатывает.
/*void ADS1252_DRdy_IRQHandler()
{
	const uint32_t eiline = ADS1252_GPIO_DRdy->extiLine;//ADS1252_GPIO_MISO->extiLine;

	if( EXTI->PR & /EXTI->IMR &/ eiline )  //EXTI_GetITStatus( eiline ) != RESET )
	{	
		EXTI->PR = eiline;  //EXTI_ClearITPendingBit( ADS1252_GPIO_MISO->extiLine );
		EXTI->IMR &= ~eiline;  // mask interrupt from DOUT/DRDY pin
		//sw delay
		for( int i=200; i>0; i-- );
		ADS1252_SPI->CR1 |= SPI_CR1_SPE;  //SPI_Cmd( ADS1252_SPI, ENABLE );  //ads1252_process();
	}
}*/
// Отлавливает преамбулу 2 импульса по 4 фронтам - 2 нарастающих и 2 спадающих. Срабатывает через раз, но чаще всё же срабатывает.
void ADS1252_DRdy_IRQHandler()
{
	const uint32_t eiline = ADS1252_GPIO_DRdy->extiLine;//ADS1252_GPIO_MISO->extiLine;

	if( EXTI->PR & /*EXTI->IMR &*/ eiline )  //EXTI_GetITStatus( eiline ) != RESET )
	{	
		EXTI->PR = eiline;  //EXTI_ClearITPendingBit( ADS1252_GPIO_MISO->extiLine );
		
		EXTI_reverse_edges(eiline);
		//input |= bsp_InputRead(ADS1252_GPIO_DRdy) << DRdy_cnt; //input = in ? input | (in << DRdy_cnt) : input & (in << DRdy_cnt);
		DRdy_cnt++;
		
		if( DRdy_cnt == 4 ){
			//if( input == input_etalone ){  //0b0101
				EXTI->IMR &= ~eiline;  // mask interrupt from DOUT/DRDY pin
				//sw delay
				for( int i=100; i>0; i-- );
				ADS1252_SPI->CR1 |= SPI_CR1_SPE;  //SPI_Cmd( ADS1252_SPI, ENABLE );  //ads1252_process();
			//}
			DRdy_cnt = 0; 
			//input = 0;
			EXTI_rising_edge(eiline);
		}
		
		//assert_amsg(DRdy_cnt<4);
	}
}
/// Отлавливаем преамбулу по обоим фронтам одновременно. 2. Срабатывает через раз.
/*void ADS1252_DRdy_IRQHandler()
{
	const uint32_t eiline = ADS1252_GPIO_DRdy->extiLine;//ADS1252_GPIO_MISO->extiLine;
	static bool prev_in = 0;
	
	if( EXTI->PR & EXTI->IMR & eiline ){  //EXTI_GetITStatus( eiline ) != RESET )
		in = bsp_InputRead(ADS1252_GPIO_DRdy);
		// Проверка на равенство с предыдущим
		if( prev_in == in && DRdy_cnt != 0 ){
			DRdy_cnt--;
		} else if( DRdy_cnt == 0 && in == 0 )
		prev_in = in;
		
		if( ++DRdy_cnt == 4 ){
			//if( input == input_etalone ){  //0b0101
				EXTI->IMR &= ~eiline;  // mask interrupt from DOUT/DRDY pin
				ADS1252_SPI->CR1 |= SPI_CR1_SPE;  //SPI_Cmd( ADS1252_SPI, ENABLE );  //ads1252_process();
			//}
			DRdy_cnt = 0; 
			input = 0;
		}
	}
	// Clear interrupt pending bit
	EXTI->PR = eiline;  //EXTI_ClearITPendingBit( ADS1252_GPIO_MISO->extiLine );
}*/
/// если прерывание по обоим фронтам можно бы заюзать эту функцию. 1
/*void ADS1252_DRdy_IRQHandler()
{
	const uint32_t eiline = ADS1252_GPIO_DRdy->extiLine;//ADS1252_GPIO_MISO->extiLine;
	
	if( EXTI->PR & EXTI->IMR & eiline ){  //EXTI_GetITStatus( eiline ) != RESET )
		in = bsp_InputRead(ADS1252_GPIO_DRdy);
		// Проверка на равенство с предыдущим
		if( in == input >> (DRdy_cnt-1) )
			DRdy_cnt--;
		else
			input |= in << DRdy_cnt;
		
		if( ++DRdy_cnt == 4 ){
			if( input == input_etalone ){  //0b0101
				EXTI->IMR &= ~eiline;  // mask interrupt from DOUT/DRDY pin
				ADS1252_SPI->CR1 |= SPI_CR1_SPE;  //SPI_Cmd( ADS1252_SPI, ENABLE );  //ads1252_process();
			}
			DRdy_cnt = 0; 
			input = 0;
		}
	}
	// Clear interrupt pending bit
	EXTI->PR = eiline;  //EXTI_ClearITPendingBit( ADS1252_GPIO_MISO->extiLine );
}*/
/*void ADS1252_DRdy_IRQHandler()
{
	const uint32_t eiline = ADS1252_GPIO_DRdy->extiLine;//ADS1252_GPIO_MISO->extiLine;
	volatile/static/ uint8_t counter = 0; 
	volatile/static/ int8_t input = 0;

	if( EXTI_GetITStatus( eiline ) != RESET )
	{
		input |= bsp_InputRead(ADS1252_GPIO_MISO) << counter;
		++counter;
		if( counter == 4 && input == 0xA ){  //0b1010
			EXTI->IMR &= ~eiline;  // mask interrupt from DOUT/DRDY pin
			ADS1252_SPI->CR1 |= SPI_CR1_SPE;  //SPI_Cmd( ADS1252_SPI, ENABLE );  //ads1252_process();
		} else if( counter < 4 ){
			EXTI_reverse_edges( eiline );
		}
		
		if( counter == 4 ){
			counter = 0; input = 0;
			EXTI_rising_edge( eiline );
		}
	}
	// Clear interrupt pending bit
	EXTI->PR = eiline;  //EXTI_ClearITPendingBit( ADS1252_GPIO_MISO->extiLine );
}*/
/*
static uint8_t DRdy_cnt = 0;
void ADS1252_DRdy_IRQHandler()
{
	const uint32_t eiline = ADS1252_GPIO_DRdy->extiLine;
	
	if( EXTI_GetITStatus( eiline ) != RESET )
	{
		// Clear interrupt pending bit
		EXTI->PR = eiline;  //EXTI_ClearITPendingBit( ADS1252_GPIO_MISO->extiLine );
		
		if( ++DRdy_cnt == 3 ){
			// Second falling edge detected
			DRdy_cnt = 0;
			// Запрещение прерываний 
			EXTI->IMR &= ~eiline;  // mask interrupt from DOUT/DRDY pin
//			SYSCFG_EXTILineReset();//SYSCFG_EXTILineConfig( EXTI_PortSourceGPIOA, EXTI_PinSource0 );  // disconnect EXTI line from SYSCFG (Reset to default)
			ADS1252_SPI->CR1 |= SPI_CR1_SPE;  //SPI_Cmd( ADS1252_SPI, ENABLE );
			// Перенастроить ножку на АФ.
//			ADS1252_GPIO_MISO->port->MODER &= ~(GPIO_MODER_MODER0 << (ADS1252_GPIO_MISO->pinsource * 2));
//			ADS1252_GPIO_MISO->port->MODER |= GPIO_Mode_AF << (ADS1252_GPIO_MISO->pinsource * 2);
		}
	}
}*/


///\todo возможно использовать SPI-ное прерывание
void ADS1252_DMA_Rx_IRQHandler()
{
	// Transfer complete? So, NDTR (number of data to transfer ) is 0.
	if( DMA_GetITStatus( ads1252_dma.streamRx, ads1252_dma.ITsRx.TCIF ) ){
		DMA_ClearITPendingBit( ads1252_dma.streamRx, ads1252_dma.ITsRx.TCIF );
		// Перейти в режим ожидания DRDY. Выключить SPI, остановить SCLK, после завершения передачи.
		//while( (ADS1252_SPI->SR & SPI_SR_BSY) != RESET );  // Это может быть долго как для прерывания.
		while( ADS1252_SPI->CR1 & SPI_CR1_SPE )
			ADS1252_SPI->CR1 &= ~SPI_CR1_SPE;  //SPI_Cmd( ADS1252_SPI, DISABLE );  //		
		
		///\todo Обработка полученных данных
		
		
		// Разрешение прерываний от ножки
//		EXTI_falling_edge( ADS1252_GPIO_DRdy->extiLine );
//		SYSCFG_EXTILineConfig( ADS1252_GPIO_DRdy->extiPortSrc, ADS1252_GPIO_DRdy->extiPinSrc );
		EXTI->IMR |= ADS1252_GPIO_DRdy->extiLine;
	}
}


void ADS1252_SPI_IRQHandler()
{
	//const uint32_t DRdyLine = ADS1252_GPIO_DRdy->extiLine;
	static uint8_t count = 0;
	
	if( SPI_GetITStatus( ADS1252_SPI, SPI_IT_RXNE ) ){
		ads1252_data.bytes[count] = (uint8_t)ADS1252_SPI->DR;  //SPI_ReceiveData(
		if( SPI_GetITStatus( ADS1252_SPI, SPI_IT_OVR ) )  //(void)ADS1252_SPI->SR;  // Clear OVR (overrun) flag. And others too...
			ads1252_data.bytes[count] = (uint8_t)ADS1252_SPI->DR;
		++count;
		if( count == 3 ){
			count = 0;
			// Перейти в режим ожидания DRDY. Выключить SPI, остановить SCLK, дождавшись окончания передачи.
			//while( (ADS1252_SPI->SR & SPI_SR_BSY) != RESET );  // Это может быть долго как для прерывания.
			while( ADS1252_SPI->CR1 & SPI_CR1_SPE )
				ADS1252_SPI->CR1 &= ~SPI_CR1_SPE;  //SPI_Cmd( ADS1252_SPI, DISABLE );  //
			
			///\todo запихнуть полученные данные в RingBuffer C++
			
			
			// Разрешение прерываний от ножки
//			EXTI_falling_edge( ADS1252_GPIO_DRdy->extiLine );
//			SYSCFG_EXTILineConfig( ADS1252_GPIO_DRdy->extiPortSrc, ADS1252_GPIO_DRdy->extiPinSrc );
			// Clear interrupt pending bit
			DRdy_cnt = 0;
			EXTI->PR = ADS1252_GPIO_DRdy->extiLine;  //EXTI_ClearITPendingBit( ADS1252_GPIO_MISO->extiLine );
			for( int i=100; i>0; i-- );
			EXTI->IMR |= ADS1252_GPIO_DRdy->extiLine;
		} else if( count > 3 ){
			assert_amsg(0);
		}
	}
}


int ads1252_reset()
{
	ads1252_power_down();
	//wait( 
	ads1252_power_up();
	return 0;
}


int ads1252_sync( /*bool dontWait*/ )
{
	// Hold SCLK for at least 4, but less than 20 DOUT/DRDY cycles
	
	// Wait DOUT/DRDY pin pulses LOW for 3 CLK cycles and then held HIGH, and the modulator is held in a reset state.
	// For all ADS1252 devices
	
	// Start SCLK. The modulator is released from reset and synchronization occurs on the falling edge of SCLK.
	
	// Valid data is not present until the sixth DOUT/DRDY pulse.
	
	return 0;
}


int ads1252_power_up( /*bool dontWait*/ )
{
	// Start SCLK. 
	
	// Valid data is not present until the sixth DOUT/DRDY pulse.
	
	return 0;
}


int ads1252_power_down( /*bool dontWait*/ )
{
	// Go Hi ADS1252_GPIO_SCK
	bsp_OutHi( ADS1252_GPIO_SCK );
//	if( dontWait )
//		return 0;
	// wait at least 20 consecutive DOUT/DRDY periods 
//	delay_ns( ADS1252_TIMING_Cycle * 20 );
	return 0;
}


///\todo receive only bit must be set.
int ads1252_spi_init()
{
	SPI_InitTypeDef sis;
	NVIC_InitTypeDef nis;
	
	// Enable peripheral clock. Make sure your SPIx connected to ABPx
	if( ADS1252_SPI == SPI1 )
		RCC_APB2PeriphClockCmd( ADS1252_SPI_RCC, ENABLE );
	else
		RCC_APB1PeriphClockCmd( ADS1252_SPI_RCC, ENABLE );
	
	// Configure SPI
	sis.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_32;//16;  // for master olny
	sis.SPI_CPHA = SPI_CPHA_2Edge;  //1Edge;  // Specifies the clock active edge for the bit capture. ↑ is 1st, if CPOL_Low. NOT need in TI mode
	sis.SPI_CPOL = SPI_CPOL_Low;  	// Specifies the serial clock steady state. NOT need in TI mode
	sis.SPI_CRCPolynomial = 0x7;	// default
	sis.SPI_DataSize = SPI_DataSize_8b;
	sis.SPI_Direction = SPI_Direction_2Lines_RxOnly; //1Line_Rx;
	sis.SPI_FirstBit = SPI_FirstBit_MSB;
	sis.SPI_Mode = SPI_Mode_Master;
	sis.SPI_NSS = SPI_NSS_Hard;  //Soft;  //
	SPI_Init( ADS1252_SPI, &sis );
	ADS1252_SPI->CR2 |= SPI_CR2_SSOE; 	// In HW mode NSS output enabled (SSM = 0, SSOE = 1). 
										// This configuration is used only when the device operates in master mode. The
										// NSS signal is driven low when the master starts the communication and is kept
										// low until the SPI is disabled.
	
	/* Enable the NVIC and the corresponding interrupt using the function 
       SPI_ITConfig() if you need to use interrupt mode. */
	SPI_ITConfig( ADS1252_SPI, SPI_IT_RXNE, ENABLE );
	
	nis.NVIC_IRQChannel = ADS1252_SPI_IRQn;
	nis.NVIC_IRQChannelPreemptionPriority = 1;//2;
	nis.NVIC_IRQChannelSubPriority = 0;
	nis.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init( &nis ); 
	
	/* When using the DMA mode 
       (++) Configure the DMA using DMA_Init() function
       (++) Active the needed channel Request using SPI_I2S_DMACmd() function */
	assert_amsg( \
			ads1252_dma_init( /*tx*/false, /*rx*/true) == 0 );
	// Enable DMA request from SPI
	//SPI_I2S_DMACmd( ADS1252_SPI, SPI_I2S_DMAReq_Rx, ENABLE );  //ADS1252_SPI->CR2 |= SPI_I2S_DMAReq_Rx;

	// Enable SPI
//	SPI_Cmd( ADS1252_SPI, ENABLE );  //ADS1252_SPI->CR1 &= ~SPI_CR1_SPE;  //
	
	return 0;
}


int ads1252_init()
{
	GPIO_InitTypeDef gis;
	EXTI_InitTypeDef eis;

	ads1252_spi_init();
	
	// Configure and disable interrupt on ADS1252_GPIO_MISO
	gis.GPIO_Speed = GPIO_Speed_100MHz;
	gis.GPIO_PuPd = GPIO_PuPd_DOWN; //NOPULL; //
	gis.GPIO_OType = GPIO_OType_PP;
	eis.EXTI_Mode = EXTI_Mode_Interrupt;
	eis.EXTI_Trigger = EXTI_Trigger_Falling; //_Rising_Falling; //_Rising; //_Falling; //
	
	// Настроить ножку на генерирование прерываний. Потом вернуть в AF
	bsp_Input_InitGeneral( ADS1252_GPIO_DRdy, &gis, eBUTTON_MODE_EXTI, &eis, 0/*3*/ );
	//ADS1252_GPIO_MISO->port->MODER &= ~(GPIO_MODER_MODER0 << (ADS1252_GPIO_MISO->pinsource * 2));
	//ADS1252_GPIO_MISO->port->MODER |= GPIO_Mode_AF << (ADS1252_GPIO_MISO->pinsource * 2);
	// Configure GPIOs
	bsp_GpioAf_InitStruct( ADS1252_GPIO_SCK,  ADS1252_GPIO_AF, &gis );
	bsp_GpioAf_InitStruct( ADS1252_GPIO_MISO, ADS1252_GPIO_AF, &gis );
	
	return 0;
}


/**	
 * Используем готовую предопределённую структуру BSP_SPI2_DMA
 * \return	 0  OK
 * 			 1  nothing to configure
 * 			-2  TX stream in use
 * 			-3  RX stream in use
 * 			other negative value on error.
 * 			positive value - passed with warnings
 */
static int ads1252_dma_init( bool use_tx, bool use_rx )
{
	DMA_InitTypeDef dis;
	NVIC_InitTypeDef nis;
	
	if( (use_tx | use_rx) == false )
		return 1;
	
	// Enable DMA clocks
	DMAx_AHB1ClockEnable(ads1252_dma);  //DMAx_AHB1ClockCmd( dma, ENABLE );  //RCC_AHB1PeriphClockCmd( RCC_AHB1Periph_DMA1, ENABLE);
	
	// Make sure DMA stream(s) are not used. To avoid collisions.
	if( use_tx )
		assert_msg("ads1252_dma_init(): DMA stream TX is already in use!", 
				!IS_DMA_stream_configured( ads1252_dma.streamTx ) );	
	if( use_rx ){
		//debugf2("ads1252_dma_init(): 0x%X, 0x%X\n", dma.streamRx->CR, dma.streamRx->FCR );
		assert_msg("ads1252_dma_init(): DMA stream RX is already in use!", 
				!IS_DMA_stream_configured( ads1252_dma.streamRx ) );
	}
/*	// Make sure DMA stream(s) are not used. To avoid collisions. DO NOT stop program flow
	if( use_tx && IS_DMA_stream_configured( dma.streamTx ) ){
		//if( DMA clock was disabled )  DMAx_AHB1ClockDisable(dma);
		return -2;
	}
	if( use_rx && IS_DMA_stream_configured( dma.streamRx ) ){
		//if( DMA clock was disabled )  DMAx_AHB1ClockDisable(dma);
		return -3;
	}*/
	
	// Receive configuration
	DMA_StructInit( &dis );
	dis.DMA_DIR 				= DMA_DIR_PeripheralToMemory;
	dis.DMA_PeripheralBaseAddr 	= (uint32_t) &(ADS1252_SPI->DR);
	dis.DMA_Memory0BaseAddr 	= (uint32_t) &ads1252_data; //&ADS1252_input_data[0];
	dis.DMA_BufferSize 			= 3; //sizeofarr(ADS1252_input_data);
	dis.DMA_PeripheralInc		= DMA_PeripheralInc_Disable;
	dis.DMA_MemoryInc 			= DMA_MemoryInc_Enable; //Disable; //
	dis.DMA_PeripheralDataSize 	= DMA_PeripheralDataSize_Byte;
	dis.DMA_MemoryDataSize 		= DMA_MemoryDataSize_Byte;//Word; //	// Don’t care in Direct mode. Only periph data size has metter.
	dis.DMA_Mode 				= DMA_Mode_Circular;
	dis.DMA_Priority 			= DMA_Priority_VeryHigh;
	dis.DMA_FIFOMode 			= DMA_FIFOMode_Disable;				/// Disable -> Direct mode used.
	//dis.DMA_FIFOThreshold  	= DMA_FIFOThreshold_3QuartersFull; 	/// Doesn't metter while Direct mode used (FIFO disabled)
	//dis.DMA_MemoryBurst 		= DMA_MemoryBurst_Single;       	/// Doesn't metter while Memory Increment mode disabled
	//dis.DMA_PeripheralBurst 	= DMA_PeripheralBurst_Single; 	/// Doesn't metter while Peripheral Increment mode disabled
	
	// SPIx_RX
	dis.DMA_Channel = ads1252_dma.channel;
	DMA_Init( ads1252_dma.streamRx, &dis );
	
	DMA_ITConfig( ads1252_dma.streamRx, DMA_IT_TC, ENABLE );
	
	nis.NVIC_IRQChannel = ads1252_dma.irqRx;
	nis.NVIC_IRQChannelPreemptionPriority = 0;//2;
	nis.NVIC_IRQChannelSubPriority = 0;
	nis.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init( &nis ); 
	
	DMA_Cmd( ads1252_dma.streamRx, ENABLE );
	
	/*
	// Transmit configuration
	dis.DMA_DIR = DMA_DIR_MemoryToPeripheral;
	dis.DMA_PeripheralBaseAddr = (uint32_t) &(ADS1252_SPI->DR);
	dis.DMA_Memory0BaseAddr = (uint32_t) &ADS1252_input_data[0];
	dis.DMA_BufferSize = sizeof(ADS1252_input_data);
	
	// SPI2_TX
	dis.DMA_Channel = DMA_Channel_0;
	DMA_Init( DMA2_Stream2, &dis);
	DMA_Cmd( DMA2_Stream2, ENABLE);
	
	// Сonfigure & enable DMA request from SPI
	SPI_I2S_DMACmd( ADS1252_SPI, SPI_I2S_DMAReq_Tx, ENABLE );
	*/
	
	return 0;
}


///\todo 
int ads1252_spi_deinit()
{
	
	return -1;
}
