/**
 * Initial impl. is from Percepio's Traceanalyzer example for STM32F400. Sad code. No configurtion.
 */

#include "FreeRTOS.h"
#include "task.h"

#include "Sledge/debug.h"


#if( configUSE_MALLOC_FAILED_HOOK == 1 )
void vApplicationMallocFailedHook( void )
{
	vTraceConsoleMessage("\n\rMalloc failed!\n\r");
	debugf("\n\rMalloc failed!\n\r");
}
#endif

void vApplicationStackOverflowHook( TaskHandle_t *pxTask, signed char *pcTaskName )
{
	(void)pxTask;
	(void)pcTaskName;
	vTraceConsoleMessage("\n\rStack overflow!\n\r");
}

#if (configUSE_IDLE_HOOK == 1)
/* Call the user defined function from within the idle task.  This
allows the application designer to add background functionality
without the overhead of a separate task.
NOTE: vApplicationIdleHook() MUST NOT, UNDER ANY CIRCUMSTANCES,
CALL A FUNCTION THAT MIGHT BLOCK. */
/**
 * @brief	This function is called from within the FreeRTOS idle task
 * 			if configUSE_IDLE_HOOK is set to 1.
 */
void vApplicationIdleHook(void) {
	
	
}

#endif /*(configUSE_IDLE_HOOK == 1)*/
