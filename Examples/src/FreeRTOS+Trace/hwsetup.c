/**
 * 
 * Initial impl. is from Percepio's Traceanalyzer example for STM32F400. Sad code. No configurtion.
 */
 
//#include "FreeRTOS.h"
//#include "trcUser.h"
#include <stm32f4xx.h>


#define RUN_TIME_STATS_TIMER				TIM8						//TIM1
#define RUN_TIME_STATS_TIMER_RCC 			RCC_APB2Periph_TIM8			//RCC_APB2Periph_TIM1
#define RUN_TIME_STATS_TIMER_IRQHandler 	TIM8_UP_TIM13_IRQHandler	//TIM1_UP_TIM10_IRQHandler
#define RUN_TIME_STATS_TIMER_IRQn 			TIM8_UP_TIM13_IRQn			//TIM1_UP_TIM10_IRQn


static long runTimeStatsCounter;


void prvSetupHardware( void );


void prvSetupHardware( void )
{}

	
void vConfigureTimerForRunTimeStats( void )
{
	TIM_TimeBaseInitTypeDef TIM_TimeBaseStructure;
	NVIC_InitTypeDef NVIC_InitStructure;

	
	/* init variables */
	runTimeStatsCounter=100;
	
	/*Init RCC clock Timer */
	RCC_APB2PeriphClockCmd(RUN_TIME_STATS_TIMER_RCC, ENABLE);

	/* TIM1 - Time Base configuration */
	TIM_TimeBaseStructInit(&TIM_TimeBaseStructure);
	TIM_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV4; 	/// TIMbase freq  	 168Mhz/4 = 42MHz
	TIM_TimeBaseStructure.TIM_Prescaler = 168 ;					/// Tick freq 	 	42MHz/168 = 250KHz
	TIM_TimeBaseStructure.TIM_Period = 10000 ;					/// Update freq	 250KHz/10000 = 25Hz	Upd.Period = 1/25Hz = 40ms
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
	TIM_TimeBaseInit( RUN_TIME_STATS_TIMER, &TIM_TimeBaseStructure);
	
	/* NVIC - Enable and set Interrupt to TIM1 */
	NVIC_InitStructure.NVIC_IRQChannel = TIM8_UP_TIM13_IRQn ;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
	
	/* TIM1 interrupt Enable */
	TIM_ITConfig( RUN_TIME_STATS_TIMER, TIM_IT_Update, ENABLE) ;
	
	/* TIM1 - counter enable */
	TIM_Cmd( RUN_TIME_STATS_TIMER, ENABLE);
}


//void TIM1_UP_TIM10_IRQHandler(void)
void TIM8_UP_TIM13_IRQHandler(void)
{
	if( TIM_GetITStatus( RUN_TIME_STATS_TIMER, TIM_IT_Update) != RESET ){
	//if( TIM_GetFlagStatus( RUN_TIME_STATS_TIMER, TIM_FLAG_Update) != RESET ){
		TIM_ClearITPendingBit( RUN_TIME_STATS_TIMER, TIM_IT_Update);		/// Clear pending interrupt bit
		runTimeStatsCounter += 1 ;
	}
}

unsigned ValueTimerForRunTimeStats(void){
	return runTimeStatsCounter;
}
