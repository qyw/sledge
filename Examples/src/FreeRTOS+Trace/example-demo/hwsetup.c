
#include "FreeRTOS.h"
#include "trcUser.h"
#include <stm32f4xx.h>

void prvSetupHardware( void );

void prvSetupHardware( void )
{
}

long Tim1RunCounter;
	
void vConfigureTimerForRunTimeStats( void )
{TIM_TimeBaseInitTypeDef   TIM_TimeBaseStructure;
 NVIC_InitTypeDef NVIC_InitStructure;

	
	/* init variables */
	Tim1RunCounter=100;
	
	/*Init RCC clock Timer */
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM1  , ENABLE);

  /* TIM1 - Time Base configuration */
  TIM_TimeBaseStructInit(&TIM_TimeBaseStructure);
  TIM_TimeBaseStructure.TIM_Period = 10000 ;
  TIM_TimeBaseStructure.TIM_Prescaler = 168 ;
  TIM_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV4;
  TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
  TIM_TimeBaseInit(TIM1, &TIM_TimeBaseStructure);

	  /* NVIC - Enable and set Interrupt to TIM1 */
  NVIC_InitStructure.NVIC_IRQChannel = TIM1_UP_TIM10_IRQn ;
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStructure);

	  /* TIM1 interrupt Enable */
	TIM_ITConfig(TIM1,TIM_IT_Update,ENABLE) ;

  /* TIM1 - counter enable */
  TIM_Cmd(TIM1, ENABLE);
}

void TIM1_UP_TIM10_IRQHandler(void)
{
  if(TIM_GetITStatus(TIM1,TIM_IT_Update) != RESET)
  {
    /* Clear TIM1 pending interrupt bit */
    TIM_ClearITPendingBit(TIM1,TIM_IT_Update);
		Tim1RunCounter += 1 ;
  }
}

unsigned ValueTimerForRunTimeStats(void){
	return Tim1RunCounter ;
}

