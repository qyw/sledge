

#ifndef __ads1252_driver_H
#define __ads1252_driver_H

#ifdef __cplusplus
 extern "C" {
#endif


int ads1252_init(void);
//int ads1252_spi_init(void);
int ads1252_power_up(void);
int ads1252_power_down(void);
int ads1252_reset(void);
//int ads1252_process(void);


#ifdef __cplusplus
}
#endif	 

#endif /*__ads1252_driver_H*/
