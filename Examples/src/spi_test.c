/**
 * 
 */

///\todo Ожидание Data_Ready. Может по прерыванию, если возможно настроить...


/*
The normal mode of reading data from the ADS1252 is for
the device reading the ADS1252 to latch the data on the
rising edge of SCLK (since data is shifted out of the ADS1252
on the falling edge of SCLK)
*/


#include "ads1252_driver.h"

#include "stm32f4xx.h"
//#include "stm32f4xx_spi.h"
//#include "stm32f4xx_dma.h"

#include "Sledge.h"
//#include "Sledge/bsp.h"
//#include "Sledge/utils.h"
//#include <stdint.h>



#ifdef BOARD_STM32F4DISCOVERY

#define TEST_MASTER_GPIO_NSS 	PA4
#define TEST_MASTER_GPIO_SCK  	PA5//PB3
#define TEST_MASTER_GPIO_MISO 	PA6//PB4
#define TEST_MASTER_GPIO_MOSI 	PA7//PB4

#define TEST_MASTER_GPIO_AF  	GPIO_AF_SPI1

#define TEST_MASTER_SPI 		SPI1
#define TEST_MASTER_SPI_RCC 	RCC_APB2Periph_SPI1
#define TEST_MASTER_SPI_IRQn 	SPI1_IRQn
#define TEST_MASTER_DMA_BSP 	BSP_SPI1_DMA  	// DMA2 Streams 3 and 0

#define test_master_spi_IRQHandler     	SPI1_IRQHandler
#define TEST_DMA_MASTER_Tx_IRQHandler  	DMA2_Stream3_IRQHandler
#define test_spi_master_DMA_Rx_IRQHandler  	DMA2_Stream0_IRQHandler



#define TEST_SLAVE_GPIO_NSS 	PA15
#define TEST_SLAVE_GPIO_SCK  	PB3
#define TEST_SLAVE_GPIO_MISO 	PB4
#define TEST_SLAVE_GPIO_MOSI 	PB5

#define TEST_SLAVE_GPIO_AF  	GPIO_AF_SPI3

#define TEST_SLAVE_SPI 			SPI3
#define TEST_SLAVE_SPI_RCC 		RCC_APB1Periph_SPI3
#define TEST_SLAVE_SPI_IRQn 	SPI3_IRQn
#define TEST_SLAVE_DMA_BSP 		BSP_SPI3_DMA  	// DMA1 Streams 5 and 0

#define test_SPI_slave_IRQHandler     	SPI3_IRQHandler
#define TEST_DMA_SLAVE_Tx_IRQHandler  	DMA1_Stream5_IRQHandler
#define TEST_DMA_SLAVE_Rx_IRQHandler  	DMA1_Stream0_IRQHandler



#elif PCB_XCORE407I == 2



#endif




static const DMA_BSP_t test_dma = TEST_MASTER_DMA_BSP;

volatile /*static*/ union {
	char     bytes[3];
	uint32_t ui32;
	int32_t  i32;
} test_data;



int test_spi_init();
int test_spi_master_init(void);
int test_spi_slave_init(void);
static int test_spi_dma_init( bool use_rx, bool use_tx );



///\todo возможно использовать SPI-ное прерывание
void test_spi_master_DMA_Rx_IRQHandler()
{
	// Transfer complete? So, NDTR (number of data to transfer ) is 0.
	if( DMA_GetITStatus( test_dma.streamRx, test_dma.ITsRx.TCIF ) ){
		DMA_ClearITPendingBit( test_dma.streamRx, test_dma.ITsRx.TCIF );
		// Перейти в режим ожидания DRDY. Выключить SPI, остановить SCLK, после завершения передачи.
		//while( (TEST_MASTER_SPI->SR & SPI_SR_BSY) != RESET );  // Это может быть долго как для прерывания.
		while( TEST_MASTER_SPI->CR1 & SPI_CR1_SPE )
			TEST_MASTER_SPI->CR1 &= ~SPI_CR1_SPE;  //SPI_Cmd( TEST_MASTER_SPI, DISABLE );  //		
		
		///\todo Обработка полученных данных
		
		
	}
}


void test_master_spi_IRQHandler()
{
	//const uint32_t DRdyLine = TEST_GPIO_DRdy->extiLine;
	static uint8_t count = 0;
	
	if( SPI_GetITStatus( TEST_MASTER_SPI, SPI_IT_RXNE ) ){
		test_data.bytes[count] = (uint8_t)TEST_MASTER_SPI->DR;  //SPI_ReceiveData(
		++count;
		if( count == 3 ){
			count = 0;
			while( TEST_MASTER_SPI->CR1 & SPI_CR1_SPE )
				TEST_MASTER_SPI->CR1 &= ~SPI_CR1_SPE;  //SPI_Cmd( TEST_MASTER_SPI, DISABLE );  //
			
			///\todo запихнуть полученные данные в RingBuffer C++
			
			
		} else if( count > 3 ){
			assert_amsg(0);
		}
	} else if( SPI_GetITStatus( TEST_MASTER_SPI, SPI_IT_TXE ) ){
		// Transfer complete. Stop SPI, SCLK, deselect NSS.
		TEST_MASTER_SPI->CR1 &= ~SPI_CR1_SPE;
	}
	
}


void test_SPI_slave_IRQHandler()
{
	volatile uint8_t recvd;
	
	if( SPI_GetITStatus( TEST_SLAVE_SPI, SPI_IT_RXNE ) ){
		recvd = (uint8_t)TEST_SLAVE_SPI->DR;
		/*switch( recvd ){
			case 0xD2:	bsp_LedToggle( LED_Green ); break;
			case 0xC0:	bsp_LedToggle( LED_Blue  ); break;
			default:  	bsp_LedToggle( LED_Red   ); break;
		}*/
		if( recvd == 0xD2 )
			//bsp_LedToggle( LED_Green );
			GPIO_ToggleBits( LED_Green->port, LED_Green->pin );
		else if( recvd == 0xC1 )
			//bsp_LedToggle( LED_Blue );
			GPIO_ToggleBits( LED_Blue->port, LED_Blue->pin );
	}
}


int test_spi()
{
	volatile /*const*/ int delay = 6000000;
	volatile uint8_t test_to_send = 0xD2;
	int i = delay;
	test_spi_init();

	while(1){
		TEST_MASTER_SPI->DR = test_to_send;
		TEST_MASTER_SPI->CR1 |= SPI_CR1_SPE;
		while( i-- );
		i = delay;
	}
}


int test_spi_init()
{
	test_spi_master_init();
	test_spi_slave_init();
}


int test_spi_master_init()
{
	SPI_InitTypeDef sis;
	GPIO_InitTypeDef gis;
	NVIC_InitTypeDef nis;
	
	// Enable peripheral clock. Make sure your SPIx connected to ABPx
	if( TEST_MASTER_SPI == SPI1 )
		RCC_APB2PeriphClockCmd( TEST_MASTER_SPI_RCC, ENABLE );
	else
		RCC_APB1PeriphClockCmd( TEST_MASTER_SPI_RCC, ENABLE );
	
	// Configure SPI
	sis.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_32;//16;  // for master olny
	sis.SPI_CPHA = SPI_CPHA_2Edge;  //1Edge;  // Specifies the clock active edge for the bit capture. ↑ is 1st, if CPOL_Low. NOT need in TI mode
	sis.SPI_CPOL = SPI_CPOL_Low;  	// Specifies the serial clock steady state. NOT need in TI mode
	sis.SPI_CRCPolynomial = 0x7;	// default
	sis.SPI_DataSize = SPI_DataSize_8b;
	sis.SPI_Direction = SPI_Direction_2Lines_FullDuplex; //1Line_Rx;
	sis.SPI_FirstBit = SPI_FirstBit_MSB;
	sis.SPI_Mode = SPI_Mode_Master;
	sis.SPI_NSS = SPI_NSS_Hard;  //Soft;  //
	SPI_Init( TEST_MASTER_SPI, &sis );
	TEST_MASTER_SPI->CR2 |= SPI_CR2_SSOE; 	// In HW mode NSS output enabled (SSM = 0, SSOE = 1). 
										// This configuration is used only when the device operates in master mode. The
										// NSS signal is driven low when the master starts the communication and is kept
										// low until the SPI is disabled.
	
	/* Enable the NVIC and the corresponding interrupt using the function 
       SPI_ITConfig() if you need to use interrupt mode. */
	SPI_ITConfig( TEST_MASTER_SPI, SPI_IT_RXNE, ENABLE );
	SPI_ITConfig( TEST_MASTER_SPI, SPI_IT_TXE, ENABLE );
	
	nis.NVIC_IRQChannel = TEST_MASTER_SPI_IRQn;
	nis.NVIC_IRQChannelPreemptionPriority = 1;//2;
	nis.NVIC_IRQChannelSubPriority = 0;
	nis.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init( &nis ); 
	
	/* When using the DMA mode 
       (++) Configure the DMA using DMA_Init() function
       (++) Active the needed channel Request using SPI_I2S_DMACmd() function */
	//assert_amsg( \
			test_spi_master_dma_init( /*tx*/false, /*rx*/true) == 0 );
	// Enable DMA request from SPI
	//SPI_I2S_DMACmd( TEST_MASTER_SPI, SPI_I2S_DMAReq_Rx, ENABLE );  //TEST_MASTER_SPI->CR2 |= SPI_I2S_DMAReq_Rx;
	
	
	// Configure GPIOs
	gis.GPIO_Speed = GPIO_Speed_100MHz;
	gis.GPIO_PuPd = GPIO_PuPd_NOPULL;//DOWN; //
	gis.GPIO_OType = GPIO_OType_PP;
	
	bsp_GpioAf_InitStruct( TEST_MASTER_GPIO_NSS,  TEST_MASTER_GPIO_AF, &gis );
	bsp_GpioAf_InitStruct( TEST_MASTER_GPIO_SCK,  TEST_MASTER_GPIO_AF, &gis );
	bsp_GpioAf_InitStruct( TEST_MASTER_GPIO_MISO, TEST_MASTER_GPIO_AF, &gis );
	bsp_GpioAf_InitStruct( TEST_MASTER_GPIO_MOSI, TEST_MASTER_GPIO_AF, &gis );
	
	// Enable SPI
//	SPI_Cmd( TEST_MASTER_SPI, ENABLE );  //TEST_MASTER_SPI->CR1 &= ~SPI_CR1_SPE;  //
	return 0;
}


int test_spi_slave_init()
{
	SPI_InitTypeDef sis;
	GPIO_InitTypeDef gis;
	NVIC_InitTypeDef nis;
	
	// Enable peripheral clock. Make sure your SPIx connected to ABPx
	if( TEST_SLAVE_SPI == SPI1 )
		RCC_APB2PeriphClockCmd( TEST_SLAVE_SPI_RCC, ENABLE );
	else
		RCC_APB1PeriphClockCmd( TEST_SLAVE_SPI_RCC, ENABLE );
	
	// Configure SPI
	SPI_StructInit( &sis );
	//sis.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_32;//16;  // for master olny
	sis.SPI_CPHA = SPI_CPHA_2Edge;  //1Edge;  // Specifies the clock active edge for the bit capture. ↑ is 1st, if CPOL_Low. NOT need in TI mode
	sis.SPI_CPOL = SPI_CPOL_Low;  	// Specifies the serial clock steady state. NOT need in TI mode
	sis.SPI_CRCPolynomial = 0x7;	// default
	sis.SPI_DataSize = SPI_DataSize_8b;
	sis.SPI_Direction = SPI_Direction_2Lines_FullDuplex;
	sis.SPI_FirstBit = SPI_FirstBit_MSB;
	sis.SPI_Mode = SPI_Mode_Slave;
	sis.SPI_NSS = SPI_NSS_Hard;  //Soft;  //
	SPI_Init( TEST_SLAVE_SPI, &sis );
	
	/* Enable the NVIC and the corresponding interrupt using the function 
       SPI_ITConfig() if you need to use interrupt mode. */
	SPI_ITConfig( TEST_SLAVE_SPI, SPI_IT_RXNE, ENABLE );
	//SPI_ITConfig( TEST_SLAVE_SPI, SPI_IT_TXE,  ENABLE );
	
	nis.NVIC_IRQChannel = TEST_SLAVE_SPI_IRQn;
	nis.NVIC_IRQChannelPreemptionPriority = 1;//2;
	nis.NVIC_IRQChannelSubPriority = 0;
	nis.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init( &nis ); 
	
	/* When using the DMA mode 
       (++) Configure the DMA using DMA_Init() function
       (++) Active the needed channel Request using SPI_I2S_DMACmd() function */
	//assert_amsg( \
			ads1252_dma_init( /*tx*/false, /*rx*/true) == 0 );
	// Enable DMA request from SPI
	//SPI_I2S_DMACmd( TEST_SLAVE_SPI, SPI_I2S_DMAReq_Rx, ENABLE );  //TEST_SLAVE_SPI->CR2 |= SPI_I2S_DMAReq_Rx;
	
	// Configure and disable interrupt on TEST_SLAVE_GPIO_MISO
	gis.GPIO_Speed = GPIO_Speed_100MHz;
	gis.GPIO_PuPd = GPIO_PuPd_NOPULL;//DOWN; //
	gis.GPIO_OType = GPIO_OType_PP;
	
	// Configure GPIOs
	bsp_GpioAf_InitStruct( TEST_SLAVE_GPIO_NSS,  TEST_SLAVE_GPIO_AF, &gis );
	bsp_GpioAf_InitStruct( TEST_SLAVE_GPIO_SCK,  TEST_SLAVE_GPIO_AF, &gis );
	bsp_GpioAf_InitStruct( TEST_SLAVE_GPIO_MISO, TEST_SLAVE_GPIO_AF, &gis );
	bsp_GpioAf_InitStruct( TEST_SLAVE_GPIO_MOSI, TEST_SLAVE_GPIO_AF, &gis );
	
	// Enable SPI
	SPI_Cmd( TEST_SLAVE_SPI, ENABLE );  //TEST_SLAVE_SPI->CR1 &= ~SPI_CR1_SPE;  //
	
	return 0;
}


/**	
 * Используем готовую предопределённую структуру BSP_SPI2_DMA
 * \return	 0  OK
 * 			 1  nothing to configure
 * 			-2  TX stream in use
 * 			-3  RX stream in use
 * 			other negative value on error.
 * 			positive value - passed with warnings
 */
static int test_spi_dma_init( bool use_tx, bool use_rx )
{
	DMA_InitTypeDef dis;
	NVIC_InitTypeDef nis;
	
	if( (use_tx | use_rx) == false )
		return 1;
	
	// Enable DMA clocks
	DMAx_AHB1ClockEnable(test_dma);  //DMAx_AHB1ClockCmd( dma, ENABLE );  //RCC_AHB1PeriphClockCmd( RCC_AHB1Periph_DMA1, ENABLE);
	
	// Make sure DMA stream(s) are not used. To avoid collisions.
	if( use_tx )
		assert_msg("ads1252_dma_init(): DMA stream TX is already in use!", 
				!IS_DMA_stream_configured( test_dma.streamTx ) );	
	if( use_rx ){
		//debugf2("ads1252_dma_init(): 0x%X, 0x%X\n", dma.streamRx->CR, dma.streamRx->FCR );
		assert_msg("ads1252_dma_init(): DMA stream RX is already in use!", 
				!IS_DMA_stream_configured( test_dma.streamRx ) );
	}
/*	// Make sure DMA stream(s) are not used. To avoid collisions. DO NOT stop program flow
	if( use_tx && IS_DMA_stream_configured( dma.streamTx ) ){
		//if( DMA clock was disabled )  DMAx_AHB1ClockDisable(dma);
		return -2;
	}
	if( use_rx && IS_DMA_stream_configured( dma.streamRx ) ){
		//if( DMA clock was disabled )  DMAx_AHB1ClockDisable(dma);
		return -3;
	}*/
	
	// Receive configuration
	DMA_StructInit( &dis );
	dis.DMA_DIR 				= DMA_DIR_PeripheralToMemory;
	dis.DMA_PeripheralBaseAddr 	= (uint32_t) &(TEST_MASTER_SPI->DR);
	dis.DMA_Memory0BaseAddr 	= (uint32_t) &test_data; //&TEST_input_data[0];
	dis.DMA_BufferSize 			= 3; //sizeofarr(TEST_input_data);
	dis.DMA_PeripheralInc		= DMA_PeripheralInc_Disable;
	dis.DMA_MemoryInc 			= DMA_MemoryInc_Enable; //Disable; //
	dis.DMA_PeripheralDataSize 	= DMA_PeripheralDataSize_Byte;
	dis.DMA_MemoryDataSize 		= DMA_MemoryDataSize_Byte;//Word; //	// Don’t care in Direct mode. Only periph data size has metter.
	dis.DMA_Mode 				= DMA_Mode_Circular;
	dis.DMA_Priority 			= DMA_Priority_VeryHigh;
	dis.DMA_FIFOMode 			= DMA_FIFOMode_Disable;				/// Disable -> Direct mode used.
	//dis.DMA_FIFOThreshold  	= DMA_FIFOThreshold_3QuartersFull; 	/// Doesn't metter while Direct mode used (FIFO disabled)
	//dis.DMA_MemoryBurst 		= DMA_MemoryBurst_Single;       	/// Doesn't metter while Memory Increment mode disabled
	//dis.DMA_PeripheralBurst 	= DMA_PeripheralBurst_Single; 	/// Doesn't metter while Peripheral Increment mode disabled
	
	// SPIx_RX
	dis.DMA_Channel = test_dma.channel;
	DMA_Init( test_dma.streamRx, &dis );
	
	DMA_ITConfig( test_dma.streamRx, DMA_IT_TC, ENABLE );
	
	nis.NVIC_IRQChannelPreemptionPriority = 0;//2;
	nis.NVIC_IRQChannelSubPriority = 0;
	nis.NVIC_IRQChannelCmd = ENABLE;
	
	DMA_Cmd( test_dma.streamRx, ENABLE );
	
	/*
	// Transmit configuration
	dis.DMA_DIR = DMA_DIR_MemoryToPeripheral;
	dis.DMA_PeripheralBaseAddr = (uint32_t) &(TEST_MASTER_SPI->DR);
	dis.DMA_Memory0BaseAddr = (uint32_t) &TEST_input_data[0];
	dis.DMA_BufferSize = sizeof(TEST_input_data);
	
	// SPIx_TX
	dis.DMA_Channel = DMA_Channel_0;
	DMA_Init( DMA2_Stream2, &dis);
	
	DMA_ITConfig( test_dma.streamRx, DMA_IT_TC, ENABLE );
	
	nis.NVIC_IRQChannel = test_dma.irqTx;
	NVIC_Init( &nis ); 
	
	DMA_Cmd( DMA2_Stream2, ENABLE);
	
	// Сonfigure & enable DMA request from SPI
	SPI_I2S_DMACmd( TEST_MASTER_SPI, SPI_I2S_DMAReq_Tx, ENABLE );
	*/
	
	return 0;
}

