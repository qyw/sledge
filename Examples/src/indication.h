/**
 *
 */

#ifndef indication_H
#define indication_H

#ifdef __cplusplus
extern "C" {
#endif


void toggleLed_task(void * pvParameters);
void LedRGB_ColorRondo_task( void/*LedRGB_t*/ *led );
//void ledsPwmPlay(void);

//extern const Parameter_t indication_PARAMETERS[];
//extern const size_t indication_PARAMETERS_COUNT = sizeofarr(indication_PARAMETERS);


#ifdef __cplusplus
}
#endif

#endif /* indication_H */
