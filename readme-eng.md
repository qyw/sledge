﻿I recommend to add directory `Library/` to the project's Include Path, if it is not done yet. And include `Sledge` components using the following syntax:

	#include "Sledge/assert/assert.h"
	#include "Sledge/bsp/bsp.h"

The most simple way is to add folder 'Sledge/Include/' to the project includes:

	#include "Sledge.h"

Full path name will eliminate errors, if there are more than one .h files with the same name. We don't recommend to add many include-folders to the project. It is inconveniently and is harder to port.

# License
Library `Sledge` allows dual licensing. MIT и BSD 2-clause. They are roughly equivalent.