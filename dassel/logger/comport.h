/**
 * @file    comport.h
 * @author  Kyb
 * @version 2.2.0
 * @date    2015-Apr-21
 * @brief   
 */
  
#ifndef __comport_H 	/// Define to prevent recursive inclusion
#define __comport_H	220

#ifdef __cplusplus
extern "C" {
#endif


#include "mainConfig.h"
#include "Sledge/bsp.h"


#define USE_SERIAL_DEBUG_CLI		1
#define SERIAL_DEBUG_QUEUE_SIZE 	512
#define SERIAL_DEBUG_NVIC_PRIO		9

#if   (SERIAL_DEBUG_COMx == 0/*COM1*/)
 #define SERIAL_DEBUG_PORT_IRQHANDLER() 	USART2_IRQHandler()
#elif (SERIAL_DEBUG_COMx == 1/*COM2*/)
  #define SERIAL_DEBUG_PORT_IRQHANDLER() 	USART2_IRQHandler()
#elif (SERIAL_DEBUG_COMx == 2/*COM3*/)
  #define SERIAL_DEBUG_PORT_IRQHANDLER() 	USART3_IRQHandler()
#elif (SERIAL_DEBUG_COMx == 3/*COM4*/)
  #define SERIAL_DEBUG_PORT_IRQHANDLER() 	USART1_IRQHandler()
#endif


//#if (USE_RTOS == 0)
 void DebugComPort_GeneralInit(void);
 #define DebugComPort_Init() 	DebugComPort_GeneralInit()
//#else
// void DebugComPort_Init(void);
//#endif

int putc_serial(int ch);
int getc_serial(void);
void comSendViaDMA( const COMPort_t* com, const uint8_t *buf, uint16_t len );
#define debugComSendViaDMA( buf, len ) 	comSendViaDMA( COM_debug, (buf), (len) )



#ifdef __cplusplus
}
#endif

#endif /* __comport_H */  
