/**
 * Sledge - wide functionality library, suitable for embedded systems, 
 * mainly for ARM Cortex and more particularly for STM32 family
 * 
 * Copyright (c) 2015, Ivan Kuvaldin. All rights reserved.
 * 
 * Licensed under BSD or MIT. 
 * Please refer to Licence.BSD.txt or Licence.MIT.txt provided with these sources.
 * You may obtain a copy of the Licenses at
 *      http://opensource.org/licenses/bsd-license.php  
 *      http://opensource.org/licenses/mit-license.php  
 */
/**
 * @file    comport.c
 * @author 	Ivan "Kyb Sledgehammer" Kuvaldin <i.kyb[2]ya,ru>
 * @version 1.0.0
 * @date    20-Jan-2015
 * @brief	
 *			
 */

#include "Sledge/dassel/logger/comport.h"	//main.h included here

#if SERIAL_DEBUG > 0


#include <stdio.h>
#include <stdbool.h>

#include "Sledge/bsp.h"
#include "Sledge/debug.h"

#include "stm32f4xx_usart.h"
#include "stm32f4xx_dma.h"
#include "stm32f4xx_rcc.h"
#include "misc.h"


volatile uint8_t/*uint16_t*/ debugCOM_RxBuffer[SERIAL_DEBUG_QUEUE_SIZE + 1];
//volatile uint8_t debugCom_TxBuffer[SERIAL_DEBUG_QUEUE_SIZE];
  
  
static void USART_NVIC_Config( IRQn_Type irq, uint8_t prio );
static void USART_DMA_Config( const COMPort_t* com );


/** @brief 	Get char from UART
  * @note 	This function will not work if DMA receive enabled
  * @retval	ch	char got
  */
int getc_serial( void ) 
{
	int c = COM_debug.usart->DR & (uint16_t)0x01FF; 
	//while( !(c = COM_debug.usart->DR & (uint16_t)0x01FF) );
	return c;
}

/** @brief 	Put char to UART
  * @param 	ch	char to put
  * @retval	ch	char was put 
  */
int putc_serial(int ch)
{
	/** Loop until the end of transmission */
	while( USART_GetFlagStatus( COM_debug.usart, USART_FLAG_TC) == RESET ) {}
	USART_SendData( COM_debug.usart, (uint8_t)ch );
	
	return ch;
}

/** @brief  Initialize COM1 interface for serial debug
  * @note   COM1 interface is defined in stm3210g_eval.h file (under Utilities\STM32_EVAL\STM324xG_EVAL)  
  * @param  None
  * @retval None
  */
void DebugComPort_GeneralInit(void)
{
	USART_InitTypeDef uis;
	
	/* Enable USART clock. USART1 and USART6 connected to APB2 */
	if ( (COM_debug.usart == USART1) || (COM_debug.usart == USART6) ) {
		RCC_APB2PeriphClockCmd( COM_debug.clk, ENABLE );
	} else {
		RCC_APB1PeriphClockCmd( COM_debug.clk, ENABLE );
	}
	
	/* USARTx configured as follow:
	 - BaudRate = 115200 baud  
	 - Word Length = 8 Bits
	 - One Stop Bit
	 - No parity
	 - Hardware flow control disabled (RTS and CTS signals)
	 - Receive and transmit enabled
	*/
	uis.USART_BaudRate = 460800;//115200;
	uis.USART_WordLength = USART_WordLength_8b;
	uis.USART_StopBits = USART_StopBits_1;
	uis.USART_Parity = USART_Parity_No;
	uis.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	uis.USART_Mode = USART_Mode_Tx
  #if USE_SERIAL_DEBUG_CLI
								   | USART_Mode_Rx
  #endif
													;
	USART_Init( COM_debug.usart, &uis );
	
	/// GPIO Initialization
	bsp_COMInit( &COM_debug );
	
  #if USE_SERIAL_DEBUG_CLI		/// Use UART RX
	/** Enable the USART Receive interrupt: this interrupt is generated when the 
		Receive Data register is not empty.
		You need to call comReceiveData() from SERIAL_DEBUG_PORT_IRQHANDLER() i.e USARTx_IRQHandler()
		in stm32fxxx_it.c or here. */
	/*USART_ITConfig( COM_debug.usart, 
					USART_IT_RXNE,// | USART_IT_IDLE, 
					ENABLE);	*/
	/** Enable the USART IDLE interrupt: this interrupt is generated when the 
		Rx line is idles (logical 1), i.e transfer is ended.
		You need to put your code in SERIAL_DEBUG_PORT_IRQHANDLER() i.e USARTx_IRQHandler()
		in stm32fxxx_it.c or here. */
	USART_ITConfig( COM_debug.usart, 
					USART_IT_IDLE, 
					ENABLE);
	USART_NVIC_Config( COM_debug.irq, SERIAL_DEBUG_NVIC_PRIO );
  #endif 
		
	USART_DMA_Config( &COM_debug );
	/// Enable USART
	USART_Cmd( COM_debug.usart, ENABLE);
}

static void USART_NVIC_Config( IRQn_Type irq, uint8_t prio )
{
	NVIC_InitTypeDef NVIC_InitStructure;
	
	// Enable the USARTx Interrupt
	NVIC_InitStructure.NVIC_IRQChannel = irq;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = prio;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init( &NVIC_InitStructure );
}

static void USART_DMA_Config( const COMPort_t* com )
{
	DMA_InitTypeDef dma;
	
	RCC_AHB1PeriphClockCmd( com->dmaBsp.rcc, ENABLE );
	
	// Configure Rx DMA request
	DMA_StructInit(&dma);
	dma.DMA_Channel				= com->dmaBsp.channel;
	dma.DMA_PeripheralBaseAddr	= (uint32_t)&( COM_debug.usart->DR );
	dma.DMA_Memory0BaseAddr		= (uint32_t)& debugCOM_RxBuffer;    
	dma.DMA_DIR					= DMA_DIR_PeripheralToMemory;
	dma.DMA_BufferSize			= SERIAL_DEBUG_QUEUE_SIZE;
	dma.DMA_PeripheralInc		= DMA_PeripheralInc_Disable;
	dma.DMA_MemoryInc			= DMA_MemoryInc_Enable;
	dma.DMA_PeripheralDataSize	= DMA_MemoryDataSize_Byte;  //DMA_PeripheralDataSize_HalfWord;
	dma.DMA_MemoryDataSize		= DMA_MemoryDataSize_Byte;  //DMA_MemoryDataSize_HalfWord;	
	dma.DMA_Mode				= DMA_Mode_Circular;
	dma.DMA_Priority			= DMA_Priority_Medium ;
	dma.DMA_FIFOMode			= DMA_FIFOMode_Disable ;		// Direct mode used.
	//dma.DMA_FIFOThreshold 	= DMA_FIFOThreshold_HalfFull;	// Doesn't metter while Direct mode used (FIFO disabled)
	//dma.DMA_MemoryBurst 		= DMA_MemoryBurst_Single;       // Doesn't metter while Memory Increment mode disabled 
	//dma.DMA_PeripheralBurst 	= DMA_PeripheralBurst_Single;   // Doesn't metter while Peripheral Increment mode disabled 
	
	DMA_Init( com->dmaBsp.streamRx, &dma );
	DMA_Cmd( com->dmaBsp.streamRx, ENABLE );
	
	// Сonfigure & enable DMA request from USART
	USART_DMACmd( com->usart, USART_DMAReq_Rx, ENABLE );
	
	
	// Configure Tx DMA request
	//dma.DMA_PeripheralBaseAddr	= (uint32_t)&( coms[SERIAL_DEBUG_COMx].com->DR );
	dma.DMA_Memory0BaseAddr		= NULL;	//(uint32_t)& debugCom_TxBuffer;    
	dma.DMA_DIR					= DMA_DIR_MemoryToPeripheral;
	dma.DMA_BufferSize			= 1;	//SERIAL_DEBUG_QUEUE_SIZE;
	//dma.DMA_PeripheralInc		= DMA_PeripheralInc_Disable;
	//dma.DMA_MemoryInc			= DMA_MemoryInc_Enable;
	//dma.DMA_PeripheralDataSize	= DMA_PeripheralDataSize_HalfWord;	//DMA_MemoryDataSize_Byte;
	//dma.DMA_MemoryDataSize		= DMA_MemoryDataSize_HalfWord;		//DMA_MemoryDataSize_Byte;
	dma.DMA_Mode				= DMA_Mode_Normal;	//DMA_Mode_Circular;
	//dma.DMA_Priority			= DMA_Priority_Medium ;
	//dma.DMA_FIFOMode			= DMA_FIFOMode_Disable ;		/// Direct mode used.
	//dma.DMA_FIFOThreshold 	= DMA_FIFOThreshold_HalfFull;	/// Doesn't metter while Direct mode used (FIFO disabled)
	//dma.DMA_MemoryBurst 		= DMA_MemoryBurst_Single;       /// Doesn't metter while Memory Increment mode disabled 
	//dma.DMA_PeripheralBurst 	= DMA_PeripheralBurst_Single;   /// Doesn't metter while Peripheral Increment mode disabled 
		
	DMA_Init( com->dmaBsp.streamTx, &dma );
	//DMA_Cmd( com->dmaBsp.streamTx, ENABLE );
	
	// Сonfigure & enable DMA request from USART Tx
	USART_DMACmd( com->usart, USART_DMAReq_Tx, ENABLE );
}


///\ToDo Возможно добавить очередь строк на отправку (uint8_t *comSenderQueue[]). Запускать следующую строку по прерываю TrnsferComplete
void comSendViaDMA( const COMPort_t* com, const uint8_t *buf, uint16_t len) 
{
	static bool firstTimeHere = true;
	
	assert_amsg( buf != NULL );
	
	if( !firstTimeHere /*== false*/ ) {
		// Wait untill whe end of transfer
		//while( com->dmaBsp.streamTx->NDTR );
		while( DMA_GetFlagStatus( com->dmaBsp.streamTx, com->dmaBsp.flagsTx.TCIF ) == RESET );
		DMA_ClearFlag( com->dmaBsp.streamTx, com->dmaBsp.flagsTx.TCIF );
		//DMA_ClearFlag( com->dmaBsp.streamTx, com->dmaBsp.flagsTx.FEIF );
	} else {
		firstTimeHere = false;
	}
	
	//com->dmaBsp.streamTx->PAR  = (uint32_t)&( com->usart->DR );
	com->dmaBsp.streamTx->M0AR = (uint32_t)(buf);
	com->dmaBsp.streamTx->NDTR = len;
	DMA_Cmd( com->dmaBsp.streamTx, ENABLE );
	//USART_SendData( com->usart, *buf );
}


void SERIAL_DEBUG_PORT_IRQHANDLER()
{
	uint16_t len = 0;
	
	// Check data received. USART_IT_RXNE means data stream stopped
	if( USART_GetITStatus( COM_debug.usart, USART_IT_RXNE) != RESET ){ 
		// Clear pending bit. We don't need to do that always, since it made automaticly after xxx_GetITStatus, but it is good practice
		USART_ClearITPendingBit( COM_debug.usart, USART_IT_RXNE );
	}
	
	// Check Idle status
	if( USART_GetITStatus( COM_debug.usart, USART_IT_IDLE) != RESET ){
		// Прием кадра окончен. Забрать буффер.
		len = SERIAL_DEBUG_QUEUE_SIZE - COM_debug.dmaBsp.streamRx->NDTR;

		///\ToDo Что делать с данными?
		debugCOM_RxBuffer[len] = '\0';
		printf( "Received: %s", debugCOM_RxBuffer );
		
		/* Сбросить счетчик DMA на начало. Возможно только при выключенном потоке.
		   Возможно, использование функций StdPeriphLib удобннее, но в прерывании особенно важна производительность */
		COM_debug.dmaBsp.streamRx->CR &= ~DMA_SxCR_EN;	//DMA_Cmd( com->dmaBsp.streamRx, DISABLE );
		COM_debug.dmaBsp.streamRx->NDTR = SERIAL_DEBUG_QUEUE_SIZE ;
		COM_debug.dmaBsp.streamRx->CR |= DMA_SxCR_EN;	//DMA_Cmd( com->dmaBsp.streamRx, ENABLE );
	}
}


#endif /* SERIAL_DEBUG */
