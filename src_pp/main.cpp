
#include <iostream>
#include "VibroClasses.h"

using namespace std;
using namespace Vibro;


int main()
{
    cout << "Hello, World!" << endl;


    Vibro::Taxometer &taxo = Vibro::Taxometer::getInstance();
    taxo.notches = 2;
    taxo.zones.push_back( Vibro::Taxometer::Zone("Barring", 3.8, 4.6 ) );
    taxo.zones.push_back( Vibro::Taxometer::Zone("Static", 2990, 3012 ) );

    Vibro::Taxometer::Zone *zone = &taxo.getZone("Barring");
    cout << "zone.name " << zone->name << "; bounds " << zone->bound_low << ", " << zone->bound_hi << endl;


    cout << "Full list:\n";
    for( int i=0; i<taxo.zones.size(); i++){
        zone = &taxo.zones[i];
        cout << i << " zone.name " << zone->name << "; bounds " << zone->bound_low << ", " << zone->bound_hi << endl;
    }

    Taxometer::Zone zone2 = Taxometer::Zone();

    return 0;
}