//
// Created by kyb on 01.10.2015.
//
/**
 * \file 	RingBuffer.hpp
 * \author 	Ivan "Kyb Sledgehammer" Kuvaldin <i.kyb[2]ya,ru>
 * \version	1.0.0
 * \date	8-Dec-2014
 * \brief   
 *			���������� ���������� ������� � ������������ �������������.
 */

#ifndef _RingBuffer_H_
#define _RingBuffer_H_

//#pragma diag_suppress 284  // Suppress null-reference is not allowed


class RingBuffer {
	
};


#endif //_RingBuffer_H_

/**
 * \file 	RingBuffer.hpp
 * \author 	Ivan "Kyb Sledgehammer" Kuvaldin <i.kyb[2]ya,ru>
 * \version	1.0.0
 * \date	8-Dec-2014
 * \brief   
 *			���������� ���������� ������� � ������������ �������������.
 */


//#include "Buffer.hpp"
#include "Sledge/utils.h"
#include "Sledge/assert.h"

#include <cstddef>
using std::size_t;


/**	
 * ������ ���������� ������
 * ��������� ��� ���������:
 * @param SIZE: 	������ ������,
 * @param Data_t:	��� ��������� ���������� � ������, �� ��������� unsigned char
 */
template< size_t SIZE, class Data_t=unsigned char >
class RingBuffer //: public Buffer< SIZE, Data_t >
{
public:
	RingBuffer() : head(0), tail(0), length_(0)  {}
	//virtual ~RingBuffer() {}  // ��� ������������.

	/// ���������� ��������� ��� ��������. int �������� ������������ ������������� �������
	typedef int Index_t;  //size_t

private:
	//const Index_t size = SIZE;
	Data_t _data[SIZE];	/// ������ ��� �����
	volatile Index_t head /*= 0*/;	/// ���������� ������, ������ ������
	volatile Index_t tail /*= 0*/;	/// ���������� �������, ������ ������, ������ ���� ����� ������� ��������� �������
	volatile Index_t length_;		/// ���������� ���������
	//volatile Index_t actualSize /*= SIZE*/; /// SIZE ���������� ������ ������� � ������, actualSize ��������� ��������� ������� ������ �������
	//volatile Index_t watermarkSize /*= SIZE*/; /// SIZE ���������� ������ ������� � ������, watermarkSize ��������� ��������� ������� ������ �������


	/*class RingIndex {
		Index_t index;
		
	public:
		typedef int Index_t;

		RingIndex()             : index(0)        {}
		RingIndex(Index_t idx)  : index(idx)      {}
		RingIndex(RingIndex ri) : index(ri.index) {}
		
		// ������� ���� ������ �� ������.
		friend inline const RingIndex& operator+( const RingIndex& i ){
			return ri;
		}
		
		// ������� �����
		friend inline const RingIndex operator-( const RingIndex& ri ){
			return RingIndex(-ri.index);
		}	
		
		// ���������� ������ ���������� �������� ����� ����������
		friend inline const RingIndex& operator++( RingIndex& ri ){
			ri.index = ++ri.index < (SIZE+1) ? ri.index : 0;
			return ri;
		}
		
		// ����������� ������ ���������� �������� �� ����������
		friend inline const RingIndex operator++( RingIndex& ri, int ){
			RingIndex oldValue(ri);
			ri.index = ++ri.index < (SIZE+1) ? ri.index : 0;
			return oldValue;
		}
		
		// ���������� ������ ���������� �������� ����� ����������
		friend inline const RingIndex& operator--(RingIndex& ri) {
			ri.index = ++ri.index < (SIZE+1) ? ri.index : 0;
			return i;
		}

		// ����������� ������ ���������� �������� �� ����������
		friend inline const RingIndex operator--(RingIndex& ri, int) {
			RingIndex oldValue(ri);
			ri.index--;
			return oldValue;
		}
		
		// �������� ���������
		friend inline const RingIndex operator+( const RingIndex& left, const RingIndex& right ){
			return RingIndex( left.index + right.index );
		}
		
		friend inline const RingIndex operator+( const RingIndex& left, const Index_t& right ){
			return RingIndex( left.index + right );
		}

		friend inline RingIndex& operator+=(RingIndex& left, const RingIndex& right) {
			left.index += right.index;
			return left;
		}

		friend inline bool operator==(const RingIndex& left, const RingIndex& right) {
			return left.index == right.index;
		}
		
		inline Index_t getIndex() const {
			return this->index;
		}
		
		inline operator int() const {
			return this->index;
		}
	}*/


	/// C�������� ������, �������� �������� �������� ���������.
	inline Index_t nextIndex( volatile Index_t &idx) // volatile & ����.
	{
		return idx = ++idx < sizeofarr(_data) ? idx : 0;
	}

	/// C�������� ������, �� �������� �������� �������� ���������.
	/*volatile*/ inline Index_t nextIndexOf( volatile Index_t idx) const
	{
		return ++idx < sizeofarr(_data) ? idx : 0;
	}

	/// ��������� idx �� inc, �� �������� �������� �������� ���������.
	inline Index_t incIndexOf( Index_t idx, Index_t inc ) const
	{
		Index_t ret = idx + inc;
		if( ret >= sizeofarr(_data) )
			ret -= SIZE;
		return ret;		//(idx + inc) < (SIZE+1) ? (idx + inc) : (idx + inc - SIZE+1);
	}

public:
	/** ������ � ����� ������ ��������
	  * \return true ���� �������� ��������, false ��� ������������
	  */
	inline bool push(Data_t value) {
		if( isFull() )
			return false;
		_data[ tail ] = value;
		nextIndex(tail);
		length_++;
		return true;
	}

	/** ������ � ����� ������� ���������. ����� �������� �� ����� ��� ��������.
	  * \return ���������� ����������� ���������
	  */
	inline Index_t push( Data_t array[], Index_t length )
	{
		Index_t i=0;
		while( i<length && push(array[i]) )
			i++;
		return i;
	}

	/** ������ � �����, ����������� ������ �������, ���� ������ ��������.
	  * ��������� ������� � ��������� ���������. ��������� �� ������� �������.
	  * \return true, ���� ������ ����� � 1�� ������� ��� ���������; false - ���� �� �����.
	  */
	inline bool pushForce(Data_t value) {
		bool isful = isFull();
		if( isful )
			pop();
		push(value);
		return isful;
	}
	/*	������ � �����, ����������� ������, ���� ������ ��������.
	  * ����������� � ���������� �������
	  */
	/*inline bool pushForce(Data_t value) {
		if( isFull() ){
			value = _data[head]; nextIndex(head) }
		_data[ nextIndex(tail) ] = value;
	}*/

	/** ������ � ����� ������� ���������, ���������� ��� ���������� ���� ������ ��������.
	  * \return number of elements pushed
	  */
	inline Index_t pushForce( Data_t array[], Index_t length )
	{
		Index_t i=0;
		while( i<length ){
			pushForce( array[i++] );
		}
		return i;
	}

	/** ������ � ����� ������������ ������� ���������, ���������� ��� ���������� ���� ������ ��������.
	  * ������������ (subsampling) ...
	  * \param 	array 	������ ���������
	  * \param 	length 	����� �������
	  * \param 	sub 	����� �� ������� ������ sub-�� �������, ��������, ������ 2��
	  * \return	number 	of elements pushed
	  */
	inline Index_t pushForceSubsampling( Data_t array[], Index_t length, unsigned sub )
	{
		Index_t i=0;
		while( i<length ){
			pushForce( array[i] );
			i+=sub;
		}
		return i;
	}

	/**
	 * 
	 */
	/*inline void setAllBuf( Data_t ){
		
	}*/

	/** ������� �������, ������ �� ������, � �������� � value
	  * \return ���������� true ���� �������� ���������, value ����������.
	  */
	inline bool pop(Data_t &value) {
		if( isEmpty() ){
			//value = 0;
			return false;
		}
		value = _data[head];
		nextIndex(head);
		length_--;
		return true;
	}

	/** ������� � ������� �� ������
	 *  \return ���������� ������� ��� 0.
	 */
	inline Data_t pop() {
		Data_t value;
		if( isEmpty() ) {
			value = 0;
		} else {
			value = _data[head];
			nextIndex(head);
			length_--;
		}
		return value;
	}

	/// ���������� ������ ������� �� ������, �� ������ ���
	inline Data_t& getFirst()/*const*/ {
		return operator[](0);
	}

	/// ���������� ��������� ������� �� ������, �� ������ ���
	inline Data_t& getLast()/*const*/ {
		return operator[](length()-1);
	}

	/// ���������� getLast()
	inline Data_t& peek() /*const*/ {
		return operator[](length()-1); 	/// ������� �� [�������]
	}

	/// 
	inline Index_t copyToArray( Data_t array[], Index_t len ) /*const*/ {
		int i=0;
		while( i<len && i<length() ){
			array[i] = operator[](i);
			i++;
		}
		return i;
	}


	/** ���������� ������� (������) �� �������
	 *  ������������� ������ �������� ������ � ������, [-1] ��� ���������, [-2] ������������� */
	inline Data_t& operator[](Index_t i) //const
	{
		assert_amsg( -length() <= i && i < length() );
		if( isEmpty() || i >= length() || i < -length() ){
			return *(Data_t*)NULL;
			//throw /*IndexOutOfBoundsException*/;
		}else if( i<0 )
			i += length();
		return _data[ incIndexOf(head, i) ];
	}

	/**	���������� const ������� (�������� ��������) �� �������. const this
	  *	������������� ������ �������� ������ � ������, [-1] ��� ���������, [-2] ������������� */
	/*inline const Data_t operator[] (Index_t i) const 
	{
		if( isEmpty() || i >= length() || i < -length() )
			return Data_t();
		else if( i<0 )
			i += length();
		return _data[ incIndexOf(head, i) ];
	}*/

	/// ���� �� �����
	inline bool isEmpty() const
	{
		return length_ == 0; //return tail == head;
	}

	/// ����� �� �����
	inline bool isFull() const
	{
		return length_ == SIZE; //return nextIndexOf(tail) == head;
	}

	/// �������� �����
	inline void clear()
	{
		head = tail = length_ = 0;
	}

	/// ���������� ��������� � ������
	/*Index_t getLength() const 
	{
		return length;//return (tail >= head) ? (tail - head) : (SIZE+1 - head + tail);
	}*/

	/// ���������� ��������� � ������
	inline Index_t length() const
	{
		return length_; //return (tail >= head) ? (tail - head) : (SIZE+1 - head + tail);
	}

	/// ������ ������
	inline size_t size()
	{
		return SIZE;
	}



	// DSP functions
	/**
	 * 
	 */
	inline Data_t average() //const
	{
		Data_t avg = 0;
		for( Index_t i=0; i<length(); i++ )
			avg += operator[](i);
		avg /= length();
		return avg;
	}

	/**
	 *
	 */
	Data_t mean() //const
	{
		return average();
	}

	/// Iterative mean. http://www.heikohoffmann.de/htmlthesis/node134.html
	inline Data_t mean_iter()
	{
		Data_t avg = 0;
		for( Index_t i=0; i<length(); i++) {
			avg += ((*this)[i] - avg) / (i + 1);
		}
		return avg;
	}


	/**
	 * 
	 */
	Data_t filterOneValue( /*const Data_t koefs[], Index_t numK=4*/ ) //const
	{
		//assert_amsg( numK < length() );
		const Index_t numK = 4;
		Data_t koefs[numK] = { 0.4, 0.3, 0.2, 0.1 };  //����� ������ ���� ����� 1.0
		Data_t filtered;
		for( Index_t i=length()-1, j=0;
			 i>=0 && j<numK;
			 i--, j++ ){
			filtered += operator[](i) * koefs[j];  //koefs[i-length()+1]
		}
		//filtered /= length();
		return filtered;
	}


	/**
	 *
	 */
	Data_t rms() //const
	{
		float r = 0;
		for( int i=0; i<this->length(); i++ ){
			r += (*this)[i] * (*this)[i];
		}
		r = __sqrtf( r / length() );  //should expand inline as the native VFP square root instructions. not specified in any standard
		//r = sqrt( r / length() );
		return r;
	}
};


#endif /* __RingBuffer_HPP */
