﻿Данный модуль представляет собой реализацию Configuration - централизованное управление параметрами других модулей и программы в целом.

# Использование  
``` C

```

# Описание  
Модуль `Sledge/c11n` являтся составляющей библиотеки (`Sledge`)[], и состоит из основы `c11n.h`+`c11n.c`, конфигурации `c11n_config.h`, пользовательских параметров `c11n_parameters.c`. Последний файл предоставляет пользователь библиотеки.


# Как с ним работать
## Обеспечить зависимости  
    `стандартная библиотека Си` - конкретно`string.h` и `stdlib.h`;  
    [`sledge/utils`](https://bitbucket.org/qyw/sledge-utils)  

## **Настроить**
Для этого служит файл `c11n_config.h`, который должен находиться в include-папке проекта, например, `project/config/`.
Ниже приведено его содержимое.
``` C
/// Maximum length of parameter name
#define c11n_MAX_PARAMETER_NAME_LEN 	30
/// Maximum length of module name
#define c11n_MAX_MODULE_NAME_LEN 		20
```  

## **Реализовать комманды**. 
Файл `c11n_parameters.c` содержит пользовательские параметры, пример `sledge/c11n/examples/c11n_parameters.c` стоит скопировать в папку с исходниками, например `/src/sledge`. Затем отредактировать:  

### Описать функции геттеры и сеттеры параметров. `GETTER_FUNCTION` и `SETTER_FUNCTION` упрощают работу с модулем и гарантирует отсутствие ошибок при объявлении функций. Функции могут быть экспоритированы из соответствующих модулей.
	extern GETTER_FUNCTION( moduleA_Param1_get );
	extern SETTER_FUNCTION( moduleA_Param1_set );
Модификатор `extern` использовать не обязательно, но он однозначно говорит о том, что функция внешняя (определена в другом файле).  

Геттеры и сеттеры также могут быть описаны в этом файле.
	static GETTER_FUNCTION( testParam_get_ );
	static SETTER_FUNCTION( testParam_set_ );
Модификатор `static` использовать не обязательно, но он однозначно запретит линковку этой функции в других модулях.  


### Задать список модулей. Сортировка по имени в алфавитном порядке обязательна!  
``` C
extern Parameter_t adc_PARAMETERS[];	///[adc_PARAMETERS_COUNT]

/// Must be sorted in ASCENDING order
const ModuleParameters_t MODULE_PARAMETERS[] = {
	{ "general",         PARAMETERS },
	{     "adc", /*adc_PARAMETERS*/ },
};
const size_t MODULE_PARAMETERS_COUNT = array_length(MODULE_PARAMETERS);
```

### Задать список параметров. Сортировка по имени в алфавитном порядке обязательна!  
``` C
/// Must be sorted in ASCENDING order
const Parameter_t PARAMETERS[] = {
	/*---Name---|--Direct pointer--|-----Type----|----Getter----|-----Setter---|Default|------Description-----*/
	{"boolean"  ,              NULL, C11N_BOOLEAN,   boolean_get,   boolean_set,  false, "" },
	{"coefB"    ,              NULL,   C11N_FLOAT,     coefB_get,     coefB_set,      0, "Linear normalize B" },
	{"coefK"    ,              NULL,   C11N_FLOAT,     coefK_get,     coefK_set,      1, "Linear normalize K" },
	{"filter"   ,              NULL,  C11N_UINT16,    filter_get,    filter_set,      2, "Hardware input filter" },
	{"testParam", (void*)&testParam,   C11N_INT32, testParam_get, testParam_set,    125, "" },
};
const size_t PARAMETERS_COUNT = array_length(PARAMETERS);
```

### Определить функции геттеров и сеттеров.
Геттеры и сеттеры модуля `Sledge/c11n` принимают и возвращают специфический тип `c11n_Uni_t`, позволяющего интерпретировать любое 32-битное поле/переменную как любой тип. 
Модуль `c11n` поддерживает все целочисленные типы размером до 4 байт, указатели и float. int64_t, double и любые структуры могут передаваться как указатели.
В связи с этим классические геттеры и сеттеры могут быть обернуты в геттеры и сеттеры `c11n`.




Подготовительный этап завершён.

## Использование
`volatile` позволит посмотреть переменную через дебаггер. `puts` - функция запишет строку в поток stdout, в вашей программе предварительно должен быть инициализирован UART или USB.VCP и перенаправлнена (retarget) функция putc. Подробнее о [retarget](...).

``` C
void main(void)
{
    /*volatile*/ char what[64];
    char cmdline[64] = "hello world";

    cli_command(resp, sizeof(resp), cmdline, 0);
    puts( resp );
	
	strcpy( cmdline, "reset" );
    cli_command(resp, sizeof(resp), cmdline, 0);
	
	/// НЕ РЕКОМЕНДУЕТСЯ, но возможно.
	/// Найти комманду по имени и вызвать соответствующий обработчик.
    cli_searchCommandByName("reset")->func(0,0,0,0,0);

    while(1);   /// We should never get here
}


/** 
 * Get the value of parameter.
 */
v( cli_get )
{
	Parameter_t *parameter = NULL;
	
	if( argc != 2 ){
		strlcpy( response, "Usage: get <paramName>"NEWLINE, respLen );
		return true;
	}
	
	parameter = c11n_findParameterByName( argv[1] );
	
	if( parameter ){
		if( parameter->getter )
			switch( parameter->type ){
				case C11N_INT8:
				case C11N_INT16:
				case C11N_INT32:
					snprintf( response, respLen, "%s = %d"NEWLINE, parameter->name, parameter->getter().int32 );
					break;
				
				case C11N_UINT8:
				case C11N_UINT16:
				case C11N_UINT32:
					snprintf( response, respLen, "%s = %u"NEWLINE, parameter->name, parameter->getter().uint32 );
					break;
				
				case C11N_FLOAT:
					//valu.pvoid = parameter->getter();
					snprintf( response, respLen, "%s = %f"NEWLINE, parameter->name, parameter->getter().floa /*valu.floa*/ );
					break;
				
				case C11N_BOOLEAN: 
					snprintf( response, respLen, "%s = %s"NEWLINE, parameter->name, parameter->getter().boolean ? "true" : "false" );
					break;
				
				case C11N_INT64:
				case C11N_UINT64:
				case C11N_DOUBLE:
				case C11N_STRUCT:
					break;
			}
			
			//snprintf( response, respLen, "%s = %d"NEWLINE, param->name, param->getter() );
		else
			snprintf( response, respLen, "Parameter %s is write-olny"NEWLINE, parameter->name );
	} else {
		strlcpy( response, "Parameter not found"NEWLINE, respLen );
	}
	
	return 0;
}


/** 
 * Set the Parameter with Value
 */
cli_COMMAND_FUNCTION( cli_set )
{
	Parameter_t *parameter;
	union {
		int32_t  int32;
		uint32_t uint32;
		float floa;
		void* pvoid;
	} value;
	
	if( argc != 3 ){
		strlcpy( response, "Usage: set <paramName> <value>"NEWLINE, respLen);
		return true;
	}
		
	parameter = c11n_findParameterByName( argv[1] );
	debugf3("parameter: %u"NEWLINE, (uint32_t)parameter);
	
	if( parameter ){
		if (parameter->setter) {
			switch( parameter->type ){
				case C11N_INT8:
				case C11N_INT16:
				case C11N_INT32:
					value.uint32 = strtol( argv[2], NULL, 10 );
					parameter->setter( value.pvoid );
					
					value.pvoid = parameter->getter().pVoid;
					snprintf( response, respLen, "%s = %d"NEWLINE, parameter->name, value.int32 /*parameter->getter()*/ );
					break;
				
				case C11N_UINT8:
				case C11N_UINT16:
				case C11N_UINT32:
					//TODO поддержка 0x
					value.uint32 = strtoul( argv[2], NULL, 10 );
					parameter->setter( value.pvoid );
					
					value.pvoid = parameter->getter().pVoid;
					snprintf( response, respLen, "%s = %u"NEWLINE, parameter->name, value.uint32 /*parameter->getter()*/ );
					break;
				
				case C11N_FLOAT:
					value.floa = strtof(argv[2], NULL);
					parameter->setter( value.pvoid );
					
					value.pvoid = parameter->getter().pVoid;
					snprintf( response, respLen, "%s = %f"NEWLINE, parameter->name, value.floa );
					break;
				
				case C11N_BOOLEAN: 
					if( strcasecmp(argv[2], "true") == 0 ){
						parameter->setter( (void*)true );
					} else if ( strcasecmp(argv[2], "false") == 0 ){						
						parameter->setter( (void*)false );
					} else {
						snprintf( response, respLen, "Parameter %s is boolean. Use true or false"NEWLINE, parameter->name );
						break;
					}
					snprintf( response, respLen, "%s = %s"NEWLINE, parameter->name, parameter->getter().pVoid ? "true" : "false" );
					break;
				
				case C11N_INT64:
				case C11N_UINT64:
				case C11N_DOUBLE:
				case C11N_STRUCT:
					break;
			}
				
			//TODO определять нечисловое значение
			//value = atol( argv[2] );
			//parameter->setter( &value );
			//parameter->setter( c11n_t_CastToInt32(value) );
			//snprintf( response, respLen, "%s = %d"NEWLINE, parameter->name, parameter->getter() );
		} else {
			snprintf( response, respLen, "Parameter %s is read-olny"NEWLINE, parameter->name );
		}
	} else {
		snprintf( response, respLen, "Parameter %s not found"NEWLINE, argv[1] );
	}
	
	return 0;
}


/** 
 * Set the Parameter with Value
 */
cli_COMMAND_FUNCTION( cli_param )
{
	size_t i, len;
	
	debugf3("cli_param()"NEWLINE);
	
	if( argc == 1 ){
		strlcpy( response, "Usage: param <paramName> [value]"NEWLINE, respLen);		//response[0] = '\0';
		/// Выдать список параметров
		len = strlen(response);
		for (i = 0; i < PARAMETERS_COUNT; ++i) {
			len += snprintf( response+len, respLen-len, "%s\t %s"NEWLINE, PARAMETERS[i].name, PARAMETERS[i].description);
		}			
	} else if( argc==2 ){
		cli_get( response, respLen, argc, argv, opts );
	} else if( argc==3 ){
		cli_set( response, respLen, argc, argv, opts );
	} else {
		strlcpy( response, "Usage: param <paramName> [value]"NEWLINE, respLen);
	}
	
	return 0;//true
}
```

### Лицензия License
BSD 4-clause or MIT with Expat


#### Статья на сайте
[sledge.com](http://localhost:4000/2015/03/31/sledge-cli-%D0%B8%D0%BD%D1%82%D0%B5%D1%80%D1%84%D0%B5%D0%B9%D1%81-%D0%BA%D0%BE%D0%BC%D0%BC%D0%B0%D0%BD%D0%B4%D0%BD%D0%BE%D0%B9-%D1%81%D1%82%D1%80%D0%BE%D0%BA%D0%B8)

#### Смотрите также
[`sledge`](https://bitbucket.org/qyw/sledge)				[sledge.com]()  
[`sledge/utils`](https://bitbucket.org/qyw/sledge-utils)	[sledge.com]()  
[`sledge/c11n`](https://bitbucket.org/qyw/sledge-c11n) 		[sledge.com]()  
