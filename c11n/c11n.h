/**
 * Sledge - wide functionality library, suitable for embedded systems, 
 * mainly for ARM Cortex and more particularly for STM32 family
 * 
 * Copyright (c) 2015, Ivan Kuvaldin. All rights reserved.
 * 
 * Licensed under BSD or MIT. 
 * Please refer to Licence.BSD.txt or Licence.MIT.txt provided with these sources.
 * You may obtain a copy of the Licenses at
 *      http://opensource.org/licenses/bsd-license.php
 *      http://opensource.org/licenses/mit-license.php
 */
/**
 * \file    c11n.h
 * @author 	Ivan "Kyb Sledgehammer" Kuvaldin <i.kyb[2]ya,ru>
 * \version 1.2.0
 * \date    2014-2015
 * \brief   Цель данного модуля - связать имя переменной с её getter'ами и setter'ами.
 * 			Обеспечить компортное и красивое написание кода для CLI и парсера .ini-файлов.
 * 
 * Данный модуль предоставляет интерфейс для управления переменными в первую очередь
 * через чтение строк из файлов или CLI.
 * Пребполагается что все Параметры могут быть интерпретированы как 32-бит поле.
 * 
 * Довольно сложный, как может показаться, для понимания код, но это не мешает ему хорошо работать.
 * Пример использования:
 
	~~~{.c}
	volatile int testInt32 = -125;
	volatile char helloC11N[] = "Hello C11N!"
	
	/// Must be sorted in ASCENDING order
	const Parameter_t PARAMETERS[] = {
		/----name---+-----pointer-----+----type-----+----getter----+----setter----+-defa-+-----description---------/ 
		{"flag"     ,             NULL, C11N_BOOLEAN,      flag_get,      flag_get, false,                      "" },
		{"helloC11N",             NULL,    C11N_CHAR, helloC11N_get, helloC11N_get,     0, "String says \"hello\"" },
		{"testInt32",(void*)&testInt32,   C11N_INT32, testInt32_get, testInt32_get,	 -125,                      "" },
		{"floatPar" ,             NULL,  C11N_UINT32,  floatPar_get,  floatPar_get,  3.28, "This is float param"   },
	};
	
	static GETTER_FUNCTION( testInt32_get )
	{
		c11n_Uni_t ret;
		ret.int32 = testInt32_set;
		return ret;
	}
	
	static SETTER_FUNCTION( testInt32_set, value )
	{
		c11n_Uni_t uni;
		uni.pVoid = value;
		testInt32 = uni.int32;
	}
	~~~
 
 * @Changes
 *  ver 1.2.2  date.2015-May-25
 * 	 * enum c11n_Type -> c11n_Type_e, c11n_Uni_t -> c11n_Type
 *   + getFloat, setFloat functions
 *  ver.1.2.0  date.2015-Mar-25
 *   + Added c11n_Uni_t and enum c11n_Type to resolve problem with any-type variables.
 *  ver.1.1.0  date.2015-Jan-25
 *  ver.1.0.0  date.2014-dec-5
 *   + Initial implementation.
 */
  
#ifndef __C11N_H
#define __C11N_H	122

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>

#include "Sledge/assert.h"


/// Maximum length of parameter name
#ifndef c11n_MAX_PARAMETER_NAME_LEN
 #define c11n_MAX_PARAMETER_NAME_LEN 	30
#endif

/// Maximum length of module name
#ifndef c11n_MAX_MODULE_NAME_LEN
 #define c11n_MAX_MODULE_NAME_LEN 		20
#endif

/// Allow c11n_Type contain 64bit data itself, but not pointers. 1:allow, 0:disable.
#ifndef c11n_64BIT_DATA
 #define c11n_64BIT_DATA  0
#endif

#ifndef c11n_CASE_SENSITIVE
 #define c11n_CASE_SENSITIVE 0
#endif


#if c11n_CASE_SENSITIVE == 0
 #define c11n_strcmp  strcasecmp
#else
 #define c11n_strcmp  strcmp
#endif



typedef enum c11n_Type_e {
	C11N_INT8,
	C11N_UINT8,
	C11N_INT16,
	C11N_UINT16,
	C11N_INT32,
	C11N_UINT32,
	C11N_INT64,		/// pointer
	C11N_UINT64,	/// pointer
	C11N_FLOAT,
	C11N_DOUBLE,	/// pointer to double
	C11N_BOOLEAN,
	C11N_STRUCT,	/// pointer to struct
	///\todo C11N_ENTRY
} c11n_Type_e;

typedef union c11n_Type {
	 uint8_t  uint8;
	  int8_t   int8;	
	uint16_t uint16;
	 int16_t  int16;
	uint32_t uint32;
	 int32_t  int32;
	   float   floa;
#if c11n_64BIT_DATA
	uint64_t uint64;
	 int64_t  int64;
	  double  doubl;
#else
	uint64_t* uint64;
	 int64_t* int64;
	  double* doubl;
#endif
	   bool boolean;
	   void * pVoid;
	   char * pChar;
} c11n_Type;

#if c11n_64BIT_DATA
 STATIC_ASSERT( sizeof(c11n_Type) == 8 );
#else
 STATIC_ASSERT( sizeof(c11n_Type) == 4 );
#endif


/// Использование этих макросов позволяет единовременно изменить прототипы всех функций и не морочить голову при написании новой.
/**	Возвращает 32-битное представление нужной переменной. 
 *	Например, может быть преобразован в (uint32_t) или ардес структуры (struct myType*)
 */
//#define GETTER_FUNCTION(funcname)			void* (funcname)(void)
#define GETTER_FUNCTION(funcname)			c11n_Type (funcname)(void)

/**	Задает значение переменной. 
 * 	Принимает 32-битное представление значения, которое будет переданно в переменную. */
//#define SETTER_FUNCTION(funcname, pValue)	void (funcname)(void* pValue)	//void (funcname)(void *pValue)
#define SETTER_FUNCTION(funcname)			void (funcname)(c11n_Type value) //void* pValue)


/// boolean
#define SETTER_BOOL(funcname,what)	\
		void (funcname)(bool value){ what = value ? true : false; }
#define SETTER_BOOLEAN  SETTER_BOOL
#define GETTER_BOOL(funcname,what)	\
		c11n_Type funcname(){ c11n_Type ret; ret.boolean = what; return ret; }
#define GETTER_BOOLEAN  GETTER_BOOL

/// uint8
#define SETTER_UINT8(funcname,what)	\
		void (funcname)(uint8_t value){ what = value; }
#define GETTER_UINT8(funcname,what)	\
		c11n_Type funcname(){ c11n_Type ret; ret.uint16 = what; return ret; }

/// uint16
#define SETTER_UINT16(funcname,what)	\
		void (funcname)(uint16_t value){ what = value; }
#define GETTER_UINT16(funcname,what)	\
		c11n_Type funcname(){ c11n_Type ret; ret.uint16 = what; return ret; }

/// uint32
#define SETTER_UINT32(funcname,what)	\
		void (funcname)(uint32_t value){ what = value; }
#define GETTER_UINT32(funcname,what)	\
		c11n_Type funcname(){ c11n_Type ret; ret.uint32 = what; return ret; }

/// Float
#define SETTER_FLOAT(funcname,what)	\
		void (funcname)(float value){ what = value; }
#define GETTER_FLOAT(funcname,what)	\
		c11n_Type funcname(){ c11n_Type ret; ret.floa = what; return ret; }



typedef GETTER_FUNCTION( *getterFunction_t );
typedef SETTER_FUNCTION( *setterFunction_t ); //typedef SETTER_FUNCTION( *setterFunction_t, pValue );

/// Описание параметра
typedef struct Param_t {
	char *name;	                            /// name of Parameter.
	c11n_Type_e type; 						/// type of parameter
	setterFunction_t set;					/// pointer to setter function
	getterFunction_t get;					/// pointer to getter function
	int  defaultValue;						/// default value of Parameter, or pointer to it
	char* description;						/// description string
} Param_t;

typedef struct Parameter_t {
	char *name;	                            /// name of Parameter
	void *value; 							/// direct pointer to Parameter variable
	c11n_Type_e type; 						/// type of parameter
	setterFunction_t set;					/// pointer to setter function
	getterFunction_t get;					/// pointer to getter function
	int  (*setFromString)(const char* str); /// useful for structs
	char*(*toString)(char *buf, size_t size);
	int  defaultValue;						/// default value of Parameter, or pointer to it
	char* description;						/// description string
} Parameter_t;	

extern const Parameter_t PARAMETERS[];
extern const size_t PARAMETERS_COUNT;


typedef struct ModulesParameters_t {
//	const char 		  name[c11n_MAX_MODULE_NAME_LEN]; /// Module name
	const char 		  *name;                          /// Module name
	const Parameter_t *parameters;					  /// List of parameters of the module
	const size_t 	  count;
} ModulesParameters_t;

//extern const ModulesParameters_t MODULES_PARAMETERS[];
extern const ModulesParameters_t * MODULES_PARAMETERS[];
extern const size_t MODULES_PARAMETERS_COUNT;



/**
 * Поиск параметра по имени
 * @param 	pname 	Имя параметра. Formats are [module.]name and [module:]name
 * @return 	Parameter member of PARAMETERS
 */
const Parameter_t * c11n_findParameterByName( const char *parname );

/**
 * Поиск параметра по имени
 * @param 	module	Имя параметра
 * @param 	pname 	Имя параметра
 * @return 	Parameter from corresponding modulePARAMETERS, or
 * 			from PARAMETERS if module is null-pointer.
 */
const Parameter_t * c11n_findParameter( const char *module, const char *pname )  __attribute__((__nonnull__(2)));

/**
 * \param[IN] *parameter Pointer to the parameter to be set.
 * \param[IN] *str Pointer to string contains value to set.
 * \return 	 0: no errors and warnings
 * 			 1: Value reached parameter's limit. Parameter was set.
 * 			-1: No valid data found. Parameter was not set.
 * 			-2: Parameter is boolean. Use true or false.
 * 			-3: Parameter is read-only (has no setter).
 * 			-0xF: Unimplemented behavior with 64bit data and pointers.
 */
int c11n_setFromString( const Parameter_t* parameter, const char* str )  __attribute__((__nonnull__(1,2)));

/**
 * String representation of the parameter.
 * \param[IN] *parameter Pointer to the parameter to be setted.
 * \param[IN] *str Pointer to string contains value to set.
 * \return	Symbols wirtten or Negative value on error.
 */
int c11n_toString( Parameter_t* parameter, char* str, size_t len )  __attribute__((__nonnull__(1,2)));


#ifdef __cplusplus
}
#endif

#endif /* __C11N_H */  
