/**
 * Sledge - wide functionality library, suitable for embedded systems, 
 * mainly for ARM Cortex and more particularly for STM32 family
 * 
 * Copyright (c) 2015, Ivan Kuvaldin. All rights reserved.
 * 
 * Licensed under BSD or MIT. 
 * Please refer to Licence.BSD.txt or Licence.MIT.txt provided with these sources.
 * You may obtain a copy of the Licenses at
 *      http://opensource.org/licenses/bsd-license.php  
 *      http://opensource.org/licenses/mit-license.php  
 */
/**
 * \file    Sledge/c11n/c11n.c
 * \author 	Ivan "Kyb Sledgehammer" Kuvaldin <i.kyb[2]ya,ru>
 * \brief 	Look at ./c11n.h for more information.
 */

#include "./c11n.h"

//#include <stdint.h>
//#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "Sledge/utils.h"
#include "Sledge/assert.h"

//#define DEBUG_LEVEL 7
#include "Sledge/debug.h"


/**
 * Поиск параметра по адресу, где хранится значение параметра
 * @param 	*pval 	Указатель на значение параметра
 * @return 	Parameter member of PARAMETERS
 */
const Parameter_t * c11n_findParameterByPointer( const char *pval )
{
	size_t i;
	for( i=0; i<PARAMETERS_COUNT; i++ ){
		if( PARAMETERS[i].value == pval )
			return &PARAMETERS[i];
	}
	return NULL;
}


/**
 * Поиск параметра по имени
 * @param 	pname 	Имя параметра. Formats are [module.]name and [module:]name
 * @return 	Parameter member of PARAMETERS
 */
const Parameter_t * c11n_findParameterByName( const char *pname )
{
	char *p;
	char token;
	const Parameter_t *parameter;
	
	assert_msg("c11n_findParameterByName: pname is NULL!\n", pname);
	
	// Найти разделитель
	p = strchr( pname, '.' );
	p = p ? p : strchr( pname, ':' );
	
	if( p ){
		token = *p;  // запомнить разделитель
		*p = '\0';   // терминировать строку
		parameter = c11n_findParameter( pname, p+1 );
	} else {
		parameter = c11n_findParameter( NULL, pname );
	}
	*p = token;  // Вернуть разделитель на место
	return parameter;
}


/**
 * Поиск параметра по имени
 * @param 	pname 	Имя параметра
 * @return 	Parameter member of PARAMETERS
 */
const Parameter_t * c11n_findParameter( const char *mod_name, const char *par_name )  /*__attribute__((__nonnull__(2)));*/
{
	const ModulesParameters_t *module = NULL;
	const Parameter_t *parameter = NULL;
	
	//assert_msg("c11n_findParameterByName: mod_name is NULL", mod_name);
	assert_msg("c11n_findParameter: par_name is NULL!\n", par_name);
	
	debugf("c11n_findParameter: %s.%s\n", mod_name, par_name );
	if( !mod_name )
		mod_name = "";
	
	for( int i=0; i<MODULES_PARAMETERS_COUNT; i++ ){
		if( c11n_strcmp(mod_name, MODULES_PARAMETERS[i]->name) == 0 ){
			module = MODULES_PARAMETERS[i];
			break;
		}
	}
//	if( mod_name )
//		modParams = bsearch(/*key*/  mod_name, 
//							/*base*/ MODULES_PARAMETERS[0]->name, 	//TODO Test how will it work with [0].name, if it is not 1st field in struct
//							/*nmemb*/MODULES_PARAMETERS_COUNT,
//							/*size*/ sizeof(ModulesParameters_t),
//							/*compareFunction*/(int(*)(const void*, const void*))c11n_strcmp );
//	else
//		//modParams = &MODULES_PARAMETERS[1]; //ref to PARAMETERS
//		parameter = bsearch(/*key*/  par_name, 
//							/*base*/ PARAMETERS,
//							/*nmemb*/PARAMETERS_COUNT,
//							/*size*/ sizeof(Parameter_t),
//							/*compareFunction*/(int(*)(const void*, const void*))c11n_strcmp );
	
	debugf("c11n_findParameter: module 0x%0X\n", (unsigned)module );
	if( module ){
		for( int i=0; i<module->count; i++ ){
			if( c11n_strcmp(par_name, module->parameters[i].name) == 0 ){
				parameter = &module->parameters[i];
				break;
			}
		}
		//parameter = bsearch(/*key*/  par_name, 
		//					/*base*/ modParams->parameters,
		//					/*nmemb*/modParams->count,
		//					/*size*/ sizeof(Parameter_t),
		//					/*compareFunction*/(int(*)(const void*, const void*))c11n_strcmp );
	}
	
	debugf("c11n_findParameter: parameter 0x%0X\n\n", (unsigned)parameter );
	return parameter;
}


/**
 * @return 	0 if ok, -1 if parameter is not float
 */
int c11n_setFloat( Parameter_t* param, float f )
{
	c11n_Type value;
	
	if( param->type == C11N_FLOAT ){
		value.floa = f;
		if( param->set )
			param->set( value/*.pVoid*/ );
		else if( param->value ){
			float *p;
			p = param->value;
			*p = f;
		}
		return 0;
	}
	return -1;
}


/**
 * Set parameter with bit to bit copy of input value.
 * \return 	-1 on error, 0: parameter set
 */
int c11n_setAsFloat( Parameter_t* param, float f )
{
	c11n_Type value;
	int ret = 0;
	
	value.floa = f;
	if( param->set )
		param->set( value/*.pVoid*/ );
	else if( param->value ){
		float *p;
		p = param->value;
		*p = f;
	}else
		ret = -1;
	
	return ret;
}

/**
 * Function tries to get float value from \p *param. 
 * \param[IN] 	*param
 * \return 	parameter value, or 
 * 			NAN if type of parameter is not float or no getter and no direct pointer
 * 			We cannot determine error if parameter contains NAN value.
 */
float c11n_getFloat( Parameter_t* param )
{
	c11n_Type value;
	if( param->type == C11N_FLOAT ){
		if( param->get )
			value = param->get();
		else if( param->value )
			value.floa = *(float*)param->value;
		else
			value.floa = NAN;
	} else
		value.floa = NAN;
	return value.floa;
}

/**
 * 
 * \return 	Float representation of value contatned in \p *param
 * 			NAN if no getter and no direct pointer
 * 			We cannot determine error if parameter contains NAN value.
 */
float c11n_getAsFloat( Parameter_t* param )
{
	c11n_Type value;
	if( param->get )
		value = param->get();
	else if( param->value )
		value.floa = *(float*)param->value;
	else 
		value.floa = NAN;
	
	return value.floa;
}


/**
 * \param[IN] *parameter Pointer to the parameter to be setted.
 * \param[IN] *str Pointer to string contains value to set.
 * \return 	 0: no errors and warnings
 * 			 1: Value reached parameter's limit. Parameter was set.
 * 			-1: No valid data found. Parameter was not set.
 * 			-2: Parameter is boolean. Use true or false.
 * 			-3: Parameter is read-only (has no setter).
 * 			-0xF: Unimplemented behaviour with 64bit data and pointers.
 */
int c11n_setFromString( const Parameter_t* parameter, const char* str )  /*__attribute__((__nonnull__(1,2)));*/
{
	int ret = 0;
	c11n_Type value;
	char* endptr;
	
	assert_amsg( parameter != NULL );
	assert_amsg( str != NULL );
	
	if( parameter->set == NULL ){
		return -3;  /// Parameter is read-olny
	}
	
	if( parameter->setFromString ){
		ret = parameter->setFromString( str );
	} else {
		switch( parameter->type ){
			case C11N_INT8:
			case C11N_INT16:
			case C11N_INT32:
				value.int32 = strtol( str, &endptr, 0 );	/// Auto-resolve the base
				if( value.int32 == 0 && str == endptr ){  /// How strtol() behaves if str begins with space?
					/// str doesn't contain number. //TODO test
					return -1;
				} else if( INT32_MIN == value.int32 || value.int32 == INT32_MAX ){
					ret = 1;
				}
				break;
			
			case C11N_UINT8:
			case C11N_UINT16:
			case C11N_UINT32:
				value.uint32 = strtoul( str, &endptr, 0 );
				if( value.uint32 == 0 && str == endptr ){  /// How strtol() behaves if str begins with space?
					return -1;
				} else if( value.uint32 == UINT32_MAX ){
					ret = 1;
				}
				break;
			
			case C11N_FLOAT:
				value.floa = strtof( str, NULL );  /// TODO check. Must return NAN if string contains no valid data. Microlib says zero.
				if( isnan(value.floa) )
					return -1;
				else if( isinf(value.floa) )
					ret = 1;
				break;
			
			case C11N_BOOLEAN: 
				if( strcasecmp( str, "true" ) == 0 ){
					value.boolean = true;
				} else if ( strcasecmp( str, "false") == 0 ){						
					value.boolean = false;
				} else {
					return -2; //snprintf( response, respLen, "Parameter %s is boolean. Use true or false"NEWLINE, parameter->name );
				}
				/// Alternative boolean detection by first symbol. Faster.
				/*char* s = strltrim( str );
				if( s[0] == 'T' || s[0] == 't' || s[0] == '1' ){
					value.boolean = true;
				}else if( s[0] == 'F' || s[0] == 'f' || s[0] == '0' ){
					value.boolean = false;
				} else {
					return -2; 
				}*/
				break;
			
			case C11N_INT64:
			case C11N_UINT64:
			case C11N_DOUBLE:
			case C11N_STRUCT:
				return -0xF; 	/// Not implemented yet.
		}
		parameter->set( value );
	}
	return ret;
}


/**
 * String representation of the parameter.
 * \param[IN] *parameter Pointer to the parameter to be setted.
 * \param[IN] *str Pointer to string contains value to set.
 * \return	Symbols wirtten or Negative value on error.
 * 			-4: Parameter has no getter;
 * 			-0xF: Unimplemented behaviour with 64bit data and pointers.
 */
int c11n_toString( Parameter_t* parameter, char *strbuf, size_t strsz )  /*__attribute__((__nonnull__(1,2)));*/
{
	int ret = -4;
	
	assert_amsg( parameter != NULL );
	assert_amsg( strbuf != NULL );
		
	if( parameter->get == NULL ){
		return -4;  /// Parameter is write-olny
		//snprintf( response, respLen, "Parameter %s is write-olny"NEWLINE, parameter->name );
	}
	
	if( parameter->toString ){
		parameter->toString( strbuf, strsz );
		ret = 0; //ret=strlen(strbuf);
	} else {
		switch( parameter->type ){
			case C11N_INT8:
			case C11N_INT16:
			case C11N_INT32:
				//snprintf( str, strsz, "%s = %d"NEWLINE, parameter->name, parameter->get().int32 );
				ret = snprintf( strbuf, strsz, "%d"NEWLINE, parameter->get().int32 );
				break;
			
			case C11N_UINT8:
			case C11N_UINT16:
			case C11N_UINT32:
				//snprintf( str, strsz, "%s = %u"NEWLINE, parameter->name, parameter->get().uint32 );
				ret = snprintf( strbuf, strsz, "%u"NEWLINE, parameter->get().uint32 );
				break;
			
			case C11N_FLOAT:
				//snprintf( str, strsz, "%s = %f"NEWLINE, parameter->name, parameter->get().floa );
				ret = snprintf( strbuf, strsz, "%f"NEWLINE, parameter->get().floa );
				break;
			
			case C11N_BOOLEAN: 
				//snprintf( str, strsz, "%s = %s"NEWLINE, parameter->name, parameter->get().boolean ? "true" : "false" );
				ret = snprintf( strbuf, strsz, "%s"NEWLINE, parameter->get().boolean ? "true" : "false" );
				break;
			
			case C11N_INT64:
			case C11N_UINT64:
			case C11N_DOUBLE:
			case C11N_STRUCT:
				return -0xF; 	/// Not implemented yet.
		}
	}
	
	return ret;
}


/**
 * Добавить параметр в список PARAMETERS во время выполнения. НЕТ НУЖДЫ.
 * @param 	добаляемый параметр
 * @return	Код ошибки: 0 в случае отсутствия таковой.
 */
/*int8_t c11n_addParam( Parameter_t parm )
{
	//abort_msg("Function "__func__" not implemented yet"NEWLINE);
	return 0;
}*/

